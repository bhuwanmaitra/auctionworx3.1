﻿/****************************************************\
*                                                    *
*                 OLD DATA CLEANUP                   *
*                                                    *
*  Deletes certain records which are flagged for     *
*  deletion, such as banners, media, etc.            *
*                                                    *
*  This script should be scheduled to be run         *
*  regularly during off-peak hours.                  *
*                                                    *
\****************************************************/

delete from RWX_BannerCategories where BannerId in (select Id from RWX_Banners where not DeletedOn is null)
delete from RWX_Banners where not DeletedOn is null
delete from RWX_ListingMedias where not DeletedOn is null
delete from RWX_AuctionEventMedias where not DeletedOn is null

delete from RWX_MediaAssetMetaData where not DeletedOn is null
delete from RWX_MediaVariations where not DeletedOn is null
delete from RWX_MediaMetaData where not DeletedOn is null
delete from RWX_MediaAssets where not DeletedOn is null
delete from RWX_Media where not DeletedOn is null

Go
