﻿/****************************************************\
*                                                    *
*  Configure AuctionWorx to use Azure Blob Storage   *
*                                                    *
\****************************************************/

update RWX_Attributes set Value = 'BlobStorage' where Value in ('DirectFileSystem', 'DateHashedFileSystem')
update RWX_Attributes set Value = 'BlobStorageURI' where Value in ('DirectURI', 'DateHashedURI')

