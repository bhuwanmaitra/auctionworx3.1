/*

                 Finish restoring AWE v3+ db to a new server

*/

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  NOTE! If you see a message like...                                                               --
--                                                                                                   --
--     "Cannot find the principal 'sa', because it does not exist or you do not have permission."    --
--                                                                                                   --
--  ...then you need to use a different SQL login when executing this command, OR                    --
--  specify an exsiting login other than [sa] below if [sa] does not exist on this server.           --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------

declare @sql nvarchar(max) = N'ALTER AUTHORIZATION ON DATABASE::'+quotename(db_name())+N' TO [sa];';
print @sql;
exec sp_executesql @sql;
Go

declare @sql nvarchar(max) = N'ALTER DATABASE '+quotename(db_name())+N' SET SINGLE_USER WITH ROLLBACK IMMEDIATE;';
print @sql;
exec sp_executesql @sql;
GO

declare @sql nvarchar(max) = N'ALTER DATABASE '+quotename(db_name())+N' SET NEW_BROKER;';
print @sql;
exec sp_executesql @sql;
GO

declare @sql nvarchar(max) = N'ALTER DATABASE '+quotename(db_name())+N' SET MULTI_USER;';
print @sql;
exec sp_executesql @sql;
GO

print N'exec RWX_SSB_CleanupConversations;';
exec RWX_SSB_CleanupConversations;
GO
