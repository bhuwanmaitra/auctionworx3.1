﻿/****************************************************\
*                                                    *
*     Add or Repair Sql Service Broker objects       *
*               for AuctionWorx 3.1+                 *
*                                                    *
\****************************************************/
 
set quoted_identifier on
go
set nocount on
go
set xact_abort on
go
set transaction isolation level read committed
go

/*****SQL Service Broker*****/

if exists (SELECT is_broker_enabled FROM sys.databases WHERE name = db_name() and is_broker_enabled = 0)
begin
	declare @signalRdisabled bit = 0
	if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'RWX_SiteProperties')
	  if exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
	    set @signalRdisabled = 1
	if (@signalRdisabled = 0)
	begin
		--enable SQL Service Broker
		declare @sqlssb nvarchar(500)
		set @sqlssb='alter database '+quotename(db_name())+' set single_user with rollback after 15 seconds';
		exec(@sqlssb)
		set @sqlssb='alter database '+quotename(db_name())+' set new_broker';
		exec(@sqlssb)
		set @sqlssb='alter database '+quotename(db_name())+' set multi_user';
		exec(@sqlssb)
		insert into [RWX_Version] (Status) values('Enabled SQL Service Broker')
	end
end
Go

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'RWX_SSB_ConversationHandles')
begin
	CREATE TABLE [RWX_SSB_ConversationHandles] (
	EntityID INT NOT NULL,	
	Handle UNIQUEIDENTIFIER NOT NULL,
	[Target] nvarchar(255) not null, 
	PRIMARY KEY CLUSTERED (EntityID, [Target]))	
	insert into [RWX_Version] (Status) values('Created Table RWX_SSB_ConversationHandles')
end
Go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_CleanupConversations'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_CleanupConversations AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_CleanupConversations')
end
Go
alter procedure RWX_SSB_CleanupConversations as
	set nocount on
	declare @tempHandles table (handle uniqueidentifier not null)
	insert into @tempHandles
	select conversation_handle from sys.conversation_endpoints where is_initiator = 0 and state = 'CO'
	declare @currentHandler uniqueidentifier
	while exists (select top 1 * from @tempHandles)
	begin
		select top 1 @currentHandler = handle from @tempHandles

		begin try
			end conversation @currentHandler
		end try
		begin catch
		end catch;			

		delete from @tempHandles where handle = @currentHandler
	end

	declare @tempHandle uniqueidentifier
	while exists (select null from sys.conversation_endpoints)
	begin
		select top 1 @tempHandle = conversation_handle from sys.conversation_endpoints

		end conversation @tempHandle with cleanup
	end

	truncate table RWX_SSB_ConversationHandles

	--declare @messageHandler uniqueidentifier
	--declare @unnecessaryVariable nvarchar(255)
	--set @unnecessaryVariable = N'GetCurrentTime_TimerService'
	--begin dialog conversation @messageHandler from service @unnecessaryVariable
	--	to service N'GetCurrentTime_TimerService', 'CURRENT DATABASE'
	--	with encryption = off
	--begin conversation timer (@messageHandler) timeout = 1;

	declare @now datetime
	set @now = getutcdate()
	exec RWX_InsertLogEntry @now, 'Cleaning Service Broker Conversations', 'Cleaning SSB', 'SSB', 'Information', 'SSB', null, null, null, 0, 0, @@SERVERNAME
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_Enqueue'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_Enqueue AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_Enqueue')
end
Go
ALTER procedure [RWX_SSB_Enqueue](@entityID int, @target nvarchar(255), @messageBody nvarchar(max)) as
	set nocount on			
	
	declare @targetService nvarchar(255)
	set @targetService = @target + '_TargetService'	
	declare @initiatorService nvarchar(255)
	set @initiatorService = @target +'_InitiatorService'

	DECLARE @counter INT;
	DECLARE @error INT;
	declare @err_msg nvarchar(max);
	SELECT @counter = 1;
	declare @messageHandler uniqueidentifier
	--------
	-- Will need a loop to retry in case the conversation is
	-- in a state that does not allow transmission
	--
	WHILE (1=1)
	BEGIN
		-- Seek an eligible conversation in [RWX_SSB_ConversationHandles]
		--
		SELECT @messageHandler = Handle
			FROM [RWX_SSB_ConversationHandles] WITH (UPDLOCK)
			WHERE EntityID = @entityID and [Target] = @target	
		IF @messageHandler IS NULL
		BEGIN			
			-- Need to start a new conversation for the current @entityID
			--
			DECLARE @sql NVARCHAR(MAX)
			DECLARE @param_def NVARCHAR(MAX)

			SET @param_def = '@messageHandler uniqueidentifier OUTPUT, @targetService nvarchar(255)'

			SET @sql = '
			begin dialog conversation @messageHandler from service [' + @initiatorService + ']
			to service @targetService, ''CURRENT DATABASE''	
			with encryption = off'
		
			EXEC sp_executesql
				@sql, 
				@param_def,		
				@messageHandler = @messageHandler OUTPUT,
				@targetService = @targetService			
			INSERT INTO [RWX_SSB_ConversationHandles]
				(EntityID, Handle, [Target])
				VALUES
				(@entityID, @messageHandler, @target);			
		END;
		-- Attempt to SEND on the associated conversation
		--
		set @error = 0;
		BEGIN TRY;
			SEND ON CONVERSATION @messageHandler			
				(@messageBody);
		END TRY
		BEGIN CATCH;
			select @error = ERROR_NUMBER(), 
			       @err_msg = replace(ERROR_MESSAGE(), '"', '''');
		END CATCH;
		--SELECT @error = @@ERROR;
		IF @error = 0
		BEGIN
			-- Successful send, just exit the loop
			--
			BREAK;
		END
		SELECT @counter = @counter+1;
		declare @now datetime
		declare @properties nvarchar(500)
		set @properties = '{"Count":"' + convert(nvarchar, @counter) + '","SQLError":"(' + convert(nvarchar, @error) + ') ' + @err_msg + '","EntityID":"' + convert(nvarchar, @entityID) + '","Target":"' + convert(nvarchar, @target) + '"}'
		set @now = getutcdate()
		exec RWX_InsertLogEntry @now, 'SSB Enqueue failed to queue, retrying', 'SSB Enqueue', 'SSB', 'Warning', 'SSB', null, null, @properties, 0, 0, @@SERVERNAME
		IF @counter > 10
		BEGIN
			-- We failed 10 times in a row, something must be broken
			--
			set @properties = '{"Count":"' + convert(nvarchar, @counter) + '","SQLError":"(' + convert(nvarchar, @error) + ') ' + @err_msg + '","EntityID":"' + convert(nvarchar, @entityID) + '","Target":"' + convert(nvarchar, @target) + '"}'
			set @now = getutcdate()
			exec RWX_InsertLogEntry @now, 'SSB Enqueue failed to queue, giving up', 'SSB Enqueue', 'SSB', 'Error', 'SSB', null, null, @properties, 0, 0, @@SERVERNAME

			RAISERROR (
				N'Failed to SEND on a conversation for more than 10 times. Error %i.'
				, 16, 1, @error); -- WITH LOG;
			BREAK;
		END
		-- Delete the associated conversation from the table and try again
		--
		DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @messageHandler;
		SELECT @messageHandler = NULL;
	END		
go

--IF EXISTS (SELECT NAME FROM sysobjects WHERE TYPE = 'P' AND NAME = 'RWX_SSB_Enqueue')
--   and exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
--begin
--	EXEC('DROP PROCEDURE RWX_SSB_Enqueue')
--	EXEC('CREATE PROCEDURE RWX_SSB_Enqueue AS RETURN')
--	insert into [RWX_Version] (Status) values('Disabled Stored Procedure RWX_SSB_Enqueue')
--end
--Go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_Dequeue'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_Dequeue AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_Dequeue')
end
Go
ALTER procedure [RWX_SSB_Dequeue](@target nvarchar(255), @messageBody nvarchar(max) out) as
	set nocount on
		
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @param_def NVARCHAR(MAX)

	SET @param_def = '@messageBody nvarchar(max) OUTPUT'

	SET @sql = '
	WAITFOR (RECEIVE TOP(1)
	@messageBody = convert(nvarchar(max), message_body)
	FROM [' + @target + '_TargetQueue]), TIMEOUT 5000'
		
	EXEC sp_executesql
		@sql, 
		@param_def,		
		@messageBody = @messageBody OUTPUT	
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingDTTMUpdateInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingDTTMUpdateInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingDTTMUpdateInitiatorActivated')
end
Go
alter PROCEDURE RWX_SSB_OnListingDTTMUpdateInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingDTTMUpdate_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingDTTMUpdate_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

--IF NOT EXISTS (
--	SELECT NAME FROM sysobjects 
--	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose'
--)
--begin
--	EXEC('CREATE PROCEDURE RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose AS RETURN')
--	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose')
--end
--Go
--ALTER procedure [RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose](@message xml) as
--	set nocount on
--	set xact_abort on

--	begin tran

--	declare @triggeringListingID int	
--	declare @sniperSeconds int
--	select @triggeringListingID = @message.value('(/ListingDTTMChange/ListingID)[1]', 'int')					
	
--	if (exists (select * from RWX_Listings where Id = @triggeringListingID and LotId is not null))
--	begin
--		--get EventID and SoftCloseGroup for the triggering listing
--		declare @eventID int
--		declare @softCloseGroup int
--		select @eventID = lot.AuctionEventID, @softCloseGroup = lot.SoftClosingGroup from RWX_Lots as lot
--			inner join RWX_Listings as listing
--			on lot.Id = listing.LotId
--			where listing.Id = @triggeringListingID

--		select @sniperSeconds = SoftClosingGroupIncrementSeconds from RWX_AuctionEvents where Id = @eventID

--		--get all OTHER listings in the same event/softclosegroup
--		declare @affectedListingIDs as table (Id int not null)
--		insert into @affectedListingIDs (Id) 
--			select Id from RWX_Listings where LotId in
--			(select Id from RWX_Lots where AuctionEventID = @eventID and SoftClosingGroup = @softCloseGroup)
--			and Id != @triggeringListingID
--			and [Status]=N'Active'

--		--update the EndDTTM for all OTHER listings in the same event/softclosegroup
--		update RWX_Listings set EndDTTM = dateadd(second, @sniperSeconds, EndDTTM) where Id in
--		(select Id from @affectedListingIDs)		

--		--update all lots with the same soft close group, set IsSoftClosing to true
--		update RWX_Lots set IsSoftClosing = 1 where AuctionEventID = @eventID and SoftClosingGroup = @softCloseGroup

--		--foreach OTHER listing, queue the message for SignalR processing
--		declare @listingID int		
--		declare @endDTTM datetime
--		while exists (Select Id from @affectedListingIDs)
--		begin
--			select top 1 @listingID = Id from @affectedListingIDs					
			
--			select @endDTTM = EndDTTM from RWX_Listings where Id = @listingID

--			set @message.modify('replace value of (/ListingDTTMChange/ListingID/text())[1] with sql:variable("@listingID")')
--			set @message.modify('replace value of (/ListingDTTMChange/Source/text())[1] with "SOFT_TARGET"')
--			set @message.modify('replace value of (/ListingDTTMChange/DTTM/text())[1] with sql:variable("@endDTTM")')
--			declare @payload nvarchar(max)
--			set @payload = convert(nvarchar(MAX), @message)
--			exec RWX_SSB_Enqueue @listingID, 'ListingDTTMUpdateForSignalR', @payload			

--			delete from @affectedListingIDs where Id = @listingID

--			--audit EndDTTM update
--			insert into RWX_ListingEndDTTMAudit (ListingID, SourceListingID, Origin, EndDTTM)
--				values(@listingID, @triggeringListingID, 'SOFT_TARGET', @endDTTM)
--		end
--	end
	
--	commit tran
--go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingDTTMUpdateTargetActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingDTTMUpdateTargetActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingDTTMUpdateTargetActivated')
end
Go
ALTER procedure [RWX_SSB_OnListingDTTMUpdateTargetActivated] as
	set nocount on
	set xact_abort on

	DECLARE @h UNIQUEIDENTIFIER;
	DECLARE @messageTypeName SYSNAME;
	DECLARE @payload nvarchar(MAX);	

	WHILE (1=1)
	BEGIN
		BEGIN TRANSACTION;
		WAITFOR(RECEIVE TOP(1)
			@h = conversation_handle,
			@messageTypeName = message_type_name,
			@payload = message_body
			FROM [ListingDTTMUpdate_TargetQueue]), TIMEOUT 1000;

		IF (@@ROWCOUNT = 0)
		BEGIN
			COMMIT;
			BREAK;
		END

		IF N'DEFAULT' = @messageTypeName
		BEGIN
			--receives ListingDTTMUpdate messages and
			--1.) re-queues the message for SignalR processing
			declare @listingID int
			declare @message xml
			declare @epoch nvarchar(20)
			declare @source nvarchar(255)
			declare @DTTM datetime
			set @message = convert(xml, @payload)
			select	@listingID = @message.value('(/ListingDTTMChange/ListingID)[1]', 'int'),		
					@epoch = @message.value('(/ListingDTTMChange/Epoch)[1]', 'nvarchar(20)'),
					@source = @message.value('(/ListingDTTMChange/Source)[1]', 'nvarchar(255)'),
					@DTTM = @message.value('(/ListingDTTMChange/DTTM)[1]', 'datetime')
			exec RWX_SSB_Enqueue @listingID, 'ListingDTTMUpdateForSignalR', @payload

			--2.) updates Listing/Lot EndDTTM's for Events			
			--if (@source = 'SOFT_ORIGIN') exec RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose @message
			
			--3.) audit EndDTTM update
			--if (@epoch = 'ending') 
			--	insert into RWX_ListingEndDTTMAudit (ListingID, Origin, EndDTTM)
			--		values(@listingID, @source, @DTTM)

			--do other work/intercept?			
		END
		ELSE IF N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' = @messageTypeName or N'http://schemas.microsoft.com/SQL/ServiceBroker/Error' = @messageTypeName
		BEGIN
			END CONVERSATION @h;
		END
		COMMIT;
	END		
go

IF EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose'
)
begin
	drop procedure RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose;
	insert into [RWX_Version] (Status) values('Dropped Stored Procedure RWX_SSB_HandleUpdateLotEndDTTMsForSoftClose')
end
Go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingDTTMUpdateForSignalRInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingDTTMUpdateForSignalRInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingDTTMUpdateForSignalRInitiatorActivated')
end
Go
alter PROCEDURE RWX_SSB_OnListingDTTMUpdateForSignalRInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingDTTMUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingDTTMUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO




IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingActionUpdateInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingActionUpdateInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingActionUpdateInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnListingActionUpdateInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingActionUpdate_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingActionUpdate_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingActionUpdateTargetActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingActionUpdateTargetActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingActionUpdateTargetActivated')
end
go
alter procedure RWX_SSB_OnListingActionUpdateTargetActivated as
	set nocount on
	set xact_abort on

	DECLARE @h UNIQUEIDENTIFIER;
	DECLARE @messageTypeName SYSNAME;
	DECLARE @payload nvarchar(MAX);	

	WHILE (1=1)
	BEGIN
		BEGIN TRANSACTION;
		WAITFOR(RECEIVE TOP(1)
			@h = conversation_handle,
			@messageTypeName = message_type_name,
			@payload = message_body
			FROM [ListingActionUpdate_TargetQueue]), TIMEOUT 1000;

		IF (@@ROWCOUNT = 0)
		BEGIN
			COMMIT;
			BREAK;
		END

		IF N'DEFAULT' = @messageTypeName
		BEGIN
			--receives ListingActionUpdate messages and
			--1.) re-queues the message for SignalR processing
			declare @listingID int
			declare @message xml
			set @message = convert(xml, @payload)
			select @listingID = @message.value('(/ListingActionChange/ListingID)[1]', 'int')						
			exec RWX_SSB_Enqueue @listingID, 'ListingActionUpdateForSignalR', @payload			
			--do other work/intercept?
		END
		ELSE IF N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' = @messageTypeName or N'http://schemas.microsoft.com/SQL/ServiceBroker/Error' = @messageTypeName
		BEGIN
			END CONVERSATION @h;
		END
		COMMIT;		
	END		
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingActionUpdateForSignalRInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingActionUpdateForSignalRInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingActionUpdateForSignalRInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnListingActionUpdateForSignalRInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingActionUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingActionUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnGetCurrentTimeForSignalRInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnGetCurrentTimeForSignalRInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnGetCurrentTimeForSignalRInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnGetCurrentTimeForSignalRInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [GetCurrentTimeForSignalR_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [GetCurrentTimeForSignalR_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

--IF NOT EXISTS (
--	SELECT NAME FROM sysobjects 
--	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnGetCurrentTime'
--)
--begin
--	EXEC('CREATE PROCEDURE RWX_SSB_OnGetCurrentTime AS RETURN')
--	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnGetCurrentTime')
--end
--go
--alter procedure RWX_SSB_OnGetCurrentTime
--as
--begin
--declare @mt sysname, @h uniqueidentifier;
--begin transaction;
--    receive top (1)
--        @mt = message_type_name
--        , @h = conversation_handle
--        from GetCurrentTime_TimerQueue;

--    if @@rowcount = 0
--    begin
--        commit transaction;
--        return;
--    end

--    if @mt in (N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
--        , N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')
--    begin
--        end conversation @h;
--    end
--    else if @mt = N'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer'
--    begin
--		declare @currentDTTM nvarchar(50)
--		set @currentDTTM = convert(nvarchar, getutcdate(), 120)
--		--clear the queue, anything in it is now obsolete
--		;receive * from GetCurrentTimeForSignalR_TargetQueue
--        exec RWX_SSB_Enqueue 0, 'GetCurrentTimeForSignalR', @currentDTTM
--        -- set a new timer after 59s to prevent edge cases where the queue processing overhead may cause a browser client to skip over a minute...
--        begin conversation timer (@h) timeout = 59;
--    end
--commit
--end
--go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_EndConversations'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_EndConversations AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_EndConversations')
end
go
alter procedure RWX_SSB_EndConversations(@entityID int) as
	set nocount on	
	set xact_abort on
	
	begin tran

	declare @handle uniqueidentifier
	declare @targetHandle uniqueidentifier
	declare @handleTable table (Handle uniqueidentifier not null)

	delete from RWX_SSB_ConversationHandles output deleted.Handle into @handleTable where EntityID = @entityID

	while exists (select * from @handleTable)
	begin
		select top 1 @handle = Handle from @handleTable

		select @targetHandle = b.conversation_handle from sys.conversation_endpoints as a
		inner join sys.conversation_endpoints as b on a.conversation_id = b.conversation_id
		where a.conversation_handle = @handle
		and b.is_initiator = 0
		
		END CONVERSATION @targetHandle;

		delete from @handleTable where Handle = @handle
	end	
	
	commit			
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_ScavengeConversations'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_ScavengeConversations AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_ScavengeConversations')
end
Go
alter procedure RWX_SSB_ScavengeConversations as
	set nocount on

	declare @listingIDs as table (ID int not null)

	insert into @listingIDs
	select distinct l.Id from RWX_Listings as l inner join
						RWX_SSB_ConversationHandles as ch
						on l.Id = ch.EntityID
	 where l.EndDTTM < dateadd(minute, -55, getutcdate())	 

	 declare @currentID int
	 while exists (select null from @listingIDs)
	 begin
		select top 1 @currentID = ID from @listingIDs
		exec RWX_SSB_EndConversations @currentID
		delete from @listingIDs where ID = @currentID
	 end	 	 
go


IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnQueueListingActionInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnQueueListingActionInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnQueueListingActionInitiatorActivated')
end
Go
alter PROCEDURE RWX_SSB_OnQueueListingActionInitiatorActivated
AS
set nocount on
set xact_abort on
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(max);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingAction_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;	  
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(max))
            FROM [ListingAction_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnQueueListingActionResponseInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnQueueListingActionResponseInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnQueueListingActionResponseInitiatorActivated')
end
Go
alter PROCEDURE RWX_SSB_OnQueueListingActionResponseInitiatorActivated
AS
set nocount on
set xact_abort on
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(max);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingActionResponse_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;	  
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(max))
            FROM [ListingActionResponse_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingStatusUpdateInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingStatusUpdateInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingStatusUpdateInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnListingStatusUpdateInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingStatusUpdate_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingStatusUpdate_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingStatusUpdateTargetActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingStatusUpdateTargetActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingStatusUpdateTargetActivated')
end
go
alter procedure RWX_SSB_OnListingStatusUpdateTargetActivated as
	set nocount on
	set xact_abort on

	DECLARE @h UNIQUEIDENTIFIER;
	DECLARE @messageTypeName SYSNAME;
	DECLARE @payload nvarchar(MAX);	

	WHILE (1=1)
	BEGIN
		BEGIN TRANSACTION;
		WAITFOR(RECEIVE TOP(1)
			@h = conversation_handle,
			@messageTypeName = message_type_name,
			@payload = message_body
			FROM [ListingStatusUpdate_TargetQueue]), TIMEOUT 1000;

		IF (@@ROWCOUNT = 0)
		BEGIN
			COMMIT;
			BREAK;
		END

		IF N'DEFAULT' = @messageTypeName
		BEGIN
			--receives ListingStatusUpdate messages and
			--1.) re-queues the message for SignalR processing
			declare @listingID int
			declare @message xml
			set @message = convert(xml, @payload)
			select @listingID = @message.value('(/ListingStatusChange/ListingID)[1]', 'int')						
			exec RWX_SSB_Enqueue @listingID, 'ListingStatusUpdateForSignalR', @payload			
			--do other work/intercept?
		END
		ELSE IF N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' = @messageTypeName or N'http://schemas.microsoft.com/SQL/ServiceBroker/Error' = @messageTypeName
		BEGIN
			END CONVERSATION @h;
		END
		COMMIT;		
	END		
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnListingStatusUpdateForSignalRInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnListingStatusUpdateForSignalRInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnListingStatusUpdateForSignalRInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnListingStatusUpdateForSignalRInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [ListingStatusUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [ListingStatusUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnEventStatusUpdateInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnEventStatusUpdateInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnEventStatusUpdateInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnEventStatusUpdateInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [EventStatusUpdate_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [EventStatusUpdate_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnEventStatusUpdateTargetActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnEventStatusUpdateTargetActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnEventStatusUpdateTargetActivated')
end
go
alter procedure RWX_SSB_OnEventStatusUpdateTargetActivated as
	set nocount on
	set xact_abort on

	DECLARE @h UNIQUEIDENTIFIER;
	DECLARE @messageTypeName SYSNAME;
	DECLARE @payload nvarchar(MAX);	

	WHILE (1=1)
	BEGIN
		BEGIN TRANSACTION;
		WAITFOR(RECEIVE TOP(1)
			@h = conversation_handle,
			@messageTypeName = message_type_name,
			@payload = message_body
			FROM [EventStatusUpdate_TargetQueue]), TIMEOUT 1000;

		IF (@@ROWCOUNT = 0)
		BEGIN
			COMMIT;
			BREAK;
		END

		IF N'DEFAULT' = @messageTypeName
		BEGIN
			--receives EventStatusUpdate messages and
			--1.) re-queues the message for SignalR processing
			declare @eventID int
			declare @message xml
			set @message = convert(xml, @payload)
			select @eventID = @message.value('(/EventStatusChange/EventID)[1]', 'int')						
			exec RWX_SSB_Enqueue @eventID, 'EventStatusUpdateForSignalR', @payload			
			--do other work/intercept?
		END
		ELSE IF N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' = @messageTypeName or N'http://schemas.microsoft.com/SQL/ServiceBroker/Error' = @messageTypeName
		BEGIN
			END CONVERSATION @h;
		END
		COMMIT;		
	END		
go

IF NOT EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnEventStatusUpdateForSignalRInitiatorActivated'
)
begin
	EXEC('CREATE PROCEDURE RWX_SSB_OnEventStatusUpdateForSignalRInitiatorActivated AS RETURN')
	insert into [RWX_Version] (Status) values('Created Stored Procedure RWX_SSB_OnEventStatusUpdateForSignalRInitiatorActivated')
end
go
alter PROCEDURE RWX_SSB_OnEventStatusUpdateForSignalRInitiatorActivated
AS
DECLARE @dh UNIQUEIDENTIFIER;
DECLARE @message_type SYSNAME;
DECLARE @message_body NVARCHAR(4000);
BEGIN TRANSACTION;
WAITFOR (
      RECEIVE @dh = [conversation_handle],
            @message_type = [message_type_name],
            @message_body = CAST([message_body] AS NVARCHAR(4000))
      FROM [EventStatusUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
WHILE @dh IS NOT NULL
BEGIN
      IF @message_type = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
      BEGIN
            RAISERROR (N'Received error %s from service [Target]', 10, 1, @message_body); -- WITH LOG;
      END

	  DELETE FROM [RWX_SSB_ConversationHandles]
			WHERE Handle = @dh;

      END CONVERSATION @dh;
      COMMIT;
      SELECT @dh = NULL;
      BEGIN TRANSACTION;
      WAITFOR (
            RECEIVE @dh = [conversation_handle],
                  @message_type = [message_type_name],
                  @message_body = CAST([message_body] AS NVARCHAR(4000))
            FROM [EventStatusUpdateForSignalR_InitiatorQueue]), TIMEOUT 1000;
END
COMMIT;
GO


/******************** SSB Queues & Services (begin) ********************/

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingDTTMUpdate_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingDTTMUpdate_InitiatorQueue]
		  WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 1,
				PROCEDURE_NAME = [RWX_SSB_OnListingDTTMUpdateInitiatorActivated],
				EXECUTE AS OWNER);
	create QUEUE [ListingDTTMUpdate_TargetQueue]
			WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 10,
				PROCEDURE_NAME = [RWX_SSB_OnListingDTTMUpdateTargetActivated],
				EXECUTE AS OWNER);
	CREATE SERVICE ListingDTTMUpdate_InitiatorService ON QUEUE ListingDTTMUpdate_InitiatorQueue
	CREATE SERVICE ListingDTTMUpdate_TargetService ON QUEUE ListingDTTMUpdate_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingDTTMUpdate')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingDTTMUpdateForSignalR_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingDTTMUpdateForSignalR_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnListingDTTMUpdateForSignalRInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingDTTMUpdateForSignalR_TargetQueue]	
	CREATE SERVICE ListingDTTMUpdateForSignalR_InitiatorService ON QUEUE ListingDTTMUpdateForSignalR_InitiatorQueue
	CREATE SERVICE ListingDTTMUpdateForSignalR_TargetService ON QUEUE ListingDTTMUpdateForSignalR_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingDTTMUpdateForSignalR')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingActionUpdate_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingActionUpdate_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnListingActionUpdateInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingActionUpdate_TargetQueue]
			WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 10,
				PROCEDURE_NAME = [RWX_SSB_OnListingActionUpdateTargetActivated],
				EXECUTE AS OWNER);
	CREATE SERVICE ListingActionUpdate_InitiatorService ON QUEUE ListingActionUpdate_InitiatorQueue
	create SERVICE ListingActionUpdate_TargetService ON QUEUE ListingActionUpdate_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingActionUpdate')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingActionUpdateForSignalR_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingActionUpdateForSignalR_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnListingActionUpdateForSignalRInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingActionUpdateForSignalR_TargetQueue]	
	CREATE SERVICE ListingActionUpdateForSignalR_InitiatorService ON QUEUE ListingActionUpdateForSignalR_InitiatorQueue
	CREATE SERVICE ListingActionUpdateForSignalR_TargetService ON QUEUE ListingActionUpdateForSignalR_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingActionUpdateForSignalR')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'GetCurrentTimeForSignalR_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [GetCurrentTimeForSignalR_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnGetCurrentTimeForSignalRInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [GetCurrentTimeForSignalR_TargetQueue]	
	CREATE SERVICE GetCurrentTimeForSignalR_InitiatorService ON QUEUE GetCurrentTimeForSignalR_InitiatorQueue
	CREATE SERVICE GetCurrentTimeForSignalR_TargetService ON QUEUE GetCurrentTimeForSignalR_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for GetCurrentTimeForSignalR')
end

--IF NOT EXISTS (select * from sys.service_queues
--				where name = 'GetCurrentTime_TimerQueue')
--   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
--begin
--	create queue GetCurrentTime_TimerQueue with activation (
--    status = on
--    , max_queue_readers = 1
--    , execute as owner
--    , procedure_name = RWX_SSB_OnGetCurrentTime)
--	create service GetCurrentTime_TimerService on queue GetCurrentTime_TimerQueue ([DEFAULT]);
--	--seed the timer
--	declare @messageHandler uniqueidentifier
--	begin dialog conversation @messageHandler from service [GetCurrentTime_TimerService]
--		to service N'GetCurrentTime_TimerService', 'CURRENT DATABASE'
--		with encryption = off
--	begin conversation timer (@messageHandler) timeout = 1;
--	insert into [RWX_Version] (Status) values('Created Queues & Services for GetCurrentTime and seed the timer')
--end

IF EXISTS (select * from sys.service_queues
				where name = 'GetCurrentTime_TimerQueue')
begin
	drop service GetCurrentTime_TimerService;
	drop queue GetCurrentTime_TimerQueue;
	insert into [RWX_Version] (Status) values('Dropped Queues & Services for GetCurrentTime timer')
end
go

IF EXISTS (
	SELECT NAME FROM sysobjects 
	WHERE TYPE = 'P' AND NAME = 'RWX_SSB_OnGetCurrentTime'
)
begin
	drop proc RWX_SSB_OnGetCurrentTime;
	insert into [RWX_Version] (Status) values('Dropped proc for GetCurrentTime timer')
end
go

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingAction_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingAction_InitiatorQueue]
		  WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 1,
				PROCEDURE_NAME = [RWX_SSB_OnQueueListingActionInitiatorActivated],
				EXECUTE AS OWNER)
	create QUEUE [ListingAction_TargetQueue]	
	CREATE SERVICE ListingAction_InitiatorService ON QUEUE ListingAction_InitiatorQueue
	CREATE SERVICE ListingAction_TargetService ON QUEUE ListingAction_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingActions')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingActionResponse_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingActionResponse_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnQueueListingActionResponseInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingActionResponse_TargetQueue]	
	CREATE SERVICE ListingActionResponse_InitiatorService ON QUEUE ListingActionResponse_InitiatorQueue
	CREATE SERVICE ListingActionResponse_TargetService ON QUEUE ListingActionResponse_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingActionResponse')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingStatusUpdate_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingStatusUpdate_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnListingStatusUpdateInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingStatusUpdate_TargetQueue]
			WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 10,
				PROCEDURE_NAME = [RWX_SSB_OnListingStatusUpdateTargetActivated],
				EXECUTE AS OWNER);
	CREATE SERVICE ListingStatusUpdate_InitiatorService ON QUEUE ListingStatusUpdate_InitiatorQueue
	create SERVICE ListingStatusUpdate_TargetService ON QUEUE ListingStatusUpdate_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingStatusUpdate')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'ListingStatusUpdateForSignalR_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [ListingStatusUpdateForSignalR_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnListingStatusUpdateForSignalRInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [ListingStatusUpdateForSignalR_TargetQueue]	
	CREATE SERVICE ListingStatusUpdateForSignalR_InitiatorService ON QUEUE ListingStatusUpdateForSignalR_InitiatorQueue
	CREATE SERVICE ListingStatusUpdateForSignalR_TargetService ON QUEUE ListingStatusUpdateForSignalR_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for ListingStatusUpdateForSignalR')
end


IF NOT EXISTS (select * from sys.service_queues
				where name = 'EventStatusUpdate_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [EventStatusUpdate_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnEventStatusUpdateInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [EventStatusUpdate_TargetQueue]
			WITH ACTIVATION (
				STATUS = ON,
				MAX_QUEUE_READERS = 10,
				PROCEDURE_NAME = [RWX_SSB_OnEventStatusUpdateTargetActivated],
				EXECUTE AS OWNER);
	CREATE SERVICE EventStatusUpdate_InitiatorService ON QUEUE EventStatusUpdate_InitiatorQueue
	create SERVICE EventStatusUpdate_TargetService ON QUEUE EventStatusUpdate_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for EventStatusUpdate')
end

IF NOT EXISTS (select * from sys.service_queues
				where name = 'EventStatusUpdateForSignalR_InitiatorQueue')
   and not exists (select * from  RWX_SiteProperties where BoolNativeValue=0 and Id=1038)
begin
	create QUEUE [EventStatusUpdateForSignalR_InitiatorQueue]
      WITH ACTIVATION (
            STATUS = ON,
            MAX_QUEUE_READERS = 1,
            PROCEDURE_NAME = [RWX_SSB_OnEventStatusUpdateForSignalRInitiatorActivated],
            EXECUTE AS OWNER);
	create QUEUE [EventStatusUpdateForSignalR_TargetQueue]	
	CREATE SERVICE EventStatusUpdateForSignalR_InitiatorService ON QUEUE EventStatusUpdateForSignalR_InitiatorQueue
	CREATE SERVICE EventStatusUpdateForSignalR_TargetService ON QUEUE EventStatusUpdateForSignalR_TargetQueue([DEFAULT])
	insert into [RWX_Version] (Status) values('Created Queues & Services for EventStatusUpdateForSignalR')
end
Go

/******************** SSB Queues & Services (end) ********************/

