﻿using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.InvoiceLogic
{
    public interface ITotalCalculator
    {
        bool CalculateTotals(Invoice invoice);
    }
}
