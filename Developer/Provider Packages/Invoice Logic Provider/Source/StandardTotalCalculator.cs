﻿using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.DTO.Querying;
using RainWorx.FrameWorx.Strings;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.InvoiceLogic
{
    /// <summary>
    /// This class provides a hook to calculate shiping, tax and subtotals, which is run before most UpdateInvoice operations
    /// </summary>
    public class StandardTotalCalculator : ITotalCalculator
    {
        private readonly IDataContext _data;

        //Via WCF (where 0 argument constructor is required)
        public StandardTotalCalculator()
        {            
            _data = UnityResolver.Get<IDataContext>();
        }

        //Via Unity
        public StandardTotalCalculator(IDataContext data)
        {            
            _data = data;
        }

        public bool CalculateTotals(Invoice invoice)
        {
            //setup
            decimal taxRate = 0.0M;
            string taxShipping = string.Empty;
            decimal subTotal = 0.0M;
            decimal salesTax = 0.0M;
            decimal shipping = 0.0M;
            decimal taxableSubTotal = 0.0M;
            decimal buyerPremiumSubTotal = 0.0M;
            bool invoiceShips;

            //determine if the invoice ships and a shipping option is set
            if (invoice.Type == Strings.InvoiceTypes.Shipping && invoice.ShippingOption != null)
            {
                invoiceShips = true;
            }
            else
            {
                invoiceShips = false;
            }

            //get tax-related site properties
            bool hideTaxFields = Cache.SiteProperties.ContainsKey(SiteProperties.HideTaxFields)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.HideTaxFields]) : false;
            bool salesTaxEnabled = Cache.SiteProperties.ContainsKey(SiteProperties.SalesTaxEnabled)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.SalesTaxEnabled]) : (!hideTaxFields);
            bool vatEnabled = Cache.SiteProperties.ContainsKey(SiteProperties.VATEnabled)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.VATEnabled]) : false;
            decimal vatRate = 0.0M;
            if (Cache.SiteProperties.ContainsKey(SiteProperties.VatRate))
                decimal.TryParse(Cache.SiteProperties[SiteProperties.VatRate], out vatRate);
            bool vatAppliesToSiteFees = Cache.SiteProperties.ContainsKey(SiteProperties.VatAppliesToSiteFees)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.VatAppliesToSiteFees]) : true;
            bool vatAppliesToShipping = Cache.SiteProperties.ContainsKey(SiteProperties.VatAppliesToShipping)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.VatAppliesToShipping]) : true;
            bool vatAppliesToBuyersPremium = Cache.SiteProperties.ContainsKey(SiteProperties.VatAppliesToBuyersPremium)
                ? bool.Parse(Cache.SiteProperties[SiteProperties.VatAppliesToBuyersPremium]) : true;
            bool payerIsTaxExempt = invoice.Payer.Properties.Any(up => up.Field.Name == StdUserProps.TaxExempt && up.Value != null)
                ? bool.Parse(invoice.Payer.Properties.First(up => up.Field.Name == StdUserProps.TaxExempt).Value) : false;

            if (payerIsTaxExempt)
            {
                taxRate = 0.0M;
            }
            else if (salesTaxEnabled)
            {
                //set tax rate
                if (invoice.Type == Strings.InvoiceTypes.Shipping && !string.IsNullOrEmpty(invoice.ShippingCountry) && !string.IsNullOrEmpty(invoice.ShippingStateRegion))
                {
                    SalesTaxRate salesTaxRate = _data.GetSalesTaxRate(invoice.Owner, invoice.ShippingCountry, invoice.ShippingStateRegion);
                    if (salesTaxRate != null)
                    {
                        taxRate = salesTaxRate.TaxRate / 100.0M;
                        taxShipping = salesTaxRate.Shipping;
                    }
                    else
                    {
                        taxRate = 0.0M;
                    }
                }
                else if (invoice.Type == Strings.InvoiceTypes.NonShipping && !string.IsNullOrEmpty(invoice.BillingCountry) && !string.IsNullOrEmpty(invoice.BillingStateRegion))
                {
                    SalesTaxRate salesTaxRate = _data.GetSalesTaxRate(invoice.Owner, invoice.BillingCountry, invoice.BillingStateRegion);
                    if (salesTaxRate != null)
                    {
                        taxRate = salesTaxRate.TaxRate / 100.0M;
                        taxShipping = salesTaxRate.Shipping;
                    }
                    else
                    {
                        taxRate = 0.0M;
                    }
                }
            }
            else if (vatEnabled)
            {
                taxRate = vatRate / 100.0M;
                taxShipping = vatAppliesToShipping ? "FullyTaxable" : "NotTaxable";
            }
            else
            {
                taxRate = 0.0M;
            }

            //calculate subtotal, sales tax, and shipping amount for each row.
            var query = new List<Query>(1)
                            {
                                new Query(Strings.Fields.InvoiceId, QueryOperator.Equal, invoice.ID)
                            };
            const int maxLineItemsPerBatch = 1000;
            var firstPageOfListingLineItems = _data.GetLineItems(query, 0, maxLineItemsPerBatch,
                                                            Strings.Fields.Id, false, false);
            decimal highestMainShippingCost = 0.0M;
            decimal relatedSecondaryShippingCost = 0.0M;
            for (int currentPageIndex = 0; currentPageIndex < firstPageOfListingLineItems.TotalPageCount; currentPageIndex++)
            {
                List<LineItem> currentBatchOfLineItems;
                if (currentPageIndex == 0)
                {
                    currentBatchOfLineItems = firstPageOfListingLineItems.List;
                }
                else
                {
                    currentBatchOfLineItems = _data.GetLineItems(query, currentPageIndex, maxLineItemsPerBatch,
                                                            Strings.Fields.Id, false, false).List;
                }
                foreach (LineItem lineItem in currentBatchOfLineItems)
                {
                    //Add line item total to subtotal
                    subTotal += lineItem.TotalAmount;

                    //add tax if line item is taxable
                    if (lineItem.Taxable || (invoice.Type == InvoiceTypes.Fee && vatEnabled && vatAppliesToSiteFees))
                    {
                        salesTax += taxRate * lineItem.TotalAmount;
                        taxableSubTotal += lineItem.TotalAmount;
                    }

                    //add buyer's premium if applicable
                    if (lineItem.BuyersPremiumApplies)
                    {
                        buyerPremiumSubTotal += lineItem.TotalAmount;
                    }

                    //if the invoice ships
                    if (invoiceShips)
                    {
                        //if the line item is for a listing
                        if (lineItem.Type == Strings.LineItemTypes.Listing)
                        {
                            Listing listing = lineItem.Listing;
                            if (listing == null && lineItem.ListingID.HasValue)
                            {
                                listing = _data.GetListingByIDWithFillLevel(lineItem.ListingID.Value, ListingFillLevels.Shipping);
                                if (listing == null)
                                    Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);
                            }

                            //add shipping amount for this line item
                            Invoice tempInvoice = invoice;
                            ShippingOption listingShippingOption =
                                listing.ShippingOptions.Where(so => so.Method.ID == tempInvoice.ShippingOption.Method.ID)
                                    .SingleOrDefault();
                            if (listingShippingOption != null)
                            {
                                if (listingShippingOption.FirstItemAmount > highestMainShippingCost)
                                {
                                    //subtract out the previous high shipping cost and add in the related secondary cost
                                    shipping -= highestMainShippingCost;
                                    shipping += relatedSecondaryShippingCost;

                                    //set the new "high shipping cost" amounts
                                    highestMainShippingCost = listingShippingOption.FirstItemAmount;
                                    relatedSecondaryShippingCost = listingShippingOption.AdditionalItemAmount.HasValue
                                        ? listingShippingOption.AdditionalItemAmount.Value
                                        : listingShippingOption.FirstItemAmount;

                                    //add this line item's (main cost * 1) + ((qty - 1) * secondary cost)
                                    shipping += highestMainShippingCost + (relatedSecondaryShippingCost * (lineItem.Quantity - 1));
                                }
                                else
                                {
                                    decimal secondayCost = listingShippingOption.AdditionalItemAmount.HasValue
                                        ? listingShippingOption.AdditionalItemAmount.Value
                                        : listingShippingOption.FirstItemAmount;
                                    shipping += secondayCost * lineItem.Quantity;
                                }

                            }
                        }
                    }
                }
            }

            if (invoiceShips)
            {
                switch (taxShipping)
                {
                    case "NotTaxable":
                        //do nothing
                        break;
                    case "PartiallyTaxable":
                        decimal taxableLineItemPercentage = taxableSubTotal / subTotal;
                        salesTax += (shipping * taxableLineItemPercentage) * taxRate;
                        break;
                    case "FullyTaxable":
                        //tax entire shipping amount
                        salesTax += taxRate * shipping;
                        break;
                }
            }

            invoice.BuyersPremiumAmount = invoice.BuyersPremiumPercent * buyerPremiumSubTotal / 100;

            if (vatEnabled && vatAppliesToBuyersPremium)
            {
                salesTax += (invoice.BuyersPremiumAmount * taxRate);
            }

            invoice.Subtotal = subTotal;
            invoice.SalesTax = salesTax;
            invoice.ShippingAmount = shipping;
            invoice.Total = invoice.Subtotal + invoice.ShippingAmount + invoice.SalesTax + invoice.BuyersPremiumAmount;

            //return (invoice.Total >= 0.0M);
            return (invoice.Total > -0.005M); // this is necessary to account for any rounding errors -- fractional cents will be rounded away when the invoice is committed to the database
        }
    }
}
