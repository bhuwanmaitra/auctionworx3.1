﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.DTO.Media;
using RainWorx.FrameWorx.Providers.MediaLoader;

namespace RainWorx.FrameWorx.MediaAsset.Images
{
    public class DirectLoader : IMediaLoader
    {
        public string Load(Dictionary<string, string> providerSettings, Media media, string variationName)
        {
            //Setup & Verify Provider Settings
            string rootURL = providerSettings["RootURL"];

            Variation variation = media.Variations[variationName];
            return rootURL + variation.Asset.Reference;
        }

        public Dictionary<string, string> DefaultProviderSettings
        {
            get
            {
                Dictionary<string, string> retVal = new Dictionary<string, string>();
                retVal.Add("RootURL", "Content/Images/Logos/");
                return retVal;
            }
        }

        public bool VerifyProviderSettings(Dictionary<string, string> providerSettings)
        {
            return (DefaultProviderSettings.Keys.Except(providerSettings.Keys).Count() <= 0);
        }

        public string TypeName
        {
            get { return "RainWorx.FrameWorx.MediaAsset.Images.DirectLoader"; }
        }
    }
}
