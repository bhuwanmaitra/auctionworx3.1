﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.DTO.Media;
using RainWorx.FrameWorx.Providers.MediaAsset;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.MediaAsset.Images
{
    public class NativeImage : MediaGeneratorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public NativeImage() {}

        //Via Unity
        public NativeImage(IDataContext data) : base(data) { }  

        public override bool RegisterSelf(string actingUserName)
        {
            return false;
        }

        #region IMediaGenerator Members

        public override Media Generate(Dictionary<string, string> providerSettings, Stream data)
        {
            //Setup & Verify Provider Settings            

            //Generate Media     
			Dictionary<string, Variation> variations = new Dictionary<string, Variation>();
            Media retVal = Factory.CreateMedia("Main", variations);

            Image originalImage = null;
            try
            {
                originalImage = Image.FromStream(data);
            } catch
            {
                //bad image data
                return null;
            }

            string extension = string.Empty;
            string format = string.Empty;

            if (originalImage.RawFormat.Guid == ImageFormat.Jpeg.Guid)
            {
                extension = ".jpg";
                format = "JPEG";
            }
            else if (originalImage.RawFormat.Guid == ImageFormat.Png.Guid)
            {
                extension = ".png";
                format = "PNG";
            }
            else if (originalImage.RawFormat.Guid == ImageFormat.Gif.Guid)
            {
                extension = ".gif";
                format = "GIF";
            }
            else if (originalImage.RawFormat.Guid == ImageFormat.Bmp.Guid)
            {
                extension = ".bmp";
                format = "BMP";
            }
            else if (originalImage.RawFormat.Guid == ImageFormat.Icon.Guid)
            {
                extension = ".ico";
                format = "ICO";
            }
            else
            {
                return null;
            }

            //Generate and Add Main Image  
            byte[] imageData = ReadFully(data);
            string imageReference = retVal.GUID + extension;
			Dictionary<string, string> imageMetaData = GenerateMetaData(originalImage.Height, originalImage.Width, format);
            Asset imageAsset = Factory.CreateAsset(TypeName, imageReference, imageMetaData, imageData);
            Variation imageVariation = Factory.CreateVariation("Main", imageAsset);
            variations.Add(imageVariation.Name, imageVariation);

            return retVal;
        }

        public override bool VerifyProviderSettings(Dictionary<string, string> providerSettings)
        {
            return (DefaultProviderSettings.Keys.Except(providerSettings.Keys).Count() <= 0);
        }

        public override Dictionary<string, string> DefaultProviderSettings
        {
            get
            {
                Dictionary<string, string> retVal = new Dictionary<string, string>(0);
                return retVal;
            }
        }

        public override IEnumerable<string> MetaDataKeys
        {
            get
            {
                List<string> retVal = new List<string>();
                retVal.Add("Height");
                retVal.Add("Width");
                retVal.Add("Format");
                return retVal;
            }
        }

        private static Dictionary<string, string> GenerateMetaData(int height, int width, string format)
        {
	    Dictionary<string, string> retVal = new Dictionary<string, string>(2);
            retVal.Add("Height", height.ToString());
            retVal.Add("Width", width.ToString());
            retVal.Add("Format", format);
            return retVal;
        }

        public override string TypeName
        {
            get { return "RainWorx.FrameWorx.MediaAsset.Images.NativeImage"; }
        }

        public static byte[] ReadFully(Stream input)
        {
            input.Seek(0, SeekOrigin.Begin);
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        #endregion
    }
}
