﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.DTO.Media;
using RainWorx.FrameWorx.Providers.MediaSaver;
using System.IO;

namespace RainWorx.FrameWorx.MediaAsset.Images
{
    public class DirectSaver : IMediaSaver
    {
        public void Save(Dictionary<string, string> providerSettings, ref Media media)
        {            
            //Setup & Verify Provider Settings
            string rootFolder = providerSettings["RootFolder"];   
            if (rootFolder.StartsWith("~"))
            {
                rootFolder = Path.Combine(providerSettings["VirtualFolder"], rootFolder.Replace("~\\", ""));
            }

            //Save Media
            foreach (Variation variation in media.Variations.Values)
            {
                string path = rootFolder;
                Directory.CreateDirectory(path);
                using (FileStream fs = new FileStream(Path.Combine(path, variation.Asset.Reference), FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        bw.Write(variation.Asset.Data);                        
                        bw.Close();
                    }
                    fs.Close();
                }
                variation.Asset.MetaData.Add("PhysicalURI", "file:///" + Path.Combine(path, variation.Asset.Reference).Replace('\\', '/'));
            }                                    
        }

        public void Delete(Dictionary<string, string> providerSettings, ref Media media)
        {
            //Setup & Verify Provider Settings
            string rootFolder = providerSettings["RootFolder"];
            if (rootFolder.StartsWith("~"))
            {
                rootFolder = Path.Combine(providerSettings["VirtualFolder"], rootFolder.Replace("~\\", ""));
            }

            //Delete Media
            foreach (Variation variation in media.Variations.Values)
            {
                string path = rootFolder;
                File.Delete(Path.Combine(path, variation.Asset.Reference));
            }
        }

        public Dictionary<string, string> DefaultProviderSettings
        {
            get
            {
                Dictionary<string, string> retVal = new Dictionary<string, string>();
                retVal.Add("RootFolder", @"~\Content\Images\Logos\");
                return retVal;
            }
        }

        public bool VerifyProviderSettings(Dictionary<string, string> providerSettings)
        {
            return (DefaultProviderSettings.Keys.Except(providerSettings.Keys).Count() <= 0);
        }

        public string TypeName
        {
            get { return "RainWorx.FrameWorx.MediaAsset.Images.DirectSaver"; }
        }
    }
}
