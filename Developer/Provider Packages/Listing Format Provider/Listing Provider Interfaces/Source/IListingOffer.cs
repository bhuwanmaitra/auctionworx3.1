﻿using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Listing
{
    /// <summary>
    /// The IListingOffer interface is the base interface for listing offers.
    /// 
    /// It handles the sending and responding to offers.
    /// </summary>
    public interface IListingOffer
    {
        void ValidateOffer(UserInput input, out Offer newOffer);

        void SendOffer(UserInput input, out Offer newOffer);

        void AcceptOffer(UserInput input, Offer offer);

        void DeclineOffer(UserInput input, Offer offer);

        void ValidateCounterOffer(UserInput input, Offer originalOffer, out Offer newOffer);

        void SendCounterOffer(UserInput input, Offer originalOffer, out Offer newOffer);
    }
}
