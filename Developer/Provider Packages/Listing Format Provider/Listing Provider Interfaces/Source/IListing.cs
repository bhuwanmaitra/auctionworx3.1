﻿using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Listing
{
    ///<summary>
    /// The IListing Interface is the base interface for all listing types.  It handles the creation and update of listing types.
    /// In general, it expects UserInput as input from the web page.
    /// 
    /// Since we would consider *any* logic in the MVC layer misplaced (because MVC is supposed to be presentation, and BLL is
    /// supposed to be logic) UserInput is just basic data a user could send, mostly consisting of key/value pairs from a webform.
    /// 
    /// Listing Types are generally a set of fields common to all (or most) listings, and a "property bag" of custom fields for additional
    /// logic.  This property bag should contain information required for internal logic for the listing type, but generally should
    /// not contain queryable data.  An example would be for Dutch Auctions where "quantity" is part of the internal logic of a Dutch
    /// Auction, but most likely users wouldn't be querying listings based on "quantity"... "Show me all listings where quantity is 5..."
    /// 
    ///</summary>    
    public interface IListing
    {
        /// <summary>
        /// The Name of the Listing Provider
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Creates a new Listing
        /// </summary>
        /// <param name="userInput">The parameters with which to create the new Listing</param>
        /// <param name="notify">Whether or not email notifications should be sent for the new listing.  This value should most often be true, but false in case of bulk imports.</param>
        /// <param name="listingID">The Id of the newly created Listing</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        /// <param name="validateOnly">true if the listing creation should only be validated and not committed</param>
        /// <returns>A bool indicating if fees are owed immediately</returns>
        bool CreateListing(UserInput userInput, bool notify, out int listingID, ProviderContext context, bool validateOnly = false);

        /// <summary>
        /// Updates an existing Listing
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="userInput">The parameters with which to modify the existing Listing</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        /// <returns>A bool indicating if fees are owed immediately</returns>
        bool UpdateListing(string actingUserName, UserInput userInput, ProviderContext context);

        /// <summary>
        /// Returns a listing to its original state
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listing">The listing to restore</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        void RestoreListing(string actingUserName, DTO.Listing listing, ProviderContext context);

        /// <summary>
        /// Retrieves a list of Listing fields and their updatability
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listing">The Listing for which to retrieve the list of updatable fields</param>
        /// <returns>A Dictionary containing field names and bool values indicating if the field can be updated</returns>
        Dictionary<string, bool> GetUpdateableListingFields(string actingUserName, DTO.Listing listing);

        /// <summary>
        /// Registers the Listing Format Provider
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <returns>A bool indicating if the system modified anything during registration of this Listing Format Provider</returns>
        bool RegisterSelf(string actingUserName);

        /// <summary>
        /// Fills the Listing's Context with respect to a specific user
        /// </summary>
        /// <remarks>
        /// A Listing's Context is basically the status of the Listing for a specific user, based on the actions that user may have taken on the Listing.
        /// </remarks>
        /// <param name="userName">The UserName of the user to fill the context for</param>
        /// <param name="listing">The Listing for which to fill the Listing Context</param>
        void FillListingContext(string userName, DTO.Listing listing);

        /// <summary>
        /// Validates Listing Type Properties as submitted from the Admin Control Panel and returns a list of custom properties
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="input">The Listing Type Properties to attempt</param>
        /// <param name="validation">The validation results collection</param>
        /// <returns>The final list of Listing Type Properties (Custom Properties)</returns>                
        List<CustomProperty> ValidateListingTypeProperties(string actingUserName, UserInput input, ValidationResults validation);
    }
}
