﻿namespace RainWorx.FrameWorx.Providers.Listing
{
    /// <summary>
    /// The IListingResolver interface is the base interface for the workflow that should take place when a Listing officially ends.
    /// </summary>
    /// <remarks>
    /// A Listing "ends" when the current Date/Time exceeds the Listing's End Date/Time.
    /// </remarks>
    public interface IListingResolver
    {
        /// <summary>
        /// Resolves the Listing when it's ending and creates the necessary LineItems for eventual association with a sales Invoice.
        /// </summary>
        /// <remarks>
        /// For auctions, this method determines the winner of the auction.  For Fixed Price Listings, this method determines if the listing was successful, etc...
        /// </remarks>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listing">The Listing to Resolve</param>
        void ResolveListing(string actingUserName, DTO.Listing listing);        
    }
}
