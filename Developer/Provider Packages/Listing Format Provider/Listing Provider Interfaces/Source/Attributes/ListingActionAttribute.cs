﻿using System;

namespace RainWorx.FrameWorx.Providers.Listing.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ListingActionAttribute : Attribute
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }

        public ListingActionAttribute(string name)
        {
            _name = name;
        }
    }
}
