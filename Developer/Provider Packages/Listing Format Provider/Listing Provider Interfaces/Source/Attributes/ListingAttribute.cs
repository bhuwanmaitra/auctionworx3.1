﻿using System;

namespace RainWorx.FrameWorx.Providers.Listing.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]                       
    public class ListingAttribute : Attribute
    {
        private string _name;
        public string Name
        {
            get { return _name; }            
        }

        public ListingAttribute(string name)
        {
            _name = name;
        }
    }
}
