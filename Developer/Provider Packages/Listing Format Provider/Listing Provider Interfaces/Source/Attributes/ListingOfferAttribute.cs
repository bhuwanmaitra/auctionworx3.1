﻿using System;

namespace RainWorx.FrameWorx.Providers.Listing.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ListingOfferAttribute : Attribute
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }

        public ListingOfferAttribute(string name)
        {
            _name = name;
        }
    }
}
