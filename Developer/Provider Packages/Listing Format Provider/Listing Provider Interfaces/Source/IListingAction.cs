﻿using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Listing
{
    /// <summary>
    /// The IListingAction interface is the base interface for all listing actions (bids, purchases, etc...).
    /// 
    /// It handles the creation of Listing Actions for all Listings using a two step process.  When initiated by a user during the normal course of business, 
    /// it first converts the raw UserInput into a ListingAction, and then submits that ListingAction for the remainder of the workflow.
    /// 
    /// When initiated by the Delete/Rollback system by an adminitrator, the ListingAction has already been persisted to the back end data store and so that
    /// ListingAction is submitted for the remainder of the workflow.
    /// </summary>
    public interface IListingAction
    {
        /// <summary>
        /// Submits a Listing Action
        /// </summary>
        /// <remarks>
        /// A ListingAction is a user's response to a listing such as a bid, purchase, etc...
        /// </remarks>
        /// <param name="action">The ListingAction to submit</param>
        /// <param name="accepted">A bool indicating if the ListingAction was accepted</param>
        /// <param name="reasonCode">A ReasonCode enumeration value indicating why a ListingAction was rejected</param>
        /// <param name="newPurchaseLineitem">If the ListingAction resulted in an immediate purchase (like in the case of Fixed Price sales), this is the resulting LineItem</param>
        /// <param name="listing">The Listing to submit the ListingAction to</param>
        /// <param name="notify">Indicates whether or not to send notification emails (false for replaying, etc)</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        /// <returns>A bool indicating whether the submission of this ListingAction incurred fees</returns>
        bool SubmitListingAction(ListingAction action, out bool accepted, out ReasonCode reasonCode, out LineItem newPurchaseLineitem, DTO.Listing listing, bool notify, ProviderContext context);

        /// <summary>
        /// Converts a UserInput "Property Bag" to a ListingAction object
        /// </summary>
        /// <param name="userInput">The UserInput from which to construct the ListingAction</param>
        /// <param name="action">The generated ListingAction</param>
        void ConvertUserInputToListingAction(UserInput userInput, ListingAction action);
    }
}
