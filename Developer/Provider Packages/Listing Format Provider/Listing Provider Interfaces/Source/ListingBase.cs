﻿using System;
using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.DAL;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System.Globalization;
using RainWorx.FrameWorx.DTO.Media;
using RainWorx.FrameWorx.Providers.MediaLoader;
using RainWorx.FrameWorx.Unity;

namespace RainWorx.FrameWorx.Providers.Listing
{
    /// <summary>
    /// Provides basic listing type functionality that may or may not be overridden in some implementations
    /// </summary>
    public abstract class ListingBase : IListing
    {
        /// <summary>
        /// The Name of the Listing Provider
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Creates a new Listing
        /// </summary>
        /// <param name="userInput">The parameters with which to create the new Listing</param>
        /// <param name="notify">Whether or not email notifications should be sent for the new listing.  This value should most often be true, but false in case of bulk imports.</param>
        /// <param name="listingID">The Id of the newly created Listing</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        /// <param name="validateOnly">true if the listing creation should only be validated and not committed</param>
        /// <returns>A bool indicating if fees are owed immediately</returns>
        public abstract bool CreateListing(UserInput userInput, bool notify, out int listingID, ProviderContext context, bool validateOnly = false);

        /// <summary>
        /// Updates an existing Listing
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="userInput">The parameters with which to modify the existing Listing</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        /// <returns>A bool indicating if fees are owed immediately</returns>
        public abstract bool UpdateListing(string actingUserName, UserInput userInput, ProviderContext context);

        /// <summary>
        /// Retrieves a list of Listing fields and their updatability
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listing">The Listing for which to retrieve the list of updatable fields</param>
        /// <returns>A Dictionary containing field names and bool values indicating if the field can be updated</returns>
        public abstract Dictionary<string, bool> GetUpdateableListingFields(string actingUserName, DTO.Listing listing);

        /// <summary>
        /// Registers the Listing Format Provider
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <returns>A bool indicating if the system modified anything during registration of this Listing Format Provider</returns>
        public abstract bool RegisterSelf(string actingUserName);

        /// <summary>
        /// Fills the Listing's Context with respect to a specific user
        /// </summary>
        /// <remarks>
        /// A Listing's Context is basically the status of the Listing for a specific user, based on the actions that user may have taken on the Listing.
        /// </remarks>
        /// <param name="userName">The UserName of the user to fill the context for</param>
        /// <param name="listing">The Listing for which to fill the Listing Context</param>
        public abstract void FillListingContext(string userName, DTO.Listing listing);

        protected readonly IDataContext _data;

        /// <summary>
        /// Constructor for implementations of this class
        /// </summary>
        /// <param name="data">a reference to the DAL</param>
        protected ListingBase(IDataContext data)
        {
            _data = data;
        }

        /// <summary>
        /// Returns a listing to its original state
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listing">The listing to restore</param>
        /// <param name="context">A fully or partially filled context object for the provider to use, or null to use the default context</param>
        public virtual void RestoreListing(string actingUserName, DTO.Listing listing, ProviderContext context)
        {            
            listing.CurrentPrice = listing.OriginalPrice;
            listing.CurrentListingAction = null;
            listing.CurrentQuantity = listing.OriginalQuantity;
            listing.AcceptedActionCount = 0;            
            listing.CurrentListingActionID = 0;
            listing.CurrentListingActionUserName = string.Empty;
        }

        /// <summary>
        /// Ensures a listing type exists
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listingTypeName">The name of the listing type</param>
        /// <returns>true if the listing type was added (somethign changed), otherwise false</returns>
        protected bool EnsureListingType(string actingUserName, string listingTypeName)
        {
            //If necessary, add Listing Type
            List<ListingType> existingListingTypes = _data.GetAllListingTypes();
            if (!existingListingTypes.Any(elt => elt.Name.Equals(listingTypeName)))
            {
                _data.CreateListingType(actingUserName, listingTypeName);
                return true; //something changed
            }

            return false; //listing type was already there
        }

        /// <summary>
        /// Ensures a listing property exists
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="existingCustomProperties">The collection of existing custom properties for this listing type</param>
        /// <param name="propertyName">The name of the field</param>
        /// <param name="fieldType">The data type of the field</param>
        /// <param name="fieldGroupName">The group name to which the field belongs</param>
        /// <param name="defaultValue">The default value of the field</param>
        /// <param name="displayOrder">The ordinal position of the field when sorted for display</param>
        /// <param name="required">true, if the field is required to have a value</param>
        /// <param name="listingTypeName">The name of the listing type to add the field to</param>
        /// <param name="scope">The scope of the field</param>
        /// <param name="visibility">ability for the CustomField to be displayed/read</param>
        /// <param name="mutability">ability for the CustomField to be edited/written</param>
        /// <returns>true, if the field/property was added (something changed), otherwise false</returns>
        protected bool EnsureListingProperty(string actingUserName, List<CustomProperty> existingCustomProperties,
                                             string propertyName, CustomFieldType fieldType, string fieldGroupName,
                                             string defaultValue, int displayOrder, bool required,
                                             string listingTypeName, string scope, CustomFieldAccess visibility, CustomFieldAccess mutability)
        {
            return EnsureListingProperty(actingUserName, existingCustomProperties, 
                                         propertyName, fieldType, fieldGroupName, 
                                         defaultValue, displayOrder, required, 
                                         listingTypeName, scope, visibility, mutability, null);
        }

        /// <summary>
        /// Ensures a listing property exists
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="existingCustomProperties">The collection of existing custom properties for this listing type</param>
        /// <param name="propertyName">The name of the field</param>
        /// <param name="fieldType">The data type of the field</param>
        /// <param name="fieldGroupName">The group name to which the field belongs</param>
        /// <param name="defaultValue">The default value of the field</param>
        /// <param name="displayOrder">The ordinal position of the field when sorted for display</param>
        /// <param name="required">true, if the field is required to have a value</param>
        /// <param name="listingTypeName">The name of the listing type to add the field to</param>
        /// <param name="scope">The scope of the field</param>
        /// <param name="enumList">If the fieldType is an "Enum," a comma delimited list of possible enumeration values for the field</param>
        /// <returns>true, if the field/property was added (something changed), otherwise false</returns>
        protected bool EnsureListingProperty(string actingUserName, List<CustomProperty> existingCustomProperties,
                                             string propertyName, CustomFieldType fieldType, string fieldGroupName,
                                             string defaultValue, int displayOrder, bool required,
                                             string listingTypeName, string scope, CustomFieldAccess visibility, CustomFieldAccess mutability, string enumList)
        {
            bool retVal = false;

            if (!existingCustomProperties.Any(scp => scp.Field.Name.Equals(propertyName)))
            {
                //If necessary, add field
                CustomField currentField = _data.GetCustomFieldByName(propertyName, fieldType, fieldGroupName);
                if (currentField == null)
                {
                    currentField = new CustomField
                                       {
                                           DefaultValue = defaultValue,
                                           Deferred = false,
                                           DisplayOrder = displayOrder,
                                           Group = fieldGroupName,
                                           Name = propertyName,
                                           Required = required,
                                           Type = fieldType,
                                           Visibility = visibility,
                                           Mutability = mutability
                                       };
                    _data.AddCustomField(actingUserName, currentField);

                    //if necessary, add enum options
                    if (!string.IsNullOrEmpty(enumList))
                    {
                        foreach(string enumData in enumList.Split(','))
                        {
                            string enumName = string.Empty;
                            string enumValue = string.Empty;
                            bool enumEnabled = false;
                            int i = 0;
                            foreach(string enumPart in enumData.Split('|'))
                            {
                                if (i == 0)
                                {
                                    enumName = enumPart;
                                }
                                else if (i == 1)
                                {
                                    enumValue = enumPart;
                                } else if (i == 2)
                                {
                                    enumEnabled = bool.Parse(enumPart);
                                }
                                i++;
                            }
                            _data.CreateEnumeration(actingUserName, currentField.ID, enumName, enumName, enumValue, enumEnabled);
                        }
                    }
                }

                //add Enabled property
                CustomProperty currentProperty = new CustomProperty
                                                    {
                                                        Field = currentField,
                                                        Value = defaultValue
                                                    };
                List<CustomProperty> newProperties = new List<CustomProperty>(1) {currentProperty};
                _data.AddCustomPropertiesToListingType(actingUserName, listingTypeName, scope, newProperties);
                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Ensures the navigational category exists for the listing type's properties in the Admin Control Panel
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listingTypeName">The name of the listing type to add the navigational category for</param>
        /// <param name="displayOrder">The ordinal position of the navigational category when sorted for display</param>
        /// <returns>true, if the navigational category was added (something changed), otherwise false</returns>
        protected bool EnsureNavigationalCategory(string actingUserName, string listingTypeName, int displayOrder, bool enabledForEvents)
        {
            //Get listing type parent category
            Category listingTypeParentCategory = _data.GetCategoryByName("Listings", Strings.CategoryTypes.Site);
            //Get child categories
            List<Category> listingTypeCategories = _data.GetChildCategories(listingTypeParentCategory.ID);
            //If necessary, add Listing Type Navigational Category
            if (!listingTypeCategories.Any(ltc => ltc.Name.Equals(listingTypeName)))
            {
                Category category = new Category
                                        {
                                            Name = listingTypeName,
                                            Type = Strings.CategoryTypes.Site,
                                            MVCAction = "ListingTypeProperties/" + listingTypeName,
                                            DisplayOrder = displayOrder,
                                            EnabledCustomProperty = enabledForEvents ? string.Empty : "!EnableEvents"
                                        };
                _data.AddChildCategory(actingUserName, "Listings", Strings.CategoryTypes.Site, category);
                return true; //something changed!
            }
            return false;
        }

        /// <summary>
        /// Ensures applicable fee providers are enabled or disabled for the given listing type
        /// </summary>
        /// <param name="applicableFeeProviders">The collection of applicable fee providers</param>
        /// <param name="listingTypeName">The name of the listing type enable/disable a specific Fee Provider for</param>
        /// <param name="providerName">The specific Fee Provider name</param>
        /// <param name="enabled">true if the Fee Provider should be applicable for the given listing type, otherwise false</param>
        /// <returns>true, if the Fee Provider was enabled or disabled (something changed), otherwise false</returns>
        protected bool EnsureFeeProviderEnablement(List<ListItem> applicableFeeProviders, string listingTypeName, string providerName, bool enabled)
        {
            if (applicableFeeProviders.Any(afp => afp.Name.Equals(providerName) && afp.Enabled == !enabled))
            {
                _data.SetListingTypeFeeProviderEnabled(listingTypeName, providerName, enabled);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Performs basic data type and required validation for "Site" scoped listing type properties
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="input">The UserInput object containing the "Property Bag" to validate</param>
        /// <param name="results">The results of the validation</param>
        /// <returns>A CustomProperty collection of the input values</returns>
        public virtual List<CustomProperty> ValidateListingTypeProperties(string actingUserName, UserInput input, ValidationResults results)
        {
            List<CustomProperty> properties = _data.GetListingTypeProperties(Name, "Site").OrderBy(p => p.Field.DisplayOrder).ToList();            

            //the validation helper assumes all values will be formatted in the default culture,
            //  so it is necessary to pre-process numeric or date values with the user's culture, first
            //this loop also handles assigning each attempted value to the property, formatted with the system culture
            CultureInfo cultureInfo = CultureInfo.GetCultureInfo(input.CultureName);
            foreach (CustomProperty property in properties)
            {
                string key = property.Field.Name;
                string attemptedValue = null;
                if (input.Items.ContainsKey(key))
                {
                    attemptedValue = input.Items[key];
                }

                if (string.IsNullOrEmpty(attemptedValue) && property.Field.Required)
                {
                    results.AddResult(
                               new ValidationResult(
                                   Strings.ValidationMessages.CustomFieldValidationPrefix +
                                   string.Format(Strings.Formats.RequiredValidationMessage, null,
                                                 property.Field.Name), this, property.Field.Name,
                                   property.Field.Name, null));
                    continue;
                }

                switch (property.Field.Type)
                {
                    case CustomFieldType.Boolean:
                        bool tempBool;
                        if (!bool.TryParse(attemptedValue, out tempBool))
                        {
                            results.AddResult(
                                new ValidationResult(
                                    Strings.ValidationMessages.CustomFieldValidationPrefix +
                                    string.Format(Strings.Formats.BooleanConversionValidationMessage, null,
                                                  property.Field.Name), this, property.Field.Name,
                                    property.Field.Name, null));
                        }
                        else
                        {
                            property.Value = tempBool.ToString();
                        }
                        break;
                    case CustomFieldType.DateTime:
                        DateTime tempDateTime;
                        if (!DateTime.TryParse(attemptedValue, cultureInfo, DateTimeStyles.None, out tempDateTime))
                        {
                            results.AddResult(
                                new ValidationResult(
                                    Strings.ValidationMessages.CustomFieldValidationPrefix +
                                    string.Format(Strings.Formats.DateTimeConversionValidationMessage, null,
                                                  property.Field.Name), this, property.Field.Name,
                                    property.Field.Name, null));
                        }
                        else
                        {
                            property.Value = tempDateTime.ToString(CultureInfo.InvariantCulture);
                        }
                        break;
                    case CustomFieldType.Int:
                        int tempInt;
                        if (!int.TryParse(attemptedValue, NumberStyles.Integer, cultureInfo, out tempInt))
                        {
                            results.AddResult(
                                new ValidationResult(
                                    Strings.ValidationMessages.CustomFieldValidationPrefix +
                                    string.Format(Strings.Formats.IntegerConversionValidationMessage, null,
                                                  property.Field.Name), this, property.Field.Name,
                                    property.Field.Name, null));
                        }
                        else
                        {
                            property.Value = tempInt.ToString(CultureInfo.InvariantCulture);
                        }
                        break;
                    case CustomFieldType.Decimal:
                        decimal tempDecimal;
                        if (!decimal.TryParse(attemptedValue, NumberStyles.Number, cultureInfo, out tempDecimal))
                        {
                            results.AddResult(
                                new ValidationResult(
                                    Strings.ValidationMessages.CustomFieldValidationPrefix +
                                    string.Format(Strings.Formats.DecimalConversionValidationMessage, null,
                                                  property.Field.Name), this, property.Field.Name,
                                    property.Field.Name, null));
                        }
                        else
                        {
                            property.Value = tempDecimal.ToString(CultureInfo.InvariantCulture);
                        }
                        break;
                    default:
                        property.Value = attemptedValue;
                        break;
                }
            }

            return properties;
        }

        /// <summary>
        /// Retrieves a count of associated image Media
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="medias">list of media to evaluate</param>
        /// <returns>count of non-youtube media</returns>
        protected int GetListingImageCount(string actingUserName, IEnumerable<Media> medias)
        {
            int retVal = 0;
            //retVal = medias.Count(m => m.Type != "RainWorx.FrameWorx.Providers.MediaAsset.YouTube"); // Type is not filled when creating a listing
            if (medias != null)
                foreach (Media media in medias)
                {
                    //Media tempMedia = _data.GetMediaByID(actingUserName, media.ID); // ID is not filled when creating a listing
                    Media tempMedia = _data.GetMediaByGUID(actingUserName, media.GUID);                                        
                    if (tempMedia != null && tempMedia.Context.Equals("UploadListingImage")) retVal++;
                }
            return retVal;
        }

        protected string GetPrimaryImageURI(List<Media> media)
        {
            if (media.Count > 0)
            {
                Media primaryMedia =
                    media.Where(m => m.Context.Equals("UploadListingImage", StringComparison.OrdinalIgnoreCase))
                        .OrderBy(m => m.DisplayOrder)
                        .FirstOrDefault();
                if (primaryMedia == null) return string.Empty;

                IMediaLoader mediaLoader = UnityResolver.Get<IMediaLoader>(primaryMedia.Loader);
                Dictionary<string, string> loaderProviderSettings = _data.GetAttributeData(mediaLoader.TypeName,
                    primaryMedia.Context);
                string newURI = mediaLoader.Load(loaderProviderSettings, primaryMedia, "FullSize");
                return newURI.Replace("fullsize", "{0}");
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Validates if the acting user has permission to edit the specified fields
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="ownerUserName">the username of the owner of the property container</param>
        /// <param name="properties">the list of properties to check</param>
        /// <param name="validation">the validation results container</param>
        /// <param name="userInput">the user input container</param>
        protected virtual void CheckFieldMutabilityAccess(string actingUserName, string ownerUserName, IEnumerable<CustomProperty> properties, ValidationResults validation, UserInput userInput)
        {
            if (properties.Any())
            {
                CustomFieldAccess actingUserAccessLevel = CustomFieldAccess.Authenticated;
                if (actingUserName == Strings.SystemActors.SystemUserName)
                {
                    actingUserAccessLevel = CustomFieldAccess.System;
                }
                else if (_data.GetIsUserInRole(actingUserName, Strings.Roles.Admin))
                {
                    actingUserAccessLevel = CustomFieldAccess.Admin;
                }
                else if (actingUserName.Equals(ownerUserName, StringComparison.OrdinalIgnoreCase))
                {
                    actingUserAccessLevel = CustomFieldAccess.Owner;
                }
                foreach (var customProperty in properties)
                {
                    if (userInput.Items.ContainsKey(customProperty.Field.Name))
                    {
                        if (actingUserAccessLevel > customProperty.Field.Mutability)
                        {
                            string key = customProperty.Field.Name;
                            validation.AddResult(new ValidationResult(Strings.ValidationMessages.CustomFieldValidationPrefix + string.Format(Strings.Formats.EditNotAllowedValidationMessage, null, key), this, key, key, null));
                        }
                    }
                }
            }
        }

    }
}
