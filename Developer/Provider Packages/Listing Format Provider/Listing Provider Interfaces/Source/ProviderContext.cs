﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RainWorx.FrameWorx.Providers.Listing
{
    public class ProviderContext
    {
        private Dictionary<string, object> Settings { get; set; }

        public ProviderContext()
        {
            Settings = new Dictionary<string, object>();
        }

        public void Add(string key, object value)
        {
            Settings.Add(key, value);
        }

        public T Get<T>(string key)
        {
            return (T) Settings[key];
        }

        public bool Has(string key)
        {
            return Settings.ContainsKey(key);
        }
    }
}
