﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Queueing;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using RainWorx.FrameWorx.Services;

namespace RainWorx.FrameWorx.Providers.Listing.FixedPrice
{
    [Attributes.ListingAction(Strings.ListingTypes.FixedPrice)]
    public class Purchase : IListingAction
    {
        private readonly IDataContext _data;
        private readonly IListingService _listing;
        private readonly IAccountingService _accounting;
        private readonly INotifierService _notifier;
        private readonly IQueueManager _queueManager;

        public Purchase(IDataContext data, IListingService listing, IAccountingService accounting, INotifierService notifier, IQueueManager queueManager)
        {
            _data = data;
            _accounting = accounting;
            _listing = listing;
            _notifier = notifier;
            _queueManager = queueManager;
        }

        public void ConvertUserInputToListingAction(UserInput userInput, ListingAction action)
        {
            //Convert the provided user input into a ListingAction here.  A ListingAction is a user's response to a Listing.  For instance, If you have an Auction as a listing, the associated
            //ListingAction is a bid.  For a Fixed Price listing, the association ListingAction is a purchase.

            //This method is called after a listing action (bid/purchase/...) is submitted to create the ListingAction object
#if TRACE
            using (LogManager.Trace())
            {
#endif
                var validation = new ValidationResults();

                int? quantity = ValidationHelper.ValidatePositiveInt(Strings.Fields.Quantity, userInput, this, validation);

                int? listingID = ValidationHelper.ValidatePositiveInt(Strings.Fields.ListingID, userInput, this, validation);
                if (listingID == null)
                {
                    validation.AddResult(new ValidationResult(Strings.Messages.ListingIDMissing, this, Strings.Fields.ListingID, Strings.Fields.ListingID, null));
                }
                /*else
                {
                    if (quantity.HasValue)
                    {
                        DTO.Listing currentListing = _data.GetListingByID(listingID.Value);
                        if (quantity.Value > currentListing.CurrentQuantity)
                        {
                            validation.AddResult(new ValidationResult(Strings.Messages.QuantityTooHigh, this, Strings.Fields.Quantity, Strings.Fields.Quantity, null));
                        }
                    }
                }*/

                action.ActionDTTM = DateTime.UtcNow;
                action.Description = Strings.FieldDefaults.FixedPriceListingActionDescripton;

                if (quantity.HasValue)
                {
                    action.Quantity = quantity.Value;
                }

                action.UserName = userInput.FBOUserName;
                action.User = _data.GetUserByUserName(userInput.FBOUserName);
                action.UserEntered = true;
                action.Properties = new List<CustomProperty>(0);

                if (listingID != null) action.ListingID = listingID.Value;

                if (!validation.IsValid)
                    Statix.ThrowValidationFaultContract(validation);
#if TRACE
            }
#endif
        }

        public bool SubmitListingAction(ListingAction action, out bool accepted, out ReasonCode reasonCode, out LineItem newPurchaseLineitem, DTO.Listing currentListing, bool notify, ProviderContext context)
        {
            //Submit the listing action to the database here.  accepted is marked true if the action is accepted (bid/purchase/... is accepted).  If an action wasn't accepted, reasonCode contains the reason why.
            //newPurchaseLineitem is a LineItem generated and returned if the listing action resulted in an immediate sale, such as "Buy Now" for an auction, or Fixed Price item purchases.
            //currentListing is the listing in its state AFTER the action has been applied

            //This method is called immediately after the ConvertUserInputToListingAction method is called after a listing action (bid) is submitted, or anytime listing actions must be replayed
            //in the event a bid is deleted, or a series of bids is rolledback
#if TRACE
            using (LogManager.Trace())
            {
#endif
                reasonCode = ReasonCode.Failure;
                accepted = false;
                newPurchaseLineitem = null;
                action.Reason = string.Empty;

                //get listing we're acting on            
                if (currentListing == null)
                {
                    currentListing = _data.GetListingByIDWithFillLevel(action.ListingID, Strings.ListingFillLevels.All);
                    if (currentListing == null) Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);
                }

                action.Amount = currentListing.CurrentPrice;
                action.ProxyAmount = currentListing.CurrentPrice;

                //do the workflow processing                            
                if (currentListing.EndDTTM.Value < action.ActionDTTM)
                {
                    //if listing not active...
                    action.Status = Strings.ListingActionStatuses.Rejected;
                    action.Reason = Strings.Messages.ListingNotActive;
                    reasonCode = ReasonCode.ListingNotActive;
                    accepted = false;
                }
                else if (currentListing.Owner.ID == action.User.ID)
                {
                    //If purchases is owner, prepare to reject action   
                    action.Status = Strings.ListingActionStatuses.Rejected;
                    action.Reason = Strings.Messages.BidderIsOwner;
                    reasonCode = ReasonCode.ListerPerformingAction;
                    accepted = false;
                }
                else if (action.Quantity <= currentListing.CurrentQuantity)
                {
                    //we're now winning, add our purchase
                    action.Status = Strings.ListingActionStatuses.Accepted;
                    reasonCode = ReasonCode.Success;
                    accepted = true;
                }
                //otherwise prepare to reject purchase
                else
                {
                    //this purchase is rejected
                    action.Status = Strings.ListingActionStatuses.Rejected;
                    action.Reason = Strings.Messages.QuantityTooHigh;
                    reasonCode = ReasonCode.QuantityTooHigh;
                    accepted = false;
                }

                try
                {
                    //attempt to create the object
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        _data.AddListingAction(action.UserName, action);
                        currentListing.ListingActions.Add(action);

                        if (action.Status == Strings.ListingActionStatuses.Accepted)
                        {
                            int tempQuantity = (currentListing.CurrentQuantity - action.Quantity);
                            currentListing.CurrentQuantity = tempQuantity;
                            currentListing.AcceptedActionCount++;
                            currentListing.CurrentListingAction = action;
                            currentListing.CurrentListingActionID = action.ID;
                            currentListing.CurrentListingActionUserName = action.UserName;

                            ListingActionChange data = new ListingActionChange()
                            {
                                ListingID = currentListing.ID,
                                Price = currentListing.CurrentPrice.Value,
                                Increment = 0.0M,
                                NextPrice = currentListing.CurrentPrice.Value,
                                Source = "BUY_ORIGIN",
                                CurrencyCode = currentListing.Currency.Code,
                                UserName = action.UserName,
                                AcceptedActionCount = currentListing.AcceptedActionCount,
                                CurrentListingActionUserName = currentListing.CurrentListingActionUserName,
                                Quantity = currentListing.CurrentQuantity
                            };
                            _queueManager.FireListingActionChange(data);

                            //end listing early, quantity is zero                                        
                            if (tempQuantity <= 0)
                            {
                                currentListing.Status = Strings.ListingStatuses.Ending;

                                ListingStatusChange statusUpdate = new ListingStatusChange()
                                {
                                    ListingID = currentListing.ID,
                                    Status = currentListing.Status,
                                    Source = "LISTINGACTION_ORIGIN"
                                };
                                _queueManager.FireListingStatusChange(statusUpdate);
                            }

                            _data.UpdateListingBasic(action.UserName, currentListing);

                            //Create LineItem for ended Listing
                            var lineItem = new LineItem
                                               {
                                                   PerUnitAmount = action.Amount.Value,
                                                   Quantity = action.Quantity,
                                                   TotalAmount = action.Amount.Value * action.Quantity,
                                                   Currency = currentListing.Currency.Code,
                                                   DateStamp = DateTime.UtcNow,
                                                   Description = currentListing.Title,
                                                   Owner = currentListing.Owner,
                                                   Payer = action.User,
                                                   ListingID = currentListing.ID,
                                                   Type = Strings.LineItemTypes.Listing,
                                                   Taxable = true,
                                                   BuyersPremiumApplies = true
                                               };
                            //create line item properties for any custom listing fields with IncludeOnInvoice or IncludeInSalesReport set
                            List<CustomProperty> propsForInvoice = currentListing.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport).ToList();
                            if (currentListing.Lot != null)
                            {
                                propsForInvoice.AddRange(currentListing.Lot.Event.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport));
                            }
                            lineItem.Properties = new Dictionary<string, string>(propsForInvoice.Count);
                            foreach (CustomProperty prop in propsForInvoice)
                            {
                                string val = string.Empty;
                                CultureInfo sellersCultureInfo = CultureInfo.GetCultureInfo(currentListing.Owner.Culture);
                                if (!string.IsNullOrEmpty(prop.Value))
                                {
                                    switch (prop.Field.Type)
                                    {
                                        case CustomFieldType.Decimal:
                                            if (prop.Value.ToString().TrimEnd('0') != "")
                                            {
                                                int count = BitConverter.GetBytes(decimal.GetBits(decimal.Parse(prop.Value.ToString().TrimEnd('0')))[3])[2];
                                                val = decimal.Parse(prop.Value).ToString("N" + count, sellersCultureInfo);
                                            }
                                            else
                                            {
                                                val = decimal.Parse(prop.Value).ToString("N0", sellersCultureInfo);
                                            }
                                            break;
                                        case CustomFieldType.Int:
                                            val = int.Parse(prop.Value).ToString(sellersCultureInfo);
                                            break;
                                        case CustomFieldType.DateTime:
                                            val = DateTime.Parse(prop.Value).ToString(sellersCultureInfo);
                                            break;
                                        case CustomFieldType.Boolean:
                                            val = Boolean.Parse(prop.Value) ? "Yes" : "No";
                                            break;
                                        default:
                                            val = prop.Value;
                                            break;
                                    }
                                    lineItem.Properties.Add(prop.Field.Name, val);
                                }
                            }

                            LineItem newLineItem = _accounting.CreateNewLineItem(Strings.SystemActors.SystemUserName, lineItem);
                            //return the line item via output parameter
                            newPurchaseLineitem = newLineItem;

                            //send notifications
                            if (notify)
                            {
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                                  currentListing.OwnerUserName,
                                                                  Strings.Templates.SellerSoldNotEnded,
                                                                  Strings.DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                                  action.UserName,
                                                                  Strings.Templates.BuyerBoughtNotEnded,
                                                                  Strings.DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);
                            }

                            //Add Fees for EndListing event
                            var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, currentListing.Title } };
                            _accounting.AddFees(Strings.SystemActors.SystemUserName,
                                                Strings.Events.EndListingSuccess, properties,
                                                null, currentListing, action);

                        }

                        scope.Complete();
                    }
                }
                catch
                {
                    //concurrency exception during the update of the listing or auction objects or during workflow processing above
                    //Mark the bid rejected (disregard any overriding bid)
                    action.Status = Strings.ListingActionStatuses.Rejected;
                    action.Reason = Strings.Messages.FixedPriceChanged;
                    //prepare to reject bid
                    reasonCode = ReasonCode.ListingChangedDuringProcessing;
                    accepted = false;
                    //attempt to create the object again (should succeed because nothing in DAL gets updated or deleted...                        
                    _data.AddListingAction(action.UserName, action);
                    currentListing.ListingActions.Add(action);
                }
                return false; //no fees owed
#if TRACE
            }
#endif
        }
    }
}
