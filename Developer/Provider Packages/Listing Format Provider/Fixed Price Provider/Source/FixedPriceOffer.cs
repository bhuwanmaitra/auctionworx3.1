﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Transactions;
using System.Xml.Serialization;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Strings;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using System.Linq;
using RainWorx.FrameWorx.Queueing;

namespace RainWorx.FrameWorx.Providers.Listing.FixedPrice
{
    [Attributes.ListingOffer(ListingTypes.FixedPrice)]
    public class FixedPriceOffer : IListingOffer
    {
        private readonly IDataContext _data;
        private readonly IListingService _listing;
        private readonly IAccountingService _accounting;
        private readonly INotifierService _notifier;
        private readonly IQueueManager _queueManager;

        public FixedPriceOffer(IDataContext data, IListingService listing, IAccountingService accounting, INotifierService notifier, IQueueManager queueManager)
        {
            _data = data;
            _listing = listing;
            _accounting = accounting;
            _notifier = notifier;
            _queueManager = queueManager;
        }

        private Offer ParseNewOfferInputs(UserInput input)
        {
            int offerTimeLimitHours = int.Parse(Cache.SiteProperties[SiteProperties.OfferTimeLimitHours]);

            var validation = new ValidationResults();

            int listingId = int.Parse(input.Items[Fields.ListingID]);
            string fillLevel = ListingFillLevels.Properties + "," + ListingFillLevels.Owner + "," + ListingFillLevels.PrimaryCategory;
            var listing = _data.GetListingByIDWithFillLevel(listingId, fillLevel);

            if (listing == null || listing.Status != ListingStatuses.Active)
            {
                validation.AddResult(new ValidationResult(Messages.ListingNotActive, this, Fields.ListingID, Fields.ListingID, null));
            }

            bool makeOfferAvailable;
            if (!listing.Properties.Any(lp => lp.Field.Name == Fields.MakeOfferAvailable &&
                bool.TryParse(lp.Value, out makeOfferAvailable) &&
                makeOfferAvailable == true))
            {
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.MakeOfferNotAvailable);
            }

            Offer newOffer = new Offer();

            //Assignment and Validation
            newOffer.Sender = _data.GetUserByUserName(input.FBOUserName);
            newOffer.Buyer = _data.GetUserByUserName(input.FBOUserName);
            newOffer.Receiver = listing.Owner;
            newOffer.ListingID = listing.ID;
            newOffer.Listing = listing;
            newOffer.Status = OfferStatuses.Active;

            newOffer.Amount = ValidationHelper.ValidatePositiveMoneyValue(Fields.Amount, input, this, validation) ?? 0;
            newOffer.Quantity = ValidationHelper.ValidatePositiveInt(Fields.Quantity, input, this, validation) ?? 1;
            newOffer.OfferMessage = input.Items.ContainsKey(Fields.OfferMessage) ? input.Items[Fields.OfferMessage] : string.Empty;

            newOffer.ExpirationDTTM = DateTime.UtcNow.AddHours(offerTimeLimitHours);
            if (newOffer.ExpirationDTTM > listing.EndDTTM.Value)
            {
                newOffer.ExpirationDTTM = listing.EndDTTM.Value;
            }

            if (newOffer.Amount >= (listing.CurrentPrice ?? 0.0M))
            {
                validation.AddResult(new ValidationResult("OfferAmountMustBeLessThanPrice", this, Fields.Quantity, Fields.Quantity, null));
            }

            if (newOffer.Quantity > listing.CurrentQuantity)
            {
                validation.AddResult(new ValidationResult("QuantityTooHigh", this, Fields.Quantity, Fields.Quantity, null));
            }

            validation.AddAllResults(ValidationHelper.Validate(newOffer, "default"));

            if (!validation.IsValid)
                Statix.ThrowValidationFaultContract(validation);

            return newOffer;
        }

        public void ValidateOffer(UserInput input, out Offer newOffer)
        {
            newOffer = ParseNewOfferInputs(input);
        }

        public void SendOffer(UserInput input, out Offer newOffer)
        {
            bool notify = bool.Parse(Cache.SiteProperties[SiteProperties.OfferNotificationsEnabled]);

            newOffer = ParseNewOfferInputs(input);
            using (var scope = TransactionUtils.CreateTransactionScope())
            {
                _data.AddListingOffer(input.ActingUserName, newOffer);
                if (notify)
                {
                    _notifier.QueueSystemNotification(SystemActors.SystemUserName, newOffer.Listing.OwnerUserName,
                        Templates.OfferReceived_Seller, DetailTypes.Offer, newOffer.ID,
                        null, null, null, null, null);
                    _notifier.QueueSystemNotification(SystemActors.SystemUserName, newOffer.BuyingUser,
                        Templates.OfferConfirmation_Buyer, DetailTypes.Offer, newOffer.ID,
                        null, null, null, null, null);
                }
                scope.Complete();
            }
        }

        public void AcceptOffer(UserInput input, Offer offer)
        {
            bool notify = bool.Parse(Cache.SiteProperties[SiteProperties.OfferNotificationsEnabled]);

            var validation = new ValidationResults();

            if (offer.Status == OfferStatuses.Accepted)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyAccepted);
            if (offer.Status == OfferStatuses.Declined)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyDeclined);
            if (offer.Status == OfferStatuses.Expired)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferIsExpired);
            if (offer.Status == OfferStatuses.Countered)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyCountered);
            if (offer.Listing.Status != ListingStatuses.Active || offer.Listing.EndDTTM < DateTime.UtcNow)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.ListingNotActive);
            if (offer.Quantity > offer.Listing.CurrentQuantity)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.QuantityTooHigh);

            if (!validation.IsValid)
                Statix.ThrowValidationFaultContract(validation);

            var currentListing = offer.Listing;
            using (var scope = TransactionUtils.CreateTransactionScope())
            {
                int tempQuantity = (currentListing.CurrentQuantity - offer.Quantity);
                currentListing.CurrentQuantity = tempQuantity;

                ListingActionChange data = new ListingActionChange()
                {
                    ListingID = currentListing.ID,
                    Price = currentListing.CurrentPrice.Value,
                    Increment = 0.0M,
                    NextPrice = currentListing.CurrentPrice.Value,
                    Source = "BUY_ORIGIN",
                    CurrencyCode = currentListing.Currency.Code,
                    UserName = offer.BuyingUser,
                    AcceptedActionCount = currentListing.AcceptedActionCount,
                    CurrentListingActionUserName = currentListing.CurrentListingActionUserName,
                    Quantity = currentListing.CurrentQuantity
                };
                _queueManager.FireListingActionChange(data);

                //end listing early, quantity is zero                                        
                if (tempQuantity <= 0)
                {
                    currentListing.Status = ListingStatuses.Ending;

                    ListingStatusChange statusUpdate = new ListingStatusChange()
                    {
                        ListingID = currentListing.ID,
                        Status = currentListing.Status,
                        Source = "LISTINGACTION_ORIGIN"
                    };
                    _queueManager.FireListingStatusChange(statusUpdate);
                }

                _data.UpdateListingBasic(input.ActingUserName, currentListing);

                //Create LineItem for ended Listing
                var lineItem = new LineItem
                {
                    PerUnitAmount = offer.Amount,
                    Quantity = offer.Quantity,
                    TotalAmount = offer.Amount * offer.Quantity,
                    Currency = currentListing.Currency.Code,
                    DateStamp = DateTime.UtcNow,
                    Description = currentListing.Title,
                    Owner = currentListing.Owner,
                    Payer = offer.Buyer,
                    ListingID = currentListing.ID,
                    Type = Strings.LineItemTypes.Listing,
                    Taxable = true,
                    BuyersPremiumApplies = true
                };
                //create line item properties for any custom listing fields with IncludeOnInvoice or IncludeInSalesReport set
                List<CustomProperty> propsForInvoice = currentListing.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport).ToList();
                if (currentListing.Lot != null)
                {
                    propsForInvoice.AddRange(currentListing.Lot.Event.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport));
                }
                lineItem.Properties = new Dictionary<string, string>(propsForInvoice.Count);
                foreach (CustomProperty prop in propsForInvoice)
                {
                    string val = string.Empty;
                    CultureInfo sellersCultureInfo = CultureInfo.GetCultureInfo(currentListing.Owner.Culture);
                    if (!string.IsNullOrEmpty(prop.Value))
                    {
                        switch (prop.Field.Type)
                        {
                            case CustomFieldType.Decimal:
                                if (prop.Value.ToString().TrimEnd('0') != "")
                                {
                                    int count = BitConverter.GetBytes(decimal.GetBits(decimal.Parse(prop.Value.ToString().TrimEnd('0')))[3])[2];
                                    val = decimal.Parse(prop.Value).ToString("N" + count, sellersCultureInfo);
                                }
                                else
                                {
                                    val = decimal.Parse(prop.Value).ToString("N0", sellersCultureInfo);
                                }
                                break;
                            case CustomFieldType.Int:
                                val = int.Parse(prop.Value).ToString(sellersCultureInfo);
                                break;
                            case CustomFieldType.DateTime:
                                val = DateTime.Parse(prop.Value).ToString(sellersCultureInfo);
                                break;
                            case CustomFieldType.Boolean:
                                val = Boolean.Parse(prop.Value) ? "Yes" : "No";
                                break;
                            default:
                                val = prop.Value;
                                break;
                        }
                        lineItem.Properties.Add(prop.Field.Name, val);
                    }
                }

                LineItem newLineItem = _accounting.CreateNewLineItem(SystemActors.SystemUserName, lineItem);

                if (notify)
                {
                    //send notifications
                    _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                        currentListing.OwnerUserName,
                                                        Templates.SellerSoldNotEnded,
                                                        DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);
                    //_notifier.QueueSystemNotification(SystemActors.SystemUserName,
                    //                                    offer.BuyingUser,
                    //                                    Templates.BuyerBoughtNotEnded,
                    //                                    DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);
                    if (offer.Receiver.UserName == offer.ListingOwnerUsername)
                    {
                        //seller accepting offer
                        _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                            offer.BuyingUser,
                                                            Templates.OfferAccepted_Buyer,
                                                            DetailTypes.Offer, offer.ID, null, null, null, null, null);
                    }
                    else
                    {
                        //buyer accepting counteroffer
                        _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                            offer.ListingOwnerUsername,
                                                            Templates.CounterOfferAccepted_Seller,
                                                            DetailTypes.Offer, offer.ID, null, null, null, null, null);
                    }
                }

                //Add Fees for EndListing event
                var properties = new Dictionary<string, string>(1) { { LineItemProperties.Listing, currentListing.Title } };

                //set offer quantity and Buyer in a temporary 'Action' object, referenced by some fee providers
                //TODO: consider adding new (optional) 'Offer' param to all fee providers instead?
                ListingAction tempAction = new ListingAction() {
                    Quantity = offer.Quantity,
                    User = offer.Buyer
                };

                _accounting.AddFees(SystemActors.SystemUserName,
                                    Events.EndListingSuccess, properties,
                                    null, currentListing, tempAction);

                //set offer status to 'Accepted'
                _data.SetOfferStatus(input.ActingUserName, offer.ID, OfferStatuses.Accepted);

                scope.Complete();
            }

        }

        public void DeclineOffer(UserInput input, Offer offer)
        {
            bool notify = bool.Parse(Cache.SiteProperties[SiteProperties.OfferNotificationsEnabled]);

            var validation = new ValidationResults();

            if (offer.Status == OfferStatuses.Accepted)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyAccepted);
            if (offer.Status == OfferStatuses.Declined)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyDeclined);
            if (offer.Status == OfferStatuses.Expired)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferIsExpired);
            if (offer.Status == OfferStatuses.Countered)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.OfferAlreadyCountered);

            if (input.Items.ContainsKey(Fields.DeclineMessage))
            {
                offer.DeclineMessage = input.Items[Fields.DeclineMessage];
                validation.AddAllResults(ValidationHelper.Validate(offer, "default"));
            }

            if (!validation.IsValid)
                Statix.ThrowValidationFaultContract(validation);

            using (var scope = TransactionUtils.CreateTransactionScope())
            {
                _data.SetOfferDeclineMessage(input.ActingUserName, offer.ID, offer.DeclineMessage);
                _data.SetOfferStatus(input.ActingUserName, offer.ID, OfferStatuses.Declined);

                if (notify)
                {
                    if (offer.SendingUserName == offer.ListingOwnerUsername)
                    {
                        //rejecting counteroffer from seller
                        _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                            offer.SendingUserName,
                                                            Templates.CounterOfferRejected_Seller,
                                                            DetailTypes.Offer, offer.ID, null, null, null, null, null);
                    }
                    else
                    {
                        //rejecting offer from buyer
                        _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                            offer.SendingUserName,
                                                            Templates.OfferRejected_Buyer,
                                                            DetailTypes.Offer, offer.ID, null, null, null, null, null);
                    }
                }

                scope.Complete();
            }
        }

        private Offer ParseCounterOfferInputs(UserInput input, Offer originalOffer)
        {
            int offerTimeLimitHours = int.Parse(Cache.SiteProperties[SiteProperties.OfferTimeLimitHours]);

            var validation = new ValidationResults();

            var listing = originalOffer.Listing;
            if (listing == null || listing.Status != ListingStatuses.Active)
            {
                validation.AddResult(new ValidationResult(Messages.ListingNotActive, this, Fields.ListingID, Fields.ListingID, null));
            }

            bool makeOfferAvailable;
            if (!listing.Properties.Any(lp => lp.Field.Name == Fields.MakeOfferAvailable &&
                bool.TryParse(lp.Value, out makeOfferAvailable) &&
                makeOfferAvailable == true))
            {
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.MakeOfferNotAvailable);
            }

            Offer newOffer = new Offer();

            //Assignment and Validation
            newOffer.Sender = originalOffer.Receiver;
            newOffer.Receiver = originalOffer.Sender;
            newOffer.Buyer = (newOffer.Sender.UserName == listing.OwnerUserName) ? newOffer.Receiver : newOffer.Sender;
            newOffer.ListingID = listing.ID;
            newOffer.Listing = listing;
            newOffer.Status = OfferStatuses.Active;

            newOffer.Amount = ValidationHelper.ValidatePositiveMoneyValue(Fields.Amount, input, this, validation) ?? 0;
            newOffer.Quantity = ValidationHelper.ValidatePositiveInt(Fields.Quantity, input, this, validation) ?? 1;
            newOffer.OfferMessage = input.Items.ContainsKey(Fields.OfferMessage) ? input.Items[Fields.OfferMessage] : string.Empty;

            newOffer.ExpirationDTTM = DateTime.UtcNow.AddHours(offerTimeLimitHours);
            if (newOffer.ExpirationDTTM > listing.EndDTTM.Value)
            {
                newOffer.ExpirationDTTM = listing.EndDTTM.Value;
            }

            if (newOffer.Amount >= (listing.CurrentPrice ?? 0.0M))
            {
                validation.AddResult(new ValidationResult("OfferAmountMustBeLessThanPrice", this, Fields.Quantity, Fields.Quantity, null));
            }

            if (newOffer.Quantity > listing.CurrentQuantity)
            {
                validation.AddResult(new ValidationResult("QuantityTooHigh", this, Fields.Quantity, Fields.Quantity, null));
            }

            validation.AddAllResults(ValidationHelper.Validate(newOffer, "default"));

            if (!validation.IsValid)
                Statix.ThrowValidationFaultContract(validation);

            return newOffer;
        }

        public void ValidateCounterOffer(UserInput input, Offer originalOffer, out Offer newOffer)
        {
            newOffer = ParseCounterOfferInputs(input, originalOffer);
        }

        public void SendCounterOffer(UserInput input, Offer originalOffer, out Offer newOffer)
        {
            bool notify = bool.Parse(Cache.SiteProperties[SiteProperties.OfferNotificationsEnabled]);

            newOffer = ParseCounterOfferInputs(input, originalOffer);
            using (var scope = TransactionUtils.CreateTransactionScope())
            {
                _data.AddListingOffer(input.ActingUserName, newOffer);
                _data.SetOfferStatus(input.ActingUserName, originalOffer.ID, OfferStatuses.Countered);

                if (notify)
                {
                    _notifier.QueueSystemNotification(SystemActors.SystemUserName, newOffer.Listing.OwnerUserName,
                        Templates.CounterOfferConfirmation_Seller, DetailTypes.Offer, newOffer.ID,
                        null, null, null, null, null);
                    _notifier.QueueSystemNotification(SystemActors.SystemUserName, newOffer.BuyingUser,
                        Templates.CounterOfferReceived_Buyer, DetailTypes.Offer, newOffer.ID,
                        null, null, null, null, null);
                }

                scope.Complete();
            }
        }

    }
}
