﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Transactions;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.DTO.Media;
using RainWorx.FrameWorx.Queueing;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using RainWorx.FrameWorx.Services;

namespace RainWorx.FrameWorx.Providers.Listing.FixedPrice
{
    [Attributes.ListingAttribute(Strings.ListingTypes.FixedPrice)]
    public class StandardFixedPrice : ListingBase
    {
        public override string Name
        {
            get
            {
                return Strings.ListingTypes.FixedPrice;
            }
        }

        private readonly IAccountingService _accounting;
        private readonly INotifierService _notifier;
        private readonly IListingService _listing;
        private readonly ICommonService _common;
        private readonly IQueueManager _queueManager;

        private const string ValidationRuleSet = Strings.FieldDefaults.ValidationRuleSet;

        public StandardFixedPrice(IDataContext data, IAccountingService accounting, INotifierService notifier, IListingService listing, ICommonService common, IQueueManager queueManager)
            : base(data)
        {
            _accounting = accounting;
            _notifier = notifier;
            _listing = listing;
            _common = common;
            _queueManager = queueManager;
        }

        public override List<CustomProperty> ValidateListingTypeProperties(string actingUserName, UserInput input, ValidationResults validation)
        {
            List<CustomProperty> properties = base.ValidateListingTypeProperties(actingUserName, input, validation);

            foreach (CustomProperty property in properties)
            {
                //for each property
                if (validation.All(v => v.Key != property.Field.Name))
                {
                    //if there were no basic (datatype & required) validation issues...
                    switch (property.Field.Name)
                    {
                        //Listing Duration Options must be one of {StartDuration, StartEnd, Duration, End}
                        case "ListingDurationOptions":
                            if (property.Value != "StartDuration" &&
                                property.Value != "StartEnd" &&
                                property.Value != "Duration" &&
                                property.Value != "End")
                            {
                                validation.AddResult(
                                    new ValidationResult(
                                       Strings.ValidationMessages.CustomFieldValidationPrefix +
                                       string.Format(Strings.Formats.InvalidValue, null,
                                                     property.Field.Name), this, property.Field.Name,
                                       property.Field.Name, null));
                            }
                            break;
                        //Duration Days List must be a comma delimited list of integers.
                        case "DurationDaysList":
                            string[] ints = property.Value.Split(',');
                            foreach (string value in ints)
                            {
                                int tempInt;
                                if (!int.TryParse(value, out tempInt) || tempInt <= 0)
                                {
                                    //any issue is an invalid format
                                    validation.AddResult(
                                        new ValidationResult(
                                            Strings.ValidationMessages.CustomFieldValidationPrefix +
                                            string.Format(Strings.Formats.CommaListInts, null,
                                                            property.Field.Name), this, property.Field.Name,
                                            property.Field.Name, null));
                                }
                            }
                            break;
                    }
                }
            }

            return properties;
        }

        public override bool CreateListing(UserInput userInput, bool notify, out int listingID, ProviderContext context, bool validateOnly = false)
        {
            //Create the new listing here.  Input with which to create the listing is contained in the UserInput object.  Once created, this method should set the out listingID.
            //This method returns a bool indicating if the creation of the new listing caused fees to be owed.

            //This method is called after the second create listing page is submitted
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //setup default context
                if (context == null) context = new ProviderContext();

                if (!context.Has(Strings.SiteProperties.ListingDurationOptions) ||
                    !context.Has(Strings.SiteProperties.EnableGTC))
                {
                    List<CustomProperty> listingTypeProperties = _data.GetListingTypeProperties(Name, "Site");

                    if (!context.Has(Strings.SiteProperties.ListingDurationOptions))
                        context.Add(Strings.SiteProperties.ListingDurationOptions, listingTypeProperties.First(p => p.Field.Name == Strings.SiteProperties.ListingDurationOptions).Value);

                    if (!context.Has(Strings.SiteProperties.EnableGTC))
                        context.Add(Strings.SiteProperties.EnableGTC, bool.Parse(listingTypeProperties.First(p => p.Field.Name == Strings.SiteProperties.EnableGTC).Value));
                }

                if (!context.Has(Strings.SiteProperties.SiteTimeZone))
                    context.Add(Strings.SiteProperties.SiteTimeZone, TimeZoneInfo.FindSystemTimeZoneById(Cache.SiteProperties[Strings.SiteProperties.SiteTimeZone]));

                if (!context.Has(Strings.SiteProperties.MaxImagesPerItem))
                    context.Add(Strings.SiteProperties.MaxImagesPerItem, int.Parse(Cache.SiteProperties[Strings.SiteProperties.MaxImagesPerItem]));
                //Default context complete

                //create validation results object
                var validation = new ValidationResults();

                //listing import helper (admin only)
                string durOpsOverride = string.Empty;
                if (userInput.Items.ContainsKey(Strings.Fields.DurOpsOverride))
                {
                    if (_data.GetIsUserInRole(userInput.ActingUserName, Strings.Roles.Admin))
                    {
                        durOpsOverride = userInput.Items[Strings.Fields.DurOpsOverride];
                    }
                }

                //if true, required fields will only throw validation errors if they are non-blank and invalid
                //  Exceptions: Title, Category
                bool saveAsDraft = false;
                if (userInput.Items.ContainsKey(Strings.Fields.SaveAsDraft))
                    bool.TryParse(userInput.Items[Strings.Fields.SaveAsDraft], out saveAsDraft);

                int? categoryID = ValidationHelper.ValidatePositiveInt(Strings.Fields.CategoryID, userInput, this, validation);
                if (categoryID == null) validation.AddResult(new ValidationResult(Strings.Messages.CategoryIDMissing, this, Strings.Fields.CategoryID, Strings.Fields.CategoryID, null));

                var listing = new DTO.Listing();
                listing.Status = saveAsDraft ? Strings.ListingStatuses.Draft : Strings.ListingStatuses.New;
                listing.AcceptedActionCount = 0;
                listing.OwnerUserName = userInput.FBOUserName;
                listing.CurrentListingActionID = 0;
                listing.CurrentListingActionUserName = string.Empty;
                listing.OriginalPrice = saveAsDraft
                    ? ValidationHelper.ValidateOptionalMoneyValue(Strings.Fields.Price, userInput, this, validation)
                    : ValidationHelper.ValidatePositiveMoneyValue(Strings.Fields.Price, userInput, this, validation);
                listing.CurrentPrice = listing.OriginalPrice;

                int? originalQuantity = saveAsDraft
                    ? ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Quantity, userInput, this, validation)
                    : ValidationHelper.ValidatePositiveInt(Strings.Fields.Quantity, userInput, this, validation);

                if (originalQuantity.HasValue)
                {
                    listing.OriginalQuantity = originalQuantity.Value;
                    listing.CurrentQuantity = listing.OriginalQuantity;
                }

                listing.Increment = 0.0M;
                listing.Title = ValidationHelper.ValidateString(Strings.Fields.Title, userInput, this, validation);
                listing.Subtitle = userInput.Items[Strings.Fields.Subtitle];
                listing.Description = saveAsDraft
                    ? (userInput.Items.ContainsKey(Strings.Fields.Description) ? userInput.Items[Strings.Fields.Description] : string.Empty)
                    : ValidationHelper.ValidateString(Strings.Fields.Description, userInput, this, validation);
                listing.OriginalRelistCount = 0; // "original" value, used for manual relists
                listing.AutoRelistRemaining = 0;
                listing.Hits = 0;
                listing.Location = Strings.FieldDefaults.Unknown;

                //DURATION STUFF
                TimeZoneInfo siteTimeZone = context.Get<TimeZoneInfo>(Strings.SiteProperties.SiteTimeZone);
                DateTime defaultStartDTTM = TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.Utc, siteTimeZone);
                DateTime defaultStartDate = DateTime.Parse(defaultStartDTTM.ToShortDateString());
                DateTime defaultStartTime = DateTime.Parse(defaultStartDTTM.ToShortTimeString());
                DateTime defaultEndTime = defaultStartTime;
                string durationOpts = !string.IsNullOrEmpty(durOpsOverride) ? durOpsOverride
                    : context.Get<string>(Strings.SiteProperties.ListingDurationOptions);
                bool gtcOptionAvailable = context.Get<bool>(Strings.SiteProperties.EnableGTC);
                if (gtcOptionAvailable
                    && userInput.Items.ContainsKey(Strings.Fields.GoodTilCanceled)
                    && bool.Parse(userInput.Items[Strings.Fields.GoodTilCanceled]))
                {
                    if (durationOpts == Strings.DurationOptions.StartDuration || durationOpts == Strings.DurationOptions.StartEnd)
                    {
                        DateTime? startDate;
                        DateTime? startTime;
                        if (saveAsDraft)
                        {
                            startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, null);
                            startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, null);
                        }
                        else
                        {
                            startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, defaultStartDate);
                            startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, defaultStartTime);
                        }
                        if (startDate.HasValue && startTime.HasValue)
                        {
                            listing.StartDTTM = startDate;
                            listing.StartDTTM = listing.StartDTTM.Value.Add(startTime.Value.TimeOfDay);
                            listing.StartDTTM = TimeZoneInfo.ConvertTime(listing.StartDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                        }
                    }
                    else
                    {
                        listing.StartDTTM = (saveAsDraft ? null : (DateTime?)DateTime.UtcNow);
                    }
                    if (listing.StartDTTM.HasValue)
                    {
                        listing.EndDTTM = listing.StartDTTM.Value.AddYears(100);
                    }
                    if (listing.StartDTTM.HasValue && listing.EndDTTM.HasValue)
                    {
                        listing.Duration = (int)listing.EndDTTM.Value.Subtract(listing.StartDTTM.Value).TotalMinutes;
                        if (listing.Duration < 1) listing.Duration = 1;
                    }
                }
                else if (durationOpts == Strings.DurationOptions.Duration)
                {
                    listing.StartDTTM = (saveAsDraft ? null : (DateTime?)DateTime.UtcNow);
                    int? duration;
                    if (saveAsDraft)
                    {
                        duration = ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Duration, userInput, this, validation);
                    }
                    else
                    {
                        duration = ValidationHelper.ValidatePositiveInt(Strings.Fields.Duration, userInput, this, validation);
                    }
                    if (duration.HasValue)
                    {
                        listing.Duration = duration * 1440;
                    }
                    if (listing.Duration.HasValue && listing.StartDTTM.HasValue)
                    {
                        listing.EndDTTM = listing.StartDTTM.Value.AddMinutes(listing.Duration.Value);
                    }
                }
                else if (durationOpts == Strings.DurationOptions.StartDuration)
                {
                    int? duration;
                    DateTime? startDate;
                    DateTime? startTime;
                    if (saveAsDraft)
                    {
                        duration = ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Duration, userInput, this, validation);
                        startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, null);
                        startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, null);
                    }
                    else
                    {
                        duration = ValidationHelper.ValidatePositiveInt(Strings.Fields.Duration, userInput, this, validation);
                        startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, defaultStartDate);
                        startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, defaultStartTime);
                    }
                    if (startDate.HasValue && startTime.HasValue)
                    {
                        listing.StartDTTM = startDate;
                        listing.StartDTTM = listing.StartDTTM.Value.Add(startTime.Value.TimeOfDay);
                        listing.StartDTTM = TimeZoneInfo.ConvertTime(listing.StartDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                    }
                    if (duration.HasValue)
                    {
                        listing.Duration = duration * 1440;
                    }
                    if (listing.StartDTTM.HasValue && listing.Duration.HasValue)
                    {
                        listing.EndDTTM = listing.StartDTTM.Value.AddMinutes(listing.Duration.Value);
                    }
                }
                else if (durationOpts == Strings.DurationOptions.StartEnd)
                {
                    DateTime? startDate;
                    DateTime? startTime;
                    DateTime? endDate;
                    DateTime? endTime;
                    if (saveAsDraft)
                    {
                        startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, null);
                        startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, null);
                        endDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndDate, userInput, this, validation, null);
                        endTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, null);
                    }
                    else
                    {
                        startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation, defaultStartDate);
                        startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation, defaultStartTime);
                        endDate = ValidationHelper.ValidateDateTime(Strings.Fields.EndDate, userInput, this, validation);
                        endTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, defaultEndTime);
                    }
                    if (startDate.HasValue && startTime.HasValue)
                    {
                        listing.StartDTTM = startDate;
                        listing.StartDTTM = listing.StartDTTM.Value.Add(startTime.Value.TimeOfDay);
                        listing.StartDTTM = TimeZoneInfo.ConvertTime(listing.StartDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                    }
                    if (endDate.HasValue && endTime.HasValue)
                    {
                        listing.EndDTTM = endDate;
                        listing.EndDTTM = listing.EndDTTM.Value.Add(endTime.Value.TimeOfDay);
                        listing.EndDTTM = TimeZoneInfo.ConvertTime(listing.EndDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                    }
                    if (listing.StartDTTM.HasValue && listing.EndDTTM.HasValue)
                    {
                        listing.Duration = (int)listing.EndDTTM.Value.Subtract(listing.StartDTTM.Value).TotalMinutes;
                        if (listing.Duration < 1) listing.Duration = 1;
                    }
                }
                else if (durationOpts == Strings.DurationOptions.End)
                {
                    listing.StartDTTM = (saveAsDraft ? null : (DateTime?)DateTime.UtcNow);
                    DateTime? endDate;
                    DateTime? endTime;
                    if (saveAsDraft)
                    {
                        endDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndDate, userInput, this, validation, null);
                        endTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, null);
                    }
                    else
                    {
                        endDate = ValidationHelper.ValidateDateTime(Strings.Fields.EndDate, userInput, this, validation);
                        endTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, defaultEndTime);
                    }
                    if (endDate.HasValue && endTime.HasValue)
                    {
                        listing.EndDTTM = endDate;
                        listing.EndDTTM = listing.EndDTTM.Value.Add(endTime.Value.TimeOfDay);
                        listing.EndDTTM = TimeZoneInfo.ConvertTime(listing.EndDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                    }
                    if (listing.StartDTTM.HasValue && listing.EndDTTM.HasValue)
                    {
                        listing.Duration = (int)listing.EndDTTM.Value.Subtract(listing.StartDTTM.Value).TotalMinutes;
                        if (listing.Duration < 1) listing.Duration = 1;
                    }
                }
                else
                {
                    Statix.ThrowInvalidOperationFaultContract(ReasonCode.InvalidDuration);
                }

                //validate that the end date is in the future (if not being saved as a draft)
                if (!saveAsDraft && listing.EndDTTM.HasValue && listing.EndDTTM < DateTime.UtcNow)
                {
                    if (durationOpts == Strings.DurationOptions.StartEnd || durationOpts == Strings.DurationOptions.End)
                    {
                        validation.AddResult(
                            new ValidationResult(
                                Strings.Messages.EndDateTimeMustBeInTheFuture, this,
                                Strings.Fields.EndDate, Strings.Fields.EndDate, null));
                    }
                    else
                    {
                        validation.AddResult(
                            new ValidationResult(
                                Strings.Messages.EndDateTimeMustBeInTheFuture, this,
                                Strings.Fields.Duration, Strings.Fields.Duration, null));
                    }
                }

                //validate end date is after start date (if start date was able to be entered)
                if (
                        (listing.EndDTTM.HasValue && listing.StartDTTM.HasValue) &&
                        (listing.EndDTTM <= listing.StartDTTM) &&
                        (durationOpts == Strings.DurationOptions.StartEnd || durationOpts == Strings.DurationOptions.StartDuration)
                    )
                {
                    validation.AddResult(
                        new ValidationResult(
                            Strings.Messages.EndDTTMBeforeStartDTTM, this,
                            Strings.Fields.EndDate, Strings.Fields.EndDate, null));
                }

                //add currency
                listing.Currency = new Currency
                                        {
                                            Code = (userInput.Items.ContainsKey(Strings.Fields.Currency) && !string.IsNullOrEmpty(userInput.Items[Strings.Fields.Currency]))
                                                ? ValidationHelper.ValidateString(Strings.Fields.Currency, userInput, this, validation)
                                                : Cache.SiteProperties[Strings.SiteProperties.SiteCurrency]
                                        };

                //Add Categories ("CategoryID" is primary category and "AllCategories" is everything)
                if (categoryID.HasValue)
                    listing.PrimaryCategory = new Category { ID = categoryID.Value };
                string categoryIDsInput = ValidationHelper.ValidateString(Strings.Fields.AllCategories, userInput, this, validation);

                //explicit check for null... should get a validation error if AllCategories is missing
                if (categoryIDsInput != null)
                {
                    string[] categoryIDs = categoryIDsInput.Split(',');
                    listing.Categories = new List<Category>(categoryIDs.Length);
                    foreach (string catID in categoryIDs)
                        listing.Categories.Add(new Category { ID = int.Parse(catID) });
                }

                //if MakeOfferAllowed is true, set MakeOfferAvailable to true as well
                string makeOfferAllowedValue = null;
                userInput.Items.TryGetValue(Strings.Fields.MakeOfferAllowed, out makeOfferAllowedValue);
                if (!string.IsNullOrWhiteSpace(makeOfferAllowedValue))
                {
                    bool makeOfferAllowed;
                    if (bool.TryParse(makeOfferAllowedValue, out makeOfferAllowed))
                    {
                        if (userInput.Items.ContainsKey(Strings.Fields.MakeOfferAvailable))
                        {
                            userInput.Items[Strings.Fields.MakeOfferAvailable] = makeOfferAllowed.ToString();
                        }
                        else
                        {
                            userInput.Items.Add(Strings.Fields.MakeOfferAvailable, makeOfferAllowed.ToString());
                        }
                    }
                }
                if (!userInput.Items.ContainsKey(Strings.Fields.MakeOfferAllowed))
                    userInput.Items.Add(Strings.Fields.MakeOfferAllowed, "false");
                if (!userInput.Items.ContainsKey(Strings.Fields.MakeOfferAvailable))
                    userInput.Items.Add(Strings.Fields.MakeOfferAvailable, "false");

                //Set listing type
                listing.Type = new ListItem { Name = ValidationHelper.ValidateString(Strings.Fields.ListingType, userInput, this, validation) };

                //add properties
                listing.Properties = new List<CustomProperty>();
                if (categoryID.HasValue)
                    listing.Properties.AddRange(ValidationHelper.ValidateProperties(_data.GetFieldsByCategoryID(categoryID.Value), this, userInput, validation, saveAsDraft));

                //add properties for the listing type
                listing.Properties.AddRange(ValidationHelper.ValidateProperties(_listing.GetListingTypeFields(listing.Type.Name, "Listing"), this, userInput, validation, saveAsDraft));

                //Add Decorations (if any exist)
                listing.Decorations = Utilities.GetDecorations(userInput);

                //Add Locations (if any exist)
                listing.Locations = Utilities.GetLocations(userInput);

                //Decode and add media
                listing.Media = Utilities.GetMedia(userInput);

                if (base.GetListingImageCount(userInput.ActingUserName, listing.Media) > context.Get<int>(Strings.SiteProperties.MaxImagesPerItem))
                    validation.AddResult(new ValidationResult(Strings.Messages.MaxImagesExceeded, this,
                                                              Strings.Fields.Images, Strings.Fields.Images, null));

                //Decode and add shipping options
                listing.ShippingOptions = Utilities.GetShippingOptions(userInput, validation);

                //validate the custom field access (e.g. confirm an owner is not attempting to update an admin-only field)
                var customProps = listing.Properties.Where(cp => cp.Field.Group.Equals(Strings.CustomFieldGroups.Item));
                base.CheckFieldMutabilityAccess(userInput.ActingUserName, userInput.FBOUserName, customProps, validation, userInput);

                //do final fixed price object validation here
                if (saveAsDraft)
                {
                    validation.AddAllResults(ValidationHelper.ValidateListing(listing, Strings.FieldDefaults.DraftValidationRuleset));
                }
                else
                {
                    validation.AddAllResults(ValidationHelper.ValidateListing(listing, ValidationRuleSet));
                }

                if (!validation.IsValid)
                {
                    Statix.ThrowValidationFaultContract(validation);
                }

                if (!validateOnly)
                {
                    bool feesOwed = false;
                    //attempt to create the object
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        _data.AddListing(userInput.ActingUserName, userInput.FBOUserName, listing);

                        listing = _data.GetListingByIDWithFillLevel(listing.ID, Strings.ListingFillLevels.All);
                        //necessary to rematerialize a partially "filled" listing (because we replace media, locations, etc... just with IDENTITY entities for the DAL method to "smart update"
                        if (listing == null) Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);

                        //update media titles
                        foreach (Media media in listing.Media.Where(m => m.Default.MetaData.ContainsKey("Title")))
                        {
                            if (userInput.Items["media_title_" + media.GUID] != string.Empty && media.Default.MetaData["Title"] != userInput.Items["media_title_" + media.GUID])
                                _data.SetMediaMetaData(userInput.ActingUserName, media.DefaultAssetID, "Title", userInput.Items["media_title_" + media.GUID]);
                        }

                        //replace primary image URI                    
                        listing.PrimaryImageURI = GetPrimaryImageURI(listing.Media);
                        _listing.UpdateListingBasic(userInput.ActingUserName, listing);

                        //only add fees if this is not a draft
                        if (!saveAsDraft)
                        {
                            Dictionary<string, string> properties = new Dictionary<string, string>(1);
                            properties.Add(Strings.LineItemProperties.Listing, listing.Title);
                            feesOwed = _accounting.AddFees(Strings.SystemActors.SystemUserName, Strings.Events.AddListing,
                                properties, null, listing, null);
                            if (notify)
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                                  userInput.FBOUserName,
                                                                  Strings.Templates.SellerNewListing,
                                                                  Strings.DetailTypes.Listing, listing.ID, null, null, null, null, null);
                        }
                        scope.Complete();
                    }

                    listingID = listing.ID;

                    return (feesOwed);
                }

                listingID = 0;
                return false;
#if TRACE
            }
#endif
        }

        public override bool UpdateListing(string actingUserName, UserInput userInput, ProviderContext context)
        {
            //Edit the listing here.  Input with which to replace listing information is contained in the UserInput object.
            //This method returns a bool indicating if the revision of the existing listing caused fees to be owed.

            //This method is called after the edit listing page is submitted
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //retrieve all listing type-specific properties
                List<CustomProperty> listingTypeSiteProperties = _data.GetListingTypeProperties(Name, "Site");
                List<CustomProperty> listingTypeListingProperties = _data.GetListingTypeProperties(Name, "Listing");

                //setup default context
                if (context == null) context = new ProviderContext();

                if (!context.Has(Strings.SiteProperties.ListingDurationOptions) ||
                    !context.Has(Strings.SiteProperties.EnableGTC))
                {
                    if (!context.Has(Strings.SiteProperties.ListingDurationOptions))
                        context.Add(Strings.SiteProperties.ListingDurationOptions, listingTypeSiteProperties.First(p => p.Field.Name == Strings.SiteProperties.ListingDurationOptions).Value);

                    if (!context.Has(Strings.SiteProperties.EnableGTC))
                        context.Add(Strings.SiteProperties.EnableGTC, bool.Parse(listingTypeSiteProperties.First(p => p.Field.Name == Strings.SiteProperties.EnableGTC).Value));
                }

                if (!context.Has(Strings.SiteProperties.SiteTimeZone))
                    context.Add(Strings.SiteProperties.SiteTimeZone, TimeZoneInfo.FindSystemTimeZoneById(Cache.SiteProperties[Strings.SiteProperties.SiteTimeZone]));

                if (!context.Has(Strings.SiteProperties.MaxImagesPerItem))
                    context.Add(Strings.SiteProperties.MaxImagesPerItem, int.Parse(Cache.SiteProperties[Strings.SiteProperties.MaxImagesPerItem]));
                //Default context complete

                //if true, required fields will only throw validation errors if they are non-blank and invalid
                //  Exceptions: Title, Category
                bool saveAsDraft = false;
                if (userInput.Items.ContainsKey(Strings.Fields.SaveAsDraft))
                    bool.TryParse(userInput.Items[Strings.Fields.SaveAsDraft], out saveAsDraft);

                //create validation results object
                var validation = new ValidationResults();
                int? listingID = ValidationHelper.ValidatePositiveInt(Strings.Fields.ListingID, userInput, this, validation);
                if (!listingID.HasValue)
                {
                    validation.AddResult(new ValidationResult(Strings.Messages.ListingIDMissing, this, Strings.Fields.ListingID, Strings.Fields.ListingID, null));
                    Statix.ThrowValidationFaultContract(validation);
                }

                //This is a hack to basically get two independant copies of the listing (rather than doing deep member cloning, etc...)
                //at the very least, let's at least guarantee we get the same copy from both calls by wrapping in a transaction
                DTO.Listing oldListing;
                DTO.Listing newListing;
                using (TransactionScope scope = TransactionUtils.CreateTransactionScope())
                {
                    oldListing = _data.GetListingByIDWithFillLevel(listingID.Value, Strings.ListingFillLevels.All);
                    newListing = _data.GetListingByIDWithFillLevel(listingID.Value, Strings.ListingFillLevels.All);
                    scope.Complete();
                }
                if (oldListing == null || newListing == null)
                {
                    Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);
                }
                var editableFields = GetUpdateableListingFields(actingUserName, newListing);
                bool wasDraftListing = oldListing.Status == Strings.ListingStatuses.Draft;
                TimeZoneInfo siteTimeZone = context.Get<TimeZoneInfo>(Strings.SiteProperties.SiteTimeZone);
                DateTime defaultStartDTTM = TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.Utc, siteTimeZone);
                DateTime defaultStartDate = DateTime.Parse(defaultStartDTTM.ToShortDateString());
                DateTime defaultStartTime = DateTime.Parse(defaultStartDTTM.ToShortTimeString());
                DateTime defaultEndTime = defaultStartTime;

                bool regionsEnabled = bool.Parse(Cache.SiteProperties[Strings.SiteProperties.EnableRegions]);
                int? newRegionID = null;

                //only process user input for fields found in editableFields
                foreach (string key in editableFields.Keys)
                {
                    if (editableFields[key])
                    {
                        switch (key)
                        {
                            case "Title":
                                newListing.Title = ValidationHelper.ValidateString(Strings.Fields.Title, userInput, this, validation);
                                break;
                            case "Subtitle":
                                newListing.Subtitle = userInput.Items[Strings.Fields.Subtitle];
                                break;
                            case "Description":
                                newListing.Description = saveAsDraft
                                    ? (userInput.Items.ContainsKey(Strings.Fields.Description) ? userInput.Items[Strings.Fields.Description] : string.Empty)
                                    : ValidationHelper.ValidateString(Strings.Fields.Description, userInput, this, validation);
                                break;
                            case "AppendDescription":
                                newListing.Description += userInput.Items[Strings.Fields.AppendDescriptionFormatted];
                                break;
                            case "CustomFields":
                                //process non-listing-format-specific properties                                    
                                List<CustomProperty> customFieldProps = newListing.AllCustomItemProperties(_common.GetFieldsByCategoryID(newListing.PrimaryCategory.ID));
                                foreach (CustomProperty property in customFieldProps)
                                {
                                    if (userInput.Items.ContainsKey(property.Field.Name))
                                    {
                                        property.Value = ValidationHelper.ValidateProperty(property.Field.Name, userInput, this, validation,
                                                                                           (!saveAsDraft && property.Field.Required), property.Field.Type);
                                        if (!newListing.Properties.Exists(p => p.Field.Name == property.Field.Name))
                                        {
                                            newListing.Properties.Add(property);
                                        }
                                    }
                                }
                                break;
                            case "ListingFields":
                                {
                                    newListing.OriginalPrice = saveAsDraft
                                        ? ValidationHelper.ValidateOptionalMoneyValue(Strings.Fields.Price, userInput, this, validation)
                                        : ValidationHelper.ValidatePositiveMoneyValue(Strings.Fields.Price, userInput, this, validation);
                                    newListing.CurrentPrice = newListing.OriginalPrice;

                                    int? originalQuantity = saveAsDraft
                                        ? ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Quantity, userInput, this, validation)
                                        : ValidationHelper.ValidatePositiveInt(Strings.Fields.Quantity, userInput, this, validation);

                                    if (originalQuantity.HasValue)
                                    {
                                        newListing.OriginalQuantity = originalQuantity.Value;
                                        newListing.CurrentQuantity = newListing.OriginalQuantity;
                                    }

                                    //process any fixed price-specific properties (none?)
                                    var fixedPriceFieldProps = newListing.Properties.Where(p => p.Field.Group == Strings.CustomFieldGroups.ListingType);
                                    CustomProperty makeOfferAllowedProp = null;
                                    CustomProperty makeOfferAvailableProp = null;
                                    foreach (CustomProperty property in fixedPriceFieldProps)
                                    {
                                        if (userInput.Items.ContainsKey(property.Field.Name))
                                        {
                                            //property.Value = userInput.Items[property.Field.Name] == string.Empty ? null : userInput.Items[property.Field.Name];
                                            property.Value = ValidationHelper.ValidateProperty(property.Field.Name, userInput, this, validation,
                                                                                               (!saveAsDraft && property.Field.Required), property.Field.Type);
                                        }
                                        if (property.Field.Name == Strings.Fields.MakeOfferAllowed)
                                            makeOfferAllowedProp = property;
                                        else if (property.Field.Name == Strings.Fields.MakeOfferAvailable)
                                            makeOfferAvailableProp = property;
                                    }

                                    //ensure MakeOffer properties exist for this listing, if make offer is enabled, e.g. if an older listing was created before MakeOffer existed
                                    bool makeOfferEnabledForThisType = false;
                                    if (listingTypeSiteProperties.Any(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer))
                                    {
                                        bool.TryParse(listingTypeSiteProperties.First(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer).Value, out makeOfferEnabledForThisType);
                                    }
                                    if (makeOfferEnabledForThisType && userInput.Items.ContainsKey(Strings.Fields.MakeOfferAllowed))
                                    {
                                        if (makeOfferAllowedProp == null)
                                        {
                                            var makeOfferAllowedField = listingTypeListingProperties.First(p => p.Field.Name == Strings.Fields.MakeOfferAllowed).Field;
                                            string makeOfferAllowedValue = ValidationHelper.ValidateProperty(makeOfferAllowedField.Name, userInput, this, validation,
                                                                                               (!saveAsDraft && makeOfferAllowedField.Required), makeOfferAllowedField.Type);
                                            makeOfferAllowedProp = new CustomProperty(makeOfferAllowedField, makeOfferAllowedValue);
                                            newListing.Properties.Add(makeOfferAllowedProp);
                                        }
                                        if (makeOfferAvailableProp == null)
                                        {
                                            var makeOfferAvailableField = listingTypeListingProperties.First(p => p.Field.Name == Strings.Fields.MakeOfferAvailable).Field;
                                            makeOfferAvailableProp = new CustomProperty(makeOfferAvailableField, Strings.FieldDefaults.True);
                                            newListing.Properties.Add(makeOfferAvailableProp);
                                        }
                                    }

                                    //set MakeOfferAvailable to false if MakeOfferAllowed is false, or true if true
                                    if (makeOfferAllowedProp != null && makeOfferAvailableProp != null)
                                    {
                                        bool makeOfferAllowed = false;
                                        bool makeOfferAvailable = false;
                                        if (!string.IsNullOrWhiteSpace(makeOfferAllowedProp.Value))
                                        {
                                            bool.TryParse(makeOfferAllowedProp.Value, out makeOfferAllowed);
                                        }
                                        if (!string.IsNullOrWhiteSpace(makeOfferAvailableProp.Value))
                                        {
                                            bool.TryParse(makeOfferAvailableProp.Value, out makeOfferAvailable);
                                        }
                                        if (!makeOfferAllowed && makeOfferAvailable)
                                        {
                                            makeOfferAvailableProp.Value = Strings.FieldDefaults.False;
                                        }
                                        else if (makeOfferAllowed && !makeOfferAvailable)
                                        {
                                            makeOfferAvailableProp.Value = Strings.FieldDefaults.True;
                                        }
                                    }

                                }
                                break;
                            case "Images":
                                //replace media                                    
                                newListing.Media = Utilities.GetMedia(userInput);
                                if (base.GetListingImageCount(userInput.ActingUserName, newListing.Media) > context.Get<int>(Strings.SiteProperties.MaxImagesPerItem))
                                    validation.AddResult(new ValidationResult(Strings.Messages.MaxImagesExceeded,
                                                                              this, Strings.Fields.Images,
                                                                              Strings.Fields.Images, null));
                                break;
                            case "Locations":
                                //replace locations
                                newListing.Locations = Utilities.GetLocations(userInput);
                                break;
                            case "LocationsEnableOnly":
                                newListing.Locations = Utilities.GetLocations(userInput);
                                //re-add any locations that somehow slipped through (javascript disabled?) and were turned off/un-checked
                                foreach (Location location in oldListing.Locations)
                                {
                                    if (newListing.Locations.Where(l => l.ID == location.ID).Count() == 0)
                                    {
                                        newListing.Locations.Add(location);
                                    }
                                }
                                break;
                            case "Decorations":
                                //replace decorations
                                newListing.Decorations = Utilities.GetDecorations(userInput);
                                break;
                            case "DecorationsEnableOnly":
                                newListing.Decorations = Utilities.GetDecorations(userInput);
                                //re-add any decorations that somehow slipped through (javascript disabled?) and were turned off/un-checked
                                foreach (Decoration decoration in oldListing.Decorations)
                                {
                                    if (newListing.Decorations.Where(d => d.ID == decoration.ID).Count() == 0)
                                    {
                                        newListing.Decorations.Add(decoration);
                                    }
                                }
                                break;
                            case "ShippingOptions":
                                newListing.ShippingOptions = Utilities.GetShippingOptions(userInput, validation);
                                break;
                            case "Region":
                                if (regionsEnabled)
                                {
                                    newRegionID = ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.RegionID, userInput, this, validation);
                                }
                                break;

                            //not implemented
                            case "AutoRelist":
                                break;

                            //not implemented
                            case "AutoRelistDescreaseOnly":
                                break;
                            case "CurrentQuantity":
                                {
                                    int? currentQuantity = saveAsDraft
                                        ? ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Quantity, userInput, this, validation)
                                        : ValidationHelper.ValidatePositiveInt(Strings.Fields.CurrentQuantity, userInput, this, validation);

                                    if (currentQuantity.HasValue)
                                    {
                                        newListing.CurrentQuantity = currentQuantity.Value;
                                    }

                                    //process MakeOffer properties if applicable -- for fixed price listings, make offer may be toggled even after some sales have been made
                                    var fixedPriceFieldProps = newListing.Properties.Where(p => p.Field.Name == Strings.Fields.MakeOfferAllowed || p.Field.Name == Strings.Fields.MakeOfferAvailable);
                                    CustomProperty makeOfferAllowedProp = null;
                                    CustomProperty makeOfferAvailableProp = null;
                                    foreach (CustomProperty property in fixedPriceFieldProps)
                                    {
                                        if (userInput.Items.ContainsKey(property.Field.Name))
                                        {
                                            property.Value = ValidationHelper.ValidateProperty(property.Field.Name, userInput, this, validation,
                                                                                               (!saveAsDraft && property.Field.Required), property.Field.Type);
                                        }
                                        if (property.Field.Name == Strings.Fields.MakeOfferAllowed)
                                            makeOfferAllowedProp = property;
                                        else if (property.Field.Name == Strings.Fields.MakeOfferAvailable)
                                            makeOfferAvailableProp = property;
                                    }

                                    //ensure MakeOffer properties exist for this listing, if make offer is enabled, e.g. if an older listing was created before MakeOffer existed
                                    bool makeOfferEnabledForThisType = false;
                                    if (listingTypeSiteProperties.Any(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer))
                                    {
                                        bool.TryParse(listingTypeSiteProperties.First(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer).Value, out makeOfferEnabledForThisType);
                                    }
                                    if (makeOfferEnabledForThisType && userInput.Items.ContainsKey(Strings.Fields.MakeOfferAllowed))
                                    {
                                        if (makeOfferAllowedProp == null)
                                        {
                                            var makeOfferAllowedField = listingTypeListingProperties.First(p => p.Field.Name == Strings.Fields.MakeOfferAllowed).Field;
                                            string makeOfferAllowedValue = ValidationHelper.ValidateProperty(makeOfferAllowedField.Name, userInput, this, validation,
                                                                                               (!saveAsDraft && makeOfferAllowedField.Required), makeOfferAllowedField.Type);
                                            makeOfferAllowedProp = new CustomProperty(makeOfferAllowedField, makeOfferAllowedValue);
                                            newListing.Properties.Add(makeOfferAllowedProp);
                                        }
                                        if (makeOfferAvailableProp == null)
                                        {
                                            var makeOfferAvailableField = listingTypeListingProperties.First(p => p.Field.Name == Strings.Fields.MakeOfferAvailable).Field;
                                            makeOfferAvailableProp = new CustomProperty(makeOfferAvailableField, Strings.FieldDefaults.True);
                                            newListing.Properties.Add(makeOfferAvailableProp);
                                        }
                                    }

                                    //set MakeOfferAvailable to false if MakeOfferAllowed is false
                                    if (makeOfferAllowedProp != null && makeOfferAvailableProp != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(makeOfferAllowedProp.Value))
                                        {
                                            bool makeOfferAllowed;
                                            if (bool.TryParse(makeOfferAllowedProp.Value, out makeOfferAllowed))
                                            {
                                                if (!makeOfferAllowed)
                                                {
                                                    makeOfferAvailableProp.Value = Strings.FieldDefaults.False;
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            //case "StartDate": -- Start Date *must* be processed in-order, before "Duration" is processed, so it needs to be exlucded as a switch/case branch
                            case "Duration":
                                
                                //attempt to process start/date if allowed and data exists...
                                if (editableFields.ContainsKey("StartDate") && editableFields["StartDate"])
                                {
                                // START DATE
                                    DateTime? startDate = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartDate, userInput, this, validation,
                                        (saveAsDraft ? null : (DateTime?)defaultStartDate));
                                    DateTime? startTime = ValidationHelper.ValidateOptionalDateTime(Strings.Fields.StartTime, userInput, this, validation,
                                        (saveAsDraft ? null : (DateTime?)defaultStartTime));
                                    if (startDate.HasValue && startTime.HasValue)
                                    {
                                        newListing.StartDTTM = startDate;
                                        newListing.StartDTTM = newListing.StartDTTM.Value.Add(startTime.Value.TimeOfDay);
                                        newListing.StartDTTM = TimeZoneInfo.ConvertTime(newListing.StartDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                                    }
                                }

                                //ensure start date exists if we're not saving as a draft and the user input did not contain this data
                                if (!saveAsDraft && !newListing.StartDTTM.HasValue)
                                {
                                    newListing.StartDTTM = defaultStartDate;
                                    newListing.StartDTTM = newListing.StartDTTM.Value.Add(defaultStartTime.TimeOfDay);
                                    newListing.StartDTTM = TimeZoneInfo.ConvertTime(newListing.StartDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                                }

                                //DURATION STUFF
                                // note - when editing, duration and end date are optional, and start date is N/A (unless "save as draft" is true)
                                string durationOpts = context.Get<string>(Strings.SiteProperties.ListingDurationOptions);
                                bool gtcOptionAvailable = context.Get<bool>(Strings.SiteProperties.EnableGTC);                                    
                                if (gtcOptionAvailable
                                    && userInput.Items.ContainsKey(Strings.Fields.GoodTilCanceled)
                                    && bool.Parse(userInput.Items[Strings.Fields.GoodTilCanceled]))
                                {
                                //GTC
                                    if (newListing.StartDTTM.HasValue)
                                    {
                                        newListing.EndDTTM = newListing.StartDTTM.Value.AddYears(100);
                                    }
                                    if (newListing.StartDTTM.HasValue && newListing.EndDTTM.HasValue)
                                    {
                                        newListing.Duration = (int)newListing.EndDTTM.Value.Subtract(newListing.StartDTTM.Value).TotalMinutes;
                                        if (newListing.Duration < 1) newListing.Duration = 1;
                                    }
                                    if (newListing.Properties.Any(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled))
                                    {
                                        newListing.Properties.Single(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled).Value = true.ToString();
                                    }
                                }
                                else if (durationOpts == Strings.DurationOptions.Duration || durationOpts == Strings.DurationOptions.StartDuration)
                                {
                                //DURATION
                                    int? duration = (saveAsDraft || newListing.EndDTTM.HasValue)
                                        ? ValidationHelper.ValidateOptionalPositiveInt(Strings.Fields.Duration, userInput, this, validation)
                                        : ValidationHelper.ValidatePositiveInt(Strings.Fields.Duration, userInput, this, validation);
                                    if (duration.HasValue)
                                    {
                                        newListing.Duration = duration * 1440;
                                        if (newListing.StartDTTM.HasValue)
                                        {
                                            newListing.EndDTTM = newListing.StartDTTM.Value.AddMinutes(newListing.Duration.Value);
                                        }
                                        if (newListing.Properties.Any(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled))
                                        {
                                            newListing.Properties.Single(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled).Value = false.ToString();
                                        }
                                    }
                                }
                                else if (durationOpts == Strings.DurationOptions.End || durationOpts == Strings.DurationOptions.StartEnd)
                                {
                                //END DATE
                                    DateTime? endDate = (saveAsDraft || newListing.EndDTTM.HasValue)
                                        ? ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndDate, userInput, this, validation, null)
                                        : ValidationHelper.ValidateDateTime(Strings.Fields.EndDate, userInput, this, validation);
                                    DateTime? endTime = (saveAsDraft || newListing.EndDTTM.HasValue)
                                        ? ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, null)
                                        : ValidationHelper.ValidateOptionalDateTime(Strings.Fields.EndTime, userInput, this, validation, defaultEndTime);
                                    if (endDate.HasValue && endTime.HasValue)
                                    {
                                        newListing.EndDTTM = endDate;
                                        newListing.EndDTTM = newListing.EndDTTM.Value.Add(endTime.Value.TimeOfDay);
                                        newListing.EndDTTM = TimeZoneInfo.ConvertTime(newListing.EndDTTM.Value, siteTimeZone, TimeZoneInfo.Utc);
                                        if (newListing.StartDTTM.HasValue)
                                        {
                                            newListing.Duration = (int)newListing.EndDTTM.Value.Subtract(newListing.StartDTTM.Value).TotalMinutes;
                                            if (newListing.Duration < 1) newListing.Duration = 1;
                                        }
                                        if (newListing.Properties.Any(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled))
                                        {
                                            newListing.Properties.Single(lp => lp.Field.Name == Strings.Fields.GoodTilCanceled).Value = false.ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    Statix.ThrowInvalidOperationFaultContract(ReasonCode.InvalidDuration);
                                }

                                //validate that this change didn't set the end date to a value in the past
                                if ((newListing.EndDTTM.HasValue && newListing.EndDTTM < DateTime.UtcNow))
                                {
                                    if (durationOpts == Strings.DurationOptions.StartEnd || durationOpts == Strings.DurationOptions.End)
                                    {
                                        if (validation.FindAll(TagFilter.Include, Strings.Fields.EndDate).Count <= 0)
                                        {
                                            validation.AddResult(
                                                new ValidationResult(
                                                    Strings.Messages.EndDateTimeMustBeInTheFuture, this,
                                                    Strings.Fields.EndDate, Strings.Fields.EndDate, null));
                                        }
                                    }
                                    else
                                    {
                                        if (validation.FindAll(TagFilter.Include, Strings.Fields.Duration).Count <= 0)
                                        {
                                            validation.AddResult(
                                                new ValidationResult(
                                                    Strings.Messages.EndDateTimeMustBeInTheFuture, this,
                                                    Strings.Fields.Duration, Strings.Fields.Duration, null));
                                        }
                                    }
                                }

                                //validate end date is after start date (if start date was able to be entered)
                                if (
                                        (newListing.EndDTTM.HasValue && newListing.StartDTTM.HasValue) &&
                                        (newListing.EndDTTM <= newListing.StartDTTM) && 
                                        (durationOpts == Strings.DurationOptions.StartEnd || durationOpts == Strings.DurationOptions.StartDuration)
                                    )
                                {
                                    if (validation.FindAll(TagFilter.Include, Strings.Fields.EndDate).Count <= 0)
                                    {
                                        validation.AddResult(
                                            new ValidationResult(
                                                Strings.Messages.EndDTTMBeforeStartDTTM, this,
                                                Strings.Fields.EndDate, Strings.Fields.EndDate, null));
                                    }
                                }

                                break;
                        }
                    }
                }

                //validate the custom field access (e.g. confirm an owner is not attempting to update an admin-only field)
                var customProps = newListing.Properties.Where(cp => cp.Field.Group.Equals(Strings.CustomFieldGroups.Item));
                base.CheckFieldMutabilityAccess(userInput.ActingUserName, userInput.FBOUserName, customProps, validation, userInput);

                //do final fixed price object validation here
                if (saveAsDraft)
                {
                    validation.AddAllResults(ValidationHelper.ValidateListing(newListing, Strings.FieldDefaults.DraftValidationRuleset));
                }
                else
                {
                    validation.AddAllResults(ValidationHelper.ValidateListing(newListing, ValidationRuleSet));
                }

                if (!validation.IsValid) Statix.ThrowValidationFaultContract(validation);

                //attempt to update the object
                bool feesOwed = false;
                try
                {
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        if (!wasDraftListing) //only applies when editing an already-activated listing, especially because of possible oldListing null values
                        {
                            if (oldListing.EndDTTM.Value != newListing.EndDTTM.Value ||
                                oldListing.StartDTTM.Value != newListing.StartDTTM.Value ||
                                oldListing.CurrentPrice.Value != newListing.CurrentPrice.Value ||
                                oldListing.CurrentQuantity != newListing.CurrentQuantity)
                            {
                                if (oldListing.EndDTTM.Value != newListing.EndDTTM.Value)
                                {
                                    ListingDTTMChange data = new ListingDTTMChange()
                                    {
                                        ListingID = newListing.ID,
                                        DTTM = newListing.EndDTTM.Value,
                                        Epoch = Strings.ListingTimeOrigins.Ending,
                                        Source = "UPDATE_ORIGIN"
                                    };
                                    _queueManager.FireListingDTTMChange(data);
                                    _data.LogListingEndDttmChange(data.ListingID, null, data.Source, data.DTTM);
                                }

                                if (oldListing.StartDTTM.Value != newListing.StartDTTM.Value)
                                {
                                    ListingDTTMChange data = new ListingDTTMChange()
                                    {
                                        ListingID = newListing.ID,
                                        DTTM = newListing.StartDTTM.Value,
                                        Epoch = Strings.ListingTimeOrigins.Starting,
                                        Source = "UPDATE_ORIGIN"
                                    };
                                    _queueManager.FireListingDTTMChange(data);
                                }

                                if (oldListing.CurrentPrice.Value != newListing.CurrentPrice.Value || oldListing.CurrentQuantity != newListing.CurrentQuantity)
                                {
                                    ListingActionChange data = new ListingActionChange()
                                    {
                                        ListingID = newListing.ID,
                                        Price = newListing.CurrentPrice.Value,
                                        Increment = newListing.Increment.Value,
                                        NextPrice = newListing.CurrentPrice.Value + newListing.Increment.Value,
                                        Source = "UPDATE_ORIGIN",
                                        CurrencyCode = newListing.Currency.Code,
                                        UserName = userInput.FBOUserName,
                                        AcceptedActionCount = newListing.AcceptedActionCount,
                                        CurrentListingActionUserName = newListing.CurrentListingActionUserName,
                                        Quantity = newListing.CurrentQuantity
                                    };
                                    _queueManager.FireListingActionChange(data);
                                }
                            }
                        }

                        _listing.UpdateListing(userInput.ActingUserName, newListing);
                        newListing = _data.GetListingByIDWithFillLevel(listingID.Value, Strings.ListingFillLevels.All);
                        //necessary to rematerialize a partially "filled" listing (because we replace media, locations, etc... just with IDENTITY entities for the DAL method to "smart update"
                        if (newListing == null) Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);

                        if (regionsEnabled)
                        {
                            _data.UpdateListingRegion(userInput.ActingUserName, newListing.ID, newRegionID);
                        }

                        //update media titles
                        foreach (Media media in newListing.Media.Where(m => m.Default.MetaData.ContainsKey("Title")))
                        {
                            if (userInput.Items["media_title_" + media.GUID] != string.Empty && media.Default.MetaData["Title"] != userInput.Items["media_title_" + media.GUID])
                                _data.SetMediaMetaData(userInput.ActingUserName, media.DefaultAssetID, "Title", userInput.Items["media_title_" + media.GUID]);
                        }

                        //replace primary image URI (if necessary)
                        string potentialNewPrimaryImageURI = GetPrimaryImageURI(newListing.Media);
                        if (newListing.PrimaryImageURI != potentialNewPrimaryImageURI)
                        {
                            newListing.PrimaryImageURI = potentialNewPrimaryImageURI;
                            _listing.UpdateListingBasic(userInput.ActingUserName, newListing);
                        }

                        Dictionary<string, string> properties = new Dictionary<string, string>(1);
                        properties.Add(Strings.LineItemProperties.Listing, newListing.Title);
                        if (!wasDraftListing)
                        {
                            feesOwed = _accounting.AddFees(Strings.SystemActors.SystemUserName, Strings.Events.UpdateListing,
                                properties, oldListing, newListing, null);
                        }
                        else if (!saveAsDraft)
                        {
                            feesOwed = _accounting.AddFees(Strings.SystemActors.SystemUserName, Strings.Events.AddListing,
                                properties, null, newListing, null);
                        }
                        scope.Complete();
                    }
                }
                catch (InvalidOperationException ioe)
                {
                    if (LogManager.HandleException(ioe, Strings.FunctionalAreas.Listing)) Statix.ThrowInvalidOperationFaultContract(ReasonCode.ListingChangedDuringProcessing);
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Listing)) throw;
                }

                return (feesOwed);
#if TRACE
            }
#endif
        }

        public override Dictionary<string, bool> GetUpdateableListingFields(string actingUserName, DTO.Listing listing)
        {
            //Determines what aspects of the listing may be edited based on the current state of the Listing.

            //This method is called before the edit listing page is rendered (to determine what aspects can be changed)
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //a listing is considered to be open (for editing) if it's Active (or Awaiting Payment) or Pending...
                bool draft = listing.Status.Equals(Strings.ListingStatuses.Draft);
                bool open = listing.Status.Equals(Strings.ListingStatuses.Active) ||
                            listing.Status.Equals(Strings.ListingStatuses.Pending) ||
                            listing.Status.Equals(Strings.ListingStatuses.Preview) ||
                            listing.Status.Equals(Strings.ListingStatuses.AwaitingPayment);
                bool activity = listing.AcceptedActionCount > 0;
                bool seller = listing.OwnerUserName.Equals(actingUserName, StringComparison.OrdinalIgnoreCase);
                bool admin = _data.GetIsUserInRole(actingUserName, Strings.Roles.Admin);

                List<CustomProperty> listingTypeProperties = _data.GetListingTypeProperties(Name, "Site");

                Dictionary<string, bool> editableFields = new Dictionary<string, bool>();

                /*
                 * Rules for editing certain fields in specific scenarios:
                 *
                    (Daft) allow all fields to be edited

                    (Owner, closed) Edit listing not allowed
                    (Admin, closed) old: Apply rules depending on bids/no bids
                        [added 1.2] new: edit listing not allowed even for admin, see comments below

                    (Owner) Turn on Locations/Decorations (turn off disabled)
                    (Admin) Turn on/off all Locations and Decorations

                    [N / A - for v1.0] (Seller) Reduce # of auto relists
                    [N / A - for v1.0] (Admin) Change # of auto relists (up or down)

                    (Admin [bids or no bids]/Seller, no bids) Change Description, Title, Subtitle, Quantity
                    (Owner, has bids) Only append to Description, Can't change Title, Subtitle, Quantity

                    (no bids) Change Price, Reserve or BuyItNow Price, Custom Fields,
                    (has bids) Can't Change Price, Reserve or BuyItNow Price, Custom Fields,
                    
                    [added in 1.2] Can change Duration for fixed price regardless of purchase activity
                  
                    (any) Change Region, Shipping Options
                 *
                 */

                //if (!(admin || open))
                //This has been changed to not allow evenadmins to edit "closed" listings.  We've had issues in the past where admins
                //will edit a closed listing and because of our workflow, the listing goes through "AddFees" as the bottom of "update"
                //and it's status is changed to Active or AwaitingPayment depending, effectively reactivating a closed listing
                //which, then becomes immediately closed and resolved by the system, but all steps performed during a listing resolution, including
                //determining a winner, creating an invoice, etc... all happen again.  This is not intended functionality.
                //
                //Additionally, we had another issue where editing a closed fixed price listing that was totally successful (quantity 0) failed because
                //there's a quantity check for > 0...
                if (draft)
                {
                    editableFields["LotNumber"] = true;
                    editableFields["Title"] = true;
                    editableFields["Subtitle"] = true;
                    editableFields["Description"] = true;
                    editableFields["AppendDescription"] = false; // this is N/A when "Description" is enabled
                    editableFields["CustomFields"] = true;
                    editableFields["ListingFields"] = true;
                    editableFields["Images"] = true;
                    editableFields["Locations"] = true;
                    editableFields["LocationsEnableOnly"] = false; // this is N/A when "Locations" is enabled
                    editableFields["Decorations"] = true;
                    editableFields["DecorationsEnableOnly"] = false; // this is N/A when "Decorations" is enabled
                    editableFields["ShippingOptions"] = true;
                    editableFields["Region"] = true;
                    editableFields["AutoRelist"] = false; // auto relist is only for auctions, see GTC
                    editableFields["AutoRelistDescreaseOnly"] = false; // auto relist is only for auctions, see GTC
                    editableFields["Duration"] = true;
                    editableFields["StartDate"] = true;
                }
                else if (!open)
                {
                    //non-admin is not allowed to edit any fields on closed listings
                    editableFields["LotNumber"] = false;
                    editableFields["Title"] = false;
                    editableFields["Subtitle"] = false;
                    editableFields["Description"] = false;
                    editableFields["AppendDescription"] = false;
                    editableFields["CustomFields"] = false;
                    editableFields["ListingFields"] = false;
                    editableFields["Images"] = false;
                    editableFields["Locations"] = false;
                    editableFields["LocationsEnableOnly"] = false;
                    editableFields["Decorations"] = false;
                    editableFields["DecorationsEnableOnly"] = false;
                    editableFields["ShippingOptions"] = false;
                    editableFields["Region"] = false;
                    editableFields["AutoRelist"] = false;
                    editableFields["AutoRelistDescreaseOnly"] = false;
                    editableFields["Duration"] = false;
                    editableFields["StartDate"] = false;
                }
                else
                {
                    //only admin can edit title, subtitle & description if the listing has activity
                    if (admin || (seller && !activity))
                    {
                        editableFields["LotNumber"] = true;
                        editableFields["Title"] = true;
                        editableFields["Subtitle"] = true;
                        editableFields["Description"] = true;
                        editableFields["AppendDescription"] = false;
                    }
                    else
                    {
                        editableFields["LotNumber"] = false;
                        editableFields["Title"] = false;
                        editableFields["Subtitle"] = false;
                        editableFields["Description"] = false;
                        editableFields["AppendDescription"] = true;
                    }

                    if (admin)
                    {
                        editableFields["Duration"] = true;
                    }
                    else
                    { // note, for fixed price it is acceptable to change duration even if partially sold, if sellers are ever allowed to edit duration
                        editableFields["Duration"] = bool.Parse(
                            listingTypeProperties.First(
                                p => p.Field.Name == Strings.SiteProperties.AllowSellerEditDuration).Value);
                    }

                    if (listing.Status.Equals(Strings.ListingStatuses.Pending) || listing.Status.Equals(Strings.ListingStatuses.Preview))
                    {
                        if (admin)
                        {
                            editableFields["StartDate"] = true;
                        }
                        else if (seller)
                        {
                            bool.Parse(
                                listingTypeProperties.First(
                                    p => p.Field.Name == Strings.SiteProperties.AllowSellerEditStartDate).Value);
                        }
                    }
                    else
                    {
                        editableFields["StartDate"] = false;
                    }

                    //custom fields and type-specific listing fields are only editable if the listing has no activity
                    // quantity (fixed price) belongs in "title" category above?
                    if (activity)
                    {
                        editableFields["CustomFields"] = !bool.Parse(Cache.SiteProperties[Strings.SiteProperties.RestrictCustomFieldEdits]);
                        editableFields["ListingFields"] = false;
                        editableFields["CurrentQuantity"] = true;
                    }
                    else
                    {
                        editableFields["CustomFields"] = true;
                        editableFields["ListingFields"] = true;
                    }

                    if (admin)
                    {
                        //admins can always toggle listing options
                        editableFields["Locations"] = true;
                        editableFields["Decorations"] = true;
                        editableFields["AutoRelist"] = false;  // auto relist is only for auctions, see GTC
                        editableFields["LocationsEnableOnly"] = false;
                        editableFields["DecorationsEnableOnly"] = false;
                        editableFields["AutoRelistDescreaseOnly"] = false;
                    }
                    else
                    {
                        //non-admins can only enable listing options
                        editableFields["Locations"] = false;
                        editableFields["Decorations"] = false;
                        editableFields["AutoRelist"] = false;
                        editableFields["LocationsEnableOnly"] = true;
                        editableFields["DecorationsEnableOnly"] = true;
                        editableFields["AutoRelistDescreaseOnly"] = false;  // auto relist is only for auctions, see GTC
                    }

                    if (true)
                    {
                        editableFields["Images"] = true;
                        editableFields["ShippingOptions"] = true;
                        editableFields["Region"] = true;
                    }
                }

                return (editableFields);
#if TRACE
            }
#endif
        }

        public override bool RegisterSelf(string actingUserName)
        {
            //Registers the new listing provider in the database.

            //This method is called on system startup to register the provider
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing has changed yet.  set retVal = true if ANYTHING changes
                try
                {
                    retVal = EnsureListingType(actingUserName, Name) || retVal;

                    retVal = EnsureNavigationalCategory(actingUserName, Name, 3, false) || retVal;

                    #region Add Site Scoped Properties

                    //get all existing Site scoped properties for Fixed Price
                    List<CustomProperty> siteCustomProperties = _data.GetListingTypeProperties(Name, "Site");

                    //disable FixedPrice by default if events mode is enabled
                    string defaultEnabledProperty = "True";
                    if (bool.Parse(Cache.SiteProperties[Strings.SiteProperties.EnableEvents]))
                    {
                        defaultEnabledProperty = "False";
                    }

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.Enabled,
                                              CustomFieldType.Boolean, "Site", defaultEnabledProperty, 0, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties,
                                              Strings.SiteProperties.ListingDurationOptions,
                                              CustomFieldType.Enum, "Site", "StartDuration", 10, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin,
                                              "Start_And_Duration|StartDuration|true,Start_And_End|StartEnd|true,Duration_Only|Duration|true,End_Only|End|true") ||
                        retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.DurationDaysList,
                                              CustomFieldType.String, "Site", "3,5,7,10,14,30", 20, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.AllowSellerEditDuration,
                                              CustomFieldType.Boolean, "Site", "False", 25, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.AllowSellerEditStartDate,
                                              CustomFieldType.Boolean, "Site", "False", 26, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.EnableGTC,
                                              CustomFieldType.Boolean, "Site", "True", 30, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.EnableShipping,
                                              CustomFieldType.Boolean, "Site", "True", 40, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, siteCustomProperties, Strings.SiteProperties.EnableMakeOffer,
                                              CustomFieldType.Boolean, "Site", "True", 110, true, Name, "Site", CustomFieldAccess.Anonymous, CustomFieldAccess.Admin) || retVal;

                    #endregion //Add Site Scoped Properties

                    #region Add Listing Scoped Properties

                    //get all existing Listing scoped properties
                    List<CustomProperty> listingTypeCustomProperties = _data.GetListingTypeProperties(Name, "Listing");

                    retVal =
                        EnsureListingProperty(actingUserName, listingTypeCustomProperties, Strings.Fields.GoodTilCanceled,
                                              CustomFieldType.Boolean, "ListingType", "False", 0, false, Name, "Listing", CustomFieldAccess.Anonymous, CustomFieldAccess.Owner) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, listingTypeCustomProperties, Strings.Fields.MakeOfferAllowed, CustomFieldType.Boolean,
                                              "ListingType", "False", 20, true, Name, "Listing", CustomFieldAccess.Owner, CustomFieldAccess.System) || retVal;

                    retVal =
                        EnsureListingProperty(actingUserName, listingTypeCustomProperties, Strings.Fields.MakeOfferAvailable, CustomFieldType.Boolean,
                                              "ListingType", "False", 4, true, Name, "Listing", CustomFieldAccess.Owner, CustomFieldAccess.System) || retVal;

                    #endregion //Add Listing Scoped Properties

                    #region Modify Applicable Fee Providers

                    //modify applicable fee providers here by specifically enabling or disabling them.  
                    //WARNING: modifying settings here will override administrator settings each time RegisterSelf (this function) is run.

                    //For Fixed Price...                  
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(Name);

                    //when FlatFee provider exists, disable it...
                    retVal = EnsureFeeProviderEnablement(applicableFeeProviders, Name, "RainWorx.FrameWorx.Providers.Fee.Standard.FlatFee", false) || retVal;

                    //when CurrentPrice Fee Provider exists, enable it...
                    retVal = EnsureFeeProviderEnablement(applicableFeeProviders, Name, "RainWorx.FrameWorx.Providers.Fee.Standard.CurrentPrice", true) || retVal;

                    #endregion //Add Applicable Fee Providers

                    #region Modify Allowed Media Types

                    //modify allowed media types here by specifically enabling or disabling them.  
                    //WARNING: modifying settings here will override administrator settings each time RegisterSelf (this function) is run.

                    //For Fixed Price, all are implicily allowed, this is the default, nothing needs to be done.

                    #endregion //Modify Allowed Media Types
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Listing)) throw;
                }
                return retVal;
#if TRACE
            }
#endif
        }

        public override void FillListingContext(string userName, DTO.Listing listing)
        {
            //Fills the Listing's .Context with a ListingContext that represents the how the current listing relates to a given user.            

            //This method is called each time a the listing detail page is shown to fill additional information pertaining to the user who is viewing the page (if known/authenticated).
#if TRACE
            using (LogManager.Trace())
            {
#endif
                ListingContext context = new ListingContext { Watched = _data.IsWatched(userName, userName, listing.ID) };

                if (!listing.IsActionsFilled)
                {
                    //listing not properly filled...
                    LogManager.WriteLog("Listing is not properly filled to FillListingContext.  Actions are required.", "FixedPrice FillListingContext Error", Strings.FunctionalAreas.Listing, TraceEventType.Error, Name);
                    context.Status = "UNKNOWN";
                    context.Disposition = ContextDispositionType.Negative;
                    context.Parameters = new List<string>(0);
                    listing.Context = context;
                    return;
                }

                if (listing.Status.Equals(Strings.ListingStatuses.Pending) || listing.Status.Equals(Strings.ListingStatuses.Preview))
                {
                    context.Status = "LISTING_NOT_STARTED";
                    context.Disposition = ContextDispositionType.Neutral;
                    context.Parameters = new List<string>(0);
                    context.Disregard = false;
                    listing.Context = context;
                    return;
                }

                if (listing.Status.Equals(Strings.ListingStatuses.Deleted))
                {
                    context.Status = "LISTING_DELETED";
                    context.Disregard = false;
                    context.Disposition = ContextDispositionType.Negative;
                    context.Parameters = new List<string>(0);
                    listing.Context = context;
                    return;
                }

                //make offer contextual statuses
                List<CustomProperty> listingTypeSiteProperties = _data.GetListingTypeProperties(Name, "Site");
                var MOEnabledProp = listingTypeSiteProperties.FirstOrDefault(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer);
                bool makeOfferEnabled = MOEnabledProp == null ? false : bool.Parse(MOEnabledProp.Value);
                if (makeOfferEnabled)
                {
                    List<Offer> allOffers = _data.GetOffersByListing(listing.ID);
                    bool youHavePendingOffer = allOffers.Any(o => o.ReceivingUserName == userName && o.Status == Strings.OfferStatuses.Active);
                    if (youHavePendingOffer)
                    {
                        context.Status = "YOU_HAVE_PENDING_OFFER";
                        context.Disregard = false;
                        context.Disposition = ContextDispositionType.Neutral;
                        context.Parameters = new List<string>(0);
                        listing.Context = context;
                        return;
                    }
                    bool anyOfferAccepted = allOffers.Any(o => o.Status == Strings.OfferStatuses.Accepted);
                    bool yourOfferAccepted = allOffers.Any(o => o.SendingUserName == userName && o.Status == Strings.OfferStatuses.Accepted);
                    if (yourOfferAccepted)
                    {
                        context.Status = "YOUR_OFFER_ACCEPTED";
                        context.Disregard = false;
                        context.Disposition = ContextDispositionType.Positive;
                        context.Parameters = new List<string>(0);
                        listing.Context = context;
                        return;
                    }
                    else if (anyOfferAccepted && listing.Status != Strings.ListingStatuses.Active)
                    {
                        context.Status = "ENDED_WITH_OFFER_ACCEPTED";
                        context.Disregard = false;
                        context.Disposition = ContextDispositionType.Positive;
                        context.Parameters = new List<string>(0);
                        listing.Context = context;
                        return;
                    }
                    bool yourOfferDeclined = allOffers.Any(o => o.SendingUserName == userName && o.Status == Strings.OfferStatuses.Declined);
                    if (yourOfferDeclined)
                    {
                        context.Status = "YOUR_OFFER_DECLINED";
                        context.Disregard = false;
                        context.Disposition = ContextDispositionType.Negative;
                        context.Parameters = new List<string>(0);
                        listing.Context = context;
                        return;
                    }
                }

                bool theSeller = listing.OwnerUserName.Equals(userName, StringComparison.OrdinalIgnoreCase);

                if (listing.Status.Equals(Strings.ListingStatuses.AwaitingPayment))
                {
                    if (theSeller)
                    {
                        context.Status = "LISTING_AWAITING_PAYMENT";
                        context.Disposition = ContextDispositionType.Negative;
                        context.Parameters = new List<string>(0);
                        context.Disregard = false;
                        listing.Context = context;
                        return;
                    }
                    else
                    {
                        context.Status = "LISTING_NOT_STARTED";
                        context.Disposition = ContextDispositionType.Neutral;
                        context.Parameters = new List<string>(0);
                        context.Disregard = false;
                        listing.Context = context;
                        return;
                    }
                }

                bool open = listing.Status.Equals(Strings.ListingStatuses.Active);
                bool accepted =
                    listing.ListingActions.Exists(
                        la => la.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && la.Status == Strings.ListingActionStatuses.Accepted);

                if (accepted)
                {
                    context.UserListingAction =
                        listing.ListingActions.Where(
                            la => la.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && la.Status == Strings.ListingActionStatuses.Accepted).
                            OrderByDescending(la => la.ActionDTTM).FirstOrDefault();
                }
                bool activity = listing.AcceptedActionCount > 0;
                bool quantityLeft = listing.CurrentQuantity > 0;

                bool draft = listing.Status.Equals(Strings.ListingStatuses.Draft);

                if (draft)
                {
                    context.Status = "DRAFT";
                    context.Disregard = true;
                    context.Disposition = ContextDispositionType.Neutral;
                    context.Parameters = new List<string>(0);
                    listing.Context = context;
                    return;
                }

                if (!theSeller && accepted)
                {
                    context.Disposition = ContextDispositionType.Positive;
                    context.Disregard = false;
                    context.Status = "BUYER_PURCHASED";
                    context.Parameters = new List<string>(1);
                    context.Parameters.Add(listing.ListingActions.Where(
                        la => la.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && la.Status == Strings.ListingActionStatuses.Accepted).Sum(
                            la => la.Quantity).ToString(CultureInfo.InvariantCulture));
                }
                else if (!theSeller && !accepted && open && quantityLeft)
                {
                    context.Disposition = ContextDispositionType.Neutral;
                    context.Disregard = false;
                    context.Status = "BUYER_NO_ACTIVITY";
                    context.Parameters = new List<string>(0);
                }
                else if (!theSeller && !accepted && open && !quantityLeft)
                {
                    context.Disposition = ContextDispositionType.Negative;
                    context.Disregard = false;
                    context.Status = "BUYER_NO_QUANTITY_LEFT";
                    context.Parameters = new List<string>(0);
                }
                else if (!theSeller && !accepted && !open)
                {
                    context.Disposition = ContextDispositionType.Neutral;
                    context.Disregard = false;
                    context.Status = "BUYER_NO_ACTIVITY";
                    context.Parameters = new List<string>(0);
                }
                else if (theSeller && activity && open)
                {
                    context.Disposition = ContextDispositionType.Positive;
                    context.Disregard = false;
                    context.Status = "SELLER_ACTIVITY";
                    context.Parameters = new List<string>(0);
                }
                else if (theSeller && !activity && open)
                {
                    context.Disposition = ContextDispositionType.Neutral;
                    context.Disregard = false;
                    context.Status = "SELLER_NO_ACTIVITY";
                    context.Parameters = new List<string>(0);
                }
                else if (theSeller && activity && !open)
                {
                    context.Disposition = ContextDispositionType.Positive;
                    context.Disregard = false;
                    context.Status = "SELLER_SUCCESSFUL";
                    context.Parameters = new List<string>(0);
                }
                else if (theSeller && !activity && !open)
                {
                    context.Disposition = ContextDispositionType.Negative;
                    context.Disregard = false;
                    context.Status = "SELLER_UNSUCCESSFUL";
                    context.Parameters = new List<string>(0);
                }

                listing.Context = context;
                return;
#if TRACE
            }
#endif
        }

    }
}
