﻿using System;
using System.Transactions;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Queueing;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Utility;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Listing.FixedPrice
{
    [Attributes.ListingAttribute(Strings.ListingTypes.FixedPrice)]
    public class StandardFixedPriceCloser : IListingResolver
    {
        private readonly INotifierService _notifier;
        private readonly IListingService _listing;
        private readonly IDataContext _data;
        private readonly IQueueManager _queueManager;

        public StandardFixedPriceCloser(INotifierService notifier, IListingService listing, IDataContext data, IQueueManager queueManager)
        {
            _notifier = notifier;
            _listing = listing;
            _data = data;
            _queueManager = queueManager;
        }

        public void ResolveListing(string actingUserName, DTO.Listing listing)
        {
            //Resolve the closing of the listing here.

            //This method is called by the scheduler when a listing's End Date/Time has expired.
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    //  if there are listing actions, we'll consider the listing a success
                    if (listing.CurrentListingAction != null)
                    {
                        using (var scope = TransactionUtils.CreateTransactionScope())
                        {
                            //In the context of a fixed price listing, a final sale fee is charged for each purchase, not when the listing expires
                            //
                            ////Add Fees for EndListing event
                            //var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, listing.Title } };
                            //_accounting.AddFees(actingUserName, Strings.Events.EndListingSuccess, properties,
                            //                    null, listing);

                            //Mark Listing "Successful"
                            listing.Status = Strings.ListingStatuses.Successful;
                            _data.UpdateListingBasic(actingUserName, listing);
                            var queueManager = UnityResolver.Get<IQueueManager>();
                            ListingStatusChange statusUpdate = new ListingStatusChange()
                            {
                                ListingID = listing.ID,
                                Status = listing.Status,
                                Source = "RESOLVE_ORIGIN"
                            };
                            queueManager.FireListingStatusChange(statusUpdate);
                            scope.Complete();
                        }
                    }
                    else
                    {
                        using (var scope = TransactionUtils.CreateTransactionScope())
                        {
                            //In the context of a fixed price listing, a final sale fee is charged for each purchase, not when the listing expires
                            //
                            ////Add Fees for EndListing event
                            //var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, listing.Title } };
                            //_accounting.AddFees(actingUserName, Strings.Events.EndListingFailure, properties,
                            //                    null, listing);

                            //otherwise Listing was unsuccessful, Mark Listing "Unsuccessful"
                            listing.Status = Strings.ListingStatuses.Unsuccessful;
                            _data.UpdateListingBasic(actingUserName, listing);

                            //send failure notifications
                            _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                              listing.OwnerUserName,
                                                              Strings.Templates.SellerNoSale,
                                                              Strings.DetailTypes.Listing,
                                                              listing.ID, null, null, null, null, null);
                            ListingStatusChange statusUpdate = new ListingStatusChange()
                            {
                                ListingID = listing.ID,
                                Status = listing.Status,
                                Source = "RESOLVE_ORIGIN"
                            };
                            _queueManager.FireListingStatusChange(statusUpdate);
                            scope.Complete();
                        }
                    }

                    //expire any offers that are still active
                    bool notify = bool.Parse(Cache.SiteProperties[Strings.SiteProperties.OfferNotificationsEnabled]);
                    foreach (var offer in _data.GetOffersByListing(listing.ID).Where(o => o.Status == Strings.OfferStatuses.Active))
                    {
                        _data.SetOfferStatus(actingUserName, offer.ID, Strings.OfferStatuses.Expired);

                        if (notify)
                        {
                            if (offer.SendingUserName == offer.ListingOwnerUsername)
                            {
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName, offer.SendingUserName,
                                    Strings.Templates.CounterOfferRejected_Seller, Strings.DetailTypes.Offer, offer.ID,
                                    null, null, null, null, null);
                            }
                            else
                            {
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName, offer.SendingUserName,
                                    Strings.Templates.OfferRejected_Buyer, Strings.DetailTypes.Offer, offer.ID,
                                    null, null, null, null, null);
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Listing)) throw;
                }
#if TRACE
            }
#endif
        }
    }
}
