﻿using System;
using System.Collections.Generic;
using System.Transactions;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Queueing;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Listing.Classified
{
    [Attributes.ListingAttribute(Strings.ListingTypes.Classified)]
    public class ClassifiedAdCloser : IListingResolver
    {
        private readonly IAccountingService _accounting;
        private readonly IListingService _listing;
        private readonly IDataContext _data;
        private readonly IQueueManager _queueManager;

        public ClassifiedAdCloser(IAccountingService accounting, IListingService listing, IDataContext data, IQueueManager queueManager)
        {
            _accounting = accounting;
            _listing = listing;
            _data = data;
            _queueManager = queueManager;
        }

        public void ResolveListing(string actingUserName, DTO.Listing listing)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        //Add Fees for EndListing event
                        var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, listing.Title } };
                        _accounting.AddFees(Strings.SystemActors.SystemUserName, Strings.Events.EndListing, properties,
                                            null, listing, null);

                        //Mark Listing "Ended"
                        listing.Status = Strings.ListingStatuses.Ended;
                        _data.UpdateListingBasic(actingUserName, listing);
                        ListingStatusChange statusUpdate = new ListingStatusChange()
                        {
                            ListingID = listing.ID,
                            Status = listing.Status,
                            Source = "RESOLVE_ORIGIN"
                        };
                        _queueManager.FireListingStatusChange(statusUpdate);
                        scope.Complete();
                    }
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Listing)) throw;
                }
#if TRACE
            }
#endif
        }
    }
}
