﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Strings;
using RainWorx.FrameWorx.Utility;
using RainWorx.FrameWorx.Unity;
using System.Collections.Generic;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Listing.Classified
{
    [Attributes.ListingAttribute(Strings.ListingTypes.Classified)]
    public class ClassifiedAdAction : IListingAction
    {
        private readonly IDataContext _data;
        private readonly INotifierService _notifier;
        //private readonly IAccountingService _accounting;
        private readonly IUserService _user;

        public ClassifiedAdAction(IDataContext data, IUserService user, INotifierService notifier)
        {
            _data = data;
            _notifier = notifier;
            _user = user;
        }

        public bool SubmitListingAction(ListingAction action, out bool accepted, out ReasonCode reasonCode, out LineItem newPurchaseLineitem, DTO.Listing currentListing, bool notify, ProviderContext context)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                if (currentListing.EndDTTM.Value < action.ActionDTTM)
                {
                    //if listing not active...                    
                    reasonCode = ReasonCode.ListingNotActive;
                    newPurchaseLineitem = null;
                    accepted = false;
                    return false;
                }
                else if (action.UserName.Equals(currentListing.OwnerUserName))
                {
                    reasonCode = ReasonCode.ListerPerformingAction;
                    accepted = false;
                    newPurchaseLineitem = null;
                    return false; //no fees owed
                }
                else
                {
                    reasonCode = ReasonCode.Success;
                    accepted = true;
                    newPurchaseLineitem = null;

                    string messageSubject = action.Properties.Single(p => p.Field.Name == Strings.Fields.MessageSubject).Value;
                    string messageBody = action.Properties.Single(p => p.Field.Name == Strings.Fields.MessageBody).Value;

                    _user.SendUserMessage(action.UserName, action.UserName, currentListing.OwnerUserName, 
                        messageSubject, messageBody, action.ListingID);

                    _notifier.QueueNotification(action.UserName, action.UserName, currentListing.OwnerUserName, "ask_listing_question",
                        DetailTypes.Listing, action.ListingID, messageBody, null, null, null, null);

                    currentListing.AcceptedActionCount++;
                    _data.UpdateListingBasic(action.UserName, currentListing);
                    return false; //no fees owed
                }
#if TRACE
            }
#endif
        }

        public void ConvertUserInputToListingAction(UserInput userInput, ListingAction action)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                var validation = new ValidationResults();
                int? listingID = ValidationHelper.ValidatePositiveInt(Strings.Fields.ListingID, userInput, this, validation);
                if (listingID == null) validation.AddResult(new ValidationResult(Strings.Messages.ListingIDMissing, this, Strings.Fields.ListingID, Strings.Fields.ListingID, null));

                string messageSubject = ValidationHelper.ValidateString(Strings.Fields.MessageSubject, userInput, this, validation);
                string messageBody = ValidationHelper.ValidateString(Strings.Fields.MessageBody, userInput, this, validation);

                DTO.Listing currentListing = _data.GetListingByIDWithFillLevel(listingID.Value, Strings.ListingFillLevels.None);
                if (userInput.FBOUserName.Equals(currentListing.OwnerUserName))
                {
                    validation.AddResult(new ValidationResult(Strings.Messages.CantSendMessageToSelf, this, Strings.Fields.UserName, Strings.Fields.UserName, null));                                                
                }

                if (!validation.IsValid)
                    Statix.ThrowValidationFaultContract(validation);

                action.ActionDTTM = DateTime.UtcNow;
                action.Description = Strings.FieldDefaults.ClassifiedListingActionDescripton;

                action.UserName = userInput.FBOUserName;
                action.User = _data.GetUserByUserName(userInput.FBOUserName);
                action.UserEntered = true;

                action.ListingID = listingID.Value;

                action.Properties = new List<CustomProperty>(2);
                action.Properties.Add(new CustomProperty() { 
                    Field = new CustomField() { Name = Strings.Fields.MessageSubject, Type = CustomFieldType.String }, 
                    Value = messageSubject });
                action.Properties.Add(new CustomProperty() { 
                    Field = new CustomField() { Name = Strings.Fields.MessageBody, Type = CustomFieldType.String }, 
                    Value = messageBody });

#if TRACE
            }
#endif
        }
    }
}
