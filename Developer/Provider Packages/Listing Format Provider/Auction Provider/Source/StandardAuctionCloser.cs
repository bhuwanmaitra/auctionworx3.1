﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Transactions;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Queueing;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Services;
using System.Linq;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Listing.Auction
{
    [Attributes.ListingAttribute(Strings.ListingTypes.Auction)]
    public class StandardAuctionCloser : IListingResolver
    {
        private readonly IAccountingService _accounting;
        private readonly INotifierService _notifier;
        private readonly IListingService _listing;
        private readonly IDataContext _data;
        private readonly IQueueManager _queueManager;

        public StandardAuctionCloser(IAccountingService accounting, INotifierService notifier, IListingService listing, IDataContext data, IQueueManager queueManager)
        {
            _accounting = accounting;
            _notifier = notifier;
            _listing = listing;
            _data = data;
            _queueManager = queueManager;
        }

        public void ResolveListing(string actingUserName, DTO.Listing listing)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    //Get Reserve Price (or null)
                    CustomProperty reservePrice =
                        listing.Properties.SingleOrDefault(p => p.Field.Name == Strings.Fields.ReservePrice);

                    const string Name = Strings.ListingTypes.Auction;
                    List<CustomProperty> listingTypeSiteProperties = _data.GetListingTypeProperties(Name, "Site");
                    var MOEnabledProp = listingTypeSiteProperties.FirstOrDefault(p => p.Field.Name == Strings.SiteProperties.EnableMakeOffer);
                    bool makeOfferEnabled = MOEnabledProp == null ? false : bool.Parse(MOEnabledProp.Value);
                    List<Offer> allOffers = null;
                    Offer acceptedOffer = null;
                    if (makeOfferEnabled)
                    {
                        allOffers = _data.GetOffersByListing(listing.ID);

                        acceptedOffer = allOffers.FirstOrDefault(o => o.Status == Strings.OfferStatuses.Accepted);
                    }

                    //  there are listing actions and either there's no reserve, or the current listing action's amount is greater than the reserve
                    //      ...or there is an accepted offer
                    if (
                            acceptedOffer != null || 
                            (
                                listing.CurrentListingAction != null && 
                                listing.CurrentListingAction.Amount.HasValue &&
                                (
                                    reservePrice == null || 
                                    string.IsNullOrEmpty(reservePrice.Value) ||
                                    listing.CurrentListingAction.Amount.Value >= decimal.Parse(reservePrice.Value)
                                )
                            )
                        )
                    {
                        using (var scope = TransactionUtils.CreateTransactionScope())
                        {
                            if (acceptedOffer != null)
                            {
                                listing.CurrentPrice = acceptedOffer.Amount;
                            }

                            //Add Fees for EndListing event
                            var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, listing.Title } };
                            _accounting.AddFees(Strings.SystemActors.SystemUserName,
                                                Strings.Events.EndListingSuccess, properties,
                                                null, listing, null);

                            bool lineItemExists = acceptedOffer != null || bool.Parse(listing.Properties.Single(p => p.Field.Name == Strings.Fields.BuyItNowUsed).Value);

                            LineItem newLineItem = null;
                            if (!lineItemExists)
                            {
                                //Create LineItem for ended Listing
                                var lineItem = new LineItem
                                                   {
                                                       PerUnitAmount = listing.CurrentListingAction.Amount.Value,
                                                       Quantity = listing.CurrentListingAction.Quantity,
                                                       TotalAmount =
                                                           listing.CurrentListingAction.Amount.Value *
                                                           listing.CurrentListingAction.Quantity,
                                                       Currency = listing.Currency.Code,
                                                       DateStamp = DateTime.UtcNow,
                                                       Description = listing.Title,
                                                       Owner = listing.Owner,
                                                       Payer = listing.CurrentListingAction.User,
                                                       ListingID = listing.ID,
                                                       Type = Strings.LineItemTypes.Listing,
                                                       Taxable = listing.Lot != null ? listing.Lot.Event.LotsTaxable : true,
                                                       BuyersPremiumApplies = true,
                                                       AuctionEventId = listing.Lot != null ? (int?)listing.Lot.Event.ID : null,
                                                       LotNumber = listing.Lot != null ? listing.Lot.LotNumber : null
                                                   };
                                //create line item properties for any custom listing fields with IncludeOnInvoice or IncludeInSalesReport set
                                List<CustomProperty> propsForInvoice = listing.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport).ToList();
                                if (listing.Lot != null)
                                {
                                    propsForInvoice.AddRange(listing.Lot.Event.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport));
                                }
                                lineItem.Properties = new Dictionary<string, string>(propsForInvoice.Count);
                                foreach (CustomProperty prop in propsForInvoice)
                                {
                                    string val = string.Empty;
                                    CultureInfo sellersCultureInfo = CultureInfo.GetCultureInfo(listing.Owner.Culture);
                                    if (!string.IsNullOrEmpty(prop.Value))
                                    {
                                        switch (prop.Field.Type)
                                        {
                                            case CustomFieldType.Decimal:
                                                if (prop.Value.ToString().TrimEnd('0') != "")
                                                {
                                                    int count = BitConverter.GetBytes(decimal.GetBits(decimal.Parse(prop.Value.ToString().TrimEnd('0')))[3])[2];
                                                    val = decimal.Parse(prop.Value).ToString("N" + count, sellersCultureInfo);
                                                }
                                                else
                                                {
                                                    val = decimal.Parse(prop.Value).ToString("N0", sellersCultureInfo);
                                                }
                                                break;
                                            case CustomFieldType.Int:
                                                val = int.Parse(prop.Value).ToString(sellersCultureInfo);
                                                break;
                                            case CustomFieldType.DateTime:
                                                val = DateTime.Parse(prop.Value).ToString(sellersCultureInfo);
                                                break;
                                            case CustomFieldType.Boolean:
                                                val = Boolean.Parse(prop.Value) ? "Yes" : "No";
                                                break;
                                            default:
                                                val = prop.Value;
                                                break;
                                        }
                                        lineItem.Properties.Add(prop.Field.Name, val);
                                    }
                                }

                                newLineItem = _accounting.CreateNewLineItem(Strings.SystemActors.SystemUserName, lineItem);

                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName, listing.OwnerUserName,
                                                                  Strings.Templates.SellerSold,
                                                                  Strings.DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);

                                if (listing.CurrentListingAction != null)
                                {
                                    _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                                      listing.CurrentListingAction.UserName,
                                                                      Strings.Templates.BuyerBought,
                                                                      Strings.DetailTypes.LineItem, newLineItem.ID, null, null, null, null, null);
                                }
                            }

                            //Mark Listing "Successful"              
                            listing.Status = Strings.ListingStatuses.Successful;

                            _data.UpdateListingBasic(actingUserName, listing);
                            ListingStatusChange statusUpdate = new ListingStatusChange()
                            {
                                ListingID = listing.ID,
                                Status = listing.Status,
                                Source = "RESOLVE_ORIGIN",
                            };
                            if (newLineItem != null)
                            {
                                statusUpdate.LineItemID = newLineItem.ID;
                                statusUpdate.LineItemPayerUN = newLineItem.Payer.UserName; //listing.CurrentListingAction.User.UserName; // newLineItem.Payer.UserName;
                            }
                            _queueManager.FireListingStatusChange(statusUpdate);
                            scope.Complete();
                        }
                    }
                    else
                    {
                        //either there are no listing actions or the reserve was not met
                        using (var scope = TransactionUtils.CreateTransactionScope())
                        {
                            //check if an auto relist is indicated
                            bool autoRelistNeeded = (listing.AutoRelistRemaining > 0);

                            var properties = new Dictionary<string, string>(1) { { Strings.LineItemProperties.Listing, listing.Title } };
                            _accounting.AddFees(Strings.SystemActors.SystemUserName,
                                                Strings.Events.EndListingFailure, properties,
                                                null, listing, null);

                            //otherwise Listing was unsuccessful, Mark Listing "Unsuccessful"
                            listing.Status = Strings.ListingStatuses.Unsuccessful;
                            if (autoRelistNeeded)
                            {
                                listing.AutoRelistRemaining--;
                            }
                            _data.UpdateListingBasic(actingUserName, listing);

                            //send failure notifications
                            _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                              listing.OwnerUserName,
                                                              Strings.Templates.SellerNoSale,
                                                              Strings.DetailTypes.Listing,
                                                              listing.ID, null, null, null, null, null);

                            if (autoRelistNeeded)
                            {
                                _listing.RelistNoGet(Strings.SystemActors.SystemUserName, listing);

                                //send listing created notification
                                _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName,
                                                                  listing.OwnerUserName,
                                                                  Strings.Templates.SellerNewListing,
                                                                  Strings.DetailTypes.Listing,
                                                                  listing.ID, null, null, null, null, null);
                            }

                            ListingStatusChange statusUpdate = new ListingStatusChange()
                            {
                                ListingID = listing.ID,
                                Status = listing.Status,
                                Source = "RESOLVE_ORIGIN"
                            };
                            _queueManager.FireListingStatusChange(statusUpdate);
                            scope.Complete();
                        }
                    }

                    if (makeOfferEnabled)
                    {
                        //expire any offers that are still active
                        bool notify = bool.Parse(Cache.SiteProperties[Strings.SiteProperties.OfferNotificationsEnabled]);
                        foreach (var offer in allOffers.Where(o => o.Status == Strings.OfferStatuses.Active))
                        {
                            _data.SetOfferStatus(actingUserName, offer.ID, Strings.OfferStatuses.Expired);

                            if (notify)
                            {
                                if (offer.SendingUserName == offer.ListingOwnerUsername)
                                {
                                    _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName, offer.SendingUserName,
                                        Strings.Templates.CounterOfferRejected_Seller, Strings.DetailTypes.Offer, offer.ID,
                                        null, null, null, null, null);
                                }
                                else
                                {
                                    _notifier.QueueSystemNotification(Strings.SystemActors.SystemUserName, offer.SendingUserName,
                                        Strings.Templates.OfferRejected_Buyer, Strings.DetailTypes.Offer, offer.ID,
                                        null, null, null, null, null);
                                }
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Listing)) throw;
                }
#if TRACE
            }
#endif
        }
    }
}
