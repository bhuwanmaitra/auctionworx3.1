﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Transactions;
using System.Xml.Serialization;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using RainWorx.FrameWorx.DTO.EventArgs;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Strings;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using System.Linq;
using RainWorx.FrameWorx.Queueing;

namespace RainWorx.FrameWorx.Providers.Listing.Auction
{
    [Attributes.ListingAction(ListingTypes.Auction)]
    public class BlindBid : IListingAction
    {
        private readonly IDataContext _data;
        private readonly IListingService _listing;
        private readonly IAccountingService _accounting;
        private readonly INotifierService _notifier;
        private readonly IQueueManager _queueManager;

        public BlindBid(IDataContext data, IListingService listing, IAccountingService accounting, INotifierService notifier, IQueueManager queueManager)
        {
            _data = data;
            _listing = listing;
            _accounting = accounting;
            _notifier = notifier;
            _queueManager = queueManager;
        }

        public void ConvertUserInputToListingAction(UserInput userInput, ListingAction action)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                var validation = new ValidationResults();

                //Assignment and Validation
                decimal? amount = ValidationHelper.ValidatePositiveMoneyValue(Fields.BidAmount, userInput, this, validation);

                int? listingID = ValidationHelper.ValidatePositiveInt(Fields.ListingID, userInput, this, validation);
                //DTO.Listing currentListing = null;
                if (listingID == null)
                {
                    validation.AddResult(new ValidationResult(Messages.ListingIDMissing, this, Fields.ListingID, Fields.ListingID, null));
                }
                //else
                //{
                //    if (amount.HasValue)
                //    {
                //        currentListing = _data.GetListingByID(listingID.Value);
                //        if (currentListing != null)
                //        {
                //            if ((currentListing.CurrentListingAction != null && amount.Value < (currentListing.CurrentPrice + currentListing.Increment)) ||
                //                (currentListing.CurrentListingAction == null && amount.Value < currentListing.CurrentPrice))
                //            {
                //                validation.AddResult(new ValidationResult(Messages.BidAmountTooLow, this,
                //                                                          Fields.BidAmount,
                //                                                          Fields.BidAmount, null));
                //            }
                //        }
                //    }
                //}

                action.ActionDTTM = DateTime.UtcNow;
                action.Description = FieldDefaults.AuctionListingActionDescripton;
                action.Amount = amount;
                action.ProxyAmount = amount;
                action.Quantity = 1;
                action.UserName = userInput.FBOUserName;
                action.User = _data.GetUserByUserName(userInput.FBOUserName);
                action.UserEntered = true;

                //changed in v1.3 - the listing action "BuyItNow" input key/property is required for disambiguation from a proxy bid greater than the buy now price
                var allAuctionSettings = _listing.GetListingTypeProperties(ListingTypes.Auction, "Listing");
                CustomField buyItNowField =
                    allAuctionSettings.Single(cf => cf.Field.Name.Equals("BuyItNow")).Field;
                bool buyItNowRequested =
                    (ValidationHelper.ValidateOptionalBool(Fields.BuyItNow, userInput, this, validation) ??
                     false);
                action.Properties = new List<CustomProperty>
                                    {
                                        new CustomProperty()
                                            {
                                                Field = buyItNowField,
                                                Value = buyItNowRequested.ToString()
                                            }
                                    };
                //if (buyItNowRequested && currentListing != null)
                //{
                //    //buy it now was requested, validate that it is actually available
                //    CustomProperty fixedPrice =
                //        currentListing.Properties.SingleOrDefault(p => p.Field.Name == Fields.FixedPrice);
                //    decimal buyItNowPrice = 0.0M;
                //    bool buyItNowAvailable = false;
                //    if (fixedPrice != null && !string.IsNullOrEmpty(fixedPrice.Value))
                //    {
                //        buyItNowPrice = decimal.Parse(fixedPrice.Value);

                //        buyItNowAvailable =
                //            bool.Parse(
                //                currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNow).Value);
                //    }
                //    if (!buyItNowAvailable)
                //    {
                //        validation.AddResult(new ValidationResult(Messages.BuyNowNotAvailable, this,
                //                                                  Fields.BuyItNow,
                //                                                  Fields.BuyItNow, null));
                //    }
                //    else if ((action.Amount ?? 0.0M) < buyItNowPrice)
                //    {
                //        validation.AddResult(new ValidationResult(Messages.BidAmountTooLow, this,
                //                                              Fields.BuyItNow,
                //                                              Fields.BuyItNow, null));
                //    }
                //}
                if (listingID != null) action.ListingID = listingID.Value;

                if (!validation.IsValid) Statix.ThrowValidationFaultContract(validation);
#if TRACE
            }
#endif
        }

        public bool SubmitListingAction(ListingAction action, out bool accepted, out ReasonCode reasonCode, out LineItem newPurchaseLineitem, DTO.Listing currentListing, bool notify, ProviderContext context)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //get auction we're bidding on if not provided           
                if (currentListing == null)
                {
                    currentListing = _data.GetListingByIDWithFillLevel(action.ListingID, ListingFillLevels.All);
                    if (currentListing == null) Statix.ThrowInvalidArgumentFaultContract(ReasonCode.ListingNotExist);
                }

                //setup default context
                if (context == null) context = new ProviderContext();

                if (!context.Has(SiteProperties.EnableProxyBidding) ||
                    !context.Has(SiteProperties.ProxyBiddingBelowReserve) ||
                    !context.Has(SiteProperties.DisableBuyNowAfterWinningBids) ||
                    !context.Has(SiteProperties.SniperProtectionSeconds))
                {
                    List<CustomProperty> listingTypeProperties = _data.GetListingTypeProperties(currentListing.Type.Name, "Site");

                    if (!context.Has(SiteProperties.EnableProxyBidding))
                        context.Add(SiteProperties.EnableProxyBidding, bool.Parse(listingTypeProperties.Single(ltp => ltp.Field.Name.Equals(SiteProperties.EnableProxyBidding)).Value));

                    if (!context.Has(SiteProperties.ProxyBiddingBelowReserve))
                        context.Add(SiteProperties.ProxyBiddingBelowReserve, bool.Parse(listingTypeProperties.Single(ltp => ltp.Field.Name.Equals(SiteProperties.ProxyBiddingBelowReserve)).Value));

                    if (!context.Has(SiteProperties.DisableBuyNowAfterWinningBids))
                        context.Add(SiteProperties.DisableBuyNowAfterWinningBids, bool.Parse(listingTypeProperties.Single(ltp => ltp.Field.Name.Equals(SiteProperties.DisableBuyNowAfterWinningBids)).Value));

                    if (!context.Has(SiteProperties.SniperProtectionSeconds))
                        context.Add(SiteProperties.SniperProtectionSeconds, int.Parse(listingTypeProperties.Single(ltp => ltp.Field.Name.Equals(SiteProperties.SniperProtectionSeconds)).Value));
                }

                if (!context.Has(Fields.Increments))
                    context.Add(Fields.Increments, Cache.Increments(ListingTypes.Auction));

                bool proxyEnabled = context.Get<bool>(SiteProperties.EnableProxyBidding);
                bool proxyBiddingBelowReserve = context.Get<bool>(SiteProperties.ProxyBiddingBelowReserve);
                bool disableBuyNowAfterWinningBid = context.Get<bool>(SiteProperties.DisableBuyNowAfterWinningBids);
                int sniperSeconds = context.Get<int>(SiteProperties.SniperProtectionSeconds);
                //default context complete

                bool reserveMet = false;
                bool reserveDefined = false;
                decimal reservePriceAmount = 0.0M;
                bool? buyItNow = null;
                bool? buyItNowUsed = null;
                DateTime? endDTTM = null;
                string status = null;

                bool makeOfferStatus = false;

                reasonCode = ReasonCode.Failure;
                accepted = false;
                bool overrideHappensFirst = true;
                bool boughtItNow = false;

                newPurchaseLineitem = null;
                action.Reason = string.Empty;

                //To store the new proxy bid if a proxy beats *this bid.
                ListingAction overridingListingAction = null;

                decimal currentAmount = currentListing.CurrentPrice.Value;

                ListingAction previousWinningBid = currentListing.CurrentListingAction;

                //do the workflow processing
                if (currentListing.EndDTTM.Value < action.ActionDTTM)
                {
                    //if listing not active...
                    action.Status = ListingActionStatuses.Rejected;
                    action.Reason = Messages.ListingNotActive;
                    reasonCode = ReasonCode.ListingNotActive;
                    accepted = false;
                }
                else if (currentListing.Owner.ID == action.User.ID)
                {
                    //If bidder is owner, prepare to reject Bid
                    action.Status = ListingActionStatuses.Rejected;
                    action.Reason = Messages.BidderIsOwner;
                    reasonCode = ReasonCode.ListerPerformingAction;
                    accepted = false;
                }
                else if ((currentListing.CurrentListingAction == null && action.Amount >= currentListing.CurrentPrice) ||
                          (action.Amount >= currentListing.CurrentPrice + currentListing.Increment))
                {
                    //the bid is accepted (so far)

                    //1.) Handle a Buy It Now
                    CustomProperty fixedPrice =
                        currentListing.Properties.SingleOrDefault(p => p.Field.Name == Fields.FixedPrice);
                    decimal buyItNowPrice = 0.0M;
                    bool buyItNowAvailable = false;
                    if (fixedPrice != null && !string.IsNullOrEmpty(fixedPrice.Value))
                    {
                        buyItNowPrice = decimal.Parse(fixedPrice.Value);

                        buyItNowAvailable =
                            bool.Parse(
                                currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNow).Value);
                    }
                    bool makeOfferAvailable = false;
                    if (currentListing.Properties.Any(p => p.Field.Name == Fields.MakeOfferAvailable && !string.IsNullOrEmpty(p.Value)))
                    {
                        if (currentListing.Properties.Any(p => p.Field.Name == Fields.MakeOfferAvailable))
                        {
                            makeOfferAvailable = bool.Parse(currentListing.Properties.Single(p => p.Field.Name == Fields.MakeOfferAvailable).Value);
                        }
                    }
                    makeOfferStatus = makeOfferAvailable;

                    //if (buyItNowAvailable && action.Amount.HasValue && action.Amount.Value >= buyItNowPrice)
                    bool buyNowRequested =
                        action.Properties.Any(
                            ap => ap.Field.Name.Equals("BuyItNow") && ap.Value.Equals(true.ToString()));
                    if (buyNowRequested && buyItNowAvailable && (action.Amount ?? 0.0M) < buyItNowPrice)
                    {
                        action.Status = ListingActionStatuses.Rejected;
                        reasonCode = ReasonCode.BidAmountTooLow;
                        accepted = false;
                        boughtItNow = false;
                    }
                    else if (buyNowRequested && buyItNowAvailable)
                    {
                        //Valid Buy It Now Submission
                        //Create LineItem for ended Listing
                        newPurchaseLineitem = new LineItem
                                                  {
                                                      PerUnitAmount = buyItNowPrice,
                                                      Quantity = action.Quantity,
                                                      TotalAmount = buyItNowPrice * action.Quantity,
                                                      Currency = currentListing.Currency.Code,
                                                      DateStamp = DateTime.UtcNow,
                                                      Description = currentListing.Title,
                                                      Owner = currentListing.Owner,
                                                      Payer = action.User,
                                                      ListingID = currentListing.ID,
                                                      Type = LineItemTypes.Listing,
                                                      Taxable = currentListing.Lot != null ? currentListing.Lot.Event.LotsTaxable : true,
                                                      BuyersPremiumApplies = true,
                                                      AuctionEventId = currentListing.Lot != null ? (int?)currentListing.Lot.Event.ID : null,
                                                      LotNumber = currentListing.Lot != null ? currentListing.Lot.LotNumber : null
                                                  };
                        //create line item properties for any custom listing fields with IncludeOnInvoice or IncludeInSalesReport set
                        List<CustomProperty> propsForInvoice = currentListing.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport).ToList();
                        if (currentListing.Lot != null)
                        {
                            propsForInvoice.AddRange(currentListing.Lot.Event.Properties.Where(p => p.Field.IncludeOnInvoice || p.Field.IncludeInSalesReport));
                        }
                        newPurchaseLineitem.Properties = new Dictionary<string, string>(propsForInvoice.Count);
                        foreach (CustomProperty prop in propsForInvoice)
                        {
                            string val = string.Empty;
                            CultureInfo sellersCultureInfo = CultureInfo.GetCultureInfo(currentListing.Owner.Culture);
                            if (!string.IsNullOrEmpty(prop.Value))
                            {
                                switch (prop.Field.Type)
                                {
                                    case CustomFieldType.Decimal:
                                        if (prop.Value.ToString().TrimEnd('0') != "")
                                        {
                                            int count =
                                                BitConverter.GetBytes(
                                                    decimal.GetBits(decimal.Parse(prop.Value.ToString().TrimEnd('0')))[3
                                                        ])[2];
                                            val = decimal.Parse(prop.Value).ToString("N" + count, sellersCultureInfo);
                                        }
                                        else
                                        {
                                            val = decimal.Parse(prop.Value).ToString("N0", sellersCultureInfo);
                                        }
                                        break;
                                    case CustomFieldType.Int:
                                        val = int.Parse(prop.Value).ToString(sellersCultureInfo);
                                        break;
                                    case CustomFieldType.DateTime:
                                        val = DateTime.Parse(prop.Value).ToString(sellersCultureInfo);
                                        break;
                                    case CustomFieldType.Boolean:
                                        val = Boolean.Parse(prop.Value) ? "Yes" : "No";
                                        break;
                                    default:
                                        val = prop.Value;
                                        break;
                                }
                                newPurchaseLineitem.Properties.Add(prop.Field.Name, val);
                            }
                        }

                        action.Status = ListingActionStatuses.Accepted;
                        reasonCode = ReasonCode.Success;
                        currentListing.CurrentPrice = buyItNowPrice;
                        accepted = true;
                        boughtItNow = true;
                    }
                    else if (buyNowRequested && !buyItNowAvailable)
                    {
                        action.Status = ListingActionStatuses.Rejected;
                        action.Reason = Messages.BuyNowNotAvailable;
                        reasonCode = ReasonCode.BuyNowNotAvailable;
                        accepted = false;
                    }
                    else if (proxyEnabled)
                    {
                        //Proxy bidding is on
                        if (currentListing.CurrentListingAction != null)
                        {
                            //This is not the first bid
                            if (currentListing.CurrentListingAction.ProxyAmount >= action.ProxyAmount)
                            {
                                //we are not higher than existing proxy bid
                                if (currentListing.CurrentListingAction.User.ID != action.User.ID)
                                {
                                    //another bidder is winning by proxy, add our rejected bid
                                    action.Status = ListingActionStatuses.Accepted;
                                    action.Reason = string.Empty;
                                    reasonCode = ReasonCode.ProxyBeatingNewBid;
                                    accepted = false;

                                    //add a new winning bid for the proxy winner
                                    overridingListingAction = currentListing.CurrentListingAction;
                                    decimal newIncrement = GetBidIncrementAmount(context.Get<List<Increment>>(Fields.Increments), action.ProxyAmount.Value);
                                    if (action.ProxyAmount + newIncrement <= overridingListingAction.ProxyAmount)
                                    {
                                        overridingListingAction.Amount = action.ProxyAmount + newIncrement;
                                    }
                                    else
                                    {
                                        overridingListingAction.Amount = overridingListingAction.ProxyAmount;
                                    }
                                    overridingListingAction.Description =
                                        FieldDefaults.AuctionOverrideListingActionDescription;
                                    overridingListingAction.ActionDTTM = action.ActionDTTM;
                                    overridingListingAction.UserEntered = false;
                                    currentListing.CurrentPrice = overridingListingAction.Amount;

                                    overrideHappensFirst = false;
                                }
                                else
                                {
                                    //same bidder can't lower proxy bid
                                    action.Status = ListingActionStatuses.Rejected;
                                    action.Reason = Messages.CantLowerProxyBid;
                                    reasonCode = ReasonCode.CantLowerProxyBid;
                                    accepted = false;
                                }
                            }
                            else
                            {
                                //we ARE higher than existing proxy bid
                                if (currentListing.CurrentListingAction.User.ID != action.User.ID)
                                {
                                    //another bidder is just lost by proxy, add our accepted bid
                                    action.Status = ListingActionStatuses.Accepted;
                                    reasonCode = ReasonCode.Success;
                                    accepted = true;

                                    //if the amounts are different, add a new "just prior" winning bid for the proxy winner
                                    if (currentListing.CurrentListingAction.Amount < currentListing.CurrentListingAction.ProxyAmount)
                                    {
                                        overridingListingAction = currentListing.CurrentListingAction;
                                        overridingListingAction.Amount =
                                            currentListing.CurrentListingAction.ProxyAmount;
                                        overridingListingAction.Description =
                                            FieldDefaults.AuctionOverrideListingActionDescription;
                                        overridingListingAction.ActionDTTM = action.ActionDTTM;
                                        overridingListingAction.UserEntered = false;
                                    }
                                    else
                                    {
                                        overridingListingAction = null;
                                    }

                                    //if new winner's proxy amount is less than or equal to the old proxy winner's proxy amount PLUS increment, set it
                                    if (action.ProxyAmount <= currentListing.CurrentListingAction.ProxyAmount + currentListing.Increment)
                                    {
                                        currentListing.CurrentPrice = action.ProxyAmount;
                                    }
                                    else
                                    {
                                        //otherwise, current price shall be set to the new winner's proxy amount (disregarding increment)                                            
                                        currentListing.CurrentPrice = currentListing.CurrentListingAction.ProxyAmount +
                                                                      currentListing.Increment;
                                        action.Amount = currentListing.CurrentPrice;
                                    }

                                    overrideHappensFirst = true;
                                }
                                else
                                {
                                    //same bidder CAN raise proxy bid                                        

                                    action.Status = ListingActionStatuses.Accepted;
                                    action.Amount = currentListing.CurrentPrice;
                                    reasonCode = ReasonCode.Success;
                                    accepted = true;
                                }
                            }
                        }
                        else
                        {
                            //we're placing the first bid
                            action.Status = ListingActionStatuses.Accepted;
                            reasonCode = ReasonCode.Success;
                            accepted = true;
                            action.Amount = currentListing.CurrentPrice;
                        }

                        //if reserve price is defined
                        CustomProperty reservePrice =
                            currentListing.Properties.SingleOrDefault(p => p.Field.Name == Fields.ReservePrice);
                        if (reservePrice != null && !string.IsNullOrEmpty(reservePrice.Value))
                        {
                            //if reserve price is defined, see if it's met
                            reservePriceAmount = decimal.Parse(reservePrice.Value);
                            reserveMet = reservePriceAmount <= currentAmount;
                            reserveDefined = true;
                        }

                        if (reserveDefined && !reserveMet)
                        {
                            if (!proxyBiddingBelowReserve)
                            {
                                //reserve matters
                                if (reservePriceAmount <= action.ProxyAmount)
                                {
                                    //reserve is being met by this bid so increase bid to reserve amount.
                                    action.Amount = reservePriceAmount;
                                }
                                else
                                {
                                    //reserve is not being met, place max bid immediately
                                    action.Amount = action.ProxyAmount;
                                }
                            }
                            if (accepted)
                            {
                                if (overridingListingAction != null && overridingListingAction.Amount > action.Amount)
                                {
                                    currentListing.CurrentPrice = overridingListingAction.Amount;
                                }
                                else
                                {
                                    currentListing.CurrentPrice = action.Amount;
                                }
                            }
                        }

                        if (buyItNowAvailable && (disableBuyNowAfterWinningBid || (buyItNowPrice <= currentListing.CurrentPrice)))
                        {
                            if (reserveDefined && action.Amount >= reservePriceAmount || !reserveDefined)
                            {
                                //disable Buy Now option...
                                currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNow).Value = "false";
                                buyItNow = false;
                            }
                        }
                        if (makeOfferAvailable && disableBuyNowAfterWinningBid && (reserveDefined && action.Amount >= reservePriceAmount) || !reserveDefined)
                        {
                            //disable make offer...
                            if (currentListing.Properties.Any(p => p.Field.Name == Fields.MakeOfferAvailable))
                            {
                                currentListing.Properties.Single(p => p.Field.Name == Fields.MakeOfferAvailable).Value = "false";
                            }
                            makeOfferStatus = false;
                        }
                    }
                    else
                    {
                        //proxy bidding is off, enter max bid
                        action.Status = ListingActionStatuses.Accepted;
                        reasonCode = ReasonCode.Success;
                        currentListing.CurrentPrice = action.Amount;
                        accepted = true;

                        if (buyItNowAvailable && (disableBuyNowAfterWinningBid || (buyItNowPrice <= currentListing.CurrentPrice)))
                        {
                            //if reserve price is defined
                            CustomProperty reservePrice =
                                currentListing.Properties.SingleOrDefault(p => p.Field.Name == Fields.ReservePrice);
                            if (reservePrice != null && !string.IsNullOrEmpty(reservePrice.Value))
                            {
                                //if reserve price is defined, see if it's met
                                reservePriceAmount = decimal.Parse(reservePrice.Value);
                                reserveDefined = true;
                            }

                            if (reserveDefined && action.Amount >= reservePriceAmount || !reserveDefined)
                            {
                                //disable Buy Now option...
                                currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNow).Value = "false";
                                buyItNow = false;
                            }
                        }
                        if (makeOfferAvailable && disableBuyNowAfterWinningBid && (reserveDefined && action.Amount >= reservePriceAmount) || !reserveDefined)
                        {
                            //disable make offer...
                            if (currentListing.Properties.Any(p => p.Field.Name == Fields.MakeOfferAvailable))
                            {
                                currentListing.Properties.Single(p => p.Field.Name == Fields.MakeOfferAvailable).Value = "false";
                            }
                            makeOfferStatus = false;
                        }
                    }
                }
                else
                {
                    //this bid is rejected because amount is too low
                    action.Status = ListingActionStatuses.Rejected;
                    action.Reason = Messages.BidAmountTooLow;
                    reasonCode = ReasonCode.BidAmountTooLow;
                    accepted = false;
                }

                bool sniperTriggered = false;
                //Begin Sniper Protection T1490
                if (accepted == true || overridingListingAction != null)
                {
                    //if sniper protection interval is set...
                    if (sniperSeconds > 0)
                    {
                        //if enddttm minus now is less than sniper protection seconds...
                        if (currentListing.EndDTTM.HasValue && currentListing.EndDTTM.Value.Subtract(DateTime.UtcNow).TotalSeconds < sniperSeconds)
                        {
                            //we're in the sniper window, advance enddttm
                            currentListing.EndDTTM = currentListing.EndDTTM.Value.AddSeconds(sniperSeconds);
                            endDTTM = currentListing.EndDTTM;
                            sniperTriggered = true;
                        }
                    }
                }
                //End Sniper Protection T1490

                //update bid increment if applicable
                if (accepted || currentListing.CurrentPrice.Value > currentAmount)
                {
                    currentListing.Increment = GetBidIncrementAmount(context.Get<List<Increment>>(Fields.Increments), currentListing.CurrentPrice.Value);
                }

                try
                {
                    //attempt to commit to DB
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        //persist bid(s)
                        if (overridingListingAction != null && overrideHappensFirst)
                        {
                            _data.AddListingAction(action.UserName, overridingListingAction);
                            currentListing.ListingActions.Add(overridingListingAction);
                            if (overridingListingAction.Status.Equals(ListingActionStatuses.Accepted))
                            {
                                currentListing.CurrentListingAction = overridingListingAction;
                                currentListing.CurrentListingActionID = overridingListingAction.ID;
                                currentListing.CurrentListingActionUserName = overridingListingAction.UserName;
                            }
                        }

                        _data.AddListingAction(action.UserName, action);
                        currentListing.ListingActions.Add(action);
                        if (action.Status.Equals(ListingActionStatuses.Accepted))
                        {
                            currentListing.CurrentListingAction = action;
                            currentListing.CurrentListingActionID = action.ID;
                            currentListing.CurrentListingActionUserName = action.UserName;
                        }

                        if (overridingListingAction != null && !overrideHappensFirst)
                        {
                            _data.AddListingAction(action.UserName, overridingListingAction);
                            currentListing.ListingActions.Add(overridingListingAction);
                            if (overridingListingAction.Status.Equals(ListingActionStatuses.Accepted))
                            {
                                currentListing.CurrentListingAction = overridingListingAction;
                                currentListing.CurrentListingActionID = overridingListingAction.ID;
                                currentListing.CurrentListingActionUserName = overridingListingAction.UserName;
                            }
                        }

                        int currentActionCount = currentListing.AcceptedActionCount;
                        if (overridingListingAction != null && overridingListingAction.Status.Equals(ListingActionStatuses.Accepted))
                            currentListing.AcceptedActionCount++;
                        if (action.Status.Equals(ListingActionStatuses.Accepted))
                            currentListing.AcceptedActionCount++;

                        if (sniperTriggered || (currentAmount != currentListing.CurrentPrice.Value) || (currentActionCount != currentListing.AcceptedActionCount))
                        {
                            if (sniperTriggered)
                            {
                                ListingDTTMChange data = new ListingDTTMChange()
                                {
                                    ListingID = currentListing.ID,
                                    DTTM = currentListing.EndDTTM.Value,
                                    Epoch = ListingTimeOrigins.Ending,
                                    Source = "SOFT_ORIGIN"
                                };
                                _queueManager.FireListingDTTMChange(data);
                                _data.LogListingEndDttmChange(data.ListingID, null, data.Source, data.DTTM);

                                List<ListingDTTMChange> messagesToQueue;
                                _data.HandleUpdateLotEndDTTMsForSoftClose(currentListing.ID, out messagesToQueue);
                                foreach(var msg in messagesToQueue)
                                {
                                    _queueManager.FireListingDTTMChange(msg);
                                    _data.LogListingEndDttmChange(msg.ListingID, currentListing.ID, msg.Source, msg.DTTM);
                                }
                            }

                            //if price changed, or # of accepted bids changed
                            if ((currentAmount != currentListing.CurrentPrice.Value) || (currentActionCount != currentListing.AcceptedActionCount))
                            {
                                CustomProperty fixedPrice =
                                    currentListing.Properties.SingleOrDefault(p => p.Field.Name == Fields.FixedPrice);
                                decimal buyItNowPrice = 0.0M;
                                bool buyItNowAvailable = false;
                                if (fixedPrice != null && !string.IsNullOrEmpty(fixedPrice.Value))
                                {
                                    buyItNowPrice = decimal.Parse(fixedPrice.Value);

                                    buyItNowAvailable =
                                        bool.Parse(
                                            currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNow).Value);
                                }

                                ListingActionChange data = new ListingActionChange()
                                {
                                    ListingID = currentListing.ID,
                                    Price = currentListing.CurrentPrice.Value,
                                    Increment = currentListing.Increment.Value,
                                    NextPrice = currentListing.CurrentPrice.Value + currentListing.Increment.Value,
                                    Source = "BID_ORIGIN",
                                    CurrencyCode = currentListing.Currency.Code,
                                    UserName = action.UserName,
                                    AcceptedActionCount = currentListing.AcceptedActionCount,
                                    CurrentListingActionUserName = currentListing.CurrentListingActionUserName,
                                    Properties = new Dictionary<string, string>()
                                    {
                                        {"BuyNowStatus", buyItNowAvailable.ToString().ToLowerInvariant()},
                                        {"BuyNowPrice", buyItNowPrice.ToString(CultureInfo.InvariantCulture)},
                                        {"ReserveDefined", reserveDefined.ToString().ToLowerInvariant()},
                                        {"ReserveMet", (reservePriceAmount <= currentListing.CurrentPrice.Value).ToString().ToLowerInvariant()},
                                        {"MakeOfferStatus", makeOfferStatus.ToString().ToLowerInvariant()}
                                    },
                                    Quantity = currentListing.CurrentQuantity
                                };
                                _queueManager.FireListingActionChange(data);
                            }
                        }

                        //Buy It Now Processing//////
                        if (newPurchaseLineitem != null)
                        {
                            //buy it now executed
                            newPurchaseLineitem = _accounting.CreateNewLineItem(SystemActors.SystemUserName, newPurchaseLineitem);
                            //mark listing ending
                            currentListing.Status = ListingStatuses.Ending;
                            status = currentListing.Status;
                            ListingStatusChange statusUpdate = new ListingStatusChange()
                            {
                                ListingID = currentListing.ID,
                                Status = currentListing.Status,
                                Source = "LISTINGACTION_ORIGIN"
                            };
                            if (currentListing.OwnerAllowsInstantCheckout())
                            {
                                statusUpdate.LineItemID = newPurchaseLineitem.ID;
                                statusUpdate.LineItemPayerUN = newPurchaseLineitem.Payer.UserName;
                            }
                            _queueManager.FireListingStatusChange(statusUpdate);

                            currentListing.EndDTTM = DateTime.UtcNow;
                            endDTTM = currentListing.EndDTTM;

                            //Queue end dttm update, with different source so as not to affect Events/Lot closing times, but to maintain signalR messages
                            ListingDTTMChange data = new ListingDTTMChange()
                            {
                                ListingID = currentListing.ID,
                                DTTM = currentListing.EndDTTM.Value,
                                Epoch = ListingTimeOrigins.Ending,
                                Source = "BUYNOW_ORIGIN"
                            };
                            _queueManager.FireListingDTTMChange(data);
                            _data.LogListingEndDttmChange(data.ListingID, null, data.Source, data.DTTM);

                            //set BuyItNow variable "Buy It Now was used"
                            currentListing.Properties.Single(p => p.Field.Name == Fields.BuyItNowUsed).Value = "true";
                            buyItNowUsed = true;
                            if (notify)
                            {
                                _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                                  currentListing.OwnerUserName,
                                                                  Templates.SellerSold,
                                                                  DetailTypes.LineItem,
                                                                  newPurchaseLineitem.ID, null, null, null, null, null);
                                _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                                  action.UserName, Templates.BuyerBought,
                                                                  DetailTypes.LineItem,
                                                                  newPurchaseLineitem.ID, null, null, null, null, null);
                            }
                        }
                        //END Buy It Now Processing//////

                        if (accepted == true || overridingListingAction != null)
                        {
                            //update...
                            //_data.UpdateListing(action.UserName, currentListing);
                            //buyItNow, buyItNowUser, endDTTM, and status are null unless actually set.
                            _data.FastPlaceBid(action.UserName,
                                                currentListing.ID,
                                                currentListing.CurrentPrice.Value,
                                                buyItNow,
                                                buyItNowUsed,
                                                endDTTM,
                                                currentListing.Increment.Value,
                                                currentListing.CurrentListingActionID,
                                                currentListing.CurrentListingActionUserName,
                                                currentListing.AcceptedActionCount,
                                                status);
                            currentListing.Version++; //the old update method would have incremented this.
                        }

                        if (accepted)
                        {
                            //a bid was accepted
                            if (!boughtItNow)
                            {
                                if (notify)
                                {
                                    _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                                      action.UserName,
                                                                      Templates.BidderBid,
                                                                      DetailTypes.ListingAction,
                                                                      action.ID, null, null, null, null, null);
                                    _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                                      currentListing.OwnerUserName,
                                                                      Templates.SellerBid,
                                                                      DetailTypes.ListingAction,
                                                                      action.ID, null, null, null, null, null);
                                }
                            }
                            else if (currentListing.Lot != null) // event EstimatedLastEndDTTM needs to be updated in case this was the last active lot
                            {
                                _data.CalculateLotEndDTTMs(action.UserName, currentListing.Lot.Event.ID);
                            }

                            if (previousWinningBid != null && previousWinningBid.User.ID != action.User.ID)
                            {
                                //someone's being outbid...
                                if (notify)
                                    _notifier.QueueSystemNotification(SystemActors.SystemUserName,
                                                                      previousWinningBid.UserName,
                                                                      Templates.BidderOutbid,
                                                                      DetailTypes.ListingAction,
                                                                      previousWinningBid.ID, null, null, null, null, null);
                            }
                        }

                        scope.Complete();
                    }
                }
                catch
                {
                    //(assumed) concurrency exception during the update of the listing or auction objects or during workflow processing above
                    //Mark the bid rejected (disregard any overriding bid)
                    action.Status = ListingActionStatuses.Rejected;
                    action.Reason = Messages.AuctionChanged;
                    //prepare to reject bid
                    reasonCode = ReasonCode.ListingChangedDuringProcessing;
                    accepted = false;
                    //attempt to create the object again (should succeed because nothing in DAL gets updated or deleted...
                    _data.AddListingAction(action.UserName, action); // won't affect lock
                    currentListing.ListingActions.Add(action);
                }
                return false; //no fees owed
#if TRACE
            }
#endif
        }

        private static decimal GetBidIncrementAmount(List<Increment> increments, decimal newPriceLevel)
        {
            decimal retVal = 0;

            Increment tempIncrement = increments
                .Where(i => i.PriceLevel <= newPriceLevel)
                .OrderByDescending(i => i.PriceLevel)
                .FirstOrDefault();
            if (tempIncrement != null)
            {
                retVal = tempIncrement.Amount;
            }

            //failsafe -- MUST be GTZero...
            if (retVal < 0.01M) retVal = 0.01M;

            return retVal;
        }
    }
}
