﻿using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Payment {

	public interface IPaymentProvider {

        /// <summary>
        /// Registers the payment provider with the system
        /// </summary>
        /// <returns>true if any data was inserted or updated</returns>
		bool RegisterSelf();

        /// <summary>
        /// The name of the payment provider
        /// </summary>
		string ProviderName { get; }
		
        /// <summary>
        /// Whether or not the payment provider is enabled
        /// </summary>
		bool Enabled { get; }

	    /// <summary>
	    /// Determines whether this PaymentProvider can be used to pay the supplied Invoice.
	    /// </summary>
	    /// <param name="invoice">The invoice.</param>
	    /// <param name="reason">If this method returns false, the "reason" parameter indicates the ReasonCode</param>
	    /// <returns>true if this payment provider can be used to pay the invoice.</returns>
	    bool CanPayInvoice(Invoice invoice, out ReasonCode reason);

        /// <summary>
        /// Synchronously processes a payment, or in the case of asynchronous providers, begins the payment process.
        /// </summary>
        /// <param name="invoice">The Invoice to pay</param>
        /// <param name="paymentParameters">The parameters necessary for the payment provider to attempt the transaction</param>
        /// <returns></returns>
        PaymentProviderResponse ProcessPayment(Invoice invoice, PaymentParameters paymentParameters);

        BatchPaymentCapability PrepareBatchPayment(Invoice invoice, out PaymentParameters paymentParameters, out string details);
 
		/// <summary>
		/// Determines whether this IPaymentProvider knows how to interpret the IPN contained in the supplied HTTP request.
		/// </summary>
		/// <param name="request">The HTTP request.</param>
		/// <returns>true if this provider implementation can interpret the IPN.</returns>
		bool IsThisMyIpn(UserInput request);

		/// <summary>
		/// Processes an IPN.
		/// </summary>
		/// <remarks>
		/// For asynchronous payment providers, this method completes the payment process.
		/// </remarks>
		/// <param name="request">The HTTP request that contains the IPN.</param>
        /// <returns>a PaymentProviderResponse</returns>
        PaymentProviderResponse ProcessIpn(UserInput request);

	}

}