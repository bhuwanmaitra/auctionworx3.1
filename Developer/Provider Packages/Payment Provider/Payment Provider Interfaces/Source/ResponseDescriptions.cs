﻿namespace RainWorx.FrameWorx.Providers.Payment {

	/// <summary>
	/// This class holds static tokens to be used in the ResponseDescription field of a PaymentProviderResponse.
	/// </summary>
	public static class ResponseDescriptions {

		public const string Completed = "PaymentCompleted";

		public static class Pending {
			public const string ECheck = "PaymentPendingECheck";
			public const string SellerAcceptance = "PaymentPendingSellerAcceptance";
			public const string HeldForReview = "PaymentPendingReview";
		}

		public static class Declined {
			public const string GenericDeclined = "PaymentDeclined";
			public const string InvalidCardNumber = "PaymentInvalidCardNumber";
			public const string ExpiredCard = "PaymentCardExpired";
			public const string InvalidAbaCode = "PaymentInvalidAbaCode";
			public const string DuplicateTransaction = "PaymentDuplicateTransaction";
			public const string InvalidCardCode = "PaymentInvalidCardCode";
			public const string AvsRejection = "PaymentAvsRejection";
			public const string InvalidExpirationDate = "PaymentInvalidExpirationDate";
			public const string InvalidAccountNumber = "PaymentInvalidAccountNumber";
			public const string MissingAuthorizationCode = "PaymentMissingAuthorizationCode";
		}

	}

}