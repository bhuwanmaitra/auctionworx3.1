﻿using System;
using System.Collections.Generic;
using System.Globalization;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Payment
{
    public abstract class PaymentProviderBase : IPaymentProvider
    {
        /// <summary>
        /// Registers the payment provider with the system
        /// </summary>
        public abstract bool RegisterSelf();

        /// <summary>
        /// The name of the payment provider
        /// </summary>
        public abstract string ProviderName { get; }

        /// <summary>
        /// Whether or not the payment provider is enabled
        /// </summary>
        public abstract bool Enabled { get; }

        public abstract BatchPaymentCapability PrepareBatchPayment(Invoice invoice,
                                                                   out PaymentParameters paymentParameters,
                                                                   out string details);        

        /// <summary>
        /// Determines whether this PaymentProvider can be used to pay the supplied Invoice
        /// </summary>
        /// <param name="invoice">The invoice.</param>
        /// <param name="reason">If this method returns false, the "reason" parameter indicates the ReasonCode</param>
        /// <returns>true if this payment provider can be used to pay the invoice</returns>
        public abstract bool CanPayInvoice(Invoice invoice, out ReasonCode reason);

        /// <summary>
        /// Synchronously processes a payment, or in the case of asynchronous providers, begins the payment process.
        /// </summary>
        /// <param name="invoice">The Invoice to pay</param>
        /// <param name="paymentParameters">The parameters necessary for the payment provider to attempt the transaction</param>
        /// <returns></returns>
        public abstract PaymentProviderResponse ProcessPayment(Invoice invoice, PaymentParameters paymentParameters);

        /// <summary>
        /// Determines whether this IPaymentProvider knows how to interpret the IPN contained in the supplied HTTP request.
        /// </summary>
        /// <param name="request">The HTTP request.</param>
        /// <returns>true if this provider implementation can interpret the IPN.</returns>
        public abstract bool IsThisMyIpn(UserInput request);

        /// <summary>
        /// Processes an IPN.
        /// </summary>
        /// <remarks>
        /// For asynchronous payment providers, this method completes the payment process.
        /// </remarks>
        /// <param name="request">The HTTP request that contains the IPN.</param>
        /// <returns>a PaymentProviderResponse</returns>
        public abstract PaymentProviderResponse ProcessIpn(UserInput request);

        protected readonly IDataContext _data;

        protected PaymentProviderBase(IDataContext data)
        {
            _data = data;            
        }

        /// <summary>
        /// Adds the payment provider navigational category to the Admin Control Panel
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="name">The payment provider name</param>
        /// <param name="displayOrder">The order in which the payment provider should appear in the Admin Control Panel</param>
        /// <param name="enabledCustomProperty">The name of the custom property that determines if this navigational category is enabled (displayed)</param>
        /// <returns>The added navigational category</returns>
        protected Category AddPaymentProviderAdminNavigation(string actingUserName, string name, int displayOrder, string enabledCustomProperty)
        {
            Category category = new Category
                                    {
                                        Name = name,
                                        Type = "Site",
                                        MVCAction = "PropertyManagement",
                                        DisplayOrder = displayOrder,
                                        EnabledCustomProperty = enabledCustomProperty
                                    };
            _data.AddChildCategory(actingUserName, "PaymentProviders", "Site", category);
            category.MVCAction = category.MVCAction + "/" + category.ID.ToString(CultureInfo.InvariantCulture);
            _data.UpdateCategory(actingUserName, category);
            return category;
        }

        /// <summary>
        /// Adds a user instance field for the payment provider
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="propertyName">The name of the property to add</param>
        /// <param name="fieldType">The datatype of the property to add</param>
        /// <param name="defaultValue">The default value (convertible string) of the property to add</param>
        /// <param name="displayOrder">The order in which the property should appear in "My Account"</param>
        /// <param name="required">Whether or not the property must contain a value</param>
        /// <param name="enumList">If the fieldType is an "Enum," a comma delimited list of possible enumeration values for the property.  For each enumeration, there are three tokens: the name of the enumeration
        /// (what will be displayed on screen, or localized to be displayed), the value of the enumeration (what the property is actually set to if the enumeration is selected), and "true" or "false" indicating
        /// whether the enumeration is enabled or not (this will almost always be true).  Each token is seperated by a "|" (pipe).  An example is "Red|1|true,Green|2|true,Blue|3|true"</param>
        /// <param name="visibility">the level of access required to view this property</param>
        /// <param name="mutability">the level of access required to edit this property</param>
        /// <param name="deferred">whether the property must be filled upon user account creation</param>
        /// <param name="encrypted">if this data will be encrypted before being stored</param>
        /// <returns>true if the property was actually added, otherwise false</returns>
        protected bool AddPaymentProviderUserField(string actingUserName, string propertyName, CustomFieldType fieldType, string defaultValue, int displayOrder, bool required,
            string enumList = null,
            CustomFieldAccess visibility = CustomFieldAccess.Owner,
            CustomFieldAccess mutability = CustomFieldAccess.Owner,
            bool deferred = false,
            bool encrypted = false)
        {
            bool retVal = false;

            //If necessary, add field
            CustomField currentField = _data.GetCustomFieldByName(propertyName, fieldType, "User");
            if (currentField == null)
            {
                //Category userCategory = _data.GetCategoryByName("Payment", "User");
                //TODO There are 2 "Payment" "User" categories, we want the one with ID 44902, this is a workaround for now.
                const int userCategoryID = 44902;

                currentField = new CustomField
                                   {
                                       DefaultValue = defaultValue,
                                       CategoryIDs = new List<int>(1) {userCategoryID},
                                       Deferred = deferred,
                                       DisplayOrder = displayOrder,
                                       Group = "User",
                                       Name = propertyName,
                                       Required = required,
                                       Type = fieldType,
                                       Visibility = visibility,
                                       Mutability = mutability,
                                       Encrypted = encrypted
                                   };
                _data.AddCustomField(actingUserName, currentField);

                _data.AssignFieldToCategories(actingUserName, currentField.ID, new[] { userCategoryID });

                //if necessary, add enum options
                if (!string.IsNullOrEmpty(enumList))
                {
                    foreach (string enumData in enumList.Split(','))
                    {
                        string enumName = string.Empty;
                        string enumValue = string.Empty;
                        bool enumEnabled = false;
                        int i = 0;
                        foreach (string enumPart in enumData.Split('|'))
                        {
                            if (i == 0)
                            {
                                enumName = enumPart;
                            }
                            else if (i == 1)
                            {
                                enumValue = enumPart;
                            }
                            else if (i == 2)
                            {
                                enumEnabled = bool.Parse(enumPart);
                            }
                            i++;
                        }
                        _data.CreateEnumeration(actingUserName, currentField.ID, enumName, enumName, enumValue, enumEnabled);
                    }
                }

                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Adds a site field for the payment provider
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="propertyName">The name of the property to add</param>
        /// <param name="fieldType">The datatype of the property to add</param>
        /// <param name="defaultValue">The default value (convertible string) of the property to add</param>
        /// <param name="displayOrder">The order in which the property should appear in "My Account"</param>
        /// <param name="required">Whether or not the property must contain a value</param>
        /// <param name="categoryID">The ID of the nagivational category to add the property to</param>
        /// <param name="enumList">If the fieldType is an "Enum," a comma delimited list of possible enumeration values for the property.  For each enumeration, there are three tokens: the name of the enumeration
        /// (what will be displayed on screen, or localized to be displayed), the value of the enumeration (what the property is actually set to if the enumeration is selected), and "true" or "false" indicating
        /// whether the enumeration is enabled or not (this will almost always be true).  Each token is seperated by a "|" (pipe).  An example is "Red|1|true,Green|2|true,Blue|3|true"</param>
        /// <param name="visibility">the level of access required to view this property</param>
        /// <param name="mutability">the level of access required to edit this property</param>
        /// <param name="encrypted">if this data will be encrypted before being stored</param>
        /// <returns>true if the property was actually added, otherwise false</returns>
        protected bool AddPaymentProviderSiteProperty(string actingUserName, string propertyName, 
            CustomFieldType fieldType, string defaultValue, int displayOrder, bool required, int categoryID, 
            string enumList = null, 
            CustomFieldAccess visibility = CustomFieldAccess.Admin, 
            CustomFieldAccess mutability = CustomFieldAccess.Admin,
            bool encrypted = false)
        {
            bool retVal = false;
            
            //If necessary, add field
            CustomField currentField = _data.GetCustomFieldByName(propertyName, fieldType, "Site");
            if (currentField == null)
            {
                currentField = new CustomField
                                   {
                                       DefaultValue = defaultValue,
                                       CategoryIDs = new List<int>(1) {categoryID},
                                       Deferred = false,
                                       DisplayOrder = displayOrder,
                                       Group = "Site",
                                       Name = propertyName,
                                       Required = required,
                                       Type = fieldType,
                                       Visibility = visibility,
                                       Mutability = mutability,
                                       Encrypted = encrypted
                };
                //_data.AddCustomField(actingUserName, ref currentField);
                
                CustomProperty currentProperty = new CustomProperty
                                                    {
                                                        Field = currentField,
                                                        Value = defaultValue
                                                    };
                _data.AddSiteProperty(actingUserName, currentProperty);
                _data.AssignFieldToCategories(actingUserName, currentField.ID, new[] { categoryID });

                //if necessary, add enum options
                if (!string.IsNullOrEmpty(enumList))
                {
                    foreach (string enumData in enumList.Split(','))
                    {
                        string enumName = string.Empty;
                        string enumValue = string.Empty;
                        bool enumEnabled = false;
                        int i = 0;
                        foreach (string enumPart in enumData.Split('|'))
                        {
                            if (i == 0)
                            {
                                enumName = enumPart;
                            }
                            else if (i == 1)
                            {
                                enumValue = enumPart;
                            }
                            else if (i == 2)
                            {
                                enumEnabled = bool.Parse(enumPart);
                            }
                            i++;
                        }
                        _data.CreateEnumeration(actingUserName, currentField.ID, enumName, enumName, enumValue, enumEnabled);
                    }
                }
                
                retVal = true; 
            }                       

            return retVal;
        }
    }
}
