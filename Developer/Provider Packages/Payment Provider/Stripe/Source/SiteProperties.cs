﻿using System;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.Stripe
{

    internal static class SiteProperties
    {

		public class Keys {
			public const string ClientId = "StripeConnect_ClientId";
            public const string SiteFeesSecretApiKey = "StripeConnect_SiteFeesSecretApiKey";
            public const string SiteFeesPublishableApiKey = "StripeConnect_SiteFeesPublishableApiKey";
            public const string Enabled = "StripeConnect_Enabled";
		    public const string DemoMode = "DemoEnabled";
            public const string SalePaymentDescription = "StripeConnect_SalePaymentDescription";
            public const string EnabledForSellers = "StripeConnect_EnabledForSellers";
            public const string FriendlySiteName = "FriendlySiteName";
        }

        public static string FriendlySiteName
        {
            get { return Cache.SiteProperties[Keys.FriendlySiteName]; }
        }

        public static string ClientId
        {
            get { return Cache.SiteProperties[Keys.ClientId]; }
        }

        public static string SiteFeesSecretApiKey
        {
            get { return Cache.SiteProperties[Keys.SiteFeesSecretApiKey]; }
        }

        public static string SiteFeesPublishableApiKey
        {
            get { return Cache.SiteProperties[Keys.SiteFeesPublishableApiKey]; }
        }

        public static bool Enabled
        {
			get { return Convert.ToBoolean(Cache.SiteProperties[Keys.Enabled]); }
        }

        public static bool DemoMode
        {
            get { return Convert.ToBoolean(Cache.SiteProperties[Keys.DemoMode]); }
        }

        public static string SalePaymentDescription
        {
            get { return Cache.SiteProperties[Keys.SalePaymentDescription]; }
        }

        public static bool EnabledForSellers
        {
            get { return Convert.ToBoolean(Cache.SiteProperties[Keys.EnabledForSellers]); }
        }

    }

}