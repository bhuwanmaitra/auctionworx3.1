﻿using System;
using System.Diagnostics;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Strings;
using System.Transactions;
using System.IO;
using System.Web;
using RainWorx.FrameWorx.Filters;
using RainWorx.FrameWorx.Utility;
using Stripe;

namespace RainWorx.FrameWorx.Providers.Payment.Stripe
{
    public class StripeConnect : PaymentProviderBase
    {
        #region Private members

        //constants used for registration
        private const string Name = "StripeConnect";
        private const string RegistratorName = "StripeConnectRegistrar";

        //allowed currency list last updated 2017-01-23, per https://support.stripe.com/questions/which-currencies-does-stripe-support
        private static string[] allowedCurrencies = new string[] {
            
            //Currency Group 1
            "AED", "ALL", "ANG", "ARS", "AUD", "AWG", "BBD", "BDT", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BWP", "BZD",
            "CAD", "CHF", "CLP", "CNY", "COP", "CRC", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ETB", "EUR", "FJD",
            "FKP", "GBP", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "ISK",
            "JMD", "JPY", "KES", "KHR", "KMF", "KRW", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "MAD", "MDL", "MNT", "MOP",
            "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN", "PGK", "PHP",
            "PKR", "PLN", "PYG", "QAR", "RUB", "SAR", "SBD", "SCR", "SEK", "SGD", "SHP", "SLL", "SOS", "STD", "SVC", "SZL",
            "THB", "TOP", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VND", "VUV", "WST", "XAF", "XOF", "XPF",
            "YER", "ZAR",

            //Currency Group 2
            "AFN", "AMD", "AOA", "AZN", "BAM", "BGN", "CDF", "GEL", "KGS", "LSL", "MGA", "MKD", "MZN", "RON", "RSD", "RWF",
            "SRD", "TJS", "TRY", "XCD", "ZMW"
        };

        //zero decimal currency list last updated 2017-02-08, per https://support.stripe.com/questions/which-zero-decimal-currencies-does-stripe-support
        private static string[] zeroDecimalCurrencies = new string[] {

            "BIF", "CLP", "DJF", "GNF", "JPY", "KMF", "KRW", "MGA", "PYG", "RWF", "VND", "VUV", "XAF", "XOF", "XPF"
        };

        #endregion

        #region Constructors

        public StripeConnect(IDataContext data) : base(data)
        {
        }

        #endregion

        #region IPaymentProvider Members

        public override BatchPaymentCapability PrepareBatchPayment(Invoice invoice, out PaymentParameters paymentParameters, out string details)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                paymentParameters = null;
                details = string.Empty;

                try
                {
                    //check if this provider is enabled
                    if (!Enabled)
                    {
                        details = "BatchPaymentProviderDisabled";
                        return BatchPaymentCapability.Disabled;
                    }

                    //check if this provider can pay the specified invoice
                    if (invoice.Type != Strings.InvoiceTypes.Fee)
                    {
                        details = "NotFeeInvoice";
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    if (string.IsNullOrEmpty(invoice.BillingStreet1))
                    {
                        details = "BillingAddressMissing";
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    ReasonCode reason;
                    if (!CanPayInvoice(invoice, out reason))
                    {
                        details = Enum.GetName(reason.GetType(), reason);
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    //check if this provider's parameters are all set
                    //if (SiteProperties.TestMode)
                    //{
                    //    details = "TestModeOn";
                    //    return BatchPaymentCapability.InvalidProviderParameters;
                    //}
                    if (SiteProperties.DemoMode)
                    {
                        details = "DemoModeOn";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    if (string.IsNullOrEmpty(SiteProperties.ClientId))
                    {
                        details = "ClientIdMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    //if (string.IsNullOrEmpty(SiteProperties.PostUrl))
                    //{
                    //    details = "PostUrlMissing";
                    //    return BatchPaymentCapability.InvalidProviderParameters;
                    //}
                    if (string.IsNullOrEmpty(SiteProperties.SiteFeesSecretApiKey))
                    {
                        details = "SiteFeesSecretApiKeyMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    if (string.IsNullOrEmpty(SiteProperties.SiteFeesPublishableApiKey))
                    {
                        details = "SiteFeesPublishableApiKeyMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }

                    ////check the payer's parameters
                    //User payer = invoice.Payer;
                    //if (!payer.BillingCreditCardID.HasValue)
                    //{
                    //    details = "NoPrimaryCreditCard";
                    //    return BatchPaymentCapability.MissingPaymentParameters;
                    //}

                    //int primaryCCID = payer.BillingCreditCardID.Value;
                    //List<CreditCard> creditCards = _data.GetCreditCardsForUser(payer);
                    //CreditCard primaryCC = creditCards.SingleOrDefault(cc => cc.ID == primaryCCID);
                    //if (primaryCC == null)
                    //{
                    //    details = "NoPrimaryCreditCard";
                    //    return BatchPaymentCapability.MissingPaymentParameters;
                    //}

                    //DateTime ccExpiration = new DateTime(primaryCC.ExpirationYear, primaryCC.ExpirationMonth, 1, 0, 0, 0).AddMonths(1);
                    //if (DateTime.UtcNow > ccExpiration)
                    //{
                    //    details = "PrimaryCreditCardExpired";
                    //    return BatchPaymentCapability.Expired;
                    //}

                    //lookup Stripe Customer ID -- SellerUserName param is null because this is a site fee invoice
                    string stripeCustomerID = _data.GetStripeCustomerId(null, invoice.Payer.UserName);
                    if (string.IsNullOrEmpty(stripeCustomerID))
                    {
                        details = "NoPrimaryCreditCard";
                        return BatchPaymentCapability.MissingPaymentParameters;
                    }

                    paymentParameters = new PaymentParameters();
                    //paymentParameters.Items.Add(Strings.Fields.SelectedCreditCardId, primaryCC.ID.ToString());
                    paymentParameters.PayerIPAddress = "127.0.0.1";
                    paymentParameters.Items.Add(Strings.Fields.CustomerID, stripeCustomerID);

                }
                catch (Exception e)
                {
                    details = e.Message;
                    return BatchPaymentCapability.Unknown;
                }

                return BatchPaymentCapability.Valid;
#if TRACE
            }
#endif
        }

        public override bool RegisterSelf()
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false;
                try
                {
                    using (TransactionScope scope = TransactionUtils.CreateTransactionScope())
                    {
                        if (_data.CreatePaymentProvider(RegistratorName, Name, true))
                        {
                            retVal = true;
                            //add the StripeConnect provider to the PaymentProviders navigational category in the Admin Control Panel
                            //This navigational category only renders if the enabledCustomProperty (SiteProperties.Keys.Enabled => "StripeConnect_Enabled") is true
                            Category category = AddPaymentProviderAdminNavigation(RegistratorName, Name, 30, SiteProperties.Keys.Enabled);

                            //Add the following properties to the new navigational category (category.ID)
                            //AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.SiteFeesPaymentDescription, CustomFieldType.String,
                            //    "Site Fees", 10, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.ClientId, CustomFieldType.String,
                                "", 20, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.SiteFeesSecretApiKey, CustomFieldType.String,
                                "", 30, true, category.ID, encrypted: true);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.SiteFeesPublishableApiKey, CustomFieldType.String,
                                "", 40, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.EnabledForSellers, CustomFieldType.Boolean,
                                "False", 50, true, category.ID);

                            //Add the following property to the parent navigational category (category.ParentCategoryID.Value)
                            //this allows us to turn the provider and the provider's navigational category (created above) on and off
                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.Enabled, CustomFieldType.Boolean,
                                "False", 30, true, category.ParentCategoryID.Value);

                            //Add PaymentProvider user fields (data.addcustomfield)
                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.SellerAccountConnected, CustomFieldType.Boolean,
                                                        "False", 1031, false, null, CustomFieldAccess.Owner, CustomFieldAccess.Owner, true);

                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.SellerUserId, CustomFieldType.String,
                                                        string.Empty, 1032, false, null, CustomFieldAccess.System, CustomFieldAccess.System, true);

                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.SellerSecretApiKey, CustomFieldType.String,
                                                        string.Empty, 1033, false, null, CustomFieldAccess.System, CustomFieldAccess.System, true, true);

                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.SellerPublishableApiKey, CustomFieldType.String, 
                                                        string.Empty, 1034, false, null, CustomFieldAccess.System, CustomFieldAccess.System, true);

                        }
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return retVal;
#if TRACE
            }
#endif
        }

        public override bool Enabled
        {
            get
            {
                return SiteProperties.Enabled;
            }
        }

        public override string ProviderName
        {
            get
            {
                return Name;
            }
        }

        public override bool CanPayInvoice(Invoice invoice, out ReasonCode reason)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                if (allowedCurrencies.Contains(invoice.Currency.ToUpper()))
                {
                    if (invoice.Type != InvoiceTypes.Fee)
                    {
                        bool stripeConnected = false;
                        CustomProperty prop = invoice.Owner.Properties.SingleOrDefault(p => p.Field.Name == StdUserProps.StripeConnect_SellerAccountConnected);
                        if (prop != null) bool.TryParse(prop.Value, out stripeConnected);
                        if (stripeConnected)
                        {
                            reason = ReasonCode.Success;
                            return true;
                        }
                        else
                        {
                            reason = ReasonCode.DisabledBySeller;
                            return false;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(SiteProperties.SiteFeesSecretApiKey))
                        {
                            reason = ReasonCode.Success;
                            return true;
                        }
                        else
                        {
                            reason = ReasonCode.MissingApiKey;
                            return false;
                        }
                    }
                }
                else
                {
                    reason = ReasonCode.CurrencyNotSupported;
                    return false;
                }
#if TRACE
            }
#endif
        }

        public override PaymentProviderResponse ProcessPayment(Invoice invoice, PaymentParameters paymentParameters)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                PaymentProviderResponse response = new PaymentProviderResponse()
                {
                    Approved = false,
                    ResponseDescription = "Unknown Error"
                };
                try
                {
                    string apiKey;
                    string stripeCustomerId;
                    if (invoice.Type == InvoiceTypes.Fee)
                    {
                        stripeCustomerId = _data.GetStripeCustomerId(null, invoice.Payer.UserName);
                        apiKey = SiteProperties.SiteFeesSecretApiKey;
                    }
                    else
                    {
                        stripeCustomerId = _data.GetStripeCustomerId(invoice.Owner.UserName, invoice.Payer.UserName);
                        apiKey = invoice.Owner.Properties.Single(p => p.Field.Name == StdUserProps.StripeConnect_SellerSecretApiKey).Value;
                        if (!string.IsNullOrWhiteSpace(apiKey))
                        {
                            apiKey = Utilities.DecryptString(apiKey);
                        }
                    }

                    //parse payment parameters
                    bool saveNewCard = false;
                    if (paymentParameters.Items.ContainsKey(Fields.SaveNewStripeCard))
                    {
                        bool.TryParse(paymentParameters.Items[Fields.SaveNewStripeCard], out saveNewCard);
                    }
                    string selectedStripeCardId = null;
                    if (paymentParameters.Items.ContainsKey(Fields.SelectedStripeCardId))
                    {
                        selectedStripeCardId = paymentParameters.Items[Fields.SelectedStripeCardId];
                        if (!string.IsNullOrWhiteSpace(selectedStripeCardId) && selectedStripeCardId == "0")
                        {
                            selectedStripeCardId = null;
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(selectedStripeCardId))
                    {
                        saveNewCard = false;
                    }
                    string stripeToken = null;
                    if (paymentParameters.Items.ContainsKey(Fields.StripeToken))
                    {
                        stripeToken = paymentParameters.Items[Fields.StripeToken];
                    }

                    if (saveNewCard && string.IsNullOrWhiteSpace(stripeCustomerId))
                    {
                        //saving first card, create custromer record
                        var myCustomer = new StripeCustomerCreateOptions();
                        myCustomer.Email = invoice.Payer.Email;
                        myCustomer.Description = string.Format("{0} {1} ({2})", invoice.BillingFirstName, invoice.BillingLastName, invoice.Payer.Email);

                        myCustomer.SourceToken = stripeToken;

                        //myCustomer.PlanId = *planId *;                          // only if you have a plan
                        //myCustomer.TaxPercent = 20;                            // only if you are passing a plan, this tax percent will be added to the price.
                        //myCustomer.Coupon = *couponId *;                        // only if you have a coupon
                        //myCustomer.TrialEnd = DateTime.UtcNow.AddMonths(1);    // when the customers trial ends (overrides the plan if applicable)
                        //myCustomer.Quantity = 1;                               // optional, defaults to 1

                        var customerService = new StripeCustomerService(apiKey);
                        StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                        stripeCustomerId = stripeCustomer.Id;
                        _data.SetStripeCustomerId(invoice.Type == InvoiceTypes.Fee ? null : invoice.Owner.UserName, invoice.Payer.UserName, stripeCustomerId);

                        var cardService = new StripeCardService(apiKey);
                        IEnumerable<StripeCard> stripeCards = cardService.List(stripeCustomerId);
                        selectedStripeCardId = stripeCards.First().Id;
                    }
                    else if (saveNewCard)
                    {
                        //customer records exists, add card to it
                        var myCard = new StripeCardCreateOptions();

                        myCard.SourceToken = stripeToken;

                        var cardService = new StripeCardService(apiKey);
                        StripeCard stripeCard = cardService.Create(stripeCustomerId, myCard); // optional isRecipient
                        selectedStripeCardId = stripeCard.Id;
                    }

                    //create the charge!
                    var myCharge = new StripeChargeCreateOptions();

                    // always set these properties
                    if (zeroDecimalCurrencies.Contains(invoice.Currency.ToUpper()))
                    {
                        myCharge.Amount = Convert.ToInt32(invoice.Total);
                    }
                    else
                    {
                        myCharge.Amount = Convert.ToInt32(invoice.Total * 100);
                    }
                    myCharge.Currency = invoice.Currency.ToLower();

                    // set this if you want to
                    myCharge.Description = string.Format("Invoice #{0}", invoice.ID);
                    myCharge.StatementDescriptor = SiteProperties.FriendlySiteName;

                    if (!string.IsNullOrWhiteSpace(selectedStripeCardId))
                    {
                        //charge the card using the customerId and cardId
                        myCharge.SourceTokenOrExistingSourceId = selectedStripeCardId;
                        myCharge.CustomerId = stripeCustomerId;
                    }
                    else
                    {
                        //charge the card directly from the token because this card will not be saved
                        myCharge.SourceTokenOrExistingSourceId = stripeToken;
                    }

                    // set this if you have your own application fees (you must have your application configured first within Stripe)
                    //myCharge.ApplicationFee = 25;

                    // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
                    myCharge.Capture = true;

                    //add metadata
                    var metaData = new Dictionary<string, string>();
                    metaData.Add("InvoiceId", invoice.ID.ToString());
                    myCharge.Metadata = metaData;

                    var chargeService = new StripeChargeService(apiKey);
                    StripeCharge stripeCharge = chargeService.Create(myCharge);

                    //process the response from stripe...
                    var x = stripeCharge.StripeResponse;
                    string x01 = x.ObjectJson;
                    DateTime x02 = x.RequestDate;
                    string x03 = x.RequestId;
                    string x04 = x.ResponseJson;
                    
                    var y = stripeCharge;
                    int y01 = y.Amount;
                    int y02 = y.AmountRefunded;
                    StripeApplicationFee y03 = y.ApplicationFee;
                    string y04 = y.ApplicationFeeId;
                    StripeBalanceTransaction y05 = y.BalanceTransaction;
                    string y06 = y.BalanceTransactionId;
                    bool? y07 = y.Captured;
                    DateTime y08 = y.Created;
                    string y09 = y.Currency;
                    StripeCustomer y10 = y.Customer;
                    string y11 = y.CustomerId;
                    string y12 = y.Description;
                    StripeAccount y13 = y.Destination;
                    string y14 = y.DestinationId;
                    StripeDispute y15 = y.Dispute;
                    string y16 = y.FailureCode;
                    string y17 = y.FailureMessage;
                    Dictionary<string, string> y18 = y.FraudDetails;
                    string y19 = y.Id;
                    StripeInvoice y20 = y.Invoice;
                    string y21 = y.InvoiceId;
                    bool y22 = y.LiveMode;
                    Dictionary<string, string> y23 = y.Metadata;
                    string y24 = y.Object;
                    bool y25 = y.Paid;
                    string y26 = y.ReceiptEmail;
                    string y27 = y.ReceiptNumber;
                    bool y28 = y.Refunded;
                    StripeList<StripeRefund> y29 = y.Refunds;
                    StripeReview y30 = y.Review;
                    string y31 = y.ReviewId;
                    StripeShipping y32 = y.Shipping;
                    Source y33 = y.Source;
                    string y34 = y.StatementDescriptor;
                    string y35 = y.Status;
                    StripeResponse y36 = y.StripeResponse;
                    StripeTransfer y37 = y.Transfer;
                    string y38 = y.TransferId;

                    //all fields
                    response.AllFields = new Dictionary<string, string>();
                    response.AllFields.Add("ResponseJson", stripeCharge.StripeResponse.ResponseJson);
                    if (zeroDecimalCurrencies.Contains(invoice.Currency.ToUpper()))
                    {
                        response.Amount = Convert.ToDecimal(stripeCharge.Amount);
                    }
                    else
                    {
                        response.Amount = Convert.ToDecimal(stripeCharge.Amount) / 100;
                    }
                    
                    response.Approved = stripeCharge.Paid; //(stripeCharge.Status == "succeeded"); //"succeeded", "pending", or "failed"

                    //response.AuthorizationCode = GetResponseField(responseFields, ResponseField.AuthorizationCode);
                    //response.AvsResponse = EnumHelpers.ParseAvsResponseCode(GetResponseField(responseFields, ResponseField.AvsResponse));
                    //response.BuyerAddress = new Address();
                    //response.BuyerAddress.Street1 = GetResponseField(responseFields, ResponseField.Address);
                    //response.BuyerAddress.City = GetResponseField(responseFields, ResponseField.City);
                    ////response.BuyerAddress.Country = GetResponseField(responseFields, ResponseField.Country);
                    //response.BuyerAddress.StateRegion = GetResponseField(responseFields, ResponseField.State);
                    //response.BuyerAddress.ZipPostal = GetResponseField(responseFields, ResponseField.Zip);
                    //response.BuyerEmail = GetResponseField(responseFields, ResponseField.EmailAddress);
                    //response.CavResponse = EnumHelpers.ParseCavResponseCode(GetResponseField(responseFields, ResponseField.CardholderAuthenticationVerificationResponse));
                    //response.Cvv2Response = EnumHelpers.ParseCvv2ResponseCode(GetResponseField(responseFields, ResponseField.CardCodeResponse));

                    ////string tempStr = GetResponseField(responseFields, ResponseField.InvoiceNumber);
                    ////int tempInt;
                    ////if (int.TryParse(tempStr, out tempInt))
                    ////    response.InvoiceNumber = Convert.ToInt32(tempInt);

                    response.InvoiceNumber = invoice.ID;

                    if (stripeCharge.Source.Type == SourceType.Card)
                    {
                        response.PaymentMethod = PaymentMethod.CreditCard;
                    }
                    else if (stripeCharge.Source.Type == SourceType.BankAccount)
                    {
                        response.PaymentMethod = PaymentMethod.ECheck;
                    }
                    else
                    {
                        response.PaymentMethod = PaymentMethod.Unknown;
                    }

                    response.ProviderIdentifier = Name;

                    response.RawResponseCode = stripeCharge.FailureCode;
                    response.ResponseDescription = stripeCharge.Status;

                    ////synchronous provider - no IPN

                    if (stripeCharge.Status == "succeeded")
                    {
                        response.ResponseStatus = PaymentProviderResponseStatus.Complete;
                    }
                    else if (stripeCharge.Status == "pending")
                    {
                        response.ResponseStatus = PaymentProviderResponseStatus.PendingProvider;
                    }

                    response.Timestamp = stripeCharge.Created;
                    response.TransactionId = stripeCharge.Id;

                    if (!response.Approved)
                    {
                        //log additional details in case of errors
                        LogManager.WriteLog("Provider Response Details", "Payment Not Approved", "Stripe", TraceEventType.Information, null, null,
                            new Dictionary<string, object>() {
                                { "Response Data", stripeCharge.StripeResponse.ResponseJson.Replace("{", "@'").Replace("}", "'@").Replace("\"", "''") }
                            });
                    }

                    ProcessPaymentResponse(invoice.Payer.UserName, invoice, response);
                    return response;
                }
                catch (StripeException se)
                {
                    LogManager.WriteLog(se.Message, "Payment Not Approved", "Stripe", TraceEventType.Warning, null, null,
                        paymentParameters.Items.ToDictionary(kvp => kvp.Key, kvp => (object)kvp.Value));
                    response.Approved = false;
                    response.ResponseDescription = se.Message;
                    return response;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return null;
#if TRACE
            }
#endif
        }

        public override bool IsThisMyIpn(UserInput request)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    return false;
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return false;
#if TRACE
            }
#endif
        }

        public override PaymentProviderResponse ProcessIpn(UserInput request)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    throw new InvalidOperationException();
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return null;
#if TRACE
            }
#endif
        }

        private void ProcessPaymentResponse(string actingUserName, Invoice invoice, PaymentProviderResponse paymentResponse)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        _data.AddPaymentResponse(actingUserName, paymentResponse);
                        if (paymentResponse.Approved)
                        {
                            _data.SetInvoicePaid(actingUserName, invoice.ID, true);

                            if (invoice.Type == Strings.InvoiceTypes.Fee)
                            {
                                _data.ActivateListingsAwaitingPayment(invoice.ID);
                            }
                        }
                        else if (paymentResponse.ResponseStatus == PaymentProviderResponseStatus.PendingProvider || paymentResponse.ResponseStatus == PaymentProviderResponseStatus.Pending)
                        {
                            _data.SetInvoiceStatus(actingUserName, invoice.ID, Strings.InvoiceStatuses.Pending);
                        }
                        scope.Complete();
                    }
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
#if TRACE
            }
#endif
        }

        #endregion
    }
}
