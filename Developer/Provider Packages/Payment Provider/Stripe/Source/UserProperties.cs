﻿using System;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.Stripe
{

    internal static class UserProperties {

		public class Keys {
            public const string SellerAccountConnected = "StripeConnect_SellerAccountConnected";
            public const string SellerSecretApiKey = "StripeConnect_SellerSecretApiKey";
            public const string SellerPublishableApiKey = "StripeConnect_SellerPublishableApiKey";
            public const string SellerUserId = "StripeConnect_SellerUserId";
        }

    }

}