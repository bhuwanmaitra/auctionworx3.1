using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System;
using System.Web.WebPages;
using RainWorx.FrameWorx.Clients;
using System.Text;
using System.Linq;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Strings;
using Stripe;
using System.Diagnostics;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.MVC.Helpers
{
    /// <summary>
    /// Provides methods for accessing stripe data
    /// </summary>
    public static class StripeHelpers
    {

        #region stripe helpers

        /// <summary>
        /// Calls out to stripe.com API to retrieve saved cards available for the specified invoice
        /// </summary>
        /// <param name="htmlHelper">an instance of the HtmlHelper class</param>
        /// <param name="invoice">the specified DTO.invoice object</param>
        /// <returns></returns>
        public static IEnumerable<StripeCard> GetStripeCards(this HtmlHelper htmlHelper, Invoice invoice)
        {
            string apiKey;
            string stripeCustomerId = null;
            if (invoice.Type == InvoiceTypes.Fee)
            {
                stripeCustomerId = AccountingClient.GetStripeCustomerId(htmlHelper.ViewContext.HttpContext.User.Identity.Name, null, invoice.Payer.UserName);
                apiKey = SiteClient.TextSetting(SiteProperties.StripeConnect_SiteFeesSecretApiKey);
            }
            else
            {
                stripeCustomerId = AccountingClient.GetStripeCustomerId(htmlHelper.ViewContext.HttpContext.User.Identity.Name, invoice.Owner.UserName, invoice.Payer.UserName);
                apiKey = invoice.Owner.Properties.Single(p => p.Field.Name == StdUserProps.StripeConnect_SellerSecretApiKey).Value;
                if (!string.IsNullOrWhiteSpace(apiKey))
                {
                    apiKey = Utilities.DecryptString(apiKey);
                }
            }
            if (string.IsNullOrWhiteSpace(stripeCustomerId))
            {
                return new List<StripeCard>();
            }
            try
            {
                var cardService = new StripeCardService(apiKey);
                return cardService.List(stripeCustomerId).ToList();
            }
            catch (Exception e)
            {
                LogManager.WriteLog(null, "Error Retrieving Cards", "Stripe", TraceEventType.Warning, htmlHelper.ViewContext.HttpContext.User.Identity.Name, e,
                    new Dictionary<string, object>() {
                        { "invoiceId", invoice == null ? -1 : invoice.ID },
                        { "User", invoice == null ? null : invoice.Payer.UserName },
                        { "stripeCustomerId", stripeCustomerId}
                    });
                return new List<StripeCard>();
            }
        }

        /// <summary>
        /// Calls out to stripe.com API to retrieve saved cards available for the specified invoice
        /// </summary>
        /// <param name="controller">an instance of the Controller class</param>
        /// <param name="invoice">the specified DTO.invoice object</param>
        /// <returns></returns>
        public static IEnumerable<StripeCard> GetStripeCards(this Controller controller, Invoice invoice)
        {
            string apiKey;
            string stripeCustomerId;
            if (invoice.Type == InvoiceTypes.Fee)
            {
                stripeCustomerId = AccountingClient.GetStripeCustomerId(controller.User.Identity.Name, null, invoice.Payer.UserName);
                apiKey = SiteClient.TextSetting(SiteProperties.StripeConnect_SiteFeesSecretApiKey);
            }
            else
            {
                stripeCustomerId = AccountingClient.GetStripeCustomerId(controller.User.Identity.Name, invoice.Owner.UserName, invoice.Payer.UserName);
                apiKey = invoice.Owner.Properties.Single(p => p.Field.Name == StdUserProps.StripeConnect_SellerSecretApiKey).Value;
                if (!string.IsNullOrWhiteSpace(apiKey))
                {
                    apiKey = Utilities.DecryptString(apiKey);
                }
            }
            if (string.IsNullOrWhiteSpace(stripeCustomerId))
            {
                return new List<StripeCard>();
            }
            var cardService = new StripeCardService(apiKey);
            return cardService.List(stripeCustomerId).ToList();
        }

        /// <summary>
        /// Generates a new stripe customer id using the site fees api key from the specified stripe token and associates it with the specified username
        /// </summary>
        /// <param name="controller">an instance of the Controller class</param>
        /// <param name="userName">the specified username</param>
        /// <param name="stripeToken">the specified stripe token</param>
        public static void SaveStripeCard(this Controller controller, string userName, string stripeToken)
        {
            var user = UserClient.GetUserByUserName(userName, userName);

            string apiKey;
            apiKey = SiteClient.TextSetting(SiteProperties.StripeConnect_SiteFeesSecretApiKey);

            //saving first card, create custromer record
            var myCustomer = new StripeCustomerCreateOptions();
            myCustomer.Email = user.Email;
            myCustomer.Description = string.Format("{0} {1} ({2})", user.FirstName(), user.LastName(), user.Email);

            myCustomer.SourceToken = stripeToken;

            //myCustomer.PlanId = *planId *;                          // only if you have a plan
            //myCustomer.TaxPercent = 20;                            // only if you are passing a plan, this tax percent will be added to the price.
            //myCustomer.Coupon = *couponId *;                        // only if you have a coupon
            //myCustomer.TrialEnd = DateTime.UtcNow.AddMonths(1);    // when the customers trial ends (overrides the plan if applicable)
            //myCustomer.Quantity = 1;                               // optional, defaults to 1

            var customerService = new StripeCustomerService(apiKey);
            StripeCustomer stripeCustomer = customerService.Create(myCustomer);
            string stripeCustomerId = stripeCustomer.Id;
            AccountingClient.SetStripeCustomerId(userName, null, userName, stripeCustomerId);
        }

        #endregion stripe helpers

    }
}
