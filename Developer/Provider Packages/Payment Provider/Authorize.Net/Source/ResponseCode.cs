﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.AuthorizeNet {
	
	internal enum ResponseCode {

		Approved = 1,
		Declined = 2,
		Error = 3,
		HeldForReview = 4

	}

}