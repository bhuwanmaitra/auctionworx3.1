﻿using System;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.AuthorizeNet {
	
	internal static class SiteProperties {

		public class Keys {
			public const string PostUrl = "AuthorizeNet_PostUrl";
			public const string MerchantLoginId = "AuthorizeNet_MerchantLoginID";
			public const string TransactionKey = "AuthorizeNet_TransactionKey";
			public const string Enabled = "AuthorizeNet_Enabled";
			public const string TestMode = "AuthorizeNet_TestMode";
		    public const string DemoMode = "DemoEnabled";
		    public const string PaymentDescription = "AuthorizeNet_PaymentDescription";
		}

		public static string PostUrl {
			get {
				return Cache.SiteProperties[Keys.PostUrl];
			}
		}

	    public static string PaymentDescription
	    {
	        get { return Cache.SiteProperties[Keys.PaymentDescription]; }
	    }

		public static string MerchantLoginId {
			get {
				return Cache.SiteProperties[Keys.MerchantLoginId];
			}
		}

		public static string TransactionKey {
			get {
				return Cache.SiteProperties[Keys.TransactionKey];
			}
		}

		public static bool Enabled {
			get {
				return Convert.ToBoolean(Cache.SiteProperties[Keys.Enabled]);
			}
		}

		public static bool TestMode {
			get {
				return Convert.ToBoolean(Cache.SiteProperties[Keys.TestMode]);
			}
		}

        public static bool DemoMode
        {
            get
            {
                return Convert.ToBoolean(Cache.SiteProperties[Keys.DemoMode]);
            }
        }

	}

}