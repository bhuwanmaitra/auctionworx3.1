﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.AuthorizeNet {

	internal enum ResponseField {

		ResponseCode = 0,
		ResponseSubcode,
		ResponseReasonCode,
		ResponseReasonText,
		AuthorizationCode,
		AvsResponse,
		TransactionId,
		InvoiceNumber,
		Description,
		Amount,
		Method,
		TransactionType,
		CustomerId,
		FirstName,
		LastName,
		Company,
		Address,
		City,
		State,
		Zip,
		Country,
		Phone,
		Fax,
		EmailAddress,
		ShipToFirstName,
		ShipToLastName,
		ShipToCompany,
		ShipToAddress,
		ShipToCity,
		ShipToState,
		ShipToZip,
		ShipToCountry,
		Tax,
		Duty,
		Freight,
		TaxExempt,
		PurchaseOrderNumber,
		MD5Hash,
		CardCodeResponse,
		CardholderAuthenticationVerificationResponse

	}

}