﻿using System;
using System.Diagnostics;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Strings;
using System.Transactions;
using System.IO;
using System.Web;
using RainWorx.FrameWorx.Filters;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.AuthorizeNet
{

    //AIM = "Advanced Integration Method"
    public class AuthorizeNetAim : PaymentProviderBase
    {

        #region Private members

        //constants used for registration
        private const string Name = "AuthorizeNetAIM";
        private const string RegistratorName = "AuthorizeNetAIMRegistrar";

        #endregion

        #region Constructors

        public AuthorizeNetAim(IDataContext data) : base(data)
        {
        }

        #endregion

        #region Helper methods

        private string GetResponseField(string[] allFields, ResponseField targetField)
        {
            int fieldIndex = (int)targetField;
            string result = string.Empty;
            if (allFields.Length > fieldIndex)
            {
                result = allFields[fieldIndex];
            }
            return result;
        }

        private string GetResponseDescription(ResponseCode responseCode, string responseReasonCode, AvsResponseCode avsResponseCode, CavResponseCode cavResponseCode, Cvv2ResponseCode cvv2ResponseCode)
        {
            int reason = 0;
            if (!string.IsNullOrEmpty(responseReasonCode))
            {
                reason = Convert.ToInt32(responseReasonCode);
            }
            switch (responseCode)
            {
                case ResponseCode.Approved:
                    {
                        return ResponseDescriptions.Completed;
                    }
                case ResponseCode.HeldForReview:
                    {
                        return ResponseDescriptions.Pending.HeldForReview;
                    }
                case ResponseCode.Declined:
                case ResponseCode.Error:
                default:
                    {
                        switch (reason)
                        {
                            case 6:
                                {
                                    return ResponseDescriptions.Declined.InvalidCardNumber;
                                }
                            case 7:
                                {
                                    return ResponseDescriptions.Declined.InvalidExpirationDate;
                                }
                            case 8:
                                {
                                    return ResponseDescriptions.Declined.ExpiredCard;
                                }
                            case 9:
                                {
                                    return ResponseDescriptions.Declined.InvalidAbaCode;
                                }
                            case 10:
                                {
                                    return ResponseDescriptions.Declined.InvalidAccountNumber;
                                }
                            case 11:
                                {
                                    return ResponseDescriptions.Declined.DuplicateTransaction;
                                }
                            case 12:
                                {
                                    return ResponseDescriptions.Declined.MissingAuthorizationCode;
                                }
                            case 44:
                                {
                                    return ResponseDescriptions.Declined.InvalidCardCode;
                                }
                            case 45:
                                {
                                    return ResponseDescriptions.Declined.AvsRejection;
                                }
                            default:
                                {
                                    return ResponseDescriptions.Declined.GenericDeclined;
                                }
                        }
                    }
            }
        }

        #endregion

        #region IPaymentProvider Members

        public override BatchPaymentCapability PrepareBatchPayment(Invoice invoice, out PaymentParameters paymentParameters, out string details)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                paymentParameters = null;
                details = string.Empty;

                try
                {
                    //check if this provider is enabled
                    if (!Enabled)
                    {
                        details = "BatchPaymentProviderDisabled";
                        return BatchPaymentCapability.Disabled;
                    }

                    //check if this provider can pay the specified invoice
                    if (invoice.Type != Strings.InvoiceTypes.Fee)
                    {
                        details = "NotFeeInvoice";
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    if (string.IsNullOrEmpty(invoice.BillingStreet1))
                    {
                        details = "BillingAddressMissing";
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    ReasonCode reason;
                    if (!CanPayInvoice(invoice, out reason))
                    {
                        details = Enum.GetName(reason.GetType(), reason);
                        return BatchPaymentCapability.InvalidInvoice;
                    }

                    //check if this provider's parameters are all set
                    //if (SiteProperties.TestMode)
                    //{
                    //    details = "TestModeOn";
                    //    return BatchPaymentCapability.InvalidProviderParameters;
                    //}
                    if (SiteProperties.DemoMode)
                    {
                        details = "DemoModeOn";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    if (string.IsNullOrEmpty(SiteProperties.MerchantLoginId))
                    {
                        details = "MerchantLoginIdMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    if (string.IsNullOrEmpty(SiteProperties.PostUrl))
                    {
                        details = "PostUrlMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }
                    if (string.IsNullOrEmpty(SiteProperties.TransactionKey))
                    {
                        details = "TransactionKeyMissing";
                        return BatchPaymentCapability.InvalidProviderParameters;
                    }

                    //check the payer's parameters
                    User payer = invoice.Payer;
                    if (!payer.BillingCreditCardID.HasValue)
                    {
                        details = "NoPrimaryCreditCard";
                        return BatchPaymentCapability.MissingPaymentParameters;
                    }

                    int primaryCCID = payer.BillingCreditCardID.Value;
                    List<CreditCard> creditCards = _data.GetCreditCardsForUser(payer);
                    CreditCard primaryCC = creditCards.SingleOrDefault(cc => cc.ID == primaryCCID);
                    if (primaryCC == null)
                    {
                        details = "NoPrimaryCreditCard";
                        return BatchPaymentCapability.MissingPaymentParameters;
                    }

                    DateTime ccExpiration = new DateTime(primaryCC.ExpirationYear, primaryCC.ExpirationMonth, 1, 0, 0, 0).AddMonths(1);
                    if (DateTime.UtcNow > ccExpiration)
                    {
                        details = "PrimaryCreditCardExpired";
                        return BatchPaymentCapability.Expired;
                    }

                    paymentParameters = new PaymentParameters();
                    paymentParameters.Items.Add(Strings.Fields.SelectedCreditCardId, primaryCC.ID.ToString());
                    paymentParameters.PayerIPAddress = "127.0.0.1";

                }
                catch (Exception e)
                {
                    details = e.Message;
                    return BatchPaymentCapability.Unknown;
                }

                return BatchPaymentCapability.Valid;
#if TRACE
            }
#endif
        }

        public override bool RegisterSelf()
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false;
                try
                {
                    using (TransactionScope scope = TransactionUtils.CreateTransactionScope())
                    {
                        if (_data.CreatePaymentProvider(RegistratorName, Name, true))
                        {
                            retVal = true;

                            //add the AuthorizeNetAim provider to the PaymentProviders navigational category in the Admin Control Panel
                            //This navigational category only renders if the enabledCustomProperty (SiteProperties.Keys.Enabled => "AuthorizeNet_Enabled") is true
                            Category category = AddPaymentProviderAdminNavigation(RegistratorName, Name, 20, SiteProperties.Keys.Enabled);

                            //Add the following properties to the new navigational category (category.ID)
                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.PostUrl, CustomFieldType.String,
                                                              "https://test.authorize.net/gateway/transact.dll", 0, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.MerchantLoginId, CustomFieldType.String,
                                                              "Enter Merchant Login ID", 1, true, category.ID, encrypted: true);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.TransactionKey, CustomFieldType.String,
                                                              "Enter Transaction Key", 2, true, category.ID, encrypted: true);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.PaymentDescription, CustomFieldType.String,
                                                              "Enter Payment Description", 3, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.TestMode, CustomFieldType.Boolean,
                                                              "True", 4, true, category.ID);

                            //Add the following property to the parent navigational category (category.ParentCategoryID.Value)
                            //this allows us to turn the provider and the provider's navigational category (created above) on and off
                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.Enabled, CustomFieldType.Boolean,
                                                              "True", 20, true, category.ParentCategoryID.Value);
                        }
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return retVal;
#if TRACE
            }
#endif
        }

        public override bool Enabled
        {
            get
            {
                return SiteProperties.Enabled;
            }
        }

        public override string ProviderName
        {
            get
            {
                return Name;
            }
        }

        public override bool CanPayInvoice(Invoice invoice, out ReasonCode reason)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //allowed currency list last updated 2016-02-09, per http://www.authorize.net/content/dam/authorize/documents/AIM_guide.pdf  pp. 16
                var allowedCurrencies = new string[] { "USD", "CAD", "CHF", "DKK", "EUR", "GBP", "NOK", "PLN", "SEK", "ZAR" };
                if (allowedCurrencies.Contains(invoice.Currency.ToUpper()))
                {
                    if (invoice.Type != InvoiceTypes.Fee)
                    {
                        if (invoice.Owner.CreditCardAccepted())
                        {
                            reason = ReasonCode.Success;
                            return true;
                        }
                        else
                        {
                            reason = ReasonCode.DisabledBySeller;
                            return false;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(SiteProperties.MerchantLoginId))
                        {
                            reason = ReasonCode.Success;
                            return true;
                        }
                        else
                        {
                            reason = ReasonCode.InvoiceTypeNotSupported;
                            return false;
                        }
                    }
                }
                else
                {
                    reason = ReasonCode.CurrencyNotSupported;
                    return false;
                }
#if TRACE
            }
#endif
        }

        public override PaymentProviderResponse ProcessPayment(Invoice invoice, PaymentParameters paymentParameters)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    //parse payment parameters
                    if (paymentParameters.CreditCard == null)
                    {
                        if (paymentParameters.Items.ContainsKey(Strings.Fields.SelectedCreditCardId) && paymentParameters.Items[Strings.Fields.SelectedCreditCardId] != "0")
                        {
                            List<CreditCard> creditCards = _data.GetCreditCardsForUser(invoice.Payer);
                            paymentParameters.CreditCard = creditCards.Where(cc => cc.ID.Equals(Convert.ToInt32(paymentParameters.Items[Strings.Fields.SelectedCreditCardId]))).SingleOrDefault();
                        }
                        else {
                            //new credit card
                            paymentParameters.CreditCard = new CreditCard();
                            paymentParameters.CreditCard.CardNumber = UnityResolver.Get<IEncryptor>("creditcardEncryptor").Encrypt(paymentParameters.Items[Strings.Fields.CardNumber]);
                            int month;
                            if (int.TryParse(paymentParameters.Items[Strings.Fields.ExpirationMonth], out month))
                            {
                                paymentParameters.CreditCard.ExpirationMonth = month;
                            }
                            int year;
                            if (int.TryParse(paymentParameters.Items[Strings.Fields.ExpirationYear], out year))
                            {
                                paymentParameters.CreditCard.ExpirationYear = year;
                            }
                        }
                    }
                    paymentParameters.PaymentMethod = PaymentMethod.CreditCard;

                    string loginId = null;
                    string transactionKey = null;

                    if (invoice.Type == InvoiceTypes.Fee)
                    {
                        loginId = SiteProperties.MerchantLoginId;        // "9faQV68kR"
                        transactionKey = SiteProperties.TransactionKey;  // "78d8C2eqA79mhB3U";
                    }
                    else
                    {
                        //first attempt to retrieve the seller's auth.net credentials
                        var loginProp = invoice.Owner.Properties.FirstOrDefault(p => p.Field.Name == StdUserProps.AuthorizeNet_SellerMerchantLoginID);
                        loginId = loginProp != null ? loginProp.Value : null;
                        if (!string.IsNullOrEmpty(loginId))
                        {
                            loginId = UnityResolver.Get<IEncryptor>("customFieldEncryptor").Decrypt(loginId);
                        }
                        var txnKeyProp = invoice.Owner.Properties.FirstOrDefault(p => p.Field.Name == StdUserProps.AuthorizeNet_SellerTransactionKey);
                        transactionKey = txnKeyProp != null ? txnKeyProp.Value : null;
                        if (!string.IsNullOrEmpty(transactionKey))
                        {
                            transactionKey = UnityResolver.Get<IEncryptor>("customFieldEncryptor").Decrypt(transactionKey);
                        }

                        //if user auth.net credentials are missing and the user is an admin, try to use site fee credentials, for backwards compatibility
                        if (string.IsNullOrEmpty(loginId) && string.IsNullOrEmpty(transactionKey) && 
                            invoice.Owner.Roles.Exists(r => r.Name.Equals("Admin", StringComparison.OrdinalIgnoreCase)))
                        {
                            loginId = SiteProperties.MerchantLoginId;
                            transactionKey = SiteProperties.TransactionKey;
                        }
                    }

                    string postUrl = SiteProperties.PostUrl;                // TestPostUrl;
                    Dictionary<string, string> formValues = new Dictionary<string, string>();
                    formValues.Add("x_login", loginId);
                    formValues.Add("x_tran_key", transactionKey);
                    if (SiteProperties.TestMode || SiteProperties.DemoMode)
                    {
                        formValues.Add("x_test_request", "TRUE");
                    }
                    formValues.Add("x_market_type", "0");
                    formValues.Add("x_delim_data", "TRUE");
                    formValues.Add("x_delim_char", "|");
                    formValues.Add("x_relay_response", "FALSE");
                    //invoice info
                    formValues.Add("x_amount", invoice.Total.ToString());
                    formValues.Add("x_currency_code", invoice.Currency);
                    formValues.Add("x_invoice_num", invoice.ID.ToString());
                    //TODO: description
                    formValues.Add("x_description", SiteProperties.PaymentDescription);
                    formValues.Add("x_type", "AUTH_CAPTURE");
                    formValues.Add("x_method", "CC");
                    //decrypt the credit card number
                    formValues.Add("x_card_num", UnityResolver.Get<IEncryptor>("creditcardEncryptor").Decrypt(paymentParameters.CreditCard.CardNumber));
                    formValues.Add("x_exp_date", string.Format("{0:00}-{1}", paymentParameters.CreditCard.ExpirationMonth, paymentParameters.CreditCard.ExpirationYear));
                    //verification code
                    if (paymentParameters.Items.ContainsKey(Strings.Fields.VerificationCode))
                    {
                        formValues.Add("x_card_code", paymentParameters.Items[Strings.Fields.VerificationCode]);
                    }
                    //billing name and address
                    paymentParameters.PayerFirstName = invoice.BillingFirstName;
                    paymentParameters.PayerLastName = invoice.BillingLastName;
                    formValues.Add("x_first_name", paymentParameters.PayerFirstName);   // paymentParameters.Items["firstname"]);
                    formValues.Add("x_last_name", paymentParameters.PayerLastName);     // paymentParameters.Items["lastname"]);
                    formValues.Add("x_address", invoice.BillingStreet1);                // paymentParameters.Items["address"]);
                    formValues.Add("x_city", invoice.BillingCity);                      // paymentParameters.Items["city"]);
                    formValues.Add("x_state", invoice.BillingStateRegion);              // paymentParameters.Items["state"]);
                    formValues.Add("x_zip", invoice.BillingZipPostal);                  // paymentParameters.Items["zip"]);
                    formValues.Add("x_country", invoice.BillingCountry);
                    //ip address
                    formValues.Add("x_customer_ip", paymentParameters.PayerIPAddress.ToString());
                    //shipping name and address for non-fee invoices
                    if (invoice.Type == InvoiceTypes.Shipping)
                    {
                        formValues.Add("x_ship_to_first_name", invoice.ShippingFirstName);
                        formValues.Add("x_ship_to_last_name", invoice.ShippingLastName);
                        formValues.Add("x_ship_to_address", invoice.ShippingStreet1);
                        formValues.Add("x_ship_to_city", invoice.ShippingCity);
                        formValues.Add("x_ship_to_state", invoice.ShippingStateRegion);
                        formValues.Add("x_ship_to_zip", invoice.ShippingZipPostal);
                        formValues.Add("x_ship_to_country", invoice.ShippingCountry);
                    }
                    //customer info
                    formValues.Add("x_cust_id", invoice.Payer.UserName);
                    formValues.Add("x_email", invoice.Payer.Email);
                    //TODO: phone?
                    //formValues.Add("x_phone", );
                    StringBuilder builder = new StringBuilder();
                    foreach (KeyValuePair<string, string> entry in formValues)
                    {
                        builder.Append(entry.Key);
                        builder.Append("=");
                        builder.Append(HttpUtility.UrlEncode(entry.Value));
                        builder.Append("&");
                    }
                    string postData = builder.ToString();
                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(postUrl);
                    webRequest.Method = "POST";
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    webRequest.ContentLength = postData.Length;
                    using (StreamWriter writer = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        writer.Write(postData);
                    }
                    HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                    string responseData;
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseData = reader.ReadToEnd();
                    }
                    string[] responseFields = responseData.Split('|');
                    PaymentProviderResponse response = new PaymentProviderResponse();
                    //all fields
                    response.AllFields = new Dictionary<string, string>();
                    Array enumValues = Enum.GetValues(typeof(ResponseField));
                    for (int i = 0; i < responseFields.Length; ++i)
                    {
                        if (i < enumValues.Length)
                        {
                            response.AllFields.Add(((ResponseField)i).ToString(), responseFields[i]);
                        }
                        else {
                            response.AllFields.Add(string.Format("Field_{0}", i), responseFields[i]);
                        }
                    }
                    ResponseCode responseCode = (ResponseCode)Convert.ToInt32(GetResponseField(responseFields, ResponseField.ResponseCode));
                    response.Amount = Convert.ToDecimal(GetResponseField(responseFields, ResponseField.Amount));
                    response.Approved = responseCode == ResponseCode.Approved;
                    response.AuthorizationCode = GetResponseField(responseFields, ResponseField.AuthorizationCode);
                    response.AvsResponse = EnumHelpers.ParseAvsResponseCode(GetResponseField(responseFields, ResponseField.AvsResponse));
                    response.BuyerAddress = new Address();
                    response.BuyerAddress.Street1 = GetResponseField(responseFields, ResponseField.Address);
                    response.BuyerAddress.City = GetResponseField(responseFields, ResponseField.City);
                    //response.BuyerAddress.Country = GetResponseField(responseFields, ResponseField.Country);
                    response.BuyerAddress.StateRegion = GetResponseField(responseFields, ResponseField.State);
                    response.BuyerAddress.ZipPostal = GetResponseField(responseFields, ResponseField.Zip);
                    response.BuyerEmail = GetResponseField(responseFields, ResponseField.EmailAddress);
                    response.CavResponse = EnumHelpers.ParseCavResponseCode(GetResponseField(responseFields, ResponseField.CardholderAuthenticationVerificationResponse));
                    response.Cvv2Response = EnumHelpers.ParseCvv2ResponseCode(GetResponseField(responseFields, ResponseField.CardCodeResponse));

                    //string tempStr = GetResponseField(responseFields, ResponseField.InvoiceNumber);
                    //int tempInt;
                    //if (int.TryParse(tempStr, out tempInt))
                    //    response.InvoiceNumber = Convert.ToInt32(tempInt);
                    response.InvoiceNumber = invoice.ID;

                    response.PaymentMethod = EnumHelpers.ParsePaymentMethod(GetResponseField(responseFields, ResponseField.Method));
                    response.ProviderIdentifier = Name;
                    response.RawResponseCode = GetResponseField(responseFields, ResponseField.ResponseCode);
                    response.ResponseDescription = GetResponseDescription(responseCode, GetResponseField(responseFields, ResponseField.ResponseReasonCode), response.AvsResponse, response.CavResponse, response.Cvv2Response);
                    //GetResponseField(responseFields, ResponseField.ResponseReasonText);
                    //synchronous provider - no IPN
                    response.ResponseStatus = PaymentProviderResponseStatus.Complete;
                    response.Timestamp = DateTime.UtcNow;
                    response.TransactionId = GetResponseField(responseFields, ResponseField.TransactionId);

                    if (!response.Approved)
                    {
                        //log additional details in case of errors
                        //LogManager.WriteLog("Response Details", "Request Not Approved", "Authorize.Net Provider", TraceEventType.Information, null, null,
                        //    response.AllFields.ToDictionary(pair => pair.Key, pair => (object)pair.Value));
                        LogManager.WriteLog("Provider Response Details", "Payment Not Approved", FunctionalAreas.Accounting, TraceEventType.Information, null, null,
                            new Dictionary<string, object>() { { "Response Data", responseData } });
                    }

                    ProcessPaymentResponse(invoice.Payer.UserName, invoice, response);
                    return response;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return null;
#if TRACE
            }
#endif
        }

        public override bool IsThisMyIpn(UserInput request)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    return false;
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return false;
#if TRACE
            }
#endif
        }

        public override PaymentProviderResponse ProcessIpn(UserInput request)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    throw new InvalidOperationException();
                }
                catch (Exception ex)
                {
                    if (LogManager.HandleException(ex, Strings.FunctionalAreas.Accounting))
                    {
                        throw;
                    }
                }
                return null;
#if TRACE
            }
#endif
        }

        private void ProcessPaymentResponse(string actingUserName, Invoice invoice, PaymentProviderResponse paymentResponse)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        _data.AddPaymentResponse(actingUserName, paymentResponse);
                        if (paymentResponse.Approved)
                        {
                            _data.SetInvoicePaid(actingUserName, invoice.ID, true);

                            if (invoice.Type == Strings.InvoiceTypes.Fee)
                            {
                                _data.ActivateListingsAwaitingPayment(invoice.ID);
                            }
                        }
                        else if (paymentResponse.ResponseStatus == PaymentProviderResponseStatus.PendingProvider || paymentResponse.ResponseStatus == PaymentProviderResponseStatus.Pending)
                        {
                            _data.SetInvoiceStatus(actingUserName, invoice.ID, Strings.InvoiceStatuses.Pending);
                        }
                        scope.Complete();
                    }
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
#if TRACE
            }
#endif
        }

        #endregion
    }

}