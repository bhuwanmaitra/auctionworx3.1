﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Payment.AuthorizeNet {

	internal static class EnumHelpers {

		#region Private members

		private static readonly Dictionary<string, AvsResponseCode> sm_dicAvsResponseCodes = new Dictionary<string, AvsResponseCode>();
		private static readonly Dictionary<string, CavResponseCode> sm_dicCavResponseCodes = new Dictionary<string, CavResponseCode>();
		private static readonly Dictionary<string, Cvv2ResponseCode> sm_dicCvv2ResponseCodes = new Dictionary<string, Cvv2ResponseCode>();

		#endregion

		#region Constructors

		static EnumHelpers() {
			//avs response codes
			sm_dicAvsResponseCodes.Add("A", AvsResponseCode.Address);
			sm_dicAvsResponseCodes.Add("B", AvsResponseCode.Unknown);
			sm_dicAvsResponseCodes.Add("E", AvsResponseCode.Unknown);
			sm_dicAvsResponseCodes.Add("G", AvsResponseCode.Unknown);
			sm_dicAvsResponseCodes.Add("N", AvsResponseCode.NoMatch);
			sm_dicAvsResponseCodes.Add("P", AvsResponseCode.Unknown);
			sm_dicAvsResponseCodes.Add("R", AvsResponseCode.Retry);
			sm_dicAvsResponseCodes.Add("S", AvsResponseCode.ServiceNotSupported);
			sm_dicAvsResponseCodes.Add("U", AvsResponseCode.Unavailable);
			sm_dicAvsResponseCodes.Add("W", AvsResponseCode.NineDigitZip);
			sm_dicAvsResponseCodes.Add("X", AvsResponseCode.ExactMatch);
			sm_dicAvsResponseCodes.Add("Y", AvsResponseCode.AddressAndFiveDigitZip);
			sm_dicAvsResponseCodes.Add("Z", AvsResponseCode.FiveDigitZip);
			//cav response codes
			sm_dicCavResponseCodes.Add(string.Empty, CavResponseCode.NotPerformed);
			sm_dicCavResponseCodes.Add("0", CavResponseCode.DataError);
			sm_dicCavResponseCodes.Add("1", CavResponseCode.Failed);
			sm_dicCavResponseCodes.Add("2", CavResponseCode.Passed);
			sm_dicCavResponseCodes.Add("3", CavResponseCode.NotPerformed);
			sm_dicCavResponseCodes.Add("4", CavResponseCode.NotPerformed);
			sm_dicCavResponseCodes.Add("5", CavResponseCode.Unknown);
			sm_dicCavResponseCodes.Add("6", CavResponseCode.Unknown);
			sm_dicCavResponseCodes.Add("7", CavResponseCode.Failed);
			sm_dicCavResponseCodes.Add("8", CavResponseCode.Failed);
			sm_dicCavResponseCodes.Add("9", CavResponseCode.Failed);
			sm_dicCavResponseCodes.Add("A", CavResponseCode.Passed);
			sm_dicCavResponseCodes.Add("B", CavResponseCode.Passed);
			//cvv2 response codes
			sm_dicCvv2ResponseCodes.Add("M", Cvv2ResponseCode.Match);
			sm_dicCvv2ResponseCodes.Add("N", Cvv2ResponseCode.NoMatch);
			sm_dicCvv2ResponseCodes.Add("P", Cvv2ResponseCode.NotProcessed);
			sm_dicCvv2ResponseCodes.Add("S", Cvv2ResponseCode.NoMatch);
			sm_dicCvv2ResponseCodes.Add("U", Cvv2ResponseCode.ServiceNotAvailable);
		}

		#endregion

		#region Helper methods

		public static AvsResponseCode ParseAvsResponseCode(string avsResponse) {
			AvsResponseCode result;
			if (!sm_dicAvsResponseCodes.TryGetValue(avsResponse.ToUpper(), out result)) {
				result = AvsResponseCode.Unknown;
			}
			return result;
		}

		public static CavResponseCode ParseCavResponseCode(string cavResponse) {
			CavResponseCode result;
			if (!sm_dicCavResponseCodes.TryGetValue(cavResponse.ToUpper(), out result)) {
				result = CavResponseCode.Unknown;
			}
			return result;
		}

		public static Cvv2ResponseCode ParseCvv2ResponseCode(string cvv2Response) {
			Cvv2ResponseCode result;
			if (!sm_dicCvv2ResponseCodes.TryGetValue(cvv2Response.ToUpper(), out result)) {
				result = Cvv2ResponseCode.Unknown;
			}
			return result;
		}

		public static PaymentMethod ParsePaymentMethod(string paymentMethod) {
			switch (paymentMethod.ToUpper()) {
				case "CC": {
					return PaymentMethod.CreditCard;
				}
				case "ECHECK": {
					return PaymentMethod.ECheck;
				}
				default: {
					return PaymentMethod.Unknown;
				}
			}
		}

		#endregion

	}

}