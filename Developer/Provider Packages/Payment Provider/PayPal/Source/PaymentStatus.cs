﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	internal static class PaymentStatus {
		public const string CanceledReversal = "Canceled_Reversal";
		public const string Completed = "Completed";
		public const string Created = "Created";
		public const string Denied = "Denied";
		public const string Expired = "Expired";
		public const string Failed = "Failed";
		public const string Pending = "Pending";
		public const string Refunded = "Refunded";
		public const string Reversed = "Reversed";
		public const string Processed = "Processed";
		public const string Voided = "Voided";
	}

}