﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	internal static class PendingReasons {
		public const string Address = "address";
		public const string Authorization = "authorization";
		public const string ECheck = "echeck";
		public const string International = "intl";
		public const string MultiCurrency = "multi-currency";
		public const string Order = "order";
		public const string PaymentReview = "paymentreview";
		public const string Unilateral = "unilateral";
		public const string Upgrade = "upgrade";
		public const string Verify = "verify";
		public const string Other = "other";
	}

}