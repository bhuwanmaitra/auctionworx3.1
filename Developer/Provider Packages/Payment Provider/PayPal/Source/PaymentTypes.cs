﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	internal static class PaymentTypes {
		public const string ECheck = "echeck";
		public const string Instant = "instant";
	}

}