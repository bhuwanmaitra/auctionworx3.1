﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Transactions;
using RainWorx.FrameWorx.Providers.Payment;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Services;
using RainWorx.FrameWorx.Utility;
using RainWorx.FrameWorx.Strings;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	public class PayPalStandard :PaymentProviderBase {

		#region Private members

		#region Registration

		//exception handling policy
        private const string ExceptionPolicy = Strings.FunctionalAreas.Accounting;

		//constants used for registration
		private const string Name = "PayPalStandard";
		private const string RegistratorName = "PayPalStandardRegistrar";
		private const string ParentCategoryName = "PaymentProviders";
		private const string UserCategoryType = "User";
		private const string SiteCategoryType = "Site";
		private const string MvcAction = "PropertyManagement";

		//IPN acting user name
		private const string IPNPaymentUserName = "PayPalIPNUser";

		#endregion

		//PayPal constants
		private const string PayPalVerification_Verified = "VERIFIED";

        //deprecated -- see SiteProperties.AcceptedCurrencies
        //private static readonly List<string> SupportedCurrencies = new List<string>(new string[] { 
        //    "AUD", "BRL", "CAD", "CHF", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "ILS", "JPY", "MXN", 
        //    "MYR", "NOK", "NZD", "PHP", "PLN", "RUB", "SEK", "SGD", "THB", "TRY", "TWD", "USD" });

        private readonly IAccountingService _accounting;

        #endregion

        #region Constructors

        public PayPalStandard(IDataContext data, IAccountingService accounting): base(data) {				
			_accounting = accounting;
		}

		#endregion

		#region IPaymentProvider Members

        public override BatchPaymentCapability PrepareBatchPayment(Invoice invoice, out PaymentParameters paymentParameters, out string details)
        {
            throw new NotImplementedException();
        }

	    public override bool RegisterSelf() {
#if TRACE
			using (LogManager.Trace()) {
#endif			    
                bool retVal = false;
                try
                {
                    using (TransactionScope scope = TransactionUtils.CreateTransactionScope())
                    {
                        if (_data.CreatePaymentProvider(RegistratorName, Name, false))
                        {
                            retVal = true;
                            //add the PayPal provider to the PaymentProviders navigational category in the Admin Control Panel
                            //This navigational category only renders if the enabledCustomProperty (SiteProperties.Keys.Enabled => "PayPal_Enabled") is true
                            Category category = AddPaymentProviderAdminNavigation(RegistratorName, Name, 10, SiteProperties.Keys.Enabled);

                            //Add the following properties to the new navigational category (category.ID)
                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.PostUrl, CustomFieldType.String,
                                                           "https://www.sandbox.paypal.com/cgi-bin/webscr", 1, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.IpnUrl, CustomFieldType.String,
                                                           "/ipn.aspx", 2, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.SuccessReturnUrl, CustomFieldType.String,
                                                           "/PaymentResult.aspx?success=true", 3, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.CancelReturnUrl, CustomFieldType.String,
                                                           "/PaymentResult.aspx?success=false", 4, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.ReturnToMerchantText, CustomFieldType.String,
                                                           "Return to Merchant", 5, true, category.ID);

                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.FeesPayeeEmail, CustomFieldType.String,
                                                           string.Empty, 6, true, category.ID);

                            //Add the following property to the parent navigational category (category.ParentCategoryID.Value)
                            //this allows us to turn the provider and the provider's navigational category (created above) on and off
                            AddPaymentProviderSiteProperty(RegistratorName, SiteProperties.Keys.Enabled, CustomFieldType.Boolean,
                                                           "True", 10, true, category.ParentCategoryID.Value, null);

                            //Add PaymentProvider user fields (data.addcustomfield)
                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.AcceptPayPal, CustomFieldType.Boolean,
                                                        "False", 1040, true);

                            AddPaymentProviderUserField(RegistratorName, UserProperties.Keys.Email, CustomFieldType.String, string.Empty,
                                                        1050, false);
                        }
                        scope.Complete();
                    }
				}
				catch (Exception ex) {
					if (LogManager.HandleException(ex, ExceptionPolicy)) {
						throw;
					}
				}
                return retVal;
#if TRACE
			}
#endif
		}

        public override bool Enabled
        {
			get {
				return SiteProperties.Enabled;
			}
		}

        public override string ProviderName
        {
			get {
				return Name;
			}
		}

        public override bool CanPayInvoice(Invoice invoice, out ReasonCode reason)
        {
#if TRACE
			using (LogManager.Trace()) {
#endif
                bool retVal;
                //if (SupportedCurrencies.Contains(invoice.Currency)) {
                if (IsPayPalCurrency(invoice.Currency) || IsPayPalCurrency(SiteProperties.BackupPaymentCurrency))
                {
                    //PayPal is not accepted by this non-fee invoice owner
                    if (invoice.Type != InvoiceTypes.Fee)
                    {
                        if (invoice.Owner.PayPalAccepted())
                        {
                            reason = ReasonCode.Success;
                            retVal = true;
                        }
                        else
                        {
                            reason = ReasonCode.DisabledBySeller;
                            retVal = false;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(SiteProperties.FeesPayeeEmail))
                        {
                            reason = ReasonCode.Success;
                            retVal = true;
                        }
                        else
                        {
                            reason = ReasonCode.InvoiceTypeNotSupported;
                            retVal = false;
                        }
                    }
                }
				else
                {
					reason = ReasonCode.CurrencyNotSupported;
					retVal = false;
				}
                return retVal;
#if TRACE
			}
#endif
		}

        private bool IsPayPalCurrency(string currencyCode)
        {
            return ("," + SiteProperties.AcceptedCurrencies.Replace(" ", "").ToUpper() + ",").Contains("," + currencyCode.ToUpper() + ",");
        }

        public override PaymentProviderResponse ProcessPayment(Invoice invoice, PaymentParameters paymentParameters)
        {
            PaymentProviderResponse response = new PaymentProviderResponse();
            response.ResponseStatus = PaymentProviderResponseStatus.Pending;
            ProcessPaymentResponse(invoice.Payer.UserName, invoice, response);
            return response;
		}

		//PayPal IPN looks like this:
		//mc_gross=19.95&protection_eligibility=Eligible&address_status=confirmed&payer_id=LPLWNMTBWMFAY&tax=0.00&address_street=1+Main+St&payment_date=20%3A12%3A59+Jan+13%2C+2009+PST&payment_status=Completed&charset=windows-1252&address_zip=95131&first_name=Test&mc_fee=0.88&address_country_code=US&address_name=Test+User&notify_version=2.6&custom=&payer_status=verified&address_country=United+States&address_city=San+Jose&quantity=1&verify_sign=AtkOfCXbDm2hu0ZELryHFjY-Vb7PAUvS6nMXgysbElEn9v-1XcmSoGtf&payer_email=gpmac_1231902590_per%40paypal.com&txn_id=61E67681CH3238416&payment_type=instant&last_name=User&address_state=CA&receiver_email=gpmac_1231902686_biz%40paypal.com&payment_fee=0.88&receiver_id=S8XGHLYDW9T3S&txn_type=express_checkout&item_name=&mc_currency=USD&item_number=&residence_country=US&test_ipn=1&handling_amount=0.00&transaction_subject=&payment_gross=19.95&shipping=0.00
		//see https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_admin_IPNIntro
        public override bool IsThisMyIpn(UserInput request)
        {
#if TRACE
			using (LogManager.Trace()) {
#endif
				try {
					//return true if there is a PayPal transaction ID
					return request.Items.ContainsKey(IpnFields.TransactionId);					
				}
				catch (Exception ex) {
					if (LogManager.HandleException(ex, ExceptionPolicy)) {
						throw;
					}
				}
			    return false;
#if TRACE
			}
#endif
		}

		/*
		 * Wait for an HTTP post from PayPal. 
Create a request that contains exactly the same IPN variables and values in the same order, preceded with cmd=_notify-validate. 
Post the request to paypal.com or sandbox.paypal.com, depending on whether you are going live or testing your listener in the Sandbox. 
Wait for a response from PayPal, which is either VERIFIED or INVALID. 
If the response is VERIFIED, perform the following checks: 
Confirm that the payment status is Completed.

PayPal sends IPN messages for pending and denied payments as well; do not ship until the payment has cleared.
Use the transaction ID to verify that the transaction has not already been processed, which prevents duplicate transactions from being processed.

Typically, you store transaction IDs in a database so that you know you are only processing unique transactions.
Validate that the receiver’s email address is registered to you.

This check provides additional protection against fraud.
Verify that the price, item description, and so on, match the transaction on your website.

This check provides additional protection against fraud.
If the verified response passes the checks, take action based on the value of the txn_type variable if it exists; otherwise, take action based on the value of the reason_code variable. 
If the response is INVALID, save the message for further investigation.
		 * */
        public override PaymentProviderResponse ProcessIpn(UserInput request)
        {
#if TRACE
            using (LogManager.Trace()) {
#endif
				try {
				    string verificationUrl;
                    if (SiteProperties.DemoMode)
                    {
                        verificationUrl = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                    } else
                    {
                        verificationUrl = SiteProperties.PostUrl;   
                    }					
					string postData = request.Raw;					
					//using (StreamReader reader = new StreamReader(request.InputStream)) {
					//	postData = reader.ReadToEnd();
					//}
					HttpWebRequest verificationRequest = (HttpWebRequest)WebRequest.Create(verificationUrl);
					verificationRequest.Method = "POST";
					using (StreamWriter writer = new StreamWriter(verificationRequest.GetRequestStream())) {
						writer.Write("cmd=_notify-validate&");
						writer.Write(postData);
					}
					//try {
						//verify that the IPN came from PayPal						
						HttpWebResponse verificationResponse = (HttpWebResponse)verificationRequest.GetResponse();
						string responseData = string.Empty;
						using (StreamReader reader = new StreamReader(verificationResponse.GetResponseStream())) {
							responseData = reader.ReadToEnd();
						}						
						if (responseData == PayPalVerification_Verified)
						{
						    Invoice invoice = null;
                            //TODO: store the transaction ID somewhere to prevent duplication attacks
                            string transactionType = request.Items.ContainsKey(IpnFields.TransactionType) ? request.Items[IpnFields.TransactionType] : string.Empty;
                            PaymentProviderResponse response = null;
                            switch (transactionType) {
								case TransactionType.WebAccept: 
									response = ProcessWebAccept(request, out invoice);
							        break;
								default:
									Statix.ThrowInvalidOperationFaultContract(ReasonCode.UnexpectedTransactionType);
							        break;
							}
                            if (invoice == null)
                                Statix.ThrowInvalidOperationFaultContract(ReasonCode.InvoiceNotFound);

                            ProcessPaymentResponse(request.ActingUserName, invoice, response);
                            return response;
						}
						else {							
							//PayPal rejected the IPN
							Statix.ThrowInvalidOperationFaultContract(ReasonCode.InvalidIPN);
						}
					//}
					//catch (WebException we) {
					//	//TODO: do we need to catch this? or can the general exception handler take it?
					//}
				}
                catch (Exception ex)
                {
                    if (request != null)
                    {
                        LogManager.WriteLog("Request Fields", "IPN Details", "PayPal Provider", TraceEventType.Information, null, null,
                            request.Items.ToDictionary(pair => pair.Key, pair => (object)pair.Value));
                    }

                    if (LogManager.HandleException(ex, ExceptionPolicy))
                    {
                        throw;
                    }
                }
                return null;
#if TRACE
			}
#endif
		}

		#endregion

		#region Implementation

        private PaymentProviderResponse ProcessWebAccept(UserInput request, out Invoice invoice)
        {
            //LogManager.WriteLog("IPN Testing", "Raw Response", "PayPal Provider", TraceEventType.Information, null, null, 
            //    request.Items.ToDictionary(pair => pair.Key, pair => (object)pair.Value));

            //get the invoice
			string invoiceNumberString = request.Items[IpnFields.InvoiceNumber];			
            int invoiceNumber = Convert.ToInt32(invoiceNumberString);			
			invoice = _data.GetInvoiceByID(invoiceNumber);
            if (invoice == null)
                Statix.ThrowInvalidOperationFaultContract(ReasonCode.InvoiceNotFound);
            //TODO: verify that the payment is for this invoice - check payee email?
			//build the response
			PaymentProviderResponse response = new PaymentProviderResponse();
			response.AllFields = new Dictionary<string, string>();
			foreach (KeyValuePair<string, string> pair in request.Items) {
				response.AllFields[pair.Key] = pair.Value;
			}			
			response.Amount = Convert.ToDecimal(request.Items[IpnFields.Amount]);			
			response.Approved = request.Items[IpnFields.PaymentStatus] == PaymentStatus.Completed;
			//response.AuthorizationCode = string.Empty;
			response.BuyerAddress = new Address();
            response.BuyerAddress.City = request.Items.ContainsKey(IpnFields.Payer.City) ? request.Items[IpnFields.Payer.City] : "Missing in IPN";
            //response.BuyerAddress.Country = request.Items.ContainsKey(IpnFields.Payer.Name) ? request.Items[IpnFields.Payer.CountryCode] : "Missing in IPN";			
            response.BuyerAddress.FirstName = request.Items.ContainsKey(IpnFields.Payer.Name) ? request.Items[IpnFields.Payer.Name] : "Missing in IPN";
            response.BuyerAddress.StateRegion = request.Items.ContainsKey(IpnFields.Payer.State) ? request.Items[IpnFields.Payer.State] : "Missing in IPN";
            response.BuyerAddress.Street1 = request.Items.ContainsKey(IpnFields.Payer.Street) ? request.Items[IpnFields.Payer.Street] : "Missing in IPN";
            response.BuyerAddress.ZipPostal = request.Items.ContainsKey(IpnFields.Payer.Zip) ? request.Items[IpnFields.Payer.Zip] : "Missing in IPN";
            response.BuyerEmail = request.Items.ContainsKey(IpnFields.Payer.Email) ? request.Items[IpnFields.Payer.Email] : "Missing in IPN";
            response.InvoiceNumber = invoiceNumber;			
			switch (request.Items[IpnFields.PaymentMethod]) {
				case PaymentTypes.ECheck: {
					response.PaymentMethod = PaymentMethod.ECheck;
					break;
				}
				case PaymentTypes.Instant: {
					response.PaymentMethod = PaymentMethod.CreditCard;
					break;
				}
				default: {
					response.PaymentMethod = PaymentMethod.Unknown;
					break;
				}
			}
			response.ProviderIdentifier = Name;
			response.RawResponseCode = request.Items[IpnFields.PaymentStatus];
			switch (request.Items[IpnFields.PaymentStatus]) {
				case PaymentStatus.Completed: {
					response.ResponseStatus = PaymentProviderResponseStatus.Complete;
					response.ResponseDescription = ResponseDescriptions.Completed;
					break;
				}
				case PaymentStatus.Pending: {
					response.ResponseStatus = PaymentProviderResponseStatus.PendingProvider;
					response.ResponseDescription = ResponseDescriptions.Pending.SellerAcceptance;
					break;
				}
				default: {
					response.ResponseStatus = PaymentProviderResponseStatus.Pending;
					response.ResponseDescription = string.Empty;
					break;
				}
			}
            response.Timestamp = DateTime.UtcNow; // ConvertIpnDate(request.Items[IpnFields.PaymentDate]);			
			response.TransactionId = request.Items[IpnFields.TransactionId];			
			CheckFraudFilters(request, response);			
            return response;
		}

		private void CheckFraudFilters(UserInput request, PaymentProviderResponse response) {
			foreach (string key in request.Items.Keys) {
				if (key.StartsWith(IpnFields.FraudManagementFilters.FraudManagementPendingFilterPrefix)) {
					int intValue;
					if (int.TryParse(request.Items[key], out intValue)) {
						if (Enum.IsDefined(typeof(FraudManagementCode), intValue)) {
							switch ((FraudManagementCode)intValue) {
								case FraudManagementCode.AvsNoMatch:
								case FraudManagementCode.AvsPartialMatch: {
									response.AvsResponse = AvsResponseCode.NoMatch;
									break;
								}
								case FraudManagementCode.AvsUnavailableUnsupported: {
									response.AvsResponse = AvsResponseCode.Unavailable;
									break;
								}
								case FraudManagementCode.CardSecurityCodeMismatch: {
									response.Cvv2Response = Cvv2ResponseCode.NoMatch;
									break;
								}
							}
						}
					}
				}
			}
		}

		private DateTime ConvertIpnDate(string input) {
			string timeZoneString = input.Substring(input.Length - 3);
			input = input.Substring(0, input.Length - 3);
			DateTime result = DateTime.Parse(input);
			/*TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneString);
			result = TimeZoneInfo.ConvertTime(result, timeZone, TimeZoneInfo.Local);*/
			return result;
		}

        private void ProcessPaymentResponse(string actingUserName, Invoice invoice, PaymentProviderResponse paymentResponse)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    using (var scope = TransactionUtils.CreateTransactionScope())
                    {
                        _data.AddPaymentResponse(actingUserName, paymentResponse);
                        if (paymentResponse.Approved)
                        {
                            //Note: We will assume the payment is for the full invoice amount if this invoice was converted to a different currency for payment,
                            //  in order to avoid possible rounding errors that could incorrectly indicate the payment is for less than the amount due.
                            if (invoice.Total <= paymentResponse.Amount || !IsPayPalCurrency(invoice.Currency))
                            {

                                _data.SetInvoicePaid(actingUserName, invoice.ID, true);

                                if (invoice.Type == Strings.InvoiceTypes.Fee)
                                {
                                    _data.ActivateListingsAwaitingPayment(invoice.ID);
                                }
                            }
                            else
                            {
                                //payment amount is for less than the invoice total, so just add a credit adjustment but do not mark it paid
                                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(Cache.SiteProperties[Strings.SiteProperties.SiteCulture]);
                                var input = new UserInput(Strings.SystemActors.SystemUserName, actingUserName, cultureInfo.Name, cultureInfo.Name);
                                input.Items.Add(Strings.Fields.AdjustmentDescription, "Partial Payment");
                                input.Items.Add(Strings.Fields.AdjustmentAmount, paymentResponse.Amount.ToString("N2", cultureInfo));
                                input.Items.Add(Strings.Fields.AdjustmentCreditDebit, Strings.Fields.AdjustmentAmountCredit);
                                input.Items.Add(Strings.Fields.AdjustmentTaxable, false.ToString());
                                input.Items.Add(Strings.Fields.BuyersPremiumApplies, false.ToString());
                                _accounting.AddInvoiceAdjustment(Strings.SystemActors.SystemUserName, invoice.ID, input);
                            }
                        }
                        else if (paymentResponse.ResponseStatus == PaymentProviderResponseStatus.PendingProvider || paymentResponse.ResponseStatus == PaymentProviderResponseStatus.Pending)
                        {
                            _data.SetInvoiceStatus(actingUserName, invoice.ID, Strings.InvoiceStatuses.Pending);
                        }
                        scope.Complete();
                    }
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
#if TRACE
            }
#endif
        }

        #endregion

	}

}
