﻿using System;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {
	
	public static class SiteProperties {

		public class Keys {
            public const string PostUrl = Strings.SiteProperties.PayPal_PostURL; //;"PayPal_PostURL";
			public const string IpnUrl = Strings.SiteProperties.PayPal_IPNURL; //"PayPal_IPNURL";
			public const string SuccessReturnUrl = Strings.SiteProperties.PayPal_SuccessReturnURL; //"PayPal_SuccessReturnURL";
			public const string CancelReturnUrl = Strings.SiteProperties.PayPal_CancelReturnURL; //"PayPal_CancelReturnURL";
			public const string ReturnToMerchantText = Strings.SiteProperties.PayPal_ReturnToMerchantText; //"PayPal_ReturnToMerchantText";
			public const string FeesPayeeEmail = Strings.SiteProperties.PayPal_FeesEmail; //"PayPal_FeesEmail";
			public const string Enabled = Strings.SiteProperties.PayPal_Enabled; //"PayPal_Enabled";
            public const string DemoMode = Strings.SiteProperties.DemoEnabled; //"DemoEnabled";

            public const string AcceptedCurrencies = Strings.SiteProperties.PayPal_AcceptedCurrencies;
            public const string BackupPaymentCurrency = Strings.SiteProperties.PayPal_BackupPaymentCurrency;

        }

        internal static string PostUrl {
			get {
				return Cache.SiteProperties[Keys.PostUrl];
			}
		}

		internal static string IpnUrl {
			get {
				return Cache.SiteProperties[Keys.IpnUrl];
			}
		}

		internal static string SuccessReturnUrl {
			get {
				return Cache.SiteProperties[Keys.SuccessReturnUrl];
			}
		}

		internal static string CancelReturnUrl {
			get {
				return Cache.SiteProperties[Keys.CancelReturnUrl];
			}
		}

		internal static string ReturnToMerchantText {
			get {
				return Cache.SiteProperties[Keys.ReturnToMerchantText];
			}
		}

		internal static string FeesPayeeEmail {
			get {
				return Cache.SiteProperties[Keys.FeesPayeeEmail];
			}
		}

		internal static bool Enabled {
			get {
				return Convert.ToBoolean(Cache.SiteProperties[Keys.Enabled]);
			}
		}

        internal static bool DemoMode
        {
            get
            {
                return Convert.ToBoolean(Cache.SiteProperties[Keys.DemoMode]);
            }
        }

        internal static string AcceptedCurrencies
        {
            get
            {
                return Cache.SiteProperties[Keys.AcceptedCurrencies];
            }
        }

        internal static string BackupPaymentCurrency
        {
            get
            {
                return Cache.SiteProperties[Keys.BackupPaymentCurrency];
            }
        }

    }

}