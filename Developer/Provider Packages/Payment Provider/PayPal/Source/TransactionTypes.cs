﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	internal static class TransactionType {
		public const string Adjustment = "adjustment";
		public const string Cart = "cart";
		public const string ExpressCheckout = "express_checkout";
		public const string MassPay = "masspay";
		public const string MerchPmt = "merch_pmt";
		public const string NewCase = "new_case";
		public const string RecurringPayment = "recurring_payment";
		public const string RecurringPaymentProfileCreated = "recurring_payment_profile_created";
		public const string SendMoney = "send_money";
		public const string SubscriptionCancelled = "subscr_cancel";
		public const string SubscriptionExpired = "subscr_eot";
		public const string SubscriptionSignupFailed = "subscr_failed";
		public const string SubscriptionModified = "subscr_modify";
		public const string SubscriptionPayment = "subscr_payment";
		public const string SubscriptionSignup = "subscr_signup";
		public const string VirtualTerminal = "virtual_terminal";
		public const string WebAccept = "web_accept";
	}

}