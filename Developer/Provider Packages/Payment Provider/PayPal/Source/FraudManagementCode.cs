﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {

	internal enum FraudManagementCode {
		AvsNoMatch = 1,
		AvsPartialMatch = 2,
		AvsUnavailableUnsupported = 3,
		CardSecurityCodeMismatch = 4,
		MaximumTransactionAmount = 5,
		UnconfirmedAddress = 6,
		CountryMonitor = 7,
		LargeOrderNumber = 8,
		BillingShippingAddressMismatch = 9,
		RiskyZipCode = 10,
		SuspectedFreightForwarder = 11,
		TotalPurchasePriceMinimum = 12,
		IpAddressVelocity = 13,
		RiskyEmailAddress = 14,
		RiskyBankIdentificationNumber = 15,
		RiskyIpAddressRange = 16,
		PayPalFraudModel = 17
	}

}