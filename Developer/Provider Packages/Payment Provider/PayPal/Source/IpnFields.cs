﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {
	
	internal static class IpnFields {

		public const string TransactionId = "txn_id";
		public const string TransactionType = "txn_type";
		public const string InvoiceNumber = "invoice";
		public const string PaymentStatus = "payment_status";
		public const string Amount = "mc_gross";
		public const string PaymentMethod = "payment_type";
		public const string PaymentDate = "payment_date";

		public static class Payer {
			public const string Name = "address_name";
			public const string Country = "address_country";
			public const string CountryCode = "address_country_code";
			public const string Zip = "address_zip";
			public const string State = "address_state";
			public const string City = "address_city";
			public const string Street = "address_street";
			public const string Email = "payer_email";
			public const string Id = "payer_id";
		}

		public static class Receiver {
			public const string Business = "business";
			public const string Email = "receiver_email";
			public const string Id = "receiver_id";
		}

		public static class FraudManagementFilters {
			public const string FraudManagementPendingFilterPrefix = "fraud_management_pending_filters_";
		}

	}

}