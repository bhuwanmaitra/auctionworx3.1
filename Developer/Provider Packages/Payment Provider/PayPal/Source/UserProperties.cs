﻿using System;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Payment.PayPal {
	
	internal static class UserProperties {

		public class Keys {
			public const string Email = "PayPal_Email";
            public const string AcceptPayPal = "AcceptPayPal";
		}

	}

}