
____________________________________________________

	AuctionWorx Enterprise v3.1 Providers
____________________________________________________



Contents:

\Fee Provider				- Example projects to write a custom fee provider
   \Fee Provider Interfaces		- Interface project for fee providers		
       \Source				- Project source code
   \Standard Fee Provider		- Standard Fee Provider
       \Source				- Project source code	

\Invoice Logic Provider		- Example project to write a custom invoice logic provider
   \Source					- Project source code (includes interface)

\Listing Format Provider	- Starter project to write a custom listing format provider
   \Listing Provider Interfaces		- Interface project for all Listing Format Providers
       \Source				- Project source code
   \Fixed Price 			- Fixed Price Listing Format Provider Project
       \Source				- Project source code
       \Languages			- Associated language file (RESX)
       \Views				- Associated partial views (MVC 5)
   \Auction Provider 			- Auction Listing Format Provider Project
       \Source				- Project source code
       \Languages			- Associated language file (RESX)
       \Views				- Associated partial views (MVC 5)
   \Classified Provider		- Classified Listing Format Provider Project
       \Source				- Project source code
       \Languages			- Associated language file (RESX)
       \Views				- Associated partial views (MVC 5)

\Media Asset Provider		- Starter project to write a custom media asset provider
   \Source					- Project source code - Native Image Provider
   \Views					- Associated partial views (MVC 5)

\Membership Provider		- Starter project to write a custom membership provider
       \Source				- Project source code

\Payment Provider			- Starter projects to write a custom payment provider
   \Payment Provider Interfaces		- Interface project for payment providers
       \Source				- Project source code	
   \Authorize.Net			- Authorize.Net Payment Provider Code
       \Source				- Project source code
       \Views				- Associated partial views (MVC 5)
   \PayPal					- PayPal: Website Payments Standard Payment Provider Code
       \Source				- Project source code
       \Views				- Associated partial views (MVC 5)
   \Stripe					- Stripe Connect Payment Provider Code
       \Content				- Associated Content files (MVC 5)
       \Controllers			- Associated Controller files (MVC 5)
       \Helpers				- Associated Helper files (MVC 5)
       \Source				- Project source code
       \Views				- Associated partial views (MVC 5)


Administration Manual and Install instructions can be found online at:

http://www.rainworx.com/admin-docs

Developer Documentation can be found online at:

http://www.rainworx.com/dev-docs

NOTE: The example code cannot be compiled without the binaries from the full AuctionWorx Enterprise package
