﻿using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Utility;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.Unity;
using System;

namespace RainWorx.FrameWorx.Providers.Fee
{
    public abstract class FeeProcessorBase : IFeeProcessor
    {
        protected readonly IDataContext _data;        
        
        /// <summary>
        /// 0 argument constructor (as required by WCF Service Hosts)
        /// </summary>
        protected FeeProcessorBase()
        {            
            _data = UnityResolver.Get<IDataContext>();
        }
        
        /// <summary>
        /// 2 argument constructor for non WCF Service Hosts.  Allows explicit definition of Unity and Data objects
        /// </summary>
        /// <param name="unity">The Unity proxy facade that exposes Exception Handling, Logging, and Tracing</param>
        /// <param name="data">The Data Context facade that exposes the DAL</param>
        protected FeeProcessorBase(IDataContext data)
        {            
            _data = data;
        }

        /// <summary>
        /// This method Generates the associated Fee Line Items.
        /// </summary>
        /// <param name="applicableFeeProviders">A list of applicable Fee Providers for this call to GenerateFees to allow 
        /// the implementation to determine if it is applicable</param>
        /// <param name="eventName">The name of the event occurring on the provided listing.  (See Strings.Events)</param>
        /// <param name="oldListing">The listing before it was changed in the case of all events except "AddListing."  This 
        /// is for comparison with the current state of the listing for fee processing.</param>
        /// <param name="newlisting">The current listing for which Fees should be charged.</param>
        /// <param name="listingAction">The Listing Action that triggered the Fee generation, otherwise null</param>
        /// <param name="commonProperties">A dictionary of key/value pairs possibly associated with fees at the time of the 
        /// AccountingService.AddFees call.</param>
        /// <param name="payToProceed">Allows the Fee Provider to indicate if the listing should be held, pending payment, 
        /// or if it can be activated immediately.  This value is checked after all Fee Providers have completed 
        /// processing.</param>
        /// <returns>A List of LineItems to be added to the Site Fee Invoice.</returns>
        public abstract List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName,
                                                    Listing oldListing, Listing newlisting, ListingAction listingAction,
                                                    Dictionary<string, string> commonProperties, ref bool payToProceed);

        /// <summary>
        /// The name of the Fee Provider
        /// </summary>
        public abstract string FeeProviderName { get; }

        /// <summary>
        /// Registers the Fee Provider with the system
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <returns>true if RegisterSelf modified the system in any way, otherwise false</returns>
        public abstract bool RegisterSelf(string actingUserName);

        /// <summary>
        /// A helper method to query the DAL for Properties for a specific Fee Provider
        /// </summary>
        /// <param name="eventName">The associated fee event for which the property is assigned (because the value of the 
        /// property could vary based on event).</param>
        /// <param name="propertyName">The name of the Fee Property requested.</param>
        /// <param name="listingTypeID">The ID of the type of listing to retrieve the property for (because the value of 
        /// the property could vary based on listing type).</param>
        /// <returns>A FeeProperty to be used internally by the calling Fee Provider</returns>
        protected FeeProperty GetFeeProperty(string eventName, string propertyName, int listingTypeID)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    return (Cache.FeeProperties.SingleOrDefault(fp => fp.Processor == GetType().FullName &&
                                                                      fp.ListingType.ID == listingTypeID &&
                                                                      fp.Event.Name == eventName &&
                                                                      fp.Name == propertyName));
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
                return null;
#if TRACE
            }
#endif
        }

        /// <summary>
        /// Ensures that a specific Fee Schedule exists
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="listingTypeName">The name of the listing type that the Fee Schedule applies to</param>
        /// <param name="description">A description of the Fee Schedule</param>
        /// <param name="eventName">The event that the Fee Schedule applies to</param>
        /// <param name="payToProceed">true if the Fee Schedule requires Site Fee Invoice payment before a listing 
        /// becomes active</param>
        /// <param name="name">the naem of the fee schedule </param>
        /// <returns>true if a Fee Schedule was added (something changed), otherwise, false</returns>
        protected bool EnsureFeeSchedule(string actingUserName, string listingTypeName, string description,
                                         string eventName, bool payToProceed, string name)
        {
            //Get FeeSchedules
            List<FeeSchedule> feeSchedules = _data.GetFeeSchedules(eventName, listingTypeName, name);
            List<ListItem> events = _data.GetEvents();
            List<ListingType> existingListingTypes = _data.GetAllListingTypes();

            if (!feeSchedules.Any(fs => fs.ListingType.Name.Equals(listingTypeName)))
            {                
                ListingType thisType = existingListingTypes.Single(lt => lt.Name.Equals(listingTypeName));
                FeeSchedule schedule = new FeeSchedule
                                           {
                                               Description = description,
                                               Event = events.Single(e => e.Name.Equals(eventName)),
                                               ListingType = new ListItem {ID = thisType.ID},
                                               PayToProceed = payToProceed,
                                               Name = name
                                           };
                _data.AddFeeSchedule(actingUserName, schedule);
                return true; //something changed!
            }
            return false; //nothing changed
        }

        /// <summary>
        /// Adds a site field and corresponding site property
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="propertyName">The name of the property to add</param>
        /// <param name="fieldType">The datatype of the property to add</param>
        /// <param name="defaultValue">The default value (convertible string) of the property to add</param>
        /// <param name="displayOrder">The order in which the property should appear in "My Account"</param>
        /// <param name="required">Whether or not the property must contain a value</param>
        /// <param name="categoryId">The ID of the nagivational category to add the property to</param>
        /// <param name="enumList">If the fieldType is an "Enum," a comma delimited list of possible enumeration values 
        /// for the property.  For each enumeration, there are three tokens: the name of the enumeration
        /// (what will be displayed on screen, or localized to be displayed), the value of the enumeration (what the 
        /// property is actually set to if the enumeration is selected), and "true" or "false" indicating
        /// whether the enumeration is enabled or not (this will almost always be true).  Each token is seperated by 
        /// a "|" (pipe).  An example is "Red|1|true,Green|2|true,Blue|3|true"</param>
        /// <returns>true if the property was actually added, otherwise false</returns>
        protected bool EnsureSiteProperty(string actingUserName, string propertyName, CustomFieldType fieldType,
                                          string defaultValue, int displayOrder, bool required, int categoryId,
                                          string enumList)
        {
            bool retVal = false;

            //If necessary, add field
            CustomField currentField = _data.GetCustomFieldByName(propertyName, fieldType, "Site");
            if (currentField == null)
            {
                currentField = new CustomField
                {
                    DefaultValue = defaultValue,
                    CategoryIDs = new List<int>(1) { categoryId },
                    Deferred = false,
                    DisplayOrder = displayOrder,
                    Group = "Site",
                    Name = propertyName,
                    Required = required,
                    Type = fieldType
                };
                //_data.AddCustomField(actingUserName, ref currentField);

                CustomProperty currentProperty = new CustomProperty
                {
                    Field = currentField,
                    Value = defaultValue
                };
                _data.AddSiteProperty(actingUserName, currentProperty);
                _data.AssignFieldToCategories(actingUserName, currentField.ID, new[] { categoryId });

                //if necessary, add enum options
                if (!string.IsNullOrEmpty(enumList))
                {
                    foreach (string enumData in enumList.Split(','))
                    {
                        string enumName = string.Empty;
                        string enumValue = string.Empty;
                        bool enumEnabled = false;
                        int i = 0;
                        foreach (string enumPart in enumData.Split('|'))
                        {
                            if (i == 0)
                            {
                                enumName = enumPart;
                            }
                            else if (i == 1)
                            {
                                enumValue = enumPart;
                            }
                            else if (i == 2)
                            {
                                enumEnabled = bool.Parse(enumPart);
                            }
                            i++;
                        }
                        _data.CreateEnumeration(actingUserName, currentField.ID, enumName, enumName, enumValue, enumEnabled);
                    }
                }

                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Updates the given list of Fee Properties
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <param name="feeProperties">The Fee Properties to update</param>
        protected void UpdateFeeProperties(string actingUserName, List<FeeProperty> feeProperties)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                try
                {
                    _data.UpdateAllFeeProperties(actingUserName, feeProperties);
                    Cache.ClearFeeProperties();
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
#if TRACE
            }
#endif
        }
    }
}
