﻿using System.Collections.Generic;
using RainWorx.FrameWorx.DTO;

namespace RainWorx.FrameWorx.Providers.Fee
{
    /// <summary>
    /// Calculates Site Fees during specific events related to a Listing's lifetime.
    /// </summary>
    public interface IFeeProcessor
    {
        /// <summary>
        /// This method Generates the associated Fee Line Items.
        /// </summary>
        /// <param name="eventName">The name of the event occurring on the provided listing.  (See Strings.Events)</param>
        /// <param name="oldListing">The listing before it was changed in the case of all events except "AddListing."  This is for comparison with the current state of the listing for fee processing.</param>
        /// <param name="newlisting">The current listing for which Fees should be charged.</param>
        /// <param name="commonProperties">A dictionary of key/value pairs possibly associated with fees at the time of the AccountingService.AddFees call.</param>
        /// <param name="payToProceed">Allows the Fee Provider to indicate if the listing should be held, pending payment, or if it can be activated immediately.  This value is checked after all Fee Providers have completed processing.</param>
        /// <returns>A List of LineItems to be added to the Site Fee Invoice.</returns>
        List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newlisting, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed);

        /// <summary>
        /// The name of the Fee Provider
        /// </summary>
        string FeeProviderName { get; }

        /// <summary>
        /// Registers the Fee Provider with the system
        /// </summary>
        /// <param name="actingUserName">The username of the user performing the request</param>
        /// <returns>true if RegisterSelf modified the system in any way, otherwise false</returns>
        bool RegisterSelf(string actingUserName);
    }
}
