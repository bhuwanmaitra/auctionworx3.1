﻿using System;
using System.Collections.Generic;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class ImageCount : FeeProcessorBase
    {        
        //Via WCF (where 0 argument constructor is required)
        public ImageCount() {}

        //Via Unity
        public ImageCount(IDataContext data) : base(data) { }

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing changed yet

                //register for all listing types & events "AddListing" and "UpdateListing"
                List<ListingType> listingTypes = _data.GetAllListingTypes();
                List<ListItem> events = _data.GetEvents();

                List<FeeProperty> propertiesToAdd = new List<FeeProperty>();

                foreach (ListingType listingType in listingTypes)
                {
                    //Check Fee Properties
                    foreach (ListItem eventItem in events.Where(e => e.Name.Equals(Strings.Events.AddListing) || e.Name.Equals(Strings.Events.UpdateListing)))
                    {                        
                        FeeProperty existingFeeProperty = GetFeeProperty(eventItem.Name, "FirstImage", listingType.ID);

                        if (existingFeeProperty == null)
                        {
                            //create this property
                            FeeProperty newFeeProperty = new FeeProperty
                                                             {
                                                                 Amount = 0.00M,
                                                                 Description = "First Image Fee",
                                                                 Event = eventItem,
                                                                 ListingType =
                                                                     new ListItem(listingType.ID, listingType.Name, true,
                                                                                  string.Empty),
                                                                 Name = "FirstImage",
                                                                 Processor = GetType().FullName
                                                             };

                            propertiesToAdd.Add(newFeeProperty);                            

                            //something changed
                            retVal = true;
                        }

                        existingFeeProperty = GetFeeProperty(eventItem.Name, "AdditionalImages", listingType.ID);

                        if (existingFeeProperty == null)
                        {
                            //create this property
                            FeeProperty newFeeProperty = new FeeProperty
                                                             {
                                                                 Amount = 0.00M,
                                                                 Description = "Each Additional Image Fee",
                                                                 Event = eventItem,
                                                                 ListingType =
                                                                     new ListItem(listingType.ID, listingType.Name, true,
                                                                                  string.Empty),
                                                                 Name = "AdditionalImages",
                                                                 Processor = GetType().FullName
                                                             };

                            propertiesToAdd.Add(newFeeProperty); 

                            //something changed
                            retVal = true;
                        }                        
                    }

                    //Check applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //Didn't exist, adding
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //something changed
                        retVal = true;
                    }
                }

                if (retVal && propertiesToAdd.Count > 0)
                {
                    UpdateFeeProperties(actingUserName, propertiesToAdd);
                }

                return retVal;
#if TRACE
            }
#endif
        }

        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.ImageCount"; }
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newListing, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                const string applicableMediaTypeName = "RainWorx.FrameWorx.Providers.MediaAsset.JPEGWithThumbnails"; // "JPEG";

                //check to see if this fee provider is applicable
                if (!applicableFeeProviders.Any(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled)) return (new List<LineItem>(0));  

                //if not adding and not updating, add no fees
                if (!eventName.Equals(Strings.Events.AddListing) && !eventName.Equals(Strings.Events.UpdateListing)) return (new List<LineItem>(0));

                List<LineItem> newLineItems = new List<LineItem>(2);                

                FeeProperty firstImageFee = GetFeeProperty(eventName, "FirstImage", newListing.Type.ID);
                FeeProperty additionalImagesFee = GetFeeProperty(eventName, "AdditionalImages", newListing.Type.ID);                                                        

                if (firstImageFee == null || additionalImagesFee == null) return newLineItems;

                if (newListing.Media.All(m => m.Type != applicableMediaTypeName)) return newLineItems;

                int firstImages;
                int additionalImages;
                if (oldListing != null)
                {
                    //old listing had the same or more images, no new charges...
                    if (oldListing.Media.Count(m => m.Type == applicableMediaTypeName) >= newListing.Media.Count(m => m.Type == applicableMediaTypeName)) return newLineItems;

                    if (oldListing.Media.Any(m => m.Type == applicableMediaTypeName))
                    {
                        //new images are all additional
                        firstImages = 0;
                        additionalImages = newListing.Media.Count(m => m.Type == applicableMediaTypeName) - oldListing.Media.Count(m => m.Type == applicableMediaTypeName);
                    } else
                    {
                        //new images are first and additional
                        firstImages = 1;
                        additionalImages = newListing.Media.Count(m => m.Type == applicableMediaTypeName) - oldListing.Media.Count(m => m.Type == applicableMediaTypeName) - 1;
                    }
                } else
                {
                    firstImages = newListing.Media.Any(m => m.Type == applicableMediaTypeName) ? 1 : 0;
                    additionalImages = newListing.Media.Count(m => m.Type == applicableMediaTypeName) > 1 ? newListing.Media.Count(m => m.Type == applicableMediaTypeName) - 1 : 0;
                }

                if (firstImages > 0)
                {
                    //make line item
                    LineItem lineItem = new LineItem
                                            {
                                                TotalAmount = firstImageFee.Amount,
                                                Quantity = 1,
                                                PerUnitAmount = firstImageFee.Amount,
                                                Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                                DateStamp = DateTime.UtcNow,
                                                Properties = new Dictionary<string, string>(commonProperties),
                                                Description = firstImageFee.Description,
                                                //Owner = _data.GetSiteOwner(),
                                                Payer = newListing.Owner,
                                                ListingID = newListing.ID,
                                                RelistIteration = newListing.RelistIteration,
                                                Type = Strings.LineItemTypes.Fee
                                            };
                    newLineItems.Add(lineItem);
                }

                if (additionalImages > 0)
                {
                    //make line item
                    LineItem lineItem = new LineItem
                                            {
                                                Quantity = 0, 
                                                PerUnitAmount = 0.0M
                                            };
                    lineItem.TotalAmount = lineItem.Quantity * additionalImagesFee.Amount;
                    lineItem.Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency];
                    lineItem.DateStamp = DateTime.UtcNow;
					lineItem.Properties = new Dictionary<string, string>(commonProperties);
                    lineItem.Description = additionalImagesFee.Description;
                    //lineItem.Owner = _data.GetSiteOwner();
                    lineItem.Payer = newListing.Owner;
                    lineItem.ListingID = newListing.ID;
                    lineItem.RelistIteration = newListing.RelistIteration;
                    lineItem.Type = Strings.LineItemTypes.Fee;
                    newLineItems.Add(lineItem);
                }

                payToProceed = payToProceed || bool.Parse(Cache.SiteProperties[Strings.SiteProperties.PayToProceed]);

                return newLineItems;
#if TRACE
            }
#endif
        }
    }
}
