﻿using System;
using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class Locations : FeeProcessorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public Locations() {}

        //Via Unity
        public Locations(IDataContext data) : base(data) { }

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing changed yet

                //register for all listing types
                List<ListingType> listingTypes = _data.GetAllListingTypes();

                foreach (ListingType listingType in listingTypes)
                {
                    //Check applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //Didn't exist, adding
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //something changed
                        retVal = true;
                    }
                }

                return retVal;
#if TRACE
            }
#endif
        }

        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.Locations"; }
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newListing, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //check to see if this fee provider is applicable
                if (!applicableFeeProviders.Any(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled)) return (new List<LineItem>(0)); 

                //if not adding and not updating, add no fees
                if (!eventName.Equals(Strings.Events.AddListing) && !eventName.Equals(Strings.Events.UpdateListing)) return (new List<LineItem>(0));

                List<Location> toAdd = new List<Location>();
                if (oldListing != null)
                {
                    //get ADDED locations
                    toAdd.AddRange(newListing.Locations.Where(location => oldListing.Locations.All(l => l.ID != location.ID)));
                }
                else
                {
                    toAdd = newListing.Locations;
                }

                List<LineItem> newLineItems = new List<LineItem>(toAdd.Count);

                foreach (Location location in toAdd)
                {

                    //make line item
                    LineItem lineItem = new LineItem
                                            {
                                                TotalAmount = location.Amount,
                                                Quantity = 0,
                                                PerUnitAmount = 0.0M,
                                                Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                                DateStamp = DateTime.UtcNow,
                                                Properties = new Dictionary<string, string>(commonProperties),
                                                /*Description = string.Format(Strings.Formats.LocationName, location.Name, location.Description),*/
                                                Description = location.Description,
                                                //Owner = _data.GetSiteOwner(),
                                                Payer = newListing.Owner,
                                                ListingID = newListing.ID,
                                                RelistIteration = newListing.RelistIteration,
                                                Type = Strings.LineItemTypes.Fee
                                            };
                    newLineItems.Add(lineItem);

                    if (!payToProceed && location.PayToProceed) payToProceed = true;
                }

                return newLineItems;
#if TRACE
            }
#endif
        }
    }
}
