﻿using System;
using System.Collections.Generic;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class SubtitleExists : FeeProcessorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public SubtitleExists() {}

        //Via Unity
        public SubtitleExists(IDataContext data) : base(data) { }

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing changed yet

                //register for all listing types & events "AddListing" and "UpdateListing"
                List<ListingType> listingTypes = _data.GetAllListingTypes();
                List<ListItem> events = _data.GetEvents();

                List<FeeProperty> propertiesToAdd = new List<FeeProperty>();

                foreach (ListingType listingType in listingTypes)
                {
                    //Check Fee Properties
                    foreach (ListItem eventItem in events.Where(e => e.Name.Equals(Strings.Events.AddListing) || e.Name.Equals(Strings.Events.UpdateListing)))
                    {
                        FeeProperty existingFeeProperty = GetFeeProperty(eventItem.Name, "Subtitle", listingType.ID);

                        if (existingFeeProperty == null)
                        {
                            //create this property
                            FeeProperty newFeeProperty = new FeeProperty
                                                             {
                                                                 Amount = 0.00M,
                                                                 Description = "Listing Subtitle",
                                                                 Event = eventItem,
                                                                 ListingType =
                                                                     new ListItem(listingType.ID, listingType.Name, true,
                                                                                  string.Empty),
                                                                 Name = "Subtitle",
                                                                 Processor = GetType().FullName
                                                             };

                            propertiesToAdd.Add(newFeeProperty);

                            //something changed
                            retVal = true;
                        }
                    }

                    //Check applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //Didn't exist, adding
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //something changed
                        retVal = true;
                    }
                }

                if (retVal && propertiesToAdd.Count > 0)
                {
                    UpdateFeeProperties(actingUserName, propertiesToAdd);
                }

                return retVal;
#if TRACE
            }
#endif
        }

        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.SubtitleExists"; }
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newListing, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //check to see if this fee provider is applicable
                if (!applicableFeeProviders.Any(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled)) return (new List<LineItem>(0)); 

                //if not adding and not updating, add no fees
                if (!eventName.Equals(Strings.Events.AddListing) && !eventName.Equals(Strings.Events.UpdateListing)) return (new List<LineItem>(0));

                List<LineItem> newLineItems = new List<LineItem>(1);

                FeeProperty fee = GetFeeProperty(eventName, "Subtitle", newListing.Type.ID);

                if (fee == null) return newLineItems;

                if (string.IsNullOrEmpty(newListing.Subtitle)) return newLineItems;

                if (oldListing == null || string.IsNullOrEmpty(oldListing.Subtitle))
                {
                    //make line item
                    LineItem lineItem = new LineItem
                                            {
                                                TotalAmount = fee.Amount,
                                                Quantity = 0,
                                                PerUnitAmount = 0.0M,
                                                Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                                DateStamp = DateTime.UtcNow,
                                                Properties = new Dictionary<string, string>(commonProperties),
                                                Description = fee.Description,
                                                //Owner = _data.GetSiteOwner(),
                                                Payer = newListing.Owner,
                                                ListingID = newListing.ID,
                                                RelistIteration = newListing.RelistIteration,
                                                Type = Strings.LineItemTypes.Fee
                                            };
                    newLineItems.Add(lineItem);
                }

                payToProceed = payToProceed || bool.Parse(Cache.SiteProperties[Strings.SiteProperties.PayToProceed]);

                return newLineItems;
#if TRACE
            }
#endif
        }
    }
}
