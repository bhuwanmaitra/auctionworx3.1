﻿using System;
using System.Collections.Generic;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;
using System.Linq;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class Decorations : FeeProcessorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public Decorations() {}

        //Via Unity
        public Decorations(IDataContext data) : base(data) { }

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing changed yet

                //register for all listing types
                List<ListingType> listingTypes = _data.GetAllListingTypes();

                foreach (ListingType listingType in listingTypes)
                {
                    //Check applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //Didn't exist, adding
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //something changed
                        retVal = true;
                    }
                }

                return retVal;
#if TRACE
            }
#endif
        }

        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.Decorations"; }
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newListing, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif                                
                //check to see if this fee provider is applicable
                if (applicableFeeProviders.Where(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled).Count() <= 0) return (new List<LineItem>(0)); 

                List<Decoration> toAdd = new List<Decoration>();

                //if not adding and not updating, add no fees
                if (!eventName.Equals(Strings.Events.AddListing) && !eventName.Equals(Strings.Events.UpdateListing)) return new List<LineItem>(0);

                if (oldListing != null)
                {
                    //get ADDED decorations
                    foreach (Decoration decoration in newListing.Decorations)
                    {
                        if (oldListing.Decorations.Where(d => d.ID == decoration.ID).Count() <= 0)
                        {
                            toAdd.Add(decoration);
                        }
                    }
                } else
                {
                    toAdd = newListing.Decorations;
                }

                List<LineItem> newLineItems = new List<LineItem>(toAdd.Count);

                foreach (Decoration decoration in toAdd)
                {
                    //make line item
                    LineItem lineItem = new LineItem();
                    lineItem.TotalAmount = decoration.Amount;
                    lineItem.Quantity = 0;
                    lineItem.PerUnitAmount = 0.0M;
                    lineItem.Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency];
                    lineItem.DateStamp = DateTime.UtcNow;
					lineItem.Properties = new Dictionary<string, string>(commonProperties);
                    //lineItem.Description = string.Format(Strings.Formats.DecorationName, decoration.Name, decoration.Description);
                    lineItem.Description = decoration.Description;
                    //lineItem.Owner = _data.GetSiteOwner();
                    lineItem.Payer = newListing.Owner;
                    lineItem.ListingID = newListing.ID;
                    lineItem.RelistIteration = newListing.RelistIteration;
                    lineItem.Type = Strings.LineItemTypes.Fee;
                    newLineItems.Add(lineItem);

                    if (!payToProceed && decoration.PayToProceed) payToProceed = true;
                }


                return newLineItems;
#if TRACE
            }
#endif
        }
    }
}
