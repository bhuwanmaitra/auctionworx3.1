﻿using System;
using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class FlatFee : FeeProcessorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public FlatFee() {}

        //Via Unity
        public FlatFee(IDataContext data) : base(data) { } 

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                bool retVal = false; //nothing changed yet
            
                //register for all listing types & events "AddListing" and "UpdateListing"
                List<ListingType> listingTypes = _data.GetAllListingTypes();
                List<ListItem> events = _data.GetEvents();                

                List<FeeProperty> propertiesToAdd = new List<FeeProperty>();
                List<FeeProperty> propertiesToDelete = new List<FeeProperty>();
                foreach (ListingType listingType in listingTypes)
                {
                    //Check Fee Properties
                    foreach (ListItem eventItem in events.Where(e => e.Name.Equals(Strings.Events.AddListing) || e.Name.Equals(Strings.Events.UpdateListing)))
                    {
                        FeeProperty existingFeeProperty = GetFeeProperty(eventItem.Name, Strings.FeeNames.FlatFee, listingType.ID);

                        //Modified 2011-10-31 - this fee should only exist for "Classified + AddListing", no others! ~CEB
                        if (!(listingType.Name == Strings.ListingTypes.Classified &&
                              eventItem.Name == Strings.Events.AddListing))
                        {
                            if (existingFeeProperty != null)
                            {
                                //delete any flat fee other than "Classified + AddListing"
                                propertiesToDelete.Add(existingFeeProperty);
                                retVal = true;
                            }
                            continue;
                        }

                        if (existingFeeProperty == null)
                        {
                            //create this property
                            FeeProperty newFeeProperty = new FeeProperty
                                                             {
                                                                 Amount = 0.00M,
                                                                 Description = "Flat Fee",
                                                                 Event = eventItem,
                                                                 ListingType =
                                                                     new ListItem(listingType.ID, listingType.Name, true,
                                                                                  string.Empty),
                                                                 Name = Strings.FeeNames.FlatFee,
                                                                 Processor = GetType().FullName
                                                             };

                            propertiesToAdd.Add(newFeeProperty);    

                            //something changed
                            retVal = true;
                        }
                    }

                    //Check applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //Didn't exist, adding
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //something changed
                        retVal = true;
                    }
                }

                if (retVal && propertiesToAdd.Count > 0)
                {
                    UpdateFeeProperties(actingUserName, propertiesToAdd);
                }

                if (retVal && propertiesToDelete.Count > 0)
                {
                    foreach (FeeProperty feeToDelete in propertiesToDelete)
                    {
                        Cache.FeeProperties.RemoveAll(f => f.ID == feeToDelete.ID);
                        _data.DeleteFeeProperty(actingUserName, feeToDelete.ID);
                    }
                }

                return retVal;
#if TRACE
            }
#endif
        }

        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.FlatFee"; }
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName, Listing oldListing, Listing newListing, ListingAction listingAction, Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif
                //check to see if this fee provider is applicable
                if (!applicableFeeProviders.Any(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled)) return (new List<LineItem>(0));

                //return "no fees" if an "exclude" category is present or an "include" category is missing from this listing for this fee type
                bool skipFlatFee = false;
                string flatFeeCatList = Cache.SiteProperties[Strings.SiteProperties.FeeCategories_FlatFee];
                string flatFeeCategoryMode = Cache.SiteProperties[Strings.SiteProperties.FeeCategoryMode_FlatFee];
                if (!string.IsNullOrEmpty(flatFeeCatList))
                {
                    var selectedCategories = new List<int>();
                    foreach (string possibleCatId in flatFeeCatList.Split(','))
                    {
                        int catId;
                        if (int.TryParse(possibleCatId.Trim(), out catId))
                        {
                            selectedCategories.Add(catId);
                        }
                    }
                    if (flatFeeCategoryMode == "ExcludeSelected" && newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipFlatFee = true;
                    }
                    else if (flatFeeCategoryMode == "IncludeSelected" && !newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipFlatFee = true;
                    }
                }

                List<LineItem> newLineItems = new List<LineItem>(1);

                //fee n/a for this listing's category
                if (skipFlatFee) return newLineItems;

                //edit if not adding or updating
                if (!eventName.Equals(Strings.Events.AddListing) && !eventName.Equals(Strings.Events.UpdateListing)) return newLineItems;

                //get listingFee
                FeeProperty listingFee = GetFeeProperty(eventName, Strings.FeeNames.FlatFee, newListing.Type.ID);

                //if no fee was defined for this class, event, and listing type, return no fees...
                if (listingFee == null) return newLineItems;

                //make line item
                LineItem lineItem = new LineItem
                                        {
                                            TotalAmount = listingFee.Amount,
                                            Quantity = 0,
                                            PerUnitAmount = 0.0M,
                                            Currency = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                            DateStamp = DateTime.UtcNow,
                                            Properties = new Dictionary<string, string>(commonProperties),
                                            Description = listingFee.Description,
                                            //Owner = _data.GetSiteOwner(),
                                            Payer = newListing.Owner,
                                            ListingID = newListing.ID,
                                            RelistIteration = newListing.RelistIteration,
                                            Type = Strings.LineItemTypes.Fee
                                        };
                newLineItems.Add(lineItem);

                payToProceed = payToProceed || bool.Parse(Cache.SiteProperties[Strings.SiteProperties.PayToProceed]);

                return newLineItems;
#if TRACE
            }
#endif
        }
    }
}
