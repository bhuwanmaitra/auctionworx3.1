﻿using System;
using System.Collections.Generic;
using System.Linq;
using RainWorx.FrameWorx.DAL;
using RainWorx.FrameWorx.DTO;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Utility;

namespace RainWorx.FrameWorx.Providers.Fee.Standard
{
    public class CurrentPrice : FeeProcessorBase
    {
        //Via WCF (where 0 argument constructor is required)
        public CurrentPrice() {}

        //Via Unity
        public CurrentPrice(IDataContext data) : base(data) {}
    
        public override string FeeProviderName
        {
            get { return "RainWorx.FrameWorx.Providers.Fee.Standard.CurrentPrice"; }
        }

        public override bool RegisterSelf(string actingUserName)
        {
#if TRACE
            using (LogManager.Trace())
            {
#endif   
                bool retVal = false; //nothing changed yet

                //Get all listing types to register this implementation with
                List<ListingType> listingTypes = _data.GetAllListingTypes();

                //for each listing type...
                foreach (ListingType listingType in listingTypes)
                {
                    //Get applicable Fee Providers
                    List<ListItem> applicableFeeProviders = _data.GetListingTypeFeeProviders(listingType.Name);

                    //if *this is not an applicable fee provider...
                    if (!applicableFeeProviders.Any(li => li.Name.Equals(GetType().FullName)))
                    {
                        //... add it
                        _data.AddListingTypeFeeProvider(listingType.Name, GetType().FullName, true);
                        //Indicate that something changed
                        retVal = true;
                    }

                    //Ensure that the "Post Listing Fee" Fee Schedule exists for the "AddListing" event of this listing type, with Name == "Seller".
                    retVal =
                        EnsureFeeSchedule(actingUserName, listingType.Name, "Post Listing Fee", Strings.Events.AddListing, false, Strings.Roles.Seller) ||
                        retVal;

                    //Ensure that the "Final % Fee" Fee Schedule exists for the "EndListingSuccess" event of this listing type, with Name == "Seller".
                    retVal =
                        EnsureFeeSchedule(actingUserName, listingType.Name, "Seller Final Fee", Strings.Events.EndListingSuccess, false, Strings.Roles.Seller) ||
                        retVal;

                    //Ensure that the "Final Buyer Fee" Fee Schedule exists for the "EndListingSuccess" event of this listing type, with Name == "Buyer".
                    retVal =
                        EnsureFeeSchedule(actingUserName, listingType.Name, "Buyer Final Fee", Strings.Events.EndListingSuccess, false, Strings.Roles.Buyer) ||
                        retVal;
                }

                //Ensure the required site properties exists
                retVal =
                    EnsureSiteProperty(actingUserName, Strings.SiteProperties.UseAggregateBuyerFinalPercentageFees,
                                       CustomFieldType.Boolean, "true", 1000, false, 150, "") || retVal;
                retVal =
                    EnsureSiteProperty(actingUserName, Strings.SiteProperties.UseFlatBuyerFinalFees,
                                       CustomFieldType.Boolean, "false", 1100, false, 150, "") || retVal;

                return retVal;
#if TRACE
            }
#endif
        }

        public override List<LineItem> GenerateFees(List<ListItem> applicableFeeProviders, string eventName,
                                                    Listing oldListing, Listing newListing, ListingAction listingAction,
                                                    Dictionary<string, string> commonProperties, ref bool payToProceed)
        {
    #if TRACE
            using (LogManager.Trace())
            {
    #endif
                //check to see if this fee provider is applicable
                if (!applicableFeeProviders.Any(afp => afp.Name.Equals(FeeProviderName) && afp.Enabled))
                    return (new List<LineItem>(0));

                //return "no fees" if an "exclude" category is present or an "include" category is missing from this listing for this fee type
                bool skipPostFee = false;
                string postFeeCatList = Cache.SiteProperties[Strings.SiteProperties.FeeCategories_PostListingFee];
                string postFeeCategoryMode = Cache.SiteProperties[Strings.SiteProperties.FeeCategoryMode_PostListingFee];
                if (!string.IsNullOrEmpty(postFeeCatList))
                {
                    var selectedCategories = new List<int>();
                    foreach (string possibleCatId in postFeeCatList.Split(','))
                    {
                        int catId;
                        if (int.TryParse(possibleCatId.Trim(), out catId))
                        {
                            selectedCategories.Add(catId);
                        }
                    }
                    if (postFeeCategoryMode == "ExcludeSelected" && newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipPostFee = true;
                    }
                    else if (postFeeCategoryMode == "IncludeSelected" && !newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipPostFee = true;
                    }
                }
                bool skipSellerFinalFee = false;
                string sellerFinalFeeCatList = Cache.SiteProperties[Strings.SiteProperties.FeeCategories_FinalSaleFee];
                string sellerFinalFeeCategoryMode = Cache.SiteProperties[Strings.SiteProperties.FeeCategoryMode_FinalSaleFee];
                if (!string.IsNullOrEmpty(sellerFinalFeeCatList))
                {
                    var selectedCategories = new List<int>();
                    foreach (string possibleCatId in sellerFinalFeeCatList.Split(','))
                    {
                        int catId;
                        if (int.TryParse(possibleCatId.Trim(), out catId))
                        {
                            selectedCategories.Add(catId);
                        }
                    }
                    if (sellerFinalFeeCategoryMode == "ExcludeSelected" && newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipSellerFinalFee = true;
                    }
                    else if (sellerFinalFeeCategoryMode == "IncludeSelected" && !newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipSellerFinalFee = true;
                    }
                }
                bool skipBuyerFinalFee = false;
                string buyerFinalFeeCatList = Cache.SiteProperties[Strings.SiteProperties.FeeCategories_FinalBuyerFee];
                string buyerFinalFeeCategoryMode = Cache.SiteProperties[Strings.SiteProperties.FeeCategoryMode_FinalBuyerFee];
                if (!string.IsNullOrEmpty(buyerFinalFeeCatList))
                {
                    var selectedCategories = new List<int>();
                    foreach (string possibleCatId in buyerFinalFeeCatList.Split(','))
                    {
                        int catId;
                        if (int.TryParse(possibleCatId.Trim(), out catId))
                        {
                            selectedCategories.Add(catId);
                        }
                    }
                    if (buyerFinalFeeCategoryMode == "ExcludeSelected" && newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipBuyerFinalFee = true;
                    }
                    else if (buyerFinalFeeCategoryMode == "IncludeSelected" && !newListing.Categories.Any(lc => selectedCategories.Any(sc => sc == lc.ID)))
                    {
                        skipBuyerFinalFee = true;
                    }
                }

                List<LineItem> newLineItems = new List<LineItem>();

                try
                {
                    if (oldListing == null || newListing.CurrentPrice > oldListing.CurrentPrice)
                    {
                        //the exchange rate needs to be factored in if the listing is not the site currency type
                        string siteCurrencyCode = Cache.SiteProperties[Strings.SiteProperties.SiteCurrency];

                        decimal oldFeeAmount = 0.00M;
                        decimal convertedOldPrice = 0.0M;
                        if (oldListing != null && newListing.CurrentPrice > oldListing.CurrentPrice)
                        {
                            convertedOldPrice = oldListing.CurrentPrice ?? 0.0M;
                            if (oldListing != null && newListing.CurrentPrice > oldListing.CurrentPrice)
                            {
                                if (!oldListing.Currency.Code.Equals(siteCurrencyCode))
                                {
                                    Currency siteCurrency = _data.GetCurrencyByCode(siteCurrencyCode);
                                    if (siteCurrency == null)
                                        Statix.ThrowInvalidOperationFaultContract(ReasonCode.CurrencyNotSupported);
                                    Currency listingCurrency = _data.GetCurrencyByCode(newListing.Currency.Code);
                                    if (listingCurrency == null)
                                        Statix.ThrowInvalidOperationFaultContract(ReasonCode.CurrencyNotSupported);
                                    convertedOldPrice = convertedOldPrice / listingCurrency.ConversionToUSD * siteCurrency.ConversionToUSD;
                                }
                            }
                        }

                        decimal convertedPrice = newListing.CurrentPrice ?? 0.0M;
                        if (!newListing.Currency.Code.Equals(siteCurrencyCode))
                        {
                            Currency siteCurrency = _data.GetCurrencyByCode(siteCurrencyCode);
                            if (siteCurrency == null)
                                Statix.ThrowInvalidOperationFaultContract(ReasonCode.CurrencyNotSupported);
                            Currency listingCurrency = _data.GetCurrencyByCode(newListing.Currency.Code);
                            if (listingCurrency == null)
                                Statix.ThrowInvalidOperationFaultContract(ReasonCode.CurrencyNotSupported);
                            convertedPrice = convertedPrice / listingCurrency.ConversionToUSD * siteCurrency.ConversionToUSD;
                        }

                        //post fee or final seller fee
                        if ((!skipPostFee && (eventName == Strings.Events.AddListing || eventName == Strings.Events.UpdateListing)) ||
                            (!skipSellerFinalFee && eventName == Strings.Events.EndListingSuccess))
                        {
                            List<FeeSchedule> sellerFeeSchedules = _data.GetFeeSchedules(eventName, newListing.Type.Name,
                                                                                         Strings.Roles.Seller);
                            if (oldListing != null && newListing.CurrentPrice > oldListing.CurrentPrice)
                            {
                                foreach (FeeSchedule feeSchedule in sellerFeeSchedules)
                                {
                                    bool bAggregateFee = false;
                                    if (bool.Parse(Cache.SiteProperties[Strings.SiteProperties.UseAggregateFinalPercentageFees]))
                                    {
                                        if (feeSchedule.Event.Name == Strings.Events.EndListingSuccess)
                                        {
                                            if (feeSchedule.Tiers.Count > 0 &&
                                                feeSchedule.Tiers[0].ValueType == Strings.TierTypes.Percent)
                                            {
                                                bAggregateFee = true;
                                            }
                                        }
                                    }

                                    if (feeSchedule.PayToProceed) payToProceed = true;
                                    //decimal feeValue = 0;
                                    decimal previousTierValue = 0;
                                    decimal amountProcessed = 0;

                                    if (bAggregateFee)
                                    {
                                        //aggregate all applicable tiers (e.g. final % fee when "Use Aggregate Final Percentage Fees" is true
                                        foreach (Tier tier in feeSchedule.Tiers.OrderBy(t => t.UpperBoundExclusive))
                                        {
                                            if (amountProcessed < convertedOldPrice)
                                            {
                                                decimal value;
                                                if (convertedOldPrice > tier.UpperBoundExclusive)
                                                {
                                                    value = tier.UpperBoundExclusive - previousTierValue;
                                                    previousTierValue = tier.UpperBoundExclusive;
                                                }
                                                else
                                                {
                                                    value = convertedOldPrice - amountProcessed;
                                                }

                                                if (value > 0)
                                                {
                                                    switch (tier.ValueType)
                                                    {
                                                        case Strings.TierTypes.Fixed:
                                                            {
                                                                oldFeeAmount += tier.Value;
                                                                break;
                                                            }
                                                        case Strings.TierTypes.Percent:
                                                            {
                                                                oldFeeAmount += tier.Value * value;
                                                                break;
                                                            }
                                                    }
                                                    amountProcessed += value;
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //use single highest applicable tier (e.g. post listing fee)
                                        var applicableTier =
                                            feeSchedule.Tiers.Where(t => convertedOldPrice < t.UpperBoundExclusive)
                                                .OrderBy(t => t.UpperBoundExclusive)
                                                .FirstOrDefault();
                                        if (applicableTier != null)
                                        {
                                            switch (applicableTier.ValueType)
                                            {
                                                case Strings.TierTypes.Fixed:
                                                    {
                                                        oldFeeAmount = applicableTier.Value;
                                                        break;
                                                    }
                                                case Strings.TierTypes.Percent:
                                                    {
                                                        oldFeeAmount += applicableTier.Value * convertedOldPrice;
                                                        break;
                                                    }
                                            }
                                        }
                                    }
                                }
                            }

                            foreach (FeeSchedule feeSchedule in sellerFeeSchedules)
                            {
                                bool bAggregateFee = false;
                                if (bool.Parse(Cache.SiteProperties[Strings.SiteProperties.UseAggregateFinalPercentageFees]))
                                {
                                    if (feeSchedule.Event.Name == Strings.Events.EndListingSuccess)
                                    {
                                        if (feeSchedule.Tiers.Count > 0 &&
                                            feeSchedule.Tiers[0].ValueType == Strings.TierTypes.Percent)
                                        {
                                            bAggregateFee = true;
                                        }
                                    }
                                }

                                if (feeSchedule.PayToProceed) payToProceed = true;
                                decimal feeValue = 0;
                                decimal previousTierValue = 0;
                                decimal amountProcessed = 0;

                                if (bAggregateFee)
                                {
                                    //aggregate all applicable tiers (e.g. final % fee when "Use Aggregate Final Percentage Fees" is true
                                    foreach (Tier tier in feeSchedule.Tiers.OrderBy(t => t.UpperBoundExclusive))
                                    {
                                        if (amountProcessed < convertedPrice)
                                        {
                                            decimal value;
                                            if (convertedPrice > tier.UpperBoundExclusive)
                                            {
                                                value = tier.UpperBoundExclusive - previousTierValue;
                                                previousTierValue = tier.UpperBoundExclusive;
                                            }
                                            else
                                            {
                                                value = convertedPrice - amountProcessed;
                                            }

                                            if (value > 0)
                                            {
                                                switch (tier.ValueType)
                                                {
                                                    case Strings.TierTypes.Fixed:
                                                        {
                                                            feeValue += tier.Value;
                                                            break;
                                                        }
                                                    case Strings.TierTypes.Percent:
                                                        {
                                                            feeValue += tier.Value * value;
                                                            break;
                                                        }
                                                }
                                                amountProcessed += value;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    //use single highest applicable tier (e.g. post listing fee)
                                    var applicableTier =
                                        feeSchedule.Tiers.Where(t => convertedPrice < t.UpperBoundExclusive)
                                            .OrderBy(t => t.UpperBoundExclusive)
                                            .FirstOrDefault();
                                    if (applicableTier != null)
                                    {
                                        switch (applicableTier.ValueType)
                                        {
                                            case Strings.TierTypes.Fixed:
                                                {
                                                    feeValue = applicableTier.Value;
                                                    break;
                                                }
                                            case Strings.TierTypes.Percent:
                                                {
                                                    feeValue += applicableTier.Value * convertedPrice;
                                                    break;
                                                }
                                        }
                                    }
                                }

                                //subtract out the old fee amount if applicable
                                feeValue -= oldFeeAmount;

                                //2012-03-05, ceb, For fixed price listings, the "Final % Fee" is to be charged for each item purchased at the time of the purchase,
                                //   not a single fee when the listing expires.
                                int applicableQuantity = (listingAction != null) ? listingAction.Quantity : 1;
                                decimal applicableUnitPrice = (listingAction != null) ? convertedPrice * applicableQuantity : 0.0M;

                                //make line item
                                LineItem lineItem = new LineItem
                                {
                                    TotalAmount = (applicableQuantity * feeValue),
                                    Quantity = 0,
                                    PerUnitAmount = applicableUnitPrice, // for fee line items, this will always be the total basis for the fee, not literally the unit price
                                    Currency =
                                                                Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                    DateStamp = DateTime.UtcNow,
                                    Description = feeSchedule.Description,
                                    //Owner = _data.GetSiteOwner(),
                                    Payer = newListing.Owner,
                                    Properties =
                                                                new Dictionary<string, string>(commonProperties),
                                    ListingID = newListing.ID,
                                    RelistIteration = newListing.RelistIteration,
                                    Type = Strings.LineItemTypes.Fee
                                };
                                newLineItems.Add(lineItem);
                            } // each seller fee schedule
                        }

                        //final buyer fee
                        if (!skipBuyerFinalFee && eventName == Strings.Events.EndListingSuccess)
                        {
                            List<FeeSchedule> buyerFeeSchedules = _data.GetFeeSchedules(eventName, newListing.Type.Name,
                                                                                        Strings.Roles.Buyer);
                            foreach (FeeSchedule feeSchedule in buyerFeeSchedules)
                            {
                                bool bAggregateFee = false;
                                if (bool.Parse(Cache.SiteProperties[Strings.SiteProperties.UseAggregateBuyerFinalPercentageFees]))
                                {
                                    if (feeSchedule.Event.Name == Strings.Events.EndListingSuccess)
                                    {
                                        if (feeSchedule.Tiers.Count > 0 &&
                                            feeSchedule.Tiers[0].ValueType == Strings.TierTypes.Percent)
                                        {
                                            bAggregateFee = true;
                                        }
                                    }
                                }

                                if (feeSchedule.PayToProceed) payToProceed = true;
                                decimal feeValue = 0;
                                decimal previousTierValue = 0;
                                decimal amountProcessed = 0;

                                if (bAggregateFee)
                                {
                                    //aggregate all applicable tiers (e.g. final % fee when "Use Aggregate Final Percentage Fees" is true
                                    foreach (Tier tier in feeSchedule.Tiers.OrderBy(t => t.UpperBoundExclusive))
                                    {
                                        if (amountProcessed < convertedPrice)
                                        {
                                            decimal value;
                                            if (convertedPrice > tier.UpperBoundExclusive)
                                            {
                                                value = tier.UpperBoundExclusive - previousTierValue;
                                                previousTierValue = tier.UpperBoundExclusive;
                                            }
                                            else
                                            {
                                                value = convertedPrice - amountProcessed;
                                            }

                                            if (value > 0)
                                            {
                                                switch (tier.ValueType)
                                                {
                                                    case Strings.TierTypes.Fixed:
                                                        {
                                                            feeValue += tier.Value;
                                                            break;
                                                        }
                                                    case Strings.TierTypes.Percent:
                                                        {
                                                            feeValue += tier.Value * value;
                                                            break;
                                                        }
                                                }
                                                amountProcessed += value;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    //use single highest applicable tier (e.g. post listing fee)
                                    var applicableTier =
                                        feeSchedule.Tiers.Where(t => convertedPrice < t.UpperBoundExclusive)
                                            .OrderBy(t => t.UpperBoundExclusive)
                                            .FirstOrDefault();
                                    if (applicableTier != null)
                                    {
                                        switch (applicableTier.ValueType)
                                        {
                                            case Strings.TierTypes.Fixed:
                                                {
                                                    feeValue = applicableTier.Value;
                                                    break;
                                                }
                                            case Strings.TierTypes.Percent:
                                                {
                                                    feeValue += applicableTier.Value * convertedPrice;
                                                    break;
                                                }
                                        }
                                    }
                                }

                                //2012-03-05, ceb, For fixed price listings, the "Final % Fee" is to be charged for each item purchased at the time of the purchase,
                                //   not a single fee when the listing expires.
                                int applicableQuantity = (listingAction == null) ? 1 : listingAction.Quantity;

                                User buyer = null;
                                if (listingAction != null)
                                {
                                    buyer = listingAction.User;
                                }
                                else if (newListing.CurrentListingAction != null)
                                {
                                    buyer = newListing.CurrentListingAction.User;
                                }

                                if (buyer != null)
                                {
                                    //make line item
                                    LineItem lineItem = new LineItem
                                    {
                                        TotalAmount = (applicableQuantity * feeValue),
                                        Quantity = applicableQuantity,
                                        PerUnitAmount = feeValue,
                                        Currency =
                                                                    Cache.SiteProperties[Strings.SiteProperties.SiteCurrency],
                                        DateStamp = DateTime.UtcNow,
                                        Description = feeSchedule.Description,
                                        //Owner = _data.GetSiteOwner(),
                                        Payer = buyer,
                                        Properties =
                                                                    new Dictionary<string, string>(commonProperties),
                                        ListingID = newListing.ID,
                                        RelistIteration = newListing.RelistIteration,
                                        Type = Strings.LineItemTypes.Fee
                                    };
                                    newLineItems.Add(lineItem);
                                }
                            } // each buyer fee schedule
                        }

                    }
                }
                catch (Exception e)
                {
                    if (LogManager.HandleException(e, Strings.FunctionalAreas.Accounting)) throw;
                }
                return newLineItems;
    #if TRACE
            }
    #endif
        }
    }
}
