﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using RainWorx.FrameWorx.Unity;
using RainWorx.FrameWorx.Queueing;

[assembly: OwinStartup(typeof(RainWorx.FrameWorx.MVC.Startup))]
namespace RainWorx.FrameWorx.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var queueManager = UnityResolver.Get<IQueueManager>();
            if (queueManager.GetType() != typeof(QueueingDisabled))
            {
                if (queueManager.GetType() == typeof(AzureServiceBus))
                {
                    GlobalHost.DependencyResolver.UseServiceBus(ConfigurationManager.ConnectionStrings["azure_service_bus"].ConnectionString, "AweSignalR");//ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"];
                }
                else //if (queueManager.GetType() == typeof(SQLServiceBroker))
                {
                    GlobalHost.DependencyResolver.UseSqlServer(ConfigurationManager.ConnectionStrings["db_connection"].ConnectionString);
                }
                app.MapSignalR();
            }
            queueManager = null;
            ConfigureAuth(app);
        }
    }
}
