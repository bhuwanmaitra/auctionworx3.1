﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RainWorx.FrameWorx.MVC.Models
{
    public class SliderModel
    {
        public int id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public HttpPostedFileBase file { get; set; }
        public string ImagePath { get; set; }
        public string ButtonLink { get; set; }
        public string OldRate { get; set; }
        public string NewRate { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }

        public List<SliderModel> SliderModel_list { get; set; }

    }
}