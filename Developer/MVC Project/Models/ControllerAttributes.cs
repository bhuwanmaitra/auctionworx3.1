﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RainWorx.FrameWorx.Clients;
using System.Linq;

namespace RainWorx.FrameWorx.MVC.Models
{
    public abstract class ModelStateTempDataTransfer : ActionFilterAttribute
    {
        protected static readonly string Key = typeof(ModelStateTempDataTransfer).FullName;
    }

    //public class ExportModelStateToTempData : ModelStateTempDataTransfer
    //{
    //    public override void OnActionExecuted(ActionExecutedContext filterContext)
    //    {
    //        //Only export when ModelState is not valid
    //        if (!filterContext.Controller.ViewData.ModelState.IsValid)
    //        {
    //            //Export if we are redirecting
    //            if ((filterContext.Result is RedirectResult) || (filterContext.Result is RedirectToRouteResult))
    //            {
    //                filterContext.Controller.TempData[Key] = filterContext.Controller.ViewData.ModelState;
    //            }
    //        }

    //        base.OnActionExecuted(filterContext);
    //    }
    //}

    //public class ImportModelStateFromTempData : ModelStateTempDataTransfer
    //{
    //    public override void OnActionExecuted(ActionExecutedContext filterContext)
    //    {
    //        ModelStateDictionary modelState = filterContext.Controller.TempData[Key] as ModelStateDictionary;

    //        if (modelState != null)
    //        {
    //            //Only Import if we are viewing
    //            if (filterContext.Result is ViewResult)
    //            {
    //                filterContext.Controller.ViewData.ModelState.Merge(modelState);
    //            }
    //            else
    //            {
    //                //Otherwise remove it.
    //                filterContext.Controller.TempData.Remove(Key);
    //            }
    //        }

    //        base.OnActionExecuted(filterContext);
    //    }
    //}


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class GoUnsecure : FilterAttribute, IAuthorizationFilter
    {
        public static bool EnableSSL = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"]);
        public static bool AlwaysSSL = bool.Parse(ConfigurationManager.AppSettings.AllKeys.Contains("AlwaysSSL") ? ConfigurationManager.AppSettings["AlwaysSSL"] : "False");

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!EnableSSL && !AlwaysSSL)
            {                
                return;
            }

            // abort if it's not a secure connection AND AlwaysSSL is false
            if (!AlwaysSSL && !filterContext.HttpContext.Request.IsSecureConnection) return;

            // abort if it's a secure connection AND AlwaysSSL is true
            if (AlwaysSSL && filterContext.HttpContext.Request.IsSecureConnection) return;

            // abort if a [GoSecure] attribute is applied to action
            //if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(GoSecure), true).Length > 0) return;
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(GoSecure), true).Length > 0) return;

            //// abort if a [RetainHttps] attribute is applied to controller or action  
            //if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;
            //if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;

            // abort if it's not a GET request - we don't want to be redirecting on a form post  
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)) return;

            if (AlwaysSSL && !filterContext.HttpContext.Request.IsSecureConnection)
            {
                // redirect to HTTPS              
                Uri urlBuilder = new Uri(SiteClient.Settings[Strings.SiteProperties.SecureURL]);

                //abort if "SecureURL" is not an "HTTPS" value
                if (!urlBuilder.Scheme.Equals("https", StringComparison.OrdinalIgnoreCase)) return;

                string url = urlBuilder.Scheme + "://" + urlBuilder.Authority + filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectResult(url);
            }
            else
            {
                // redirect to HTTP  
                Uri urlBuilder = new Uri(SiteClient.Settings[Strings.SiteProperties.URL]);

                //abort if "URL" is not an "HTTP" value
                if (!urlBuilder.Scheme.Equals("http", StringComparison.OrdinalIgnoreCase)) return;

                string url = urlBuilder.Scheme + "://" + urlBuilder.Authority + filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectResult(url);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class GoSecure : FilterAttribute, IAuthorizationFilter
    {
        public static bool EnableSSL = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"]);
        public static bool AlwaysSSL = bool.Parse(ConfigurationManager.AppSettings.AllKeys.Contains("AlwaysSSL") ? ConfigurationManager.AppSettings["AlwaysSSL"] : "False");

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!EnableSSL && !AlwaysSSL)
            {                
                return;
            }

            // abort if it is a secure connection  
            if (filterContext.HttpContext.Request.IsSecureConnection) return;

            if (!AlwaysSSL) // skip these checks if the AlwaysSSL is true
            {
                // abort if a [GoUnsecure] attribute is applied to action
                //if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(GoUnsecure), true).Length > 0) return;
                if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(GoUnsecure), true).Length > 0) return;

                // abort if a [RequireHttps] attribute is applied to controller or action  
                //if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(RequireHttpsAttribute), true).Length > 0) return;
                //if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RequireHttpsAttribute), true).Length > 0) return;

                // abort if a [RetainHttps] attribute is applied to controller or action  
                //if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;
                //if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;
            }

            // abort if it's not a GET request - we don't want to be redirecting on a form post  
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)) return;

            // redirect to HTTPS              
            Uri urlBuilder = new Uri(SiteClient.Settings[Strings.SiteProperties.SecureURL]);

            //abort if "SecureURL" is not an "HTTPS" value
            if (!urlBuilder.Scheme.Equals("https", StringComparison.OrdinalIgnoreCase)) return;

            string url = urlBuilder.Scheme + "://" + urlBuilder.Authority + filterContext.HttpContext.Request.RawUrl;
            filterContext.Result = new RedirectResult(url);         
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class Authenticate : FilterAttribute, IAuthorizationFilter
    {        
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            //check for skip
            ActionDescriptor action = filterContext.ActionDescriptor;
            bool isNoAuthenticate = action.GetCustomAttributes(typeof(NoAuthenticate), true).Any();
            if (isNoAuthenticate) return;

            if (SiteClient.MaintenanceMode)
            {                
                if (filterContext.HttpContext.User.IsInRole("admin"))
                {
                    return;
                }
                else
                {
                    // redirect to Maintenance CMS                          
                    filterContext.Result = new RedirectResult(SiteClient.Settings[Strings.SiteProperties.URL] + "/Home/Maintenance");        
                }
            }
            else
            {
                if (!SiteClient.RequireAuthentication) return;

                if (filterContext.HttpContext.User.Identity.IsAuthenticated) return;

                // redirect to login                          
                filterContext.Result = new RedirectResult(SiteClient.Settings[Strings.SiteProperties.SecureURL] + "/Account/LogOn?ReturnUrl=" + filterContext.HttpContext.Request.RawUrl.Replace("/default.aspx?", "/"));    
            }                        
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class NoAuthenticate : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            return;            
        }
    }

    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    //public class RetainHttpsAttribute : Attribute
    //{
    //}  
}