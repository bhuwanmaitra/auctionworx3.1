﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace RainWorx.FrameWorx.MVC.Controllers
{
    /// <summary>
    /// allows SignalR to fire message pertaining to changes to listings in real time
    /// </summary>
    public class ListingHub : Hub
    {
        //public void Hello()
        //{
        //    Clients.All.hello();
        //}

        /// <summary>
        /// registers interest in the specified listing to allow SignalR to fire for a specific listing
        /// </summary>
        /// <param name="listingID">ID of the specified listing</param>
        public void RegisterListingInterest(int listingID)
        {
            Groups.Add(Context.ConnectionId, listingID.ToString());
        }

        /// <summary>
        /// registers interest in the specified event to allow SignalR to fire for a specific listing
        /// </summary>
        /// <param name="eventID">ID of the specified event</param>
        public void RegisterEventInterest(int eventID)
        {
            Groups.Add(Context.ConnectionId, eventID.ToString());
        }

        /// <summary>
        /// registers the username of the user viewing the page to allow SignalR needs to fire to a specific user
        /// </summary>
        /// <param name="userName">username of the specified user</param>
        public void RegisterUserName(string userName)
        {
            Groups.Add(Context.ConnectionId, userName);
        }
    }
}