﻿var basic_signalR_Logging = false;
var rwx_signalR_Logging = true;
function jslog(logMessage) {
    if (rwx_signalR_Logging) console.log(logMessage)
}

var interestingListings = [];
var interestingEvents = [];

//Register interest in listing ID's at most one time per page, for Listing specific SignalR broadcasts
function RegisterInterestingListing(listingID) {    
    if (interestingListings.indexOf(listingID) < 0) {        
        $.connection.listingHub.server.registerListingInterest(listingID);
        interestingListings[interestingListings.length] = listingID;
        jslog("RegisterInterestingListing: " + listingID);
    } else {
        //alert("Registering Listing ID: " + listingID + " skipped");
        jslog("RegisterInterestingListing: (skipped) " + listingID);
    }
}

//Register interest in event ID's at most one time per page, for Event specific SignalR broadcasts
function RegisterInterestingEvent(eventID) {
    if (interestingEvents.indexOf(eventID) < 0) {
        $.connection.listingHub.server.registerEventInterest(eventID);
        interestingEvents[interestingEvents.length] = eventID;
        jslog("RegisterInterestingEvent: " + eventID);
    } else {
        //alert("Registering Event ID: " + listingID + " skipped");
        jslog("RegisterInterestingEvent: (skipped) " + eventID);
    }
}

//Register current user name for directed SignalR broadcasts
function RegisterUserName(userName) {    
    $.connection.listingHub.server.registerUserName(userName);
    jslog("RegisterUserName: " + userName);
}

//global variable to store client side date/time (according to the server)
var browserDateTime = null;
//global variable to store localized remaining time strings
var timeDifferenceDictionary = {};
//global variable to store localized statuses
var statusDictionary = {};
//global variable to store localized lot status HTML snippets
var lotStatusHtmlDictionary = {};
//global variable to store localized event status HTML snippets
var eventStatusHtmlDictionary = {};
//global variable to store localized event homepage bidding status HTML snippets
var eventHomepageStatusHtmlDictionary = {};
//global variable to store localized event homepage time label HTML snippets
var eventHomepageTimeLabelHtmlDictionary = {};
//global variable to store event homepage time/countdown information for 'end early' function
var eventHomepageTimeHtmlDictionary = {};
//global variable to store localized context messages
var contextMessageDictionary = {};
//global variable to store localized signalr indicator titles
var signalrIndicatorTitlesDictionary = {};

if (!String.prototype.format) {
    String.prototype.format = function () {
        //var args = arguments;
        var args = arguments[0];
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}

jQuery.fn.quickEach = (function () {
    var jq = jQuery([1]);
    return function (c) {
        var i = -1, el, len = this.length;
        try {
            while (
                 ++i < len &&
                 (el = jq[0] = this[i]) &&
                 c.call(jq, i, el) !== false
            );
        } catch (e) {
            delete jq[0];
            throw e;
        }
        delete jq[0];
        return this;
    };
}());

$.fn.pulse = function () {
    //$(this).clearQueue().queue(function (next) {
    //    $(this).addClass("signalr-updating");
    //    next();
    //}).delay(300).queue(function (next) {
    //    $(this).addClass("signalr-updatable").removeClass("signalr-updating"); next();
    //}).delay(300).queue(function (next) {
    //    $(this).removeClass("signalr-updatable"); next();
    //});

    //$(this).fadeOut(200, function() {        
    //    $(this).fadeIn(200);
    //});

    $(this).addClass("signalr-pulse", 10, function () { $(this).removeClass("signalr-pulse", 3000); });

    //var theColorIs = $(this).css("color");
    //$(this).animate({
    //    color: '#FF8C00'
    //}, 2000, "swing", function () {        
    //    $(this).animate({
    //        color: theColorIs
    //    }, 2000);
    //});
};
$.fn.pulse_block = function () {
    $(this).addClass("signalr-pulse-block", 10, function () { $(this).removeClass("signalr-pulse-block", 3000); });
};

$(document).ready(function () {

    if (rwx_SignalRDisabled) return;

    //animate changes
    //var notLocked = true;
    //$.fn.animateHighlight = function (highlightColor, duration) {
    //    var highlightBg = highlightColor || "#FFFF9C";
    //    var animateMs = duration || 1500;
    //    var originalBg = this.css("backgroundColor");
    //    if (notLocked) {
    //        notLocked = false;
    //        this.stop().css("background-color", highlightBg)
    //            .animate({ backgroundColor: originalBg }, animateMs);
    //        setTimeout(function () { notLocked = true; }, animateMs);
    //    }
    //};

    //$.fn.pulse = function () {                
    //    $(this).clearQueue().queue(function (next) {
    //        $(this).addClass("signalr-updating");
    //        next();
    //    }).delay(300).queue(function (next) {
    //        $(this).addClass("signalr-updatable").removeClass("signalr-updating"); next();
    //    }).delay(300).queue(function (next) {
    //        $(this).removeClass("signalr-updatable"); next();
    //    });
    //};            

    if (basic_signalR_Logging) {
        $.connection.hub.logging = true;
    }

    //signalr lifetime events
    $.connection.hub.disconnected(function () {
        //alert("AWE_SignalR.js:Disconnected");
        $.event.trigger({
            type: "SignalR_Disconnected"
        });
        setTimeout(function () {
            //alert("AWE Reconnecting");
            $.connection.hub.start().done(function () {
                //alert("Started");
                $.event.trigger({
                    type: "SignalR_Started"
                });
            });
        }, 5000); // Restart connection after 5 seconds.            
    });

    $.connection.hub.connectionSlow(function () {
        //alert("AWE_SignalR.js:ConnectionSlow");
        $.event.trigger({
            type: "SignalR_ConnectionSlow"
        });
    });

    $.connection.hub.reconnecting(function () {
        //alert("AWE_SignalR.js:Reconnecting");
        $.event.trigger({
            type: "SignalR_Reconnecting"
        });
    });

    $.connection.hub.reconnected(function () {
        //alert("AWE_SignalR.js:Reconnected");
        $.event.trigger({
            type: "SignalR_Reconnected"
        });
    });

    // Start the connection
    $.connection.hub.start().done(function () {
        //alert("AWE_SignalR.js:Started");
        $.event.trigger({
            type: "SignalR_Started"
        });
    });    
    
    // Get the hub
    var hub = $.connection.listingHub;

    //Dispatch events

    //UpdateCurrentTime
    hub.client.updateCurrentTime = function (data) {        
        browserDateTime = new Date(data);        
        jslog("Browser Time Set (signalR): " + data);
        $.event.trigger(
            "SignalR_UpdateCurrentTime"
        );
    }

    //UpdateListingAction
    hub.client.updateListingAction = function (data) {                
        $.event.trigger(
            "SignalR_UpdateListingAction",
            data
        );
    }

    //UpdateListingDTTM
    hub.client.updateListingDTTM = function (data) {        
        $.event.trigger(
            "SignalR_UpdateListingDTTM",
            data
        );
    }

    //UpdateListingStatus
    hub.client.updateListingStatus = function (data) {        
        $.event.trigger(
            "SignalR_UpdateListingStatus",
            data
        );
    }

    //ListingActionResponse
    hub.client.listingActionResponse = function (data) {
        $.event.trigger(
            "SignalR_ListingActionResponse",
            data
        );
    }

    //UpdateEventStatus
    hub.client.updateEventStatus = function (data) {
        $.event.trigger(
            "SignalR_UpdateEventStatus",
            data
        );
    }

});

//This is called from SignalRHandler.cshtml, and can't be in a document.ready, because it must happen after the document.ready code in SignalRHandler.cshtml
function CompleteSignalRHandling() {
    //Prepare handlers for SignalR Status
    $(document).on("SignalR_Started", function () {
        $("#SignalRStatus").html('<div title="' + signalrIndicatorTitlesDictionary["Started"] + '"><span class="glyphicon glyphicon-stats SignalRStatus-connected"></span></div>');
        HideSignalRAlert();
    });

    $(document).on("SignalR_ConnectionSlow", function () {
        $("#SignalRStatus").html('<div title="' + signalrIndicatorTitlesDictionary["ConnectionSlow"] + '"><span class="glyphicon glyphicon-stats SignalRStatus-reconnect"></span></div>');
    });

    $(document).on("SignalR_Reconnecting", function () {
        $("#SignalRStatus").html('<div title="' + signalrIndicatorTitlesDictionary["Reconnecting"] + '"><span class="glyphicon glyphicon-stats SignalRStatus-reconnect"></span></div>');
    });

    $(document).on("SignalR_Reconnected", function () {
        $("#SignalRStatus").html('<div title="' + signalrIndicatorTitlesDictionary["Reconnected"] + '"><span class="glyphicon glyphicon-stats SignalRStatus-connected"></span></div>');
        HideSignalRAlert();
    });

    $(document).on("SignalR_Disconnected", function () {
        $("#SignalRStatus").html('<div title="' + signalrIndicatorTitlesDictionary["Disconnected"] + '"><span class="glyphicon glyphicon-stats SignalRStatus-stopped"></span></div>');
        setTimeout(function () { ShowSignalRAlert(); }, 10000);// display disconnect alert after 10 seconds, to prevents normal navigational clicks from triggering it unless the server takes more than 10 seconds to respond
    });

    //prepare current time update handler, this should fire every 59 seconds
    $(document).on("SignalR_UpdateCurrentTime", function () {
        var localizedDTTM = Globalize.formatDate(browserDateTime, { date: "full" }) + ' ' + Globalize.formatDate(browserDateTime, { time: "short" });
        if (timeZoneLabel) localizedDTTM += (' ' + timeZoneLabel);
        $('#Time').text(localizedDTTM);
        HideSignalRAlert();
    });

    //initial update all countdowns
    UpdateAllCountdowns();

    if (!rwx_SignalRDisabled) {
        //update all countdowns every second
        setInterval(function () {
            browserDateTime.setTime(browserDateTime.getTime() + 1000); //milliseconds...
            UpdateAllCountdowns();
        }, 1000);
    }

    //[re]register interesting listings
    $(document).on("SignalR_Started", function () {
        interestingListings = [];
        $("[data-listingid]").each(function () {
            RegisterInterestingListing($(this).data("listingid"));
        });
        interestingEvents = [];
        $("[data-eventid]").each(function () {
            RegisterInterestingEvent($(this).data("eventid"));
        });
    });
    $(document).on("SignalR_Reconnected", function () {
        interestingListings = [];
        $("[data-listingid]").each(function () {
            RegisterInterestingListing($(this).data("listingid"));
        });
        interestingEvents = [];
        $("[data-eventid]").each(function () {
            RegisterInterestingEvent($(this).data("eventid"));
        });
    });

    //Update Action Processing For All Listings
    $(document).on("SignalR_UpdateListingAction", function (event, data) {
        //Update Quantity                      
        $('[data-listingid="' + data.ListingID + '"] .awe-rt-Quantity').each(function () {
            $(this).html(Globalize.formatNumber(data.Quantity, { minimumFractionDigits: 0, maximumFractionDigits: 0 }));
            $(this).pulse();
        });

        //Set Accepted Listing Action Count
        $('[data-listingid="' + data.ListingID + '"] .awe-rt-AcceptedListingActionCount').each(function () {
            if ($(this).data("previousValue") != data.AcceptedActionCount) {
                $(this).data("previousValue", data.AcceptedActionCount);
                $(this).html(data.AcceptedActionCount);
                $(this).pulse();
            }
        });

        //Update Current Price
        $('[data-listingid="' + data.ListingID + '"] .awe-rt-CurrentPrice span.NumberPart').each(function () {
            $(this).html(Globalize.formatNumber(data.Price, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
            $(this).pulse();
        });

        //if the listing's currency is different from the user's currency...
        var listingCurrency = $('[data-currency]').data("currency");
        var currentUserCurrency = $.cookie("currency");
        if (listingCurrency != currentUserCurrency) {
            //show an informational currency conversion
            var convertedAmount = ConvertPrice(data.Price, listingCurrency, currentUserCurrency);
            $(".Bidding_Local_Price span.NumberPart").html(Globalize.formatNumber(convertedAmount, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
            $(".Bidding_Local_Price").pulse();
        }
    });

    //Update Listing DTTM Processing for All Listing Types
    $(document).on("SignalR_UpdateListingDTTM", function (event, data) {
        //Remaining Time Updates
        $('[data-listingid="' + data.ListingID + '"] [data-epoch="' + data.Epoch + '"]').each(function () {
            $(this).data("actionTime", data.DTTMString);
            var tempDate = new Date($(this).data("actionTime"));
            $(this).data("actionMilliseconds", tempDate.getTime());
            var diff = tempDate.getTime() - browserDateTime.getTime();
            $(this).html(TimeDifference(diff));
            //$(this).pulse();
        });

        //Literal DTTM Updates
        var localizedDTTM = Globalize.formatDate(new Date(data.DTTMString), { date: "full" }) + ' ' + Globalize.formatDate(new Date(data.DTTMString), { time: "short" });
        if (timeZoneLabel) localizedDTTM += (' ' + timeZoneLabel);
        $('[data-listingid="' + data.ListingID + '"] .awe-rt-' + data.Epoch + 'DTTM').each(function () {
            $(this).html(localizedDTTM);
            $(this).pulse();
        });
    });

    //Update Listing Status Processing
    $(document).on("SignalR_UpdateListingStatus", function (event, data) {
        jslog("SignalR_UpdateListingStatus: " + data.ListingID + ", " + data.Source + ", " + data.Status);
        //show refresh alert on "updated"
        if (data.Source == "UPDATELISTING_ORIGIN") {
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-RefreshAlert').each(function () {
                $(this).show();
            });
        } else {
            //force countdown updating
            if (data.Status == "Active") {
                $('[data-listingid="' + data.ListingID + '"] [data-epoch="starting"]').each(function () {
                    $(this).removeAttr("data-action-time");
                    $(this).removeAttr("data-action-milliseconds");
                    $(this).removeAttr("data-epoch");
                    var endVal = $(this).attr("data-end-value");
                    if (typeof endVal != typeof undefined && endVal != false) {
                        $(this).html($(this).data("endValue"));
                        $(this).pulse();
                    }
                    var hideSelector = $(this).attr("data-end-hide-selector");
                    if (typeof hideSelector != typeof undefined && hideSelector != false) {
                        $($(this).data("endHideSelector")).hide();
                    }
                    var showSelector = $(this).attr("data-end-show-selector");
                    if (typeof showSelector != typeof undefined && showSelector != false) {
                        $($(this).data("endShowSelector")).show();
                    }
                });
            } else {
                $('[data-listingid="' + data.ListingID + '"] [data-epoch="ending"]').each(function () {
                    $(this).removeAttr("data-action-time");
                    $(this).removeAttr("data-action-milliseconds");
                    $(this).removeAttr("data-epoch");
                    var endVal = $(this).attr("data-end-value");
                    if (typeof endVal != typeof undefined && endVal != false) {
                        $(this).html($(this).data("endValue"));
                        $(this).pulse();
                    }
                    var hideSelector = $(this).attr("data-end-hide-selector");
                    if (typeof hideSelector != typeof undefined && hideSelector != false) {
                        $($(this).data("endHideSelector")).hide();
                    }
                    var showSelector = $(this).attr("data-end-show-selector");
                    if (typeof showSelector != typeof undefined && showSelector != false) {
                        $($(this).data("endShowSelector")).show();
                    }
                });
            }

            //show/hide ActionBox
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-BuyBox').each(function () {
                if (data.Status == "Active") {
                    if (!$(this).is(":visible")) {
                        //$(this).fadeTo(1000, 1, function () {
                        $(this).slideDown(500);
                        //});
                    }
                } else {
                    if ($(this).is(":visible")) {
                        //$(this).fadeTo(1000, 0, function () {
                        $(this).slideUp(500);
                        //});
                    }
                }
            });

            //update Listing "Options" dropdown, first hide everything hideable
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-hideable').each(function () {
                $(this).hide();
            });
            //then show explicitly
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-ShowStatus' + data.Status).each(function () {
                $(this).show();
            });

            //update Status string
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-Status').each(function () {
                $(this).html(statusDictionary[data.Status]);
                $(this).pulse();
            });

            //update colored Status tag
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-ColoredStatus').each(function () {
                $(this).html(lotStatusHtmlDictionary[data.Status]);
                $(this).pulse();
            });

        }

        //clear the status which is potentially invalidated
        $(".ContextualStatus").each(function () {
            if ($(this).is(":visible")) {
                $(this).slideUp(500);
            }
        });

        //show the ListingClosedMessage
        if (data.Status == "Ended" || data.Status == "Unsuccessful" || data.Status == "Successful") {
            $('[data-listingid="' + data.ListingID + '"] .awe-rt-ListingClosedMessage').each(function () {
                $(this).slideDown();
            });
        }
    });

    $(document).on("SignalR_UpdateEventStatus", function (event, data) {
        jslog("onSignalR_UpdateEventStatus: " + data.EventID + ", " + data.Source + ", " + data.Status);
        if (data.Source == "EVENT_PUBLICATION_FINISHED_ORIGIN") {
            $('[data-eventid="' + data.EventID + '"] .awe-rt-PublishIndicator').hide();
            $('[data-eventid="' + data.EventID + '"] .awe-rt-PublishCompletedMessage').show();
        }
        if (data.Source == "EVENT_DRAFT_VALIDATION_FINISHED_ORIGIN") {
            $('[data-eventid="' + data.EventID + '"] .awe-rt-ValidationIndicator').hide();
            $('[data-eventid="' + data.EventID + '"] .awe-rt-ValidationCompletedMessage').show();
            $("#ValidateAllDraftsLink").prop("disabled", false);
        }

        //update Event "Options" dropdown, first hide everything hideable
        $('[data-eventid="' + data.EventID + '"] .awe-rt-hideable').each(function () {
            $(this).hide();
        });
        //then show explicitly
        $('[data-eventid="' + data.EventID + '"] .awe-rt-ShowStatus' + data.Status).each(function () {
            $(this).show();
        });

        //update Status string
        $('[data-eventid="' + data.EventID + '"] .awe-rt-Status').each(function () {
            $(this).html(statusDictionary[data.Status]).pulse();
        });

        //update colored Status tag
        $('[data-eventid="' + data.EventID + '"] .awe-rt-ColoredStatus').each(function () {
            $(this).html(eventStatusHtmlDictionary[data.Status]).pulse();
        });

        //update homepage time label 
        $('[data-eventid="' + data.EventID + '"] .awe-rt-eventtimelabel').each(function () {
            $(this).html(eventHomepageTimeLabelHtmlDictionary[data.Status]).pulse_block();
        });
        //update homepage bid status label 
        $('[data-eventid="' + data.EventID + '"] .awe-rt-eventbidstatuslabel').each(function () {
            $(this).html(eventHomepageStatusHtmlDictionary[data.Status]).pulse_block();
        });
        //update homepage bid status label 
        $('[data-eventid="' + data.EventID + '"] .awe-rt-eventtimecountdown').each(function () {
            if (data.Status == "Active") {
                $(this).find(".awe-rt-Pending").hide();
                $(this).find(".awe-rt-Active").show();
            }
            if (data.Status == "Closing") {
                $(this).find(".awe-rt-Active").hide();
            }
            if (data.Status == "Closed") {
               $(this).find(".awe-rt-Active").hide();
                $(this).find(".awe-rt-Ended").show();
            }
        });
    }
    )};

function UpdateAllCountdowns() {
    if (browserDateTime == null) {
        return;
    }

    $("[data-action-time]").each(function () {
        var thisElement = $(this);
        var attr = $(this).attr("data-action-milliseconds");
        if (typeof attr == typeof undefined || attr == false) {
            var tempDate = new Date($(this).data("actionTime"));
            $(this).attr("data-action-milliseconds", tempDate.getTime());
        }
        var diff = thisElement.data("actionMilliseconds") - browserDateTime.getTime();
        if (diff <= 0) {
            var listingId = thisElement.closest("[data-listingid]").attr("data-listingid");

            if (listingId) {
                var promise = Proxy.invokeAsync("GetTimeRemaining", {
                    listingId: listingId
                }, function (data) {
                    //got result
                    if (data.error == "") {
                        var newDiff = data.secondsRemaining * 1000; //reported milliseconds remaining
                        if (newDiff <= 1000) { // if it's within one second of closing, consider this valid
                            //end dttm confirmed, render "Ended"
                            thisElement.removeAttr("data-action-time");
                            thisElement.removeAttr("data-action-milliseconds");
                            thisElement.removeAttr("data-epoch");
                            var endVal = thisElement.attr("data-end-value");
                            if (typeof endVal != typeof undefined && endVal != false) {
                                thisElement.html(thisElement.data("endValue"));
                                thisElement.pulse();
                            }
                            var hideSelector = thisElement.attr("data-end-hide-selector");
                            if (typeof hideSelector != typeof undefined && hideSelector != false) {
                                $(thisElement.data("endHideSelector")).hide();
                            }
                            var showSelector = thisElement.attr("data-end-show-selector");
                            if (typeof showSelector != typeof undefined && showSelector != false) {
                                $(thisElement.data("endShowSelector")).show();
                            }
                        } else {
                            //the end dttm was wrong for some reason, fix it
                            thisElement.data("actionMilliseconds", browserDateTime.getTime() + newDiff);
                            thisElement.html(TimeDifference(newDiff));
                            thisElement.pulse();
                            RefreshListingVitals(listingId);
                            ShowSignalRAlert();
                            jslog("corrected EndDTTM for listing #" + listingId);
                        }
                    } else {
                        //invalid request parameters?  this may happen if the EndDTTM is more than 100 years from "Now"
                        //window.alert(data.error);
                        ShowSignalRAlert();
                        jslog("error (1) retrieving EndDTTM for #" + listingId);
                    }
                }, function (error) {
                    //website down?  request timed out?
                    //window.alert(error);
                    ShowSignalRAlert();
                    jslog("error (2) retrieving EndDTTM for #" + listingId);
                });
                promise.fail(function (jqXHR, textStatus) {
                    ShowSignalRAlert();
                    jslog("error (3) retrieving EndDTTM for #" + listingId);
                });
            } else {
                //event countdown, not listing?
                //end dttm confirmed, render "Ended"
                thisElement.removeAttr("data-action-time");
                thisElement.removeAttr("data-action-milliseconds");
                thisElement.removeAttr("data-epoch");
                var endVal = thisElement.attr("data-end-value");
                if (typeof endVal != typeof undefined && endVal != false) {
                    thisElement.html(thisElement.data("endValue"));
                    thisElement.pulse();
                }
                var hideSelector = thisElement.attr("data-end-hide-selector");
                if (typeof hideSelector != typeof undefined && hideSelector != false) {
                    $(thisElement.data("endHideSelector")).hide();
                }
                var showSelector = thisElement.attr("data-end-show-selector");
                if (typeof showSelector != typeof undefined && showSelector != false) {
                    $(thisElement.data("endShowSelector")).show();
                }

            }
        } else {
            thisElement.html(TimeDifference(diff));
        }
    });
}

function ConvertPrice(amount, fromCurrency, toCurrency) {
    var result = new Number(amount);
    if (fromCurrency != toCurrency) {
        result = PriceFromUSD(PriceToUSD(result, fromCurrency), toCurrency);
    }
    return result;
}
    
function RefreshListingVitals(listingId) {
    var promise = Proxy.invokeAsync("GetListingVitals", {
        listingId: listingId
    }, function (data) {
        //got result
        //Update Quantity                      
        $('[data-listingid="' + listingId + '"] .awe-rt-Quantity').each(function () {
            $(this).html(Globalize.formatNumber(data.Quantity, { minimumFractionDigits: 0, maximumFractionDigits: 0 }));
            $(this).pulse();
        });

        //Set Accepted Listing Action Count
        $('[data-listingid="' + listingId + '"] .awe-rt-AcceptedListingActionCount').each(function () {
            if ($(this).data("previousValue") != data.AcceptedActionCount) {
                $(this).data("previousValue", data.AcceptedActionCount);
                $(this).html(data.AcceptedActionCount);
                $(this).pulse();
            }
        });

        //Update Current Price
        $('[data-listingid="' + listingId + '"] .awe-rt-CurrentPrice span.NumberPart').each(function () {
            $(this).html(Globalize.formatNumber(data.Price, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
            $(this).pulse();
        });

        //if the listing's currency is different from the user's currency...
        var listingCurrency = data.Currency;
        var currentUserCurrency = $.cookie("currency");
        if (listingCurrency != currentUserCurrency) {
            //show an informational currency conversion
            var convertedAmount = ConvertPrice(data.Price, listingCurrency, currentUserCurrency);
            $(".Bidding_Local_Price span.NumberPart").html(Globalize.formatNumber(convertedAmount, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
            $(".Bidding_Local_Price").pulse();
        }
    }, function (error) {
        ShowSignalRAlert();
        jslog("error (1) retrieving vital stats for listing #" + listingId);
    });
    promise.fail(function (jqXHR, textStatus) {
        ShowSignalRAlert();
        jslog("error (2) retrieving vital stats for listing #" + listingId);
    });
}
